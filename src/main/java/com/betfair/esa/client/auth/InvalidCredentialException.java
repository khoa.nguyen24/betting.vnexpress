package com.betfair.esa.client.auth;

/**
 * Created by mulveyj on 08/07/2016.
 */
public class InvalidCredentialException extends Exception {
	private static final long serialVersionUID = -8963836971277008006L;

	public InvalidCredentialException(String message) {
        super(message);
    }

}
