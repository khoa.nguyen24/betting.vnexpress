package com.betfair.esa.client.cache.market;

import com.betfair.esa.client.protocol.ChangeMessage;
import com.betfair.esa.client.protocol.ChangeType;
import com.betfair.esa.client.protocol.SegmentType;
import com.betfair.esa.swagger.model.MarketChange;
import jayeson.lib.betting.api.datastructure.DeleteRecordInfo;
import jayeson.lib.betting.api.datastructure.SportbookRecord;
import jayeson.lib.feed.api.SportType;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

/**
 * Thread safe cache of markets
 */
public class MarketCache {
    private Logger LOG;

    private Map<String, Market> markets = new ConcurrentHashMap<>();

    //whether markets are automatically removed on close (default is True)
    private boolean isMarketRemovedOnClose;
    //conflation indicates slow consumption
    private CopyOnWriteArrayList<MarketChangeListener> listeners = new CopyOnWriteArrayList<>();

    private Map<SportType, List<SportbookRecord>> upsertMap = new ConcurrentHashMap<>();
    private Map<SportType, List<DeleteRecordInfo>> deletedMap = new ConcurrentHashMap<>();

    public MarketCache(Logger logger) {
        this.isMarketRemovedOnClose = true;
        this.LOG = logger;
    }

    public void onMarketChange(ChangeMessage<MarketChange> changeMessage) {

        SegmentType segmentType = changeMessage.getSegmentType();
        ChangeType changeType = changeMessage.getChangeType();
        long arrivalTime = changeMessage.getArrivalTime();
        if (changeMessage.isStartOfNewSubscription()){
            //clear cache
            markets.clear();
        }
        if (changeMessage.getItems() != null) {
            //lazy build events

            for (MarketChange marketChange : changeMessage.getItems()) {
                Market market = onMarketChange(marketChange);

                if (isMarketRemovedOnClose && market.isClosed()) {
                    //remove on close
                    markets.remove(market.getMarketId());
                }

                //lazy build events
                if (listeners.size() != 0) {
                    MarketChangeEvent changeEvent = new MarketChangeEvent(this);
                    changeEvent.setChange(marketChange);
                    changeEvent.setMarket(market);
                    if (listeners != null) {
                        dispatchMarketChanged(changeEvent);
                    }
                }
            }

            long end = System.currentTimeMillis();
            long parsingTime = end - arrivalTime;
            List<String> itemIds = changeMessage.getItems().stream().map(MarketChange::getId).collect(Collectors.toList());
            if (listeners != null) {
                if (parsingTime > 0) {
                    LOG.info("Parsed change message {}_{}_{}_{}_{} in {} millisecond for {}", changeMessage.getId(), changeType,
                        segmentType, changeMessage.getPublishTime(), arrivalTime, parsingTime, itemIds.size());
                }

                dispatchMarketNotify();
            }
        }
    }

    public void pushDeleteRecords(SportType sportType, List<DeleteRecordInfo> items) {
        List<DeleteRecordInfo> deleteList = deletedMap.computeIfAbsent(sportType, sport -> new ArrayList<>());
        deleteList.addAll(items);
    }

    public void pushDeleteRecord(SportType sportType, DeleteRecordInfo item) {
        List<DeleteRecordInfo> deleteList = deletedMap.computeIfAbsent(sportType, sport -> new ArrayList<>());
        deleteList.add(item);
    }

    public void pushUpsertRecords(SportType sportType, List<SportbookRecord> items) {
        List<SportbookRecord> upsertList = upsertMap.computeIfAbsent(sportType, sport -> new ArrayList<>());
        upsertList.addAll(items);
    }

    public List<DeleteRecordInfo> getDeleteRecords(SportType sportType) {
        return deletedMap.computeIfAbsent(sportType, sport -> new ArrayList<>());
    }

    public List<SportbookRecord> getUpsertRecords(SportType sportType) {
        return upsertMap.computeIfAbsent(sportType, sport -> new ArrayList<>());
    }

    public void cleanRecords(SportType sportType){
        deletedMap.remove(sportType);
        upsertMap.remove(sportType);
    }

    private Market onMarketChange(MarketChange marketChange) {
        Market market = markets.computeIfAbsent(marketChange.getId(), k -> new Market(this, k));
        market.onMarketChange(marketChange);
        return market;
    }

    // Event for each market change
    private void dispatchMarketChanged(MarketChangeEvent marketChangeEvent){
        try {
            listeners.forEach(l->l.marketChange(marketChangeEvent));
        } catch (Exception e) {
            LOG.error("Exception from event listener", e);
        }
    }

    private void dispatchMarketNotify(){
        try {
            listeners.forEach(MarketChangeListener::notifyChange);
        } catch (Exception e) {
            LOG.error("Exception from event listener", e);
        }
    }

    public void addMarketChangeListener(MarketChangeListener marketChangeListener){
        listeners.add(marketChangeListener);
    }

    // Listeners

    public static class MarketChangeEvent extends EventObject{
        //the raw change message that was just applied
        private MarketChange change;
        //the market changed - this is reference invariant
        private Market market;

        /**
         * Constructs a prototypical Event.
         *
         * @param source The object on which the Event initially occurred.
         * @throws IllegalArgumentException if source is null.
         */
        public MarketChangeEvent(Object source) {
            super(source);
        }

        public MarketChange getChange() {
            return change;
        }

        void setChange(MarketChange change) {
            this.change = change;
        }

        public Market getMarket() {
            return market;
        }

        void setMarket(Market market) {
            this.market = market;
        }

        public MarketSnap getSnap() {
            return market.getSnap();
        }
    }
}
