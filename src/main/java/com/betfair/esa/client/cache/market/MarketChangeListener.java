package com.betfair.esa.client.cache.market;

import com.betfair.esa.client.cache.market.MarketCache.MarketChangeEvent;

public interface MarketChangeListener extends java.util.EventListener {
	void marketChange(MarketChangeEvent marketChangeEvent);

	void notifyChange();
}
