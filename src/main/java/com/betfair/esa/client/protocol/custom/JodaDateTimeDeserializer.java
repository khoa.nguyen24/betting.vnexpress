package com.betfair.esa.client.protocol.custom;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import java.io.IOException;

public class JodaDateTimeDeserializer extends JsonDeserializer<DateTime> {
	
	private final DateTimeFormatter formatter = ISODateTimeFormat.dateTime();

	@Override
	public DateTime deserialize(JsonParser parser, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		JsonNode root = parser.getCodec().readTree(parser);
		String date = root.asText();
        return formatter.parseDateTime(date);
	}
}
