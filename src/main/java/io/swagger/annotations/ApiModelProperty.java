package io.swagger.annotations;

public @interface ApiModelProperty {

	String example();

	String value();

}
