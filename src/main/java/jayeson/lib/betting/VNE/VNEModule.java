package jayeson.lib.betting.VNE;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoSet;
import jayeson.lib.betting.VNE.bet.BetFairBetMasterComponent;
import jayeson.lib.betting.VNE.betticket.BetFairGetTicketComponent;
import jayeson.lib.betting.VNE.datatransfer.ResponseSerializer;
import jayeson.lib.betting.VNE.deletebet.BetFairDeleteMasterTaskComponent;
import jayeson.lib.betting.VNE.getbalance.BetFairGetBalanceComponent;
import jayeson.lib.betting.VNE.getbethistorybyid.BetFairGetHistoriesByIdsComponent;
import jayeson.lib.betting.VNE.maintenance.BetFairMaintenanceComponent;
import jayeson.lib.betting.VNE.recentbetstatus.RecentBetStatusMasterTaskComponent;
import jayeson.lib.betting.VNE.scrape.BetfairGetEventInfoComponent;
import jayeson.lib.betting.VNE.scrape.BetfairGetEventOddComponent;
import jayeson.lib.betting.VNE.scrape.BetfairGetEventStateComponent;
import jayeson.lib.betting.api.datatransfer.IAccountDataDeserializer;
import jayeson.lib.betting.api.datatransfer.IAccountDataSerializer;
import jayeson.lib.betting.VNE.datatransfer.BetFairAccountDataDeserializer;
import jayeson.lib.betting.VNE.datatransfer.BetFairAccountDataSerializer;
import jayeson.lib.betting.VNE.datatransfer.ResponseDeserializer;
import jayeson.lib.betting.VNE.getbethistory.BetFairHistoryMasterTaskComponent;
import jayeson.lib.betting.VNE.getbethistorybystatement.BetFairGetHistoriesByStatementComponent;
import jayeson.lib.betting.VNE.login.LoginComponent;
import jayeson.lib.betting.VNE.logout.LogoutComponent;
import jayeson.lib.betting.VNE.runningbetlist.BetFairRunningBetListComponent;
import jayeson.lib.betting.VNE.scrape.BetfairScrapeComponent;
import jayeson.lib.betting.core.BettingAccount;
import jayeson.lib.betting.core.configuration.BookConfig;
import jayeson.lib.betting.core.dagger.accountscope.AccountScope;
import jayeson.lib.betting.core.dagger.taskscope.BetMasterTaskComponent;
import jayeson.lib.betting.core.dagger.taskscope.ITaskComponent;
import jayeson.lib.betting.core.http.IResponse;
import jayeson.lib.betting.core.http.ResponseListener;

import javax.inject.Named;
import javax.inject.Provider;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Module(subcomponents = {
		LoginComponent.class // login task
		, LogoutComponent.class // logout task
		, BetFairGetBalanceComponent.class // balance task
		, BetFairMaintenanceComponent.class // maintenance task
		, BetFairBetMasterComponent.class // place bet task
		, BetFairHistoryMasterTaskComponent.class // history master task
		, BetFairGetHistoriesByStatementComponent.class // history by statement task
		, BetFairGetHistoriesByIdsComponent.class // history by id task
		, BetFairRunningBetListComponent.class
		, BetFairGetTicketComponent.class // get ticket
		, BetFairDeleteMasterTaskComponent.class // delete bet
		, RecentBetStatusMasterTaskComponent.class // recent bet status
		, BetfairScrapeComponent.class 			//scrape
		, BetfairGetEventOddComponent.class		// get event state
		, BetfairGetEventInfoComponent.class	// get odd of market
		, BetfairGetEventStateComponent.class	// get event info
})
public class VNEModule {

	@AccountScope @Provides
	ResponseListener provideResponseListener(VNEResponseListener responseListener) {
		return responseListener;
	}

	@AccountScope @Provides
	BettingAccount provideBettingAccount(VNE account) {
		return account;
	}

	@AccountScope @Provides
	List<BookConfig.BetLimit> provideBetLimit(BookConfig bookConfig) {
		return bookConfig.getBetLimits().get("betfair");
	}

	@AccountScope @Provides @IntoSet
	Map<Class<? extends ITaskComponent>, Provider<? extends ITaskComponent.Builder>> provideTaskBuilders(
            Provider<LoginComponent.Builder> loginBuilder// login
			, Provider<LogoutComponent.Builder> logoutBuilder // logout
			, Provider<BetFairGetBalanceComponent.Builder> balanceBuilder
			, Provider<BetFairMaintenanceComponent.Builder> maintenanceBuilder
			, Provider<BetFairBetMasterComponent.Builder> placeBetBuilder // place bet
			, Provider<BetFairHistoryMasterTaskComponent.Builder> betHistoryMasterBuilder // history master
			, Provider<BetFairGetHistoriesByStatementComponent.Builder> betHistoryByStatementBuilder // history by statement
			, Provider<BetFairGetHistoriesByIdsComponent.Builder> betHistoryBuilder // history by id
			, Provider<BetFairRunningBetListComponent.Builder> runningBetListBuilder // running bet list
			, Provider<BetFairGetTicketComponent.Builder> getTicketBuilder //get ticket
			, Provider<BetFairDeleteMasterTaskComponent.Builder> deleteBetBuilder // delete bet
			, Provider<RecentBetStatusMasterTaskComponent.Builder> recentBetBuilder // recent bet status
			, Provider<BetfairScrapeComponent.Builder> scrapeBuilder //scrape
			, Provider<BetfairGetEventStateComponent.Builder> eventStateBuilder // get event state
			, Provider<BetfairGetEventOddComponent.Builder> eventOddBuilder // get odd of market
			, Provider<BetfairGetEventInfoComponent.Builder> eventInfoBuilder // get event info
	) {
		Map<Class<? extends ITaskComponent>, Provider<? extends ITaskComponent.Builder>> taskBuilders = new HashMap<>();
		taskBuilders.put(LoginComponent.class, loginBuilder); // login
		taskBuilders.put(LogoutComponent.class, logoutBuilder); // logout
		taskBuilders.put(BetFairHistoryMasterTaskComponent.class, betHistoryMasterBuilder); // history master
		taskBuilders.put(BetFairGetHistoriesByStatementComponent.class, betHistoryByStatementBuilder); // history by statement
		taskBuilders.put(BetFairGetHistoriesByIdsComponent.class, betHistoryBuilder); // history by id
		taskBuilders.put(BetFairRunningBetListComponent.class, runningBetListBuilder); // running bet list
		taskBuilders.put(BetFairGetTicketComponent.class, getTicketBuilder);
		taskBuilders.put(BetMasterTaskComponent.class, placeBetBuilder);
		taskBuilders.put(BetFairDeleteMasterTaskComponent.class, deleteBetBuilder);
		taskBuilders.put(BetFairGetBalanceComponent.class, balanceBuilder);
		taskBuilders.put(BetFairMaintenanceComponent.class, maintenanceBuilder);
		taskBuilders.put(RecentBetStatusMasterTaskComponent.class, recentBetBuilder);// recent bet status
		taskBuilders.put(BetfairScrapeComponent.class, scrapeBuilder);
		taskBuilders.put(BetfairGetEventStateComponent.class, eventStateBuilder);
		taskBuilders.put(BetfairGetEventInfoComponent.class, eventInfoBuilder);
		taskBuilders.put(BetfairGetEventOddComponent.class, eventOddBuilder);
		return taskBuilders;
	}

	@Provides
	@AccountScope @Named("BetFairSerializer")
	public IAccountDataSerializer provideAccountDataSerializer(BetFairAccountDataSerializer serializer) {
		return serializer;
	}

	@Provides
	@AccountScope @Named("BetFairDeserializer")
	public IAccountDataDeserializer provideAccountDataDeserializer(BetFairAccountDataDeserializer deserializer) {
		return deserializer;
	}

	@Provides
	@Named("BetfairParamObjectMapper")
	ObjectMapper providesMapper() {
		return new ObjectMapper();
	}

	@Provides
	@AccountScope @Named("BetFairObjectMapper")
	public ObjectMapper provideObjectMapper() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

		// register custom serializer and deserializer
		SimpleModule module = new SimpleModule();
		module.addSerializer(IResponse.class, new ResponseSerializer());
		module.addDeserializer(IResponse.class, new ResponseDeserializer());
		mapper.registerModule(module);
		mapper.registerModule(new Jdk8Module());

		return mapper;
	}
}
