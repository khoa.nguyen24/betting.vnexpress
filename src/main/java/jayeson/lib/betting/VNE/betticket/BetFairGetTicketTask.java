package jayeson.lib.betting.VNE.betticket;

import dagger.Lazy;
import jayeson.lib.betting.api.task.AccountTaskHandler;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.api.task.TaskObservable;
import jayeson.lib.betting.core.BettingAccount;
import jayeson.lib.betting.core.dagger.qualifier.TaskId;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.task.GetBetTicketTask;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;

import javax.inject.Inject;

@TaskScope
public class BetFairGetTicketTask extends GetBetTicketTask<BetFairBetTicketResult> {

    private static final int GET_BET_TICKET_WL = 10;
    private Lazy<BetFairGetTicketFragment> betTicketLazy;

    @Inject
    public BetFairGetTicketTask(ITaskExecutor executor, IResponseListener responseListener, AccountTaskInfo data,
                                BettingAccount account, @TaskId String id, AccountTaskHandler<BetFairBetTicketResult> handler,
                                TaskObservable<AccountTaskInfo> observable, BetFairBetTicketResult result,
                                Lazy<BetFairGetTicketFragment> betTicketLazy) {
        super(executor, responseListener, data, account, id, GET_BET_TICKET_WL, handler, observable, result);
        this.betTicketLazy = betTicketLazy;
    }

    @Override
    protected void execute() {
        account.getCommonLogger().info("START_GET_BET_TICKET");
        executor.submit(betTicketLazy.get());
        account.getCommonLogger().info("LAUNCH_FRAGMENT_GET_BET_TICKET");
    }
}
