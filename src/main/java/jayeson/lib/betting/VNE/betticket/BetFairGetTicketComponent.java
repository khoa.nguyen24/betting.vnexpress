package jayeson.lib.betting.VNE.betticket;

import dagger.BindsInstance;
import dagger.Subcomponent;
import jayeson.lib.betting.api.actioncontext.BetInfoContext;
import jayeson.lib.betting.core.dagger.qualifier.OriginTicketContext;
import jayeson.lib.betting.core.dagger.taskscope.ITaskComponent;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;

@TaskScope
@Subcomponent(modules = BetFairGetTicketModule.class)
public interface BetFairGetTicketComponent extends ITaskComponent<BetFairBetTicketResult> {
    @Override
    BetFairGetTicketTask getTask();

    @Subcomponent.Builder
    interface Builder extends BuilderContext<BetFairBetTicketResult, BetInfoContext> {
        @Override
        BetFairGetTicketComponent build();

        @Override @BindsInstance
        Builder bindsContext(@OriginTicketContext BetInfoContext context);
    }
}
