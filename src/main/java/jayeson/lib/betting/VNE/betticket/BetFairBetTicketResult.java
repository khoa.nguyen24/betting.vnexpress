package jayeson.lib.betting.VNE.betticket;

import jayeson.lib.betting.api.actioncontext.BetInfoContext;
import jayeson.lib.betting.api.actionresults.BetTicketResult;

public class BetFairBetTicketResult extends BetTicketResult {
    private String marketVersion;

    public BetFairBetTicketResult(BetInfoContext originContext, BetInfoContext betInfoContext) {
        super(originContext, betInfoContext);
    }

    public String getMarketVersion() {
        return marketVersion;
    }

    public void setMarketVersion(String marketVersion) {
        this.marketVersion = marketVersion;
    }

    @Override
    public String toString() {
        return "BetFairBetTicketResult{" +
                "marketVersion='" + marketVersion + '\'' +
                ", BetTicketResult= " + super.toString() +
                '}';
    }
}
