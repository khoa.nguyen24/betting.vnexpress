package jayeson.lib.betting.VNE.betticket;

import dagger.Module;
import dagger.Provides;
import jayeson.lib.betting.api.actioncontext.BetInfoContext;
import jayeson.lib.betting.core.dagger.qualifier.OriginTicketContext;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;
import jayeson.lib.betting.core.dagger.taskscope.TicketTaskModule;
import jayeson.lib.betting.core.task.TaskFragmentListener;

@Module(includes = TicketTaskModule.class)
public class BetFairGetTicketModule {

    @TaskScope
    @Provides
    BetFairBetTicketResult providesBetFairBetTicketResult(@OriginTicketContext BetInfoContext originContext, BetInfoContext betInfoContext) {
        return new BetFairBetTicketResult(originContext, betInfoContext);
    }

    @TaskScope @Provides
    TaskFragmentListener<BetFairBetTicketResult> provideTaskFragmentListener(BetFairGetTicketTask task) {
        return task;
    }
}
