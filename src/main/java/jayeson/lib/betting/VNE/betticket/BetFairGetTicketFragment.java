package jayeson.lib.betting.VNE.betticket;

import com.google.common.base.Strings;
import jayeson.lib.betting.VNE.VNE;
import jayeson.lib.betting.VNE.VNEURIDefinition;
import jayeson.lib.betting.VNE.VNEUtility;
import jayeson.lib.betting.VNE.json.PriceSize;
import jayeson.lib.betting.VNE.json.marketbook.MarketBook;
import jayeson.lib.betting.VNE.json.marketbook.MarketBookResponse;
import jayeson.lib.betting.VNE.json.marketbook.Runner;
import jayeson.lib.betting.VNE.json.marketbook.TicketRequest;
import jayeson.lib.betting.api.actioncontext.BetInfoContext;
import jayeson.lib.betting.api.actionresults.ActionResultCode;
import jayeson.lib.betting.api.datastructure.BADRuntime;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.core.configuration.BookConfig;
import jayeson.lib.betting.core.exception.BuildRequestFailException;
import jayeson.lib.betting.core.exception.UnknownResponseException;
import jayeson.lib.betting.core.http.IClient;
import jayeson.lib.betting.core.http.IResponse;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.http.URIDefinition;
import jayeson.lib.betting.core.http.request.BettingRequest;
import jayeson.lib.betting.core.http.request.JsonRequest;
import jayeson.lib.betting.core.task.AsyncHttpTaskFragment;
import jayeson.lib.betting.core.task.TaskFragmentListener;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;
import jayeson.lib.betting.core.utility.BettingUtility;
import jayeson.lib.betting.exception.BettingException;
import jayeson.lib.betting.exception.InvalidAttributeException;
import jayeson.lib.betting.exception.ParsingFailException;
import jayeson.lib.betting.utility.OddsConverter;
import jayeson.lib.feed.api.LBType;
import jayeson.lib.feed.api.OddType;
import jayeson.lib.feed.api.SportType;
import jayeson.lib.feed.api.twoside.PivotBias;
import jayeson.lib.feed.api.twoside.PivotType;
import jayeson.lib.feed.api.twoside.TargetType;
import org.slf4j.Logger;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class BetFairGetTicketFragment extends AsyncHttpTaskFragment<BetFairBetTicketResult, MarketBookResponse[]> {
    private BetInfoContext context;
    private final BADRuntime badRuntime;
    private final List<BookConfig.BetLimit> betLimits;
    private VNE VNEAccount;

    @Inject
    public BetFairGetTicketFragment(AccountTaskInfo data, ITaskExecutor executor, IResponseListener responseListener,
                                    VNE account, TaskFragmentListener<BetFairBetTicketResult> listener,
                                    BetFairBetTicketResult initialResult, BetInfoContext context,
                                    BADRuntime badRuntime, IClient client, List<BookConfig.BetLimit> betLimits) {
        super(data, executor, responseListener, account, listener, initialResult, client);
        this.context = context;
        this.badRuntime = badRuntime;
        this.betLimits = betLimits;
        this.VNEAccount = account;
        this.setDef(VNEURIDefinition.getJsonRpcURI(MarketBookResponse[].class));
        needProceed = false;
    }


    private BettingRequest buildRequest(BetInfoContext context) throws BuildRequestFailException, InvalidAttributeException {

        if (VNEUtility.isNeedToResetBettingApi(client.host().url().toString())) {
            client.resetHost(VNEUtility.getBettingApiUrl(client.host().url().toString()));
        }

        // Prepare ticketRequestData
        TicketRequest ticketRequest = new TicketRequest(context);
        List<TicketRequest> requests = new ArrayList<>();
        requests.add(ticketRequest);

        // Use JSON context
        return new JsonRequest.Builder()
        .def(this.getDef())
        .host(client.host())
        .data(requests)
        .addHeader("X-Authentication", this.badRuntime.getLoggedInUid())
        .addHeader("X-Application", VNEAccount.getAppKey())
        .build();
    }

    /**
     * [{"jsonrpc": "2.0", "method": "SportsAPING/v1.0/listMarketBook", "params": {"marketIds":["1.127771425"],"priceProjection":{"priceData":["EX_BEST_OFFERS"],"virtualise":"true"}}, "id": 1}]
     * @throws BettingException
     */
    @Override
    public void execute() throws BettingException {
        BettingRequest request = buildRequest(context);
        executeRequest(request, false);
    }

    @Override
    public void consumeResponse(IResponse<MarketBookResponse[]> response) throws BettingException {
        if (response.getStatusLine().getCode() != 200) {
            account.getCommonLogger().error("FAIL to get betInfo for BetFair {}", response);
            result().setCode(ActionResultCode.FAILED);
            result().setMessage("The request failed with http code " + response.getStatusLine().getCode());
            return;
        }

        MarketBookResponse[] marketBookResponses = response.content();
        if(marketBookResponses[0].error != null) {
            account.getCommonLogger().error("Failed to get ticket: {} ", BettingUtility.writeObjectToJson(marketBookResponses[0].error));
            throw new UnknownResponseException("Failed to get ticket", getDef(), "");
        }

        if (marketBookResponses[0].result == null) {
            account.getCommonLogger().error("FAIL to get betInfo for BetFair {}", response);
            result().setCode(ActionResultCode.FAILED);
            result().setMessage("Empty Market Book Responses");
            return;
        }

        result().setMinStake(getMinStakeFromConfig());
        parseInfoFromTicket(marketBookResponses[0], context, result(), this.getDef(), account.getCommonLogger());
        try {
            // Handle score, red cards and target type due to team swap
            if (result().getOriginContext().isTeamSwapped()) {
                BettingUtility.swapWebsiteReturnedTargetType(result());
                BettingUtility.swapPivotBias(result());
                account.getCommonLogger().info("Swap target type and bias due to team swapped");
            }
        } catch (Exception e) {
            account.getCommonLogger().error("failed to swap team", e);
            result().setCode(ActionResultCode.FAILED);
            result().setMessage("failed to swap team");
            result().addException(e);
        }

        if (!validateTicket(context.getOddId(), result().getPivotValue(), context.getHostPoint(), context.getGuestPoint(),
            context.getTargetType(), context.getBias())) {
            account.getCommonLogger().error("failed to validate pivot and score, {}, {}", result().getPivotValue(),
                    context.getOddId());
            result().setCode(ActionResultCode.FAILED);
            result().setMessage("Invalid Pivot and Score");
        }

        account.getCommonLogger().info("TICKET RESULT: {}", result());
    }

    /**
     * Must be SOCCER LIVE HDP
     * @param oddId
     * @param resultPivot
     * @param homeScore
     * @param awayScore
     * @return
     */
    public boolean validateTicket(String oddId, String resultPivot, int homeScore, int awayScore, TargetType target, PivotBias bias) {
        if (!context.getSportType().equals(SportType.SOCCER) || !context.getOddType().equals(OddType.LIVE)
                || !context.getPivotType().equals(PivotType.HDP))
            return true;

        //Formula:  pivot_value = rec.original_pivot + rec.host_point - rec.guest_point
        String oddIds[] = oddId.split(OddsConverter.ODD_DELIMETER);
        String originalPivotStr = oddIds[oddIds.length-1];

        account.getCommonLogger().debug("Validate Ticket ResultPivot: {} HomeScore: {} GuestScore: {} and OddId: {}",
                resultPivot, homeScore, awayScore, oddId);

        Double realPivot = 0.0d;
        PivotBias realBias = PivotBias.NEUTRAL;

        if (target == TargetType.AWAY || (target == TargetType.GIVE && bias == PivotBias.GUEST)
                || (target == TargetType.TAKE && bias == PivotBias.HOST)) {
            realPivot = Double.valueOf(originalPivotStr) +
                    (Double.valueOf(awayScore) - Double.valueOf(homeScore));

            if (realPivot < 0)
                realBias = PivotBias.GUEST;
            else if (realPivot > 0)
                realBias = PivotBias.HOST;
        } else if (target == TargetType.HOME || (target == TargetType.TAKE && bias == PivotBias.GUEST)
                || (target == TargetType.GIVE && bias == PivotBias.HOST)) {
            realPivot = Double.valueOf(originalPivotStr) +
                    (Double.valueOf(homeScore) - Double.valueOf(awayScore));

            if (realPivot > 0)
                realBias = PivotBias.GUEST;
            else if (realPivot < 0)
                realBias = PivotBias.HOST;
        }

        if (realBias != bias) {
            account.getCommonLogger().error("Real Bias {} and Ticket Bias {} do not match. Real Pivot {} Result Pivot {}",
                    realBias, bias, realPivot, resultPivot);
            return false;
        }

        if (Math.abs(Math.abs(Double.valueOf(resultPivot)) - Math.abs(realPivot)) > 0.01) {
            account.getCommonLogger().error("Real Pivot {} Result Pivot {} with Home Score {} vs Away Score {}",
                    realPivot, resultPivot, homeScore, awayScore);
            return false;
        }

        return true;
    }

    public void parseInfoFromTicket(MarketBookResponse marketBookResponse, BetInfoContext context, BetFairBetTicketResult result, URIDefinition def, Logger commonLogger) {
        try {
            MarketBook marketBook = marketBookResponse.result.get(0);
            if (marketBook.status.equalsIgnoreCase("CLOSED")) {
                result.setCode(ActionResultCode.MARKET_CLOSED);
                result.setMessage("Market Closed");
                return;
            }
            // Get runner based on context
            String selectionId = VNEUtility.findSelectionId(context);
            Runner runner = findRunner(marketBook, context, selectionId, def);
            result.addTargetType(context.getTargetType());
            PriceSize priceSize = getPriceSize(context.getLbType(), runner, def);
            result.setPivotValue(context.getPivotValue());
            result.setMaxStake(priceSize.size);

            double targetOdd = priceSize.price;
            
            if (PivotType.ONE_TWO != context.getPivotType()) {
                targetOdd = targetOdd - 1;
            }

            result.setOriginalOdd( VNEUtility.oddTo3Decimal(targetOdd));
            result.setTimeType(context.getTimeType());
            result.setPivotBias(context.getBias());
            result.setOddType(context.getOddType());

            result.setMarketVersion(marketBook.version);

            result.setCode(ActionResultCode.SUCCESSFUL);
        } catch (Exception ex) {
            result.setCode(ActionResultCode.PARSING_ERROR);
            result.setMessage("Cannot parse ticket data");
            commonLogger.error("Exception while parsing the ticket", ex);
        }
    }

    private Runner findRunner(MarketBook marketBook, BetInfoContext context, String selectionId, URIDefinition def) throws ParsingFailException, BuildRequestFailException {
        Runner runner = null;
        List<Runner> runnerList = marketBook.runners;
        if( context.getPivotType() == PivotType.ONE_TWO){
            runner = runnerList.stream()
                    .filter(x -> selectionId.equals(x.selectionId.toString()))
                    .findAny()
                    .orElse(null);
        } else if(context.getPivotType() == PivotType.HDP){
            double targetHdpPivot = VNEUtility.toHdpPivot(context);
            runner = runnerList.stream()
                    .filter(x -> selectionId.equals(x.selectionId.toString()))
                    .filter(x -> targetHdpPivot == x.handicap)
                    .findAny()
                    .orElse(null);
        } else if (context.getPivotType() == PivotType.TOTAL) {

            Double pivotValue = Double.parseDouble(context.getPivotValue());
            if(VNEUtility.isGoalLinesSelection(context.getSportType(), pivotValue)) {
                runner = runnerList.stream()
                        .filter(x -> selectionId.equals(x.selectionId.toString()))
                        .filter(x -> pivotValue.equals(x.handicap))
                        .findAny()
                        .orElse(null);
            } else {
                runner = runnerList.stream()
                        .filter(x -> selectionId.equals(x.selectionId.toString()))
                        .findAny()
                        .orElse(null);
            }
        }
        if(runner == null)
            throw new ParsingFailException(String.format("Cannot find selection id %s in list runners", selectionId), def.uri(), "", "");

        return runner;
    }

    private PriceSize getPriceSize(LBType lbtype, Runner runner, URIDefinition def) throws ParsingFailException {
        List<PriceSize> priceSizes = lbtype == LBType.LAY ? runner.ex.availableToLay : runner.ex.availableToBack;
        if (priceSizes == null || priceSizes.isEmpty()) {
            throw new ParsingFailException("Cannot parse max stake and current odd", def.uri(), "", "");
        }
        return priceSizes.get(0);
    }

    private double getMinStakeFromConfig() throws InvalidAttributeException {
        String accountCurrency = VNEAccount.getAccountCurrency();
        if (Strings.isNullOrEmpty(accountCurrency)) {
            throw new InvalidAttributeException(String.format("Account Currency should be valid, %s", accountCurrency), "");
        } else {
            Optional<BookConfig.BetLimit> min = betLimits.stream()
                    .filter(x -> x.getCurrency().equalsIgnoreCase(accountCurrency)).findFirst();
            if (min.isPresent()) {
                return min.get().getMinBet();
            }

            throw new InvalidAttributeException(String.format("Bet Limit should contain min stake, %s, %s", accountCurrency, min), "");
        }
    }

    @Override
    public void proceed(BetFairBetTicketResult betTicketResult) {
        // No need to proceed
    }

    @Override
    public boolean logHttpRequest() {
        return true;
    }
}
