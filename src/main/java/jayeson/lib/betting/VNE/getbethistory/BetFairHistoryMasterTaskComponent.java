package jayeson.lib.betting.VNE.getbethistory;

import dagger.BindsInstance;
import dagger.Subcomponent;
import jayeson.lib.betting.api.actioncontext.BetHistoryByStatementContext;
import jayeson.lib.betting.api.actionresults.ListBetHistoryResult;
import jayeson.lib.betting.api.task.AccountTaskHandler;
import jayeson.lib.betting.core.dagger.qualifier.MasterTaskHandler;
import jayeson.lib.betting.core.dagger.taskscope.ITaskComponent;
import jayeson.lib.betting.core.dagger.taskscope.TaskModule;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;

@TaskScope
@Subcomponent(modules = {TaskModule.class, BetFairHistoryMasterTaskModule.class})
public interface BetFairHistoryMasterTaskComponent extends ITaskComponent<ListBetHistoryResult> {
    @Override
    BetFairHistoryMasterTask getTask();

    @Subcomponent.Builder
    interface Builder extends BuilderContext<ListBetHistoryResult, BetHistoryByStatementContext> {
        @Override
        @BindsInstance
        Builder bindsHandler(@MasterTaskHandler AccountTaskHandler<ListBetHistoryResult> handler);

        BetFairHistoryMasterTaskComponent build();
    }
}
