package jayeson.lib.betting.VNE.getbethistory;

import dagger.Lazy;
import jayeson.lib.betting.api.actionresults.ActionResultCode;
import jayeson.lib.betting.api.actionresults.BetHistoryResult;
import jayeson.lib.betting.api.actionresults.ListBetHistoryResult;
import jayeson.lib.betting.api.task.*;
import jayeson.lib.betting.VNE.getbethistorybyid.BetFairGetHistoriesByIdsTask;
import jayeson.lib.betting.VNE.getbethistorybystatement.BetFairGetHistoriesByStatementTask;
import jayeson.lib.betting.VNE.getbethistorybystatement.BetFairListBetHistoriesByStatementResult;
import jayeson.lib.betting.core.BettingAccount;
import jayeson.lib.betting.core.dagger.qualifier.MasterTaskHandler;
import jayeson.lib.betting.core.dagger.qualifier.MasterTaskId;
import jayeson.lib.betting.core.dagger.qualifier.MasterTaskInfo;
import jayeson.lib.betting.core.dagger.qualifier.MasterTaskObservable;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;
import jayeson.lib.betting.core.task.MasterAccountTask;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Get histories by statement time to get success bets, missing bets and dangling bets
 * --> the list of bet id
 * Then get histories by ids to avoid blinking bet case.
 */
@TaskScope
public class BetFairHistoryMasterTask extends MasterAccountTask<ListBetHistoryResult> {
    private final Lazy<BetFairGetHistoriesByStatementTask> getBetsByStatementTaskLazy;
    private final Lazy<BetFairGetHistoriesByIdsTask> getBetsByIdsTaskLazy;

    @Inject
    public BetFairHistoryMasterTask(ITaskExecutor executor, BettingAccount account,
                                    @MasterTaskInfo AccountTaskInfo data,
                                    @MasterTaskId String id,
                                    @MasterTaskHandler AccountTaskHandler<ListBetHistoryResult> handler,
                                    @MasterTaskObservable TaskObservable<AccountTaskInfo> observable,
                                    Lazy<BetFairGetHistoriesByStatementTask> getBetsByStatementTaskLazy,
                                    Lazy<BetFairGetHistoriesByIdsTask> getBetsByIdsTaskLazy,
                                    ListBetHistoryResult result) {
        super(executor, data, account, id, handler, observable, result);
        this.getBetsByStatementTaskLazy = getBetsByStatementTaskLazy;
        this.getBetsByIdsTaskLazy = getBetsByIdsTaskLazy;
    }

    @Override
    public void execute() {
        currentSubTask = proceedGetHistoriesByStatement();
    }

    private TaskObservable<AccountTaskInfo> proceedGetHistoriesByStatement() {
        BetFairGetHistoriesByStatementTask task = getBetsByStatementTaskLazy.get();
        executor.submit(task);
        return task.getObservable();
    }

    private final AccountTaskHandler<BetFairListBetHistoriesByStatementResult> getHistoriesByStatementTaskHandler =
            (BetFairListBetHistoriesByStatementResult result, AccountTask<BetFairListBetHistoriesByStatementResult> task) -> {

                synchronized (data) {
                    if (data.getState() == AccountTaskState.CANCELLED) {
                        reportResult(ActionResultCode.FAILED, "Task is cancelled");
                        return;
                    }
                    // Update progress
                    if(result.getCode() != ActionResultCode.SUCCESSFUL) {
                        updateProgress(100, AccountTaskState.FAILED);
                        reportResult(result.getCode(), "Fail to get histories by statement time "
                                + task.id() + ": " + result.getMessage());
                        return;
                    }

                    // Prepare distinct bet ids for getting histories by ids
                    List<String> betIds = result.getResultList().stream()
                            .map(BetHistoryResult::getBetId)
                            .distinct()
                            .collect(Collectors.toList());
                    if (!betIds.isEmpty()) {
                        getBetHistoriesByIds(betIds);
                    } else {
                        updateProgress(100, AccountTaskState.SUCCEEDED);
                        reportResult(result.getCode(), result.getMessage());
                    }
                }
            };

    private void getBetHistoriesByIds(List<String> betIds) {
        currentSubTask = proceedBetHistoriesByIds(betIds);
    }

    private TaskObservable<AccountTaskInfo> proceedBetHistoriesByIds(List<String> betIds) {
        BetFairGetHistoriesByIdsTask task = getBetsByIdsTaskLazy.get();
        task.getContext().setBetIds(betIds);
        executor.submit(task);
        return task.getObservable();
    }

    private final AccountTaskHandler<ListBetHistoryResult> getHistoryByIdTaskHandler =
            (ListBetHistoryResult result, AccountTask<ListBetHistoryResult> task) -> {
                synchronized (data) {
                    if (data.getState() == AccountTaskState.CANCELLED) {
                        reportResult(ActionResultCode.FAILED, "Task is cancelled");
                        return;
                    }
                    // Update progress
                    if(result.getCode() == ActionResultCode.FAILED) {
                        updateProgress(100, AccountTaskState.FAILED);
                        reportResult(result.getCode(), "Fail to get histories by ids "
                                + task.id() + ": " + result.getMessage());
                        return;
                    }

                    updateProgress(100, AccountTaskState.SUCCEEDED);
                    reportResult(result.getCode(), result.getMessage());
                }
            };

    public AccountTaskHandler<BetFairListBetHistoriesByStatementResult> getGetHistoriesByStatementTaskHandler() {
        return getHistoriesByStatementTaskHandler;
    }

    public AccountTaskHandler<ListBetHistoryResult> getGetHistoryByIdTaskHandler() {
        return getHistoryByIdTaskHandler;
    }
}
