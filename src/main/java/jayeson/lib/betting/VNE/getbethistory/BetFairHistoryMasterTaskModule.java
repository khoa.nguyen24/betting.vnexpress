package jayeson.lib.betting.VNE.getbethistory;

import dagger.Module;
import dagger.Provides;
import jayeson.lib.betting.VNE.getbethistorybyid.BetFairGetHistoriesByIdsModule;
import jayeson.lib.betting.VNE.getbethistorybystatement.BetFairGetHistoriesByStatementModule;
import jayeson.lib.betting.VNE.getbethistorybystatement.BetFairListBetHistoriesByStatementResult;
import jayeson.lib.betting.api.actioncontext.BetHistoryByBetIdContext;
import jayeson.lib.betting.api.actionresults.ListBetHistoryResult;
import jayeson.lib.betting.api.task.AccountTaskHandler;
import jayeson.lib.betting.core.dagger.taskscope.MasterTaskModule;
import jayeson.lib.betting.core.dagger.taskscope.TaskModule;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;

import java.util.ArrayList;

@Module(includes = {TaskModule.class, MasterTaskModule.class
        , BetFairGetHistoriesByIdsModule.class
        , BetFairGetHistoriesByStatementModule.class
})
public class BetFairHistoryMasterTaskModule {

    @TaskScope @Provides
    BetHistoryByBetIdContext provideGetHistoriesByIdsContext() {
        return new BetHistoryByBetIdContext(new ArrayList<>());
    }

    @TaskScope
    @Provides
    AccountTaskHandler<ListBetHistoryResult> provideBetByIDTaskHandler(BetFairHistoryMasterTask masterTask) {
        return masterTask.getGetHistoryByIdTaskHandler();
    }


    @TaskScope
    @Provides
    AccountTaskHandler<BetFairListBetHistoriesByStatementResult> provideGetHistoryByStatementTaskHandler(
            BetFairHistoryMasterTask masterTask) {
        return masterTask.getGetHistoriesByStatementTaskHandler();
    }
}
