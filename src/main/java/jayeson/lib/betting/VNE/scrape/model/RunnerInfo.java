package jayeson.lib.betting.VNE.scrape.model;

public class RunnerInfo {
    private Integer selectionId;
    private Double back = 0d;
    private Double lay = 0d;
    private Double handicap = 0d;

    public RunnerInfo(Integer selectionId, Double back, Double lay, Double handicap) {
        this.selectionId = selectionId;
        this.back = back;
        this.lay = lay;
        this.handicap = handicap;
    }

    public Integer getSelectionId() {
        return selectionId;
    }

    public void setSelectionId(Integer selectionId) {
        this.selectionId = selectionId;
    }

    public Double getBack() {
        return back;
    }

    public void setBack(Double back) {
        this.back = back;
    }

    public Double getLay() {
        return lay;
    }

    public void setLay(Double lay) {
        this.lay = lay;
    }

    public Double getHandicap() {
        return handicap;
    }

    public void setHandicap(Double handicap) {
        this.handicap = handicap;
    }
}
