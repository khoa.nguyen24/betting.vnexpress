package jayeson.lib.betting.VNE.scrape;

import com.betfair.esa.client.auth.InvalidCredentialException;
import com.betfair.esa.client.cache.market.Market;
import com.betfair.esa.client.cache.market.MarketCache;
import com.betfair.esa.client.cache.market.MarketChangeListener;
import com.betfair.esa.client.protocol.*;
import com.betfair.esa.swagger.model.MarketChange;
import com.betfair.esa.swagger.model.MarketDataFilter;
import com.betfair.esa.swagger.model.MarketFilter;
import com.betfair.esa.swagger.model.RunnerChange;
import jayeson.lib.betting.VNE.VNE;
import jayeson.lib.betting.api.actioncontext.ScrapeContext;
import jayeson.lib.betting.api.actionresults.ActionResultCode;
import jayeson.lib.betting.api.actionresults.DeltaScrapeResult;
import jayeson.lib.betting.api.actionresults.ScrapeResult;
import jayeson.lib.betting.api.datastructure.*;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.api.task.AccountTaskState;
import jayeson.lib.betting.VNE.IAccountChangeListener;
import jayeson.lib.betting.VNE.scrape.model.MarketUpdate;
import jayeson.lib.betting.VNE.scrape.stream.BetfairStreamClient;
import jayeson.lib.betting.VNE.scrape.utils.BetFairScrapeUtility;
import jayeson.lib.betting.core.BettingAccount;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;
import jayeson.lib.betting.core.task.TaskFragment;
import jayeson.lib.betting.core.task.TaskFragmentListener;
import jayeson.lib.feed.api.LBType;
import jayeson.lib.feed.api.OddType;
import jayeson.lib.feed.api.SportType;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;

import javax.inject.Inject;
import java.util.*;

@TaskScope
public class MarketStreamFragment implements TaskFragment<ScrapeResult>, MarketChangeListener, ConnectionStatusListener {

    private final TaskFragmentListener<ScrapeResult> listener;
    private final ScrapeResult result;
    private boolean needProceed = true;
    private final AccountTaskInfo data;
    private final BetfairStreamClient streamClient;
    private final VNE account;
    private final ScrapeContext context;
    private final SportType sportType;
    private final String LOG_PREFIX;
    private final Logger LOGGER;
    private long notifyTime = 0;

    @Inject
    public MarketStreamFragment(AccountTaskInfo data, BettingAccount account, TaskFragmentListener<ScrapeResult> listener,
                                ScrapeResult initialResult, BetfairStreamClient streamClient, ScrapeContext context) {
        this.listener = listener;
        this.result = initialResult;
        this.data = data;
        this.streamClient = streamClient;
        this.account = (VNE) account;
        this.context = context;
        this.sportType = context.getSportType();
        this.LOG_PREFIX = String.format("MARKET_STREAM %s", sportType);
        this.LOGGER = account.getScrapeLogger();
    }

    @Override
    public TaskFragmentListener<ScrapeResult> listener() {
        return listener;
    }

    @Override
    public ScrapeResult result() {
        return this.result;
    }

    @Override
    public void proceed(ScrapeResult scrapeResult) {
    }

    @Override
    public int workload() {
        return 10;
    }

    public AccountTaskInfo accountTaskInfo() {
        return this.data;
    }

    @Override
    public void run() {
        String sportId = BetFairScrapeUtility.getSportId(sportType);
        if (sportId != null) {
            try {
                this.streamClient.addSportId(BetFairScrapeUtility.getSportId(sportType));
                this.streamClient.addMarketChangeListener(this);
                this.streamClient.addConnectionStatusListener(this);
                MarketDataFilter dataFilter = new MarketDataFilter();
                dataFilter.addFieldsItem(MarketDataFilter.FieldsEnum.EX_BEST_OFFERS_DISP);
                dataFilter.addFieldsItem(MarketDataFilter.FieldsEnum.EX_MARKET_DEF);
                this.streamClient.getClient().setMarketDataFilter(dataFilter);
                MarketFilter filter = new MarketFilter();
                filter.addEventTypeIdsItem(sportId);
                filter.setMarketTypes(BetFairScrapeUtility.getMarketTypeCodes(sportType));
                this.streamClient.getClient().setMarketFilter(filter);
                this.streamClient.getClient().setAutoReconnect(false);
                this.streamClient.subscribe(filter);

                this.account.registerAccountChangeListener(new IAccountChangeListener() {
                    @Override
                    public void accountDisconnect() {
                        LOGGER.info("Account disconnect => stop Betfair socket stream QUIETLY");
                        //MarketStreamFragment.this.streamClient.removeConnectionStatusListener(MarketStreamFragment.this);
                        closeStreamClient();
                        //needProceed = false;
                        //result.setCode(ActionResultCode.FAILED);
                        //result.setAccount(account.getId());
                        //result.setContext(context);
                        //result.setMessage("Account disconnected");
                        //listener.onFragmentCompleted(result, MarketStreamFragment.this);
                    }

                    @Override
                    public boolean removable() {
                        return streamClient.getClient().getStatus() == ConnectionStatus.DISCONNECTED
                                || streamClient.getClient().getStatus() == ConnectionStatus.STOPPED;
                    }
                });

                LOGGER.info("{}_{}_{} Betfair stream subscribed => use client {}", context.getSportType(),
                        context.getOddType(), context.getRequestTime(), this.streamClient.getClient());
            } catch (InvalidCredentialException e) {
                LOGGER.error("{} InvalidCredentialException happened", LOG_PREFIX, e);
                onError(e.getMessage());
            } catch (StatusException e) {
                LOGGER.error("{} StatusException happened", LOG_PREFIX, e);
                onError(e.getMessage());
            } catch (ConnectionException e) {
                LOGGER.error("{} ConnectionException happened", LOG_PREFIX, e);
                onError(e.getMessage());
            }
        } else {
            LOGGER.error("{} Cannot find sportId", LOG_PREFIX);
            onError("Cannot find sportId");
        }
    }


    private void onError(String message) {
        LOGGER.error("{} Stream return error {}", LOG_PREFIX, message);
        needProceed = false;
        result.setCode(ActionResultCode.NETWORK_ERROR);
        result.setAccount(account.getId());
        result.setContext(context);
        result.setMessage(message);
        listener.onFragmentCompleted(result, this);
    }

    public boolean needProceed() {
        return this.needProceed;
    }

    @Override
    public void marketChange(MarketCache.MarketChangeEvent event) {
        Market changeMarket = event.getMarket();
        if (changeMarket == null) return;

        String marketId = changeMarket.getMarketId();
        boolean isMarketOpen = changeMarket.isOpen();
        try {
            if (StringUtils.isEmpty(marketId)) return;

            if (isMarketOpen) {
                MarketChange change = event.getChange();
                try {
                    Pair<List<SportbookRecord>, List<DeleteRecordInfo>> parsedResult = parseRc(change.getRc(), marketId);
                    List<SportbookRecord> upsertList = parsedResult.getLeft();
                    List<DeleteRecordInfo> deleteList = parsedResult.getRight();
                    if(!deleteList.isEmpty()){
                        streamClient.pushDeleteRecords(sportType, deleteList);
                    }
                    if(!upsertList.isEmpty()){
                        streamClient.pushUpsertRecords(sportType, upsertList);
                    }
                } catch (Exception ex) {
                    LOGGER.error("{} Failed to update odd for {}: ", LOG_PREFIX, marketId, ex);
                }
            } else {
                streamClient.pushDeleteRecord(sportType, new MarketUpdate(marketId).toDeleteRecord());
            }
        } catch (Exception ex) {
            LOGGER.error("{} Error while processing market updated", LOG_PREFIX, ex);
        }
    }

    @Override
    public void notifyChange() {
        if (closeStreamClientIfRequestCancelled()) {
            return;
        }
        try {
            this.notifyTime = System.currentTimeMillis();
            List<DeleteRecordInfo> deleteList = streamClient.getDeleteRecords(sportType);
            List<SportbookRecord> upsertList = streamClient.getUpsertRecords(sportType);
            if (deleteList.isEmpty() && upsertList.isEmpty()) {
                return;
            }

            LOGGER.info("{} Notify change start for {} upsert {} delete", LOG_PREFIX, upsertList.size(), deleteList.size());

            DeltaScrapeResult result = BetFairScrapeUtility.constructResult(context, account.getId());
            result.setMessage(String.format("Received market change from %s", sportType));
            result.getUpsertRecords().put(OddType.LIVE, upsertList);
            result.getDeleteRecords().put(OddType.LIVE, deleteList);
            logNotify(deleteList, upsertList);

            listener.onFragmentCompleted(result, this);
            LOGGER.info("{} Notify change end", LOG_PREFIX);

            streamClient.cleanRecordsOnCache(sportType);
        } catch (Exception ex) {
            LOGGER.error("{} Failed to notify change: ", sportType, ex);
        }
    }

    @Override
    public void connectionStatusChange(ConnectionStatusChangeEvent statusChangeEvent) {
        LOGGER.info("{}_{}_{} [Client {}] Connection status change from {} to {} : {}",
            context.getSportType(), context.getOddType(), context.getRequestTime(), this.streamClient.getClient(),
            statusChangeEvent.getOldStatus(), statusChangeEvent.getNewStatus(), this);
        ConnectionStatus status = statusChangeEvent.getNewStatus();
        if (ConnectionStatus.DISCONNECTED.equals(status) || ConnectionStatus.STOPPED.equals(status)) {
            onError("Connection status changed to " + statusChangeEvent.getNewStatus().name());
        }
    }

    public void abort() {
        closeStreamClient();
    }

    private Pair<List<SportbookRecord>, List<DeleteRecordInfo>> parseRc(List<RunnerChange> rcs, String marketId){
        List<SportbookRecord> upsertList = new ArrayList<>();
        List<DeleteRecordInfo> deleteList = new ArrayList<>();
        for (RunnerChange rc : rcs){
            Double oddLay = getLayOddFromRunnerChange(rc);
            Double oddBack = getBackOddFromRunnerChange(rc);
            Double pivotValue = rc.getHc() == null ? 0.0d : rc.getHc();
            String runnerId = rc.getId().toString();
            if(oddBack != null){
                if(oddBack == 0){
                    deleteList.add(new MarketUpdate(marketId, runnerId, LBType.BACK, pivotValue).toDeleteRecord());
                } else if(oddBack > 0) {
                    upsertList.add(new MarketUpdate(marketId, runnerId, LBType.BACK, pivotValue, oddBack).toRecord(sportType));
                }
            }
            if(oddLay != null){
                if(oddLay == 0){
                    deleteList.add(new MarketUpdate(marketId, runnerId, LBType.LAY, pivotValue).toDeleteRecord());
                } else if(oddLay > 0) {
                    upsertList.add(new MarketUpdate(marketId, runnerId, LBType.LAY, pivotValue, oddLay).toRecord(sportType));
                }
            }
        }
        return Pair.of(upsertList, deleteList);
    }

    private Double getLayOddFromRunnerChange(RunnerChange rc){
        Double odd = null;
        for (List<Double> price : rc.getBdatl()){
            if(price.get(0) == 0){
                odd = price.get(1);
                if(price.get(2) == 0) odd = (double) 0;
            }
        }
        return odd;
    }

    private Double getBackOddFromRunnerChange(RunnerChange rc){
        Double odd = null;
        for (List<Double> price : rc.getBdatb()){
            if(price.get(0) == 0){
                odd = price.get(1);
                if(price.get(2) == 0) odd = (double) 0;
            }
        }
        return odd;
    }

    private void logNotify(List<DeleteRecordInfo> deleteList, List<SportbookRecord> upsertList){
        LOGGER.info("{} Notify Upsert size {}, Delete size {}", LOG_PREFIX, upsertList.size(), deleteList.size());
        deleteList.forEach(r ->  LOGGER.info("{} Notify Delete {} {}", LOG_PREFIX, r.getEventId(), r.getLbType()));
        upsertList.forEach(r ->  LOGGER.info("{} Notify Upsert {} {} {} => {}", LOG_PREFIX, r.getEventId(), r.getLbType(), r.getPivotValue(), r.getRateOver()));
    }

    private boolean closeStreamClientIfRequestCancelled() {
        if ((data.getState() == AccountTaskState.CANCELLED || data.getState() == AccountTaskState.FAILED)
                || context.getRequestStatus() == ScrapeContext.RequestStatus.CANCELLED) {
            LOGGER.info("Request status CANCELLED => stop Betfair socket stream");
            return closeStreamClient();
        }
        return false;
    }

    private boolean closeStreamClient() {
        try {
            LOGGER.info("{}_{}_{} Try to close stream client for {}",
                    context.getSportType(), context.getOddType(), context.getRequestTime(), streamClient.getClient());
            streamClient.getClient().stop();
            return true;
        } catch (Exception e) {
            LOGGER.warn("Error when trying to close stream client", e);
        } finally {
            LOGGER.info("Client {} status {}", streamClient.getClient(), streamClient.getClient().getStatus());
        }
        return false;
    }
}
