package jayeson.lib.betting.VNE.scrape;

import jayeson.lib.betting.VNE.scrape.model.DataTransfer;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public interface IGetBookFragment {
    GetBookFragment create(AtomicInteger batchTracker, AtomicInteger itemTracker, List<String> marketIds, DataTransfer dataTransfer);
}
