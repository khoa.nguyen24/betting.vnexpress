package jayeson.lib.betting.VNE.scrape;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.google.auto.factory.AutoFactory;
import com.google.auto.factory.Provided;
import jayeson.lib.betting.VNE.VNE;
import jayeson.lib.betting.VNE.VNEURIDefinition;
import jayeson.lib.betting.api.actioncontext.ScrapeContext;
import jayeson.lib.betting.api.actionresults.DeltaScrapeResult;
import jayeson.lib.betting.api.actionresults.ScrapeResult;
import jayeson.lib.betting.api.datastructure.DeleteRecordInfo;
import jayeson.lib.betting.api.datastructure.SportbookRecord;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.VNE.scrape.model.DataTransfer;
import jayeson.lib.betting.VNE.scrape.model.ILiveState;
import jayeson.lib.betting.VNE.scrape.parser.IBetfairParser;
import jayeson.lib.betting.VNE.scrape.utils.BetFairScrapeUtility;
import jayeson.lib.betting.core.http.IClient;
import jayeson.lib.betting.core.http.IResponse;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.http.request.BasicRequest;
import jayeson.lib.betting.core.task.AsyncHttpTaskFragment;
import jayeson.lib.betting.core.task.TaskFragmentListener;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;
import jayeson.lib.betting.core.utility.BettingUtility;
import jayeson.lib.betting.exception.BettingException;

import jayeson.lib.feed.api.OddType;
import okhttp3.Call;
import okhttp3.Response;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;


@AutoFactory(
        implementing = IGetLiveStateFragment.class
)
public class GetLiveStateFragment extends AsyncHttpTaskFragment<ScrapeResult, String> {

    private final ObjectMapper objectMapper;
    private final IBetfairParser parser;
    private final List<String> eventIds;
    private final String LOG_PREFIX;
    private final Logger LOGGER;
    private final ScrapeContext context;
    private final GetEventFragmentFactory getEventFragmentFactory;
    private DataTransfer dataTransfer;

    public GetLiveStateFragment(@Provided AccountTaskInfo data, @Provided ITaskExecutor executor, @Provided IResponseListener responseListener,
                                @Provided VNE account, @Provided TaskFragmentListener<ScrapeResult> listener, @Provided ScrapeContext context,
                                @Provided ScrapeResult initialResult, @Provided IClient client, @Provided GetEventFragmentFactory getEventFragmentFactory,
                                @Provided IBetfairParser parser, List<String> eventIds, DataTransfer dataTransfer) {
        super(data, executor, responseListener, account, listener, initialResult, client);
        this.objectMapper = new ObjectMapper();
        this.dataTransfer = dataTransfer;
        this.parser = parser;
        this.getEventFragmentFactory = getEventFragmentFactory;
        this.eventIds = eventIds;
        this.context = context;
        needProceed = false;
        this.LOG_PREFIX = "GET_LIVE_STATE " + context.getSportType().name() + StringUtils.SPACE + context.getRequestType().name();
        this.LOGGER = account.getScrapeLogger();
    }

    @Override
    public void execute() throws BettingException {
        Map<String, String> params = new HashMap<>();
        List<String> eventIds = new ArrayList<>(this.eventIds);
        params.put("_ak", "dyMLAanpRyIsjkpJ");
        params.put("alt", "json");
        params.put("eventIds", String.join(",", eventIds));
        params.put("ts", String.valueOf(System.currentTimeMillis()));

        client.resetHost(BetFairScrapeUtility.getInPlayServiceApiUrl(client.host().scheme(), client.originalUrl()));

//        BasicRequest request = new BasicRequest.Builder()
//                .def(VNEURIDefinition.GET_LIVE_STATE)
//                .host(client.host())
//                .queries(params)
//                .build();
//
//        executeRequest(request, true);
    }

    @Override
    public void onResponse(Call call, Response response) throws IOException {
        int code = response.code();
        String message = response.message();
        if (code == 200) {
            super.onResponse(call, response);
        } else {
            LOGGER.error("{} RESPONSE_INVALID Code {} Message {} need Proceed {}", LOG_PREFIX, code, message, needProceed);
        }
    }

    @Override
    public void consumeResponse(IResponse<String> response) {
        int statusCode = response.getStatusLine().getCode();
        LOGGER.info("{} RESPONSE_READY Status {}", LOG_PREFIX, statusCode);
        if (statusCode != 200) LOGGER.error("{} Failed to get live state, code {}", LOG_PREFIX, statusCode);

        try {
            account.getHttpLogger(this.accountTaskInfo().getAccountTaskType()).info("{} RESPONSE {}",
                    LOG_PREFIX, BettingUtility.writeObjectToJson(response.content()));
            JsonNode jsonContent = objectMapper.readTree(response.content());
            if (jsonContent.isArray()) {
                ArrayNode eventLiveStates = (ArrayNode) jsonContent;
                parseLiveStates(eventLiveStates);
                executeNextFragment();
            } else {
                LOGGER.error("{} Invalid live state", LOG_PREFIX);
            }
        } catch (IOException e) {
            LOGGER.error("{} Unable to parse live state response: {}", LOG_PREFIX, e);
        }
    }

    private void parseLiveStates(ArrayNode eventLiveStates) {
        eventLiveStates.forEach(event -> {
            String eventId = event.get("eventId").asText();
            ILiveState liveState = parser.parseLiveState(event);
            if (liveState != null && StringUtils.isNotBlank(eventId)) {
                dataTransfer.addLiveState(liveState);
            }
        });

        LOGGER.debug("{} Successfully parsed {} state", LOG_PREFIX, dataTransfer.getLiveStates().size());
        LOGGER.debug("{} Parsed live states : {}", LOG_PREFIX, BettingUtility.writeObjectToJson(dataTransfer.getLiveStates()));

    }

    private void executeNextFragment(){
        switch (context.getRequestType()) {
            case FETCH_DELTA:
                needProceed = true;
                break;
            case FETCH_DELTA_STATE:
                notifyEventState();
                break;
        }
    }

    private void notifyEventState(){
        List<String> finishedEventIds = dataTransfer.getLiveStates().stream().filter(ILiveState::isFinished).map(ILiveState::getEventId).collect(Collectors.toList());
        dataTransfer.addDeleteIds(finishedEventIds);

        List<DeleteRecordInfo> deletes = dataTransfer.getDeleteRecords();
        List<SportbookRecord> records = dataTransfer.getLiveStates().stream().filter(event -> !event.isFinished()).map(ILiveState::toRecord).collect(Collectors.toList());

        if(records.isEmpty() && deletes.isEmpty()) return;

        DeltaScrapeResult result = BetFairScrapeUtility.constructResult(context, account.getId());
        result.getUpsertRecords().put(OddType.LIVE, records);
        result.getDeleteRecords().put(OddType.LIVE, deletes);
        LOGGER.debug("{} Notify {} upsert {} deleted for {}_LIVE", LOG_PREFIX, records.size(), deletes.size(), context.getSportType());
        listener().onFragmentCompleted(result, this);
    }

    @Override
    public void proceed(ScrapeResult scrapeResult) {
    }
}
