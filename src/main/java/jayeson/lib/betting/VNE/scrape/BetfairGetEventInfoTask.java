package jayeson.lib.betting.VNE.scrape;

import jayeson.lib.betting.VNE.scrape.model.DataTransfer;
import jayeson.lib.betting.VNE.scrape.parser.GetEventRequestParserFactory;
import jayeson.lib.betting.api.actioncontext.ScrapeContext;
import jayeson.lib.betting.api.actionresults.ScrapeResult;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.api.task.ScrapTaskHandler;
import jayeson.lib.betting.api.task.TaskObservable;
import jayeson.lib.betting.VNE.json.BetfairGetEventParam;
import jayeson.lib.betting.VNE.scrape.parser.GetEventRequestParserFactory;
import jayeson.lib.betting.core.BettingAccount;
import jayeson.lib.betting.core.dagger.qualifier.TaskId;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.task.ScrapeTask;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;
import jayeson.lib.betting.core.utility.BettingUtility;

import javax.inject.Inject;
import java.util.List;
import java.util.concurrent.TimeUnit;

@TaskScope
public class BetfairGetEventInfoTask extends ScrapeTask {

    private final GetEventFragmentFactory getEventFragmentFactory;
    private final GetEventRequestParserFactory parserFactory;

    @Inject
    public BetfairGetEventInfoTask(ITaskExecutor executor, IResponseListener responseListener, AccountTaskInfo data,
                                   BettingAccount account, @TaskId String id, ScrapTaskHandler<ScrapeResult> handler,
                                   TaskObservable<AccountTaskInfo> observable, ScrapeContext context, ScrapeResult result,
                                   GetEventFragmentFactory getEventFragmentFactory, GetEventRequestParserFactory parserFactory) {
        super(executor, responseListener, data, account, id, handler, observable, context, result);
        this.getEventFragmentFactory = getEventFragmentFactory;
        this.parserFactory = parserFactory;
    }

    @Override
    protected void execute() {
        account.getScrapeLogger().info("START_GET_EVENT_INFO_TASK {}_{} ", context.getSportType(), context.getRequestTime());
        BetfairGetEventParam param = parserFactory.create().parse(context.getParamString());
        List<String> eventIds = param.getEventIds();
        account.getScrapeLogger().info("{} LAUNCH_GET_LIVE_EVENT_FRAGMENT {}", context.getSportType(), BettingUtility.writeObjectToJson(eventIds));
        GetEventFragment fragment = getEventFragmentFactory.create(true, param.getEventIds(), new DataTransfer());
        executor.schedule(fragment, 50L, TimeUnit.MILLISECONDS);
    }
}
