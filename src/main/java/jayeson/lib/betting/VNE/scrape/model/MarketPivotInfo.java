package jayeson.lib.betting.VNE.scrape.model;

import jayeson.lib.feed.api.TimeType;
import jayeson.lib.feed.api.twoside.PivotType;

public class MarketPivotInfo {
    private PivotType pivotType;
    private TimeType timeType;
    private Double goal;
    private String eventType;

    public PivotType getPivotType() {
        return pivotType;
    }

    public void setPivotType(PivotType pivotType) {
        this.pivotType = pivotType;
    }

    public TimeType getTimeType() {
        return timeType;
    }

    public void setTimeType(TimeType timeType) {
        this.timeType = timeType;
    }

    public Double getGoal() {
        return goal;
    }

    public void setGoal(Double goal) {
        this.goal = goal;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public MarketPivotInfo(PivotType pivotType, TimeType timeType, Double goal, String eventType) {
        this.pivotType = pivotType;
        this.timeType = timeType;
        this.goal = goal;
        this.eventType = eventType;
    }
}
