package jayeson.lib.betting.VNE.scrape;

import jayeson.lib.betting.VNE.scrape.model.DataTransfer;

import java.util.List;

public interface IGetLiveStateFragment {
    GetLiveStateFragment create(List<String> eventIds, DataTransfer dataTransfer);
}
