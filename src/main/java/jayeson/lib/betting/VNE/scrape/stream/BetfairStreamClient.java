package jayeson.lib.betting.VNE.scrape.stream;

import com.betfair.esa.client.Client;
import com.betfair.esa.client.ClientCache;
import com.betfair.esa.client.auth.AppKeyAndSessionProvider;
import com.betfair.esa.client.auth.InvalidCredentialException;
import com.betfair.esa.client.cache.market.MarketChangeListener;
import com.betfair.esa.client.protocol.ConnectionException;
import com.betfair.esa.client.protocol.ConnectionStatusListener;
import com.betfair.esa.client.protocol.StatusException;
import com.betfair.esa.swagger.model.MarketFilter;
import jayeson.lib.betting.VNE.VNE;
import jayeson.lib.betting.VNE.VNEURIDefinition;
import jayeson.lib.betting.api.datastructure.DeleteRecordInfo;
import jayeson.lib.betting.api.datastructure.SportbookRecord;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;
import jayeson.lib.betting.core.http.IClient;
import jayeson.lib.feed.api.SportType;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;

@TaskScope
public class BetfairStreamClient {
	private final VNE account;
	private final IClient bettingHttpClient;
	private int traceCutoff;
	private Client client;
	private ClientCache cache;
	private List<String> sportIds;

	@Inject
	public BetfairStreamClient(VNE account, IClient bettingHttpClient) {
		this.account = account;
		this.traceCutoff = 0;
		this.sportIds = new LinkedList<>();
		this.bettingHttpClient = bettingHttpClient;
		initialize();
	}
	
	public void initialize() {
		//1: Create a session provider
        AppKeyAndSessionProvider sessionProvider = new AppKeyAndSessionProvider(
            AppKeyAndSessionProvider.SSO_HOST_COM,
            account.getAppKey(),
            account.getUsername(),
            account.getPassword(),
			bettingHttpClient);

        //2: Create a client
        client = new Client(
			VNEURIDefinition.STREAM_API_HOST,
            VNEURIDefinition.STREAM_API_PORT,
            sessionProvider, account);
        client.setTraceChangeTruncation(traceCutoff);

        //3: Create a cache
        cache = new ClientCache(client, account);
	}
	
	public void addSportId(String sportId) {
		this.sportIds.add(sportId);
	}
	
	public void addMarketChangeListener(MarketChangeListener listener) {
		cache.getMarketCache().addMarketChangeListener(listener);
	}

	public void addConnectionStatusListener(ConnectionStatusListener listener) {
		client.addConnectionStatusListener(listener);
	}

	public void removeConnectionStatusListener(ConnectionStatusListener listener) {
		client.removeConnectionStatusListener(listener);
	}

	public void pushDeleteRecords(SportType sportType, List<DeleteRecordInfo> items) {
		cache.getMarketCache().pushDeleteRecords(sportType, items);
	}

	public void pushDeleteRecord(SportType sportType, DeleteRecordInfo item) {
		cache.getMarketCache().pushDeleteRecord(sportType, item);
	}

	public void pushUpsertRecords(SportType sportType, List<SportbookRecord> items) {
		cache.getMarketCache().pushUpsertRecords(sportType, items);
	}

	public List<DeleteRecordInfo> getDeleteRecords(SportType sportType) {
		return cache.getMarketCache().getDeleteRecords(sportType);
	}

	public List<SportbookRecord> getUpsertRecords(SportType sportType) {
		return cache.getMarketCache().getUpsertRecords(sportType);
	}

	public void cleanRecordsOnCache(SportType sportType){
		cache.getMarketCache().cleanRecords(sportType);
	}
	
	public void subscribe() throws InvalidCredentialException, StatusException, ConnectionException {
		subscribe(null);
	}
	
	public void subscribe(MarketFilter filter) throws InvalidCredentialException, StatusException, ConnectionException {
		if (filter == null) {
			filter = new MarketFilter();
		}
		
		for (String sportId: sportIds) {
			filter.addEventTypeIdsItem(sportId);
		}
        
        cache.subscribeMarkets(filter);
	}
	
	public Client getClient() {
		return client;
	}
}
