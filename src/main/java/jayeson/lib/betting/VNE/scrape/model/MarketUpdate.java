package jayeson.lib.betting.VNE.scrape.model;

import jayeson.lib.betting.api.datastructure.*;
import jayeson.lib.feed.api.LBType;
import jayeson.lib.feed.api.SportType;
import org.apache.commons.lang3.StringUtils;

public class MarketUpdate {
    private String marketId;
    private String runnerId;
    private LBType lbType;
    private Double oddValue;
    private Double pivotValue;

    public static String generateRcKey(String marketId, String runnerId,Double pivotValue){
        return String.format("%s&%s&%s", marketId, runnerId, String.format("%.2f", pivotValue));
    }

    public MarketUpdate(String marketId, String runnerId, LBType lbType, Double pivotValue, Double oddValue){
        this.marketId = marketId;
        this.runnerId = runnerId;
        this.lbType = lbType;
        this.pivotValue = pivotValue;
        this.oddValue = oddValue;
    }

    public MarketUpdate(String marketId, String runnerId, LBType lbType, Double pivotValue){
        this.marketId = marketId;
        this.runnerId = runnerId;
        this.lbType = lbType;
        this.pivotValue = pivotValue;
    }

    public MarketUpdate(String marketId){
        this.marketId = marketId;
    }

    public SportbookRecord toRecord(SportType sportType){
        SportbookRecord record = null;
        switch (sportType){
            case SOCCER:
                record = new SoccerSportbookRecord();
                break;
            case BASKETBALL:
                record = new BasketballSportbookRecord();
                break;
            case TENNIS:
                record = new TennisSportbookRecord();
                break;
        }
        if(record != null){
            record.setEventId(String.format("%s&%s", marketId, runnerId));
            record.setLbType(lbType);
            record.setRateOver(oddValue);
            record.setPivotValue(pivotValue);
        }
        return record;
    }

    public DeleteRecordInfo toDeleteRecord(){
        if(runnerId != null){
            String id = MarketUpdate.generateRcKey(marketId, runnerId, pivotValue);
            return new DeleteRecordInfo(id, StringUtils.EMPTY,StringUtils.EMPTY,StringUtils.EMPTY, StringUtils.EMPTY, lbType);
        }
        return new DeleteRecordInfo(marketId, StringUtils.EMPTY,StringUtils.EMPTY,StringUtils.EMPTY,StringUtils.EMPTY, LBType.BACK);
    }
}
