package jayeson.lib.betting.VNE.scrape.model;

import jayeson.lib.betting.api.datastructure.SportbookRecord;

public interface ILiveState {
    SportbookRecord toRecord();
    String getEventId();
    boolean isFinished();
    SportbookRecord setState(SportbookRecord record);
}
