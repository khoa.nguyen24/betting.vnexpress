package jayeson.lib.betting.VNE.scrape.parser;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import jayeson.lib.betting.VNE.json.eventdata.EventResult;
import jayeson.lib.betting.VNE.json.eventdata.MarketEvent;
import jayeson.lib.betting.api.datastructure.SportbookRecord;
import jayeson.lib.betting.VNE.json.PriceSize;
import jayeson.lib.betting.VNE.json.marketbook.Ex;
import jayeson.lib.betting.VNE.json.marketbook.Runner;
import jayeson.lib.betting.VNE.json.marketcatalogue.Event;
import jayeson.lib.betting.VNE.scrape.model.EventData;
import jayeson.lib.betting.VNE.scrape.model.MarketData;
import jayeson.lib.betting.VNE.scrape.model.RunnerInfo;
import jayeson.lib.feed.api.LBType;
import jayeson.lib.feed.api.twoside.PivotBias;
import jayeson.lib.feed.api.twoside.PivotType;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

public abstract class BetfairAbstractParser implements IBetfairParser {

    public final List<LBType> LB_TYPE_LIST = ImmutableList.of(LBType.BACK, LBType.LAY);
    private static final Logger logger = LoggerFactory.getLogger("betfair.scrape");
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

    @Override
    public EventData parseEventData(EventResult result, boolean isLive) {
        try {
            if (result.event == null) return null;

            MarketEvent event = result.event;
            if (StringUtils.isAnyBlank(event.id, event.name) || event.openDate == null) return null;

            String[] teams = event.name.split(" v | @ ");
            if (teams.length != 2) return null;

            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

            return new EventData(null, teams[0], teams[1], event.id, sdf.parse(event.openDate).getTime(), result.marketCount, isLive);
        } catch (Exception ex) {
            logger.error("Failed to parse match data: ", ex);
        }

        return null;
    }

    @Override
    public EventData parseEventData(Event event, String leagueName){
        try {
            if (event == null) return null;

            if (StringUtils.isAnyBlank(event.id, event.name) || event.openDate == null) return null;

            String[] teams = event.name.split(" v | @ ");
            if (teams.length != 2) return null;

            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

            return new EventData(leagueName, teams[0], teams[1], event.id, sdf.parse(event.openDate).getTime(), 0, false);
        } catch (Exception ex) {
            logger.error("Failed to parse match data: ", ex);
        }

        return null;
    }
    
    @Override
    public RunnerInfo parseRunnerInfo(Runner runner, MarketData marketData) {
        try {
            if (runner.selectionId == null || runner.ex == null) return null;

            Integer selectionId = runner.selectionId;
            Ex ex = runner.ex;
            Double back = getPriceInfo(ex.availableToBack, marketData.getType());
            Double lay = getPriceInfo(ex.availableToLay, marketData.getType());
            Double handicap = runner.handicap != null ? runner.handicap : 0.0;

            logger.debug("[Runner] market={} : {} {} {} {}", marketData.getId(), selectionId, back, lay, handicap);

            return new RunnerInfo(selectionId, back, lay, handicap);
        } catch (Exception ex) {
            logger.error("Failed to parse runner info: ", ex);
        }

        return null;
    }

    private Double getPriceInfo(List<PriceSize> availablePrices, PivotType pivotType) {
        try {
            if (availablePrices != null && availablePrices.size() > 0) {
                PriceSize priceSize = availablePrices.get(0);
                if (priceSize != null && priceSize.price != null) {
                    return round(PivotType.ONE_TWO.equals(pivotType) ?
                        priceSize.price : convertEUToHKOdds(priceSize.price));
                }
            }
        } catch (Exception ex) {
            logger.error("Failed to get price info: ", ex);
        }

        return 0.0;
    }

    private Double round(Double value) {
        return Math.round(value * 100.0) / 100.0;
    }

    private Double convertEUToHKOdds(Double euOdds) {
        return euOdds - 1;
    }

    protected void removeInvalidRecords(List<SportbookRecord> records) {
        records.removeIf(r  -> r.getRateOver() <= 0 || r.getRateUnder() <= 0 ||
            (PivotType.ONE_TWO.equals(r.getPivotType()) && r.getRateEqual() <=0) ||
            (PivotType.TOTAL.equals(r.getPivotType()) && r.getPivotValue() == 0.0));
    }

    protected Pair<Double, String> getRateInfo(RunnerInfo data, LBType lbType, String matchId, String marketId,
                                               PivotType pivotType, Double pivotValue) {
        Double rateValue = lbType == LBType.LAY ? data.getLay() : data.getBack();
        if (rateValue == null) {
            return null;
        }

        String rateId = getRateId(matchId, marketId, data, pivotType, pivotValue);
        return Pair.of(rateValue, rateId);
    }

    protected String getRateId(String matchId, String marketId, RunnerInfo data, PivotType pivotType,
                                        Double pivotValue) {
        if (PivotType.HDP.equals(pivotType)) {
            return String.format("%s&%s&%s&%s", matchId, marketId, data.getSelectionId(), data.getHandicap());
        }

        if (PivotType.ONE_TWO.equals(pivotType)) {
            return String.format("%s&%s&%s", matchId, marketId, data.getSelectionId());
        }

        return String.format("%s&%s&%s&%s", matchId, marketId, data.getSelectionId(), pivotValue);
    }

    protected Pair<PivotBias, Double> getPivotInfo(Double hostPivot) {
        if (hostPivot == 0.0) return Pair.of(PivotBias.NEUTRAL, Math.abs(hostPivot));

        if (hostPivot > 0.0) return Pair.of(PivotBias.GUEST, Math.abs(hostPivot));

        return Pair.of(PivotBias.HOST, Math.abs(hostPivot));
    }

    protected Map<Double, List<RunnerInfo>> groupRunner(List<RunnerInfo> infoList, PivotType pivotType) {
        Map<Double, List<RunnerInfo>> map = new HashMap<>();
        try {
            List<List<RunnerInfo>> partitionedList = Lists.partition(infoList, 2);
            for (List<RunnerInfo> itemList : partitionedList) {
                if (itemList.size() < 2) continue;

                RunnerInfo over = itemList.get(PivotType.TOTAL.equals(pivotType) ? 1 : 0);
                Double hostPivot = over.getHandicap();
                map.put(hostPivot, itemList);
            }
        } catch (Exception ex) {
            logger.error("Failed to group runner: ", ex);
        }

        return map;
    }
}
