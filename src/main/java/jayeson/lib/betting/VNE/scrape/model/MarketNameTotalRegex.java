package jayeson.lib.betting.VNE.scrape.model;

import jayeson.lib.feed.api.twoside.PivotType;
import jayeson.lib.feed.soccer.SoccerTimeType;
import org.apache.commons.lang3.StringUtils;

public enum MarketNameTotalRegex {
    TOTAL("Over/Under (\\d\\.\\d) Goals.*", PivotType.TOTAL, SoccerTimeType.FT, StringUtils.EMPTY),
    TOTAL_FIRST_HALF("First Half Goals (\\d\\.\\d).*", PivotType.TOTAL, SoccerTimeType.HT, StringUtils.EMPTY),
    TOTAL_CORNER("Corners Over/Under (\\d+\\.\\d).*", PivotType.TOTAL, SoccerTimeType.FT, "No. of Corners"),
    TOTAL_BOOKING("Cards Over/Under (\\d+\\.\\d).*", PivotType.TOTAL, SoccerTimeType.FT, "No. of Bookings");

    private final String regex;
    private final PivotType pivotType;
    private final SoccerTimeType soccerTimeType;
    private final String eventType;

    MarketNameTotalRegex(String regex, PivotType pivotType, SoccerTimeType soccerTimeType, String eventType) {
        this.regex = regex;
        this.pivotType = pivotType;
        this.soccerTimeType = soccerTimeType;
        this.eventType = eventType;
    }


    public String getRegex() {
        return regex;
    }

    public PivotType getPivotType() {
        return pivotType;
    }

    public SoccerTimeType getSoccerTimeType() {
        return soccerTimeType;
    }

    public String getEventType() {
        return eventType;
    }
}
