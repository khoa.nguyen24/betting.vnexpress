package jayeson.lib.betting.VNE.scrape;

import jayeson.lib.betting.VNE.scrape.model.DataTransfer;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public interface IGetCatalogueFragment {
    GetCatalogueFragment create(List<String> eventIds, List<String> marketIds, DataTransfer dataTransfer, AtomicInteger fragmentLeft);
}
