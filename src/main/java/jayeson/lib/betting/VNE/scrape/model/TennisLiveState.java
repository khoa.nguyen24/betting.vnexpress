package jayeson.lib.betting.VNE.scrape.model;

import jayeson.lib.betting.api.datastructure.SportbookRecord;
import jayeson.lib.betting.api.datastructure.TennisSportbookRecord;
import jayeson.lib.feed.tennis.TennisSegment;

public class TennisLiveState implements ILiveState {
    private String eventId;
    private int hostPoint;
    private int guestPoint;
    private int duration;
    private int hostSetScore;
    private int guestSetScore;
    private int hostGameScore;
    private int guestGameScore;
    private int hostFirstSetGame;
    private int guestFirstSetGame;
    private int hostSecondSetGame;
    private int guestSecondSetGame;
    private int hostThirdSetGame;
    private int guestThirdSetGame;
    private int hostFourthSetGame;
    private int guestFourthSetGame;
    private int hostFifthSetGame;
    private int guestFifthSetGame;
    private boolean isHostAv;
    private boolean isGuestAv;
    private boolean isFinished;

    public TennisLiveState(String eventId, int hostPoint, int guestPoint, int duration, int hostSetScore, int guestSetScore,
                           int hostGameScore, int guestGameScore, int hostFirstSetGame, int guestFirstSetGame, int hostSecondSetGame,
                           int guestSecondSetGame, int hostThirdSetGame, int guestThirdSetGame, int hostFourthSetGame,
                           int guestFourthSetGame, int hostFifthSetGame, int guestFifthSetGame, boolean isHostAv, boolean isGuestAv, boolean isFinished) {
        this.eventId = eventId;
        this.hostPoint = hostPoint;
        this.guestPoint = guestPoint;
        this.duration = duration;
        this.hostSetScore = hostSetScore;
        this.guestSetScore = guestSetScore;
        this.hostGameScore = hostGameScore;
        this.guestGameScore = guestGameScore;
        this.hostFirstSetGame = hostFirstSetGame;
        this.guestFirstSetGame = guestFirstSetGame;
        this.hostSecondSetGame = hostSecondSetGame;
        this.guestSecondSetGame = guestSecondSetGame;
        this.hostThirdSetGame = hostThirdSetGame;
        this.guestThirdSetGame = guestThirdSetGame;
        this.hostFourthSetGame = hostFourthSetGame;
        this.guestFourthSetGame = guestFourthSetGame;
        this.hostFifthSetGame = hostFifthSetGame;
        this.guestFifthSetGame = guestFifthSetGame;
        this.isHostAv = isHostAv;
        this.isGuestAv = isGuestAv;
        this.isFinished = isFinished;
    }

    @Override
    public SportbookRecord toRecord(){
        TennisSportbookRecord record = new TennisSportbookRecord();
        record.setEventId(eventId);
        record.setHostPoint(hostPoint);
        record.setGuestPoint(guestPoint);
        record.setEventDuration(duration);
        record.setEventSegment(TennisSegment.LIVE);
        record.setHostSetScore(hostSetScore);
        record.setGuestSetScore(guestSetScore);
        record.setHostGameScore(hostGameScore);
        record.setGuestGameScore(guestGameScore);
        record.setHostFirstSetGame(hostFirstSetGame);
        record.setGuestFirstSetGame(guestFirstSetGame);
        record.setHostSecondSetGame(hostSecondSetGame);
        record.setGuestSecondSetGame(guestSecondSetGame);
        record.setHostThirdSetGame(hostThirdSetGame);
        record.setGuestThirdSetGame(guestThirdSetGame);
        record.setHostFourthSetGame(hostFourthSetGame);
        record.setGuestFourthSetGame(guestFourthSetGame);
        record.setHostFifthSetGame(hostFifthSetGame);
        record.setGuestFifthSetGame(guestFifthSetGame);
        record.setHostAv(isHostAv);
        record.setGuestAv(isGuestAv);
        return record;
    }

    @Override
    public SportbookRecord setState(SportbookRecord r){
        TennisSportbookRecord record = (TennisSportbookRecord)r;
        record.setEventId(eventId);
        record.setHostPoint(hostPoint);
        record.setGuestPoint(guestPoint);
        record.setEventDuration(duration);
        record.setEventSegment(TennisSegment.LIVE);
        record.setHostSetScore(hostSetScore);
        record.setGuestSetScore(guestSetScore);
        record.setHostGameScore(hostGameScore);
        record.setGuestGameScore(guestGameScore);
        record.setHostFirstSetGame(hostFirstSetGame);
        record.setGuestFirstSetGame(guestFirstSetGame);
        record.setHostSecondSetGame(hostSecondSetGame);
        record.setGuestSecondSetGame(guestSecondSetGame);
        record.setHostThirdSetGame(hostThirdSetGame);
        record.setGuestThirdSetGame(guestThirdSetGame);
        record.setHostFourthSetGame(hostFourthSetGame);
        record.setGuestFourthSetGame(guestFourthSetGame);
        record.setHostFifthSetGame(hostFifthSetGame);
        record.setGuestFifthSetGame(guestFifthSetGame);
        record.setHostAv(isHostAv);
        record.setGuestAv(isGuestAv);
        return record;
    }

    @Override
    public String getEventId() {
        return eventId;
    }

    @Override
    public boolean isFinished() {
        return isFinished;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public int getHostPoint() {
        return hostPoint;
    }

    public void setHostPoint(int hostPoint) {
        this.hostPoint = hostPoint;
    }

    public int getGuestPoint() {
        return guestPoint;
    }

    public void setGuestPoint(int guestPoint) {
        this.guestPoint = guestPoint;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getHostSetScore() {
        return hostSetScore;
    }

    public void setHostSetScore(int hostSetScore) {
        this.hostSetScore = hostSetScore;
    }

    public int getGuestSetScore() {
        return guestSetScore;
    }

    public void setGuestSetScore(int guestSetScore) {
        this.guestSetScore = guestSetScore;
    }

    public int getHostGameScore() {
        return hostGameScore;
    }

    public void setHostGameScore(int hostGameScore) {
        this.hostGameScore = hostGameScore;
    }

    public int getGuestGameScore() {
        return guestGameScore;
    }

    public void setGuestGameScore(int guestGameScore) {
        this.guestGameScore = guestGameScore;
    }

    public int getHostFirstSetGame() {
        return hostFirstSetGame;
    }

    public void setHostFirstSetGame(int hostFirstSetGame) {
        this.hostFirstSetGame = hostFirstSetGame;
    }

    public int getGuestFirstSetGame() {
        return guestFirstSetGame;
    }

    public void setGuestFirstSetGame(int guestFirstSetGame) {
        this.guestFirstSetGame = guestFirstSetGame;
    }

    public int getHostSecondSetGame() {
        return hostSecondSetGame;
    }

    public void setHostSecondSetGame(int hostSecondSetGame) {
        this.hostSecondSetGame = hostSecondSetGame;
    }

    public int getGuestSecondSetGame() {
        return guestSecondSetGame;
    }

    public void setGuestSecondSetGame(int guestSecondSetGame) {
        this.guestSecondSetGame = guestSecondSetGame;
    }

    public int getHostThirdSetGame() {
        return hostThirdSetGame;
    }

    public void setHostThirdSetGame(int hostThirdSetGame) {
        this.hostThirdSetGame = hostThirdSetGame;
    }

    public int getGuestThirdSetGame() {
        return guestThirdSetGame;
    }

    public void setGuestThirdSetGame(int guestThirdSetGame) {
        this.guestThirdSetGame = guestThirdSetGame;
    }

    public int getHostFourthSetGame() {
        return hostFourthSetGame;
    }

    public void setHostFourthSetGame(int hostFourthSetGame) {
        this.hostFourthSetGame = hostFourthSetGame;
    }

    public int getGuestFourthSetGame() {
        return guestFourthSetGame;
    }

    public void setGuestFourthSetGame(int guestFourthSetGame) {
        this.guestFourthSetGame = guestFourthSetGame;
    }

    public int getHostFifthSetGame() {
        return hostFifthSetGame;
    }

    public void setHostFifthSetGame(int hostFifthSetGame) {
        this.hostFifthSetGame = hostFifthSetGame;
    }

    public int getGuestFifthSetGame() {
        return guestFifthSetGame;
    }

    public void setGuestFifthSetGame(int guestFifthSetGame) {
        this.guestFifthSetGame = guestFifthSetGame;
    }

    public boolean isHostAv() {
        return isHostAv;
    }

    public void setHostAv(boolean hostAv) {
        isHostAv = hostAv;
    }

    public boolean isGuestAv() {
        return isGuestAv;
    }

    public void setGuestAv(boolean guestAv) {
        isGuestAv = guestAv;
    }
}
