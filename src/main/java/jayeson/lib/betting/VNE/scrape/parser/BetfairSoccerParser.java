package jayeson.lib.betting.VNE.scrape.parser;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.collect.Lists;
import jayeson.lib.betting.VNE.json.marketcatalogue.MarketCatalogue;
import jayeson.lib.betting.VNE.scrape.model.*;
import jayeson.lib.betting.api.datastructure.SoccerSportbookRecord;
import jayeson.lib.betting.api.datastructure.SportbookRecord;

import jayeson.lib.betting.VNE.scrape.model.*;
import jayeson.lib.feed.api.LBType;
import jayeson.lib.feed.api.OddFormat;
import jayeson.lib.feed.api.OddType;
import jayeson.lib.feed.api.twoside.PivotBias;
import jayeson.lib.feed.api.twoside.PivotType;
import jayeson.lib.feed.soccer.SoccerSegment;
import jayeson.lib.feed.soccer.SoccerTimeType;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.regex.Pattern;

public class BetfairSoccerParser extends BetfairAbstractParser {

    private static final Logger logger = LoggerFactory.getLogger("betfair.scrape");

    @Override
    public MarketData parseMarket(MarketCatalogue catalogue) {
        try {
            String marketId = catalogue.marketId;
            String marketName = catalogue.marketName;
            String eventId = catalogue.event.id;
            if (StringUtils.isAnyBlank(marketId, eventId, marketName)) return null;

            String eventName = catalogue.event.name;
            MarketPivotInfo info = getMarketInfo(marketName, eventName);
            if (info == null) return null;

            return new MarketData(marketId, eventId, info.getPivotType(), info.getTimeType(), info.getGoal(), info.getEventType());
        } catch (Exception ex) {
            logger.error("Failed to parse market: ", ex);
        }

        return null;
    }

    @Override
    public List<SportbookRecord> parseMarketBook(MarketData marketData, EventData event, ILiveState liveState,
                                                 List<RunnerInfo> infoList) {
        try {
            SoccerSportbookRecord base = constructBaseRecord(marketData, event);

            if (StringUtils.isNotBlank(marketData.getEventType())) {
                base.setEventId(base.getEventId() + ";" + marketData.getId());
                base.setLeague(base.getLeague() +  " - " + marketData.getEventType());
            }

            if (OddType.LIVE.equals(event.getOddType())) {
                updateLiveState(base, liveState);
            } else {
                base.setEventSegment(SoccerSegment.PENDING);
            }

            List<SportbookRecord> records = parseByLBType(base, marketData, infoList);
            records.removeIf(record  -> record == null || (record.getRateOver() <= 0 && record.getRateUnder() <= 0 && record.getRateEqual() <= 0));
            return records;
        } catch (Exception ex) {
            logger.error("Failed to parse market book: ", ex);
        }

        return Collections.emptyList();
    }

    @Override
    public ILiveState parseLiveState(JsonNode event) {
        try {
            String eventId = event.get("eventId").asText();
            JsonNode scoreNode = event.get("score");
            if (scoreNode == null) return null;
            if (!scoreNode.has("home") || !scoreNode.has("away") ||
                !event.has("timeElapsed") || !event.has("matchStatus")) return null;

            JsonNode homeNode = scoreNode.get("home");
            JsonNode awayNode = scoreNode.get("away");
            if (!homeNode.has("score") && !awayNode.has("score")) return null;

            int hostScore = homeNode.get("score").asInt();
            int guestScore = awayNode.get("score").asInt();
            SoccerSegment segment = getSoccerSegment(event.get("matchStatus").asText());
            if (segment == null) return null;

            int timeElapsed = (segment == SoccerSegment.HALF_BREAK) ? 45 : event.get("timeElapsed").asInt();

            String matchStatus = event.get("matchStatus").asText();
            return new SoccerLiveState(eventId, hostScore, guestScore ,segment, timeElapsed, "Finished".equals(matchStatus));
        } catch (Exception ex) {
            logger.error("Failed to parse live state: ", ex);
        }

        return null;
    }

    private SoccerSegment getSoccerSegment(String matchStatus) {
        if (matchStatus == null) return null;
        switch (matchStatus) {
            case "KickOff":
                return SoccerSegment.FIRST_HALF;
            case "FirstHalfEnd":
                return SoccerSegment.HALF_BREAK;
            case "SecondHalfKickOff":
                return SoccerSegment.SECOND_HALF;
            case "Finished":
                return SoccerSegment.FINISHED;
            default:
                return null;
        }
    }

    private List<SportbookRecord> parseByLBType(SoccerSportbookRecord base, MarketData marketData, List<RunnerInfo> infoList) {
        List<SportbookRecord> result = new ArrayList<>();
        List<SoccerSportbookRecord> backResult = parseWithLbType(base, marketData, infoList, LBType.BACK);
        List<SoccerSportbookRecord> layResult = parseWithLbType(base, marketData, infoList, LBType.LAY);
        if (!backResult.isEmpty()) {
            result.addAll(backResult);
            if (!layResult.isEmpty() && layResult.size() == backResult.size()) {
                result.addAll(layResult);
            }
        }

        return result;
    }

    private List<SoccerSportbookRecord> parseWithLbType(SoccerSportbookRecord base, MarketData marketData, List<RunnerInfo> infoList, LBType lbType) {
        switch (marketData.getType()) {
            case HDP:
                return parseHDP(base, infoList, marketData, lbType);
            case TOTAL:
                if (infoList.size() > 2) {
                    return parseTotalGoalLines(base, infoList, marketData, lbType);
                } else {
                    SoccerSportbookRecord record = parseTotal(base, infoList, marketData, lbType);
                    List<SoccerSportbookRecord> result = new ArrayList<>();
                    if (record != null) result.add(record);

                    return result;
                }

            case ONE_TWO:
                SoccerSportbookRecord record = parseOneTwo(base, infoList, marketData, lbType);
                if (record != null) {
                    List<SoccerSportbookRecord> result = new ArrayList<>();
                    result.add(record);

                    return result;
                }

                return Collections.emptyList();
        }

        return Collections.emptyList();
    }

    private void updateLiveState(SoccerSportbookRecord base, ILiveState liveState) {
        if (liveState instanceof SoccerLiveState) {
            SoccerLiveState soccerLiveState = (SoccerLiveState) liveState;
            base.setEventDuration(soccerLiveState.getDuration());
            base.setGuestPoint(soccerLiveState.getGuestScore());
            base.setHostPoint(soccerLiveState.getHostScore());
            base.setEventSegment(soccerLiveState.getSegment());
        } else {
            base.setEventSegment(null);
        }
    }

    private List<SoccerSportbookRecord> parseHDP(SoccerSportbookRecord base, List<RunnerInfo> infoList,
                                                 MarketData marketData, LBType lbType) {
        List<SoccerSportbookRecord> records = new ArrayList<>();
        Map<Double, List<RunnerInfo>> runnerListMap = groupRunner(infoList, PivotType.HDP);
        for (Map.Entry<Double, List<RunnerInfo>> entry : runnerListMap.entrySet()) {
            SoccerSportbookRecord record = generateRecordFromRunner(base, entry.getValue(), marketData, PivotType.HDP, lbType);
            if (record == null) continue;

            record.setLbType(lbType);
            Double hostPivot = entry.getKey();
            Pair<PivotBias, Double> pivotInfo = getPivotInfo(hostPivot);
            record.setPivotBias(pivotInfo.getLeft());
            record.setPivotValue(pivotInfo.getRight());
            record.setRateEqualUid("pivotInRunner");
            records.add(record);
        }

        return records;
    }

    private SoccerSportbookRecord parseTotal(SoccerSportbookRecord base, List<RunnerInfo> infoList,
                                             MarketData marketData, LBType lbType) {
        SoccerSportbookRecord record = generateRecordFromRunner(base, infoList, marketData, PivotType.TOTAL, lbType);
        if (record == null) return null;

        record.setLbType(lbType);
        record.setPivotBias(PivotBias.NEUTRAL);
        return record;
    }

    private List<SoccerSportbookRecord> parseTotalGoalLines(SoccerSportbookRecord base, List<RunnerInfo> infoList,
                                                            MarketData marketData, LBType lbType) {
        List<SoccerSportbookRecord> records = new ArrayList<>();
        Map<Double, List<RunnerInfo>> runnerListMap = groupRunner(infoList, PivotType.TOTAL);
        for (Map.Entry<Double, List<RunnerInfo>> entry : runnerListMap.entrySet()) {
            SoccerSportbookRecord record = generateRecordTotalGoalLines(base, entry.getValue(), marketData, lbType);
            if (record == null) continue;

            record.setLbType(lbType);
            record.setPivotBias(PivotBias.NEUTRAL);
            record.setPivotValue(entry.getKey());
            record.setRateEqualUid("pivotInRunner");
            records.add(record);
        }

        return records;
    }

    private SoccerSportbookRecord parseOneTwo(SoccerSportbookRecord base, List<RunnerInfo> infoList,
                                              MarketData marketData, LBType lbType) {
        SoccerSportbookRecord record = generateRecordFromRunner(base, infoList, marketData, PivotType.ONE_TWO, lbType);
        if (record == null) return null;

        record.setLbType(lbType);
        record.setPivotValue(0.0);
        record.setPivotBias(PivotBias.NEUTRAL);
        record.setOddFormat(OddFormat.EU);

        return record;
    }

    private MarketPivotInfo getTeamGoalMarketInfo(String eventName, String marketName) {
        if (eventName == null) return null;

        String[] teams = eventName.split(" v | @ ");
        if (teams.length != 2) return null;
        for (int i = 0; i < teams.length; i++) {
            String regex = Pattern.quote(teams[i]) + " " + MarketNameTotalRegex.TOTAL.getRegex();
            if (marketName.matches(regex)) {
                Double goals = Double.parseDouble(marketName.replaceAll(regex, "$1"));
                String eventType = ((i == 0) ? "Home " : "Away ") + "Team Total Goals";
                return new MarketPivotInfo(PivotType.TOTAL, SoccerTimeType.FT, goals, eventType);
            }
        }
        logger.trace("Unknown market {} {}", eventName, marketName);
        return null;
    }

    private SoccerSportbookRecord generateRecordFromRunner(SoccerSportbookRecord base, List<RunnerInfo> infoList,
                                                           MarketData marketData, PivotType pivotType, LBType lbType) {
        try {
            SoccerSportbookRecord record = base.clone();
            int[] rateIndexes = PivotType.TOTAL.equals(pivotType) ? new int[]{1, 0} : new int[]{0, 1};

            //Parse rate over
            Pair<Double, String> overInfo = getRateInfo(infoList.get(rateIndexes[0]), lbType,
                marketData.getEventId(), marketData.getId(), pivotType, marketData.getPivotValue());
            if (overInfo == null) return null;

            record.setRateOver(overInfo.getLeft());
            record.setRateOverUid(overInfo.getRight());

            //Parse rate under
            Pair<Double, String> underInfo = getRateInfo(infoList.get(rateIndexes[1]), lbType,
                marketData.getEventId(), marketData.getId(), pivotType, marketData.getPivotValue());
            if (underInfo ==  null) return null;

            record.setRateUnder(underInfo.getLeft());
            record.setRateUnderUid(underInfo.getRight());

            //Parse rate equal
            if (PivotType.ONE_TWO.equals(pivotType) && infoList.size() == 3 && infoList.get(2) != null) {
                Pair<Double, String> equalInfo = getRateInfo(infoList.get(2), lbType, marketData.getEventId(), marketData.getId(),
                    pivotType, marketData.getPivotValue());
                if (equalInfo ==  null) return null;

                record.setRateEqual(equalInfo.getLeft());
                record.setRateEqualUid(equalInfo.getRight());
            }
            return record;
        } catch (Exception ex) {
            logger.error("Failed to generate record from runner: ", ex);
        }

        return null;
    }

    @Override
    protected String getRateId(String matchId, String marketId, RunnerInfo data, PivotType pivotType,
                               Double pivotValue) {
        if (PivotType.HDP.equals(pivotType) || (PivotType.TOTAL.equals(pivotType) && pivotValue == null)) {
            return String.format("%s&%s&%s&%s", matchId, marketId, data.getSelectionId(), data.getHandicap());
        }

        if (PivotType.ONE_TWO.equals(pivotType)) {
            return String.format("%s&%s&%s", matchId, marketId, data.getSelectionId());
        }

        return String.format("%s&%s&%s&%s", matchId, marketId, data.getSelectionId(), pivotValue);
    }

    private SoccerSportbookRecord generateRecordTotalGoalLines(SoccerSportbookRecord base, List<RunnerInfo> infoList,
                                                               MarketData marketData, LBType lbType) {
        try {
            SoccerSportbookRecord record = base.clone();
            //Parse rate over
            Pair<Double, String> overInfo = getRateInfo(infoList.get(1), lbType,
                marketData.getEventId(), marketData.getId(), PivotType.TOTAL, null);
            if (overInfo == null) return null;

            record.setRateOver(overInfo.getLeft());
            record.setRateOverUid(overInfo.getRight());

            //Parse rate under
            Pair<Double, String> underInfo = getRateInfo(infoList.get(0), lbType,
                marketData.getEventId(), marketData.getId(), PivotType.TOTAL, null);
            if (underInfo ==  null) return null;

            record.setRateUnder(underInfo.getLeft());
            record.setRateUnderUid(underInfo.getRight());

            return record;
        } catch (Exception ex) {
            logger.error("Failed to generate record from runner: ", ex);
        }

        return null;
    }

    /**
     * Group Runners by pair for HDP and TOTAL
     *
     * @param runnerInfoList    List of runner info
     * @param pivotType         Pivot type used to identify runner info for over or under
     * @return  Map of Host Pivot and list combine 2 item of RunnerInfo
     */
    @Override
    protected Map<Double, List<RunnerInfo>> groupRunner(List<RunnerInfo> runnerInfoList, PivotType pivotType) {
        Map<Double, List<RunnerInfo>> map = new HashMap<>();
        List<List<RunnerInfo>> partitionedList = Lists.partition(runnerInfoList, 2);
        for (List<RunnerInfo> itemList : partitionedList) {
            if (itemList.size() < 2) continue;

            RunnerInfo over = itemList.get(PivotType.TOTAL.equals(pivotType) ? 1 : 0);
            Double hostPivot = over.getHandicap();
            if (PivotType.TOTAL.equals(pivotType) && (hostPivot * 10) % 10 == 5) continue; //Apply for Total Goal Lines

            map.put(hostPivot, itemList);
        }

        return map;
    }

    private MarketPivotInfo getMarketInfo(String marketName, String eventName) {
        double goals = 0.0;
        String eventType = StringUtils.EMPTY;
        if (marketName != null && marketName.startsWith("Match Odds"))
            return new MarketPivotInfo(PivotType.ONE_TWO, SoccerTimeType.FT, goals, eventType);

        if (marketName != null && marketName.startsWith("Half Time"))
            return new MarketPivotInfo(PivotType.ONE_TWO, SoccerTimeType.HT, goals, eventType);

        if (marketName != null && marketName.startsWith("Asian Handicap"))
            return new MarketPivotInfo(PivotType.HDP, SoccerTimeType.FT, goals, eventType);

//        if ("To Qualify".equals(marketName)) return new MarketPivotInfo(PivotType.HDP, SoccerTimeType.FT, goals, eventType);

        for (MarketNameTotalRegex regex : MarketNameTotalRegex.values()) {
            if (marketName != null && marketName.matches(regex.getRegex())) {
                goals = Double.parseDouble(marketName.replaceAll(regex.getRegex(), "$1"));
                return new MarketPivotInfo(regex.getPivotType(), regex.getSoccerTimeType(), goals, regex.getEventType());
            }
        }

        if (marketName != null && marketName.startsWith("Goal Lines"))
            return new MarketPivotInfo(PivotType.TOTAL, SoccerTimeType.FT, goals, eventType);

        return getTeamGoalMarketInfo(eventName, marketName);
    }

    protected SoccerSportbookRecord constructBaseRecord(MarketData marketData, EventData event) {
        try {
            SoccerSportbookRecord base = new SoccerSportbookRecord();
            base.setLeague(event.getLeague());
            base.setLeagueId(event.getLeague());
            base.setHost(event.getHost());
            base.setGuest(event.getGuest());
            base.setEventId(event.getId());
            base.setEventStartTime(event.getStartTime());
            base.setPivotValue(marketData.getPivotValue());
            base.setTimeType(marketData.getTimeType());
            base.setPivotType(marketData.getType());
            return base;
        } catch (Exception ex) {
            logger.error("Failed to construct base record: ", ex);
        }

        return null;
    }
}
