package jayeson.lib.betting.VNE.scrape.model;

import jayeson.lib.betting.api.datastructure.BasketballSportbookRecord;
import jayeson.lib.betting.api.datastructure.SoccerSportbookRecord;
import jayeson.lib.betting.api.datastructure.SportbookRecord;
import jayeson.lib.betting.api.datastructure.TennisSportbookRecord;
import jayeson.lib.betting.VNE.scrape.utils.BetFairScrapeUtility;
import jayeson.lib.feed.api.OddType;
import jayeson.lib.feed.api.SportType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

public class EventData {
    private String league;
    private String host;
    private String guest;
    private String id;
    private long startTime;
    private OddType oddType;
    private Integer marketCount;
    private static final Logger logger = LoggerFactory.getLogger("betfair.scrape");

    public EventData(String league, String host, String guest, String id, long startTime, Integer marketCount, boolean isLive) {
        this.league = league;
        this.host = host;
        this.guest = guest;
        this.id = id;
        this.startTime = startTime;
        this.marketCount = marketCount;
        this.oddType = BetFairScrapeUtility.getOddType(startTime, isLive);
        if(OddType.LIVE.equals(oddType) != isLive){
            logger.info("Event {} {} {} have startTime {} {} now but {}live",
                    id, host, guest, startTime, isLive ? ">" : "<", isLive ? "" : "non ");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EventData eventData = (EventData) o;
        return Objects.equals(id, eventData.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public String getLeague() {
        return league;
    }

    public void setLeague(String league) {
        this.league = league;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getGuest() {
        return guest;
    }

    public void setGuest(String guest) {
        this.guest = guest;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public OddType getOddType() {
        return oddType;
    }

    public Integer getMarketCount() {
        return marketCount;
    }

    public void setOddType(OddType oddType) {
        this.oddType = oddType;
    }

    public void setMarketCount(Integer marketCount) {
        this.marketCount = marketCount;
    }

    public SportbookRecord toRecord(SportType sportType){
        SportbookRecord record = null;
        switch (sportType){
            case SOCCER:
                record = new SoccerSportbookRecord();
                break;
            case BASKETBALL:
                record = new BasketballSportbookRecord();
                break;
            case TENNIS:
                record = new TennisSportbookRecord();
                break;
        }
        if(record != null){
            record.setEventId(id);
            record.setEventStartTime(startTime);
        }
        return record;
    }
}
