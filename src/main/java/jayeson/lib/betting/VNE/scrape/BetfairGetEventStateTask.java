package jayeson.lib.betting.VNE.scrape;

import jayeson.lib.betting.VNE.scrape.parser.GetEventRequestParserFactory;
import jayeson.lib.betting.api.actioncontext.ScrapeContext;
import jayeson.lib.betting.api.actionresults.ScrapeResult;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.api.task.ScrapTaskHandler;
import jayeson.lib.betting.api.task.TaskObservable;
import jayeson.lib.betting.VNE.json.BetfairGetEventParam;
import jayeson.lib.betting.VNE.scrape.model.DataTransfer;
import jayeson.lib.betting.VNE.scrape.parser.GetEventRequestParserFactory;
import jayeson.lib.betting.core.BettingAccount;
import jayeson.lib.betting.core.dagger.qualifier.TaskId;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.task.ScrapeTask;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;
import jayeson.lib.betting.core.utility.BettingUtility;

import javax.inject.Inject;
import java.util.List;
import java.util.concurrent.TimeUnit;

@TaskScope
public class BetfairGetEventStateTask extends ScrapeTask {

    private final GetLiveStateFragmentFactory getLiveStateFragmentFactory;
    private final GetEventRequestParserFactory parserFactory;

    @Inject
    public BetfairGetEventStateTask(ITaskExecutor executor, IResponseListener responseListener, AccountTaskInfo data,
                                    BettingAccount account, @TaskId String id, ScrapTaskHandler<ScrapeResult> handler,
                                    TaskObservable<AccountTaskInfo> observable, ScrapeContext context, ScrapeResult result,
                                    GetLiveStateFragmentFactory getLiveStateFragmentFactory, GetEventRequestParserFactory parserFactory) {
        super(executor, responseListener, data, account, id, handler, observable, context, result);
        this.getLiveStateFragmentFactory = getLiveStateFragmentFactory;
        this.parserFactory = parserFactory;
    }

    @Override
    protected void execute() {
        account.getScrapeLogger().info("START_GET_EVENT_STATE_TASK {}_{} ", context.getSportType(), context.getRequestTime());
        BetfairGetEventParam param = parserFactory.create().parse(context.getParamString());
        List<String> eventIds = param.getEventIds();
        account.getScrapeLogger().info("{} LAUNCH_GET_LIVE_STATE_FRAGMENT {}", context.getSportType(), BettingUtility.writeObjectToJson(eventIds));
        GetLiveStateFragment fragment = getLiveStateFragmentFactory.create(eventIds, new DataTransfer());
        executor.schedule(fragment, 50L, TimeUnit.MILLISECONDS);
    }

}
