package jayeson.lib.betting.VNE.scrape.model;

import com.google.common.collect.Lists;
import jayeson.lib.betting.api.datastructure.DeleteRecordInfo;
import jayeson.lib.betting.api.datastructure.SportbookRecord;
import jayeson.lib.feed.api.LBType;
import jayeson.lib.feed.api.OddType;
import org.apache.commons.lang3.StringUtils;


import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class DataTransfer {
    private Map<String, EventData> eventMap;
    private Map<String, MarketData> marketMap;
    private Map<String, ILiveState> stateMap;
    private Set<String> liveEventIds;
    private Map<OddType,List<SportbookRecord>> recordMap;
    private List<String> deleteIds;

    public DataTransfer(){
        eventMap = new ConcurrentHashMap<>();
        marketMap = new ConcurrentHashMap<>();
        stateMap = new ConcurrentHashMap<>();
        liveEventIds =  Collections.synchronizedSet(new HashSet<>());
        recordMap = new ConcurrentHashMap<>();
        deleteIds = new ArrayList<>();
    }

    public Set<String> getEventIds(){
        return eventMap.keySet();
    }

    public Set<String> getMarketIds(){
        return marketMap.keySet();
    }

    public Set<String> getLiveEventIds(){
        return liveEventIds;
    }

    public void addEventData(EventData eventData){
        eventMap.put(eventData.getId(), eventData);
    }

    public void addEventData(List<EventData> events){
        events.stream().forEach(event -> eventMap.put(event.getId(), event));
    }

    public void addLiveEventId(String eventId){
        liveEventIds.add(eventId);
    }

    public void addLiveEventId(Set<String> eventIds){
        liveEventIds.addAll(eventIds);
    }

    public void addLiveState(ILiveState liveState){
        stateMap.put(liveState.getEventId(), liveState);
    }

    public void addMarketData(MarketData marketData){
        marketMap.put(marketData.getId(), marketData);
    }

    public void addMarketData(List<MarketData> marketDataList){
        marketDataList.forEach(marketData -> marketMap.put(marketData.getId(), marketData));
    }

    public EventData getEventData(String eventId){
        return eventMap.computeIfAbsent(eventId, id -> null);
    }

    public List<EventData> getEventDataList(){
        return Lists.newArrayList(eventMap.values());
    }

    public ILiveState getLiveState(String eventId){
        return stateMap.get(eventId);
    }

    public MarketData getMarketData(String marketId){
        return marketMap.computeIfAbsent(marketId, id -> null);
    }

    public List<MarketData> getMarketDataList(){
        return Lists.newArrayList(marketMap.values());
    }

    public List<ILiveState> getLiveStates(){
        return Lists.newArrayList(stateMap.values());
    }

    public void addRecord(SportbookRecord record, OddType oddType){
        List<SportbookRecord> records = recordMap.computeIfAbsent(oddType, id -> new ArrayList<>());
        records.add(record);
    }

    public void addRecord(List<SportbookRecord> recordList, OddType oddType){
        List<SportbookRecord> records = recordMap.computeIfAbsent(oddType, id -> new ArrayList<>());
        records.addAll(recordList);
    }

    public List<SportbookRecord> getRecords(OddType oddType){
        return recordMap.computeIfAbsent(oddType, i-> new ArrayList<>());
    }

    public List<DeleteRecordInfo> getDeleteRecords(){
        return deleteIds.stream()
                .map(id -> new DeleteRecordInfo(id, StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY, LBType.BACK))
                .collect(Collectors.toList());
    }

    public void addDeleteIds(List<String> ids){
        deleteIds.addAll(ids);
    }

    public void addDeleteId(String id){
        deleteIds.add(id);
    }

    public boolean hasMarket(){
        return !marketMap.isEmpty();
    }

    public Set<String> getEventIdFromMarkets(){
        return marketMap.values().stream().map(MarketData::getEventId).collect(Collectors.toSet());
    }

    public List<EventData> getEventDataListByOddType(OddType oddType){
        return getEventDataList().stream().filter(event -> oddType.equals(event.getOddType())).collect(Collectors.toList());
    }
}
