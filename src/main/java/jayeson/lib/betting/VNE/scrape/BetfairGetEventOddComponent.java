package jayeson.lib.betting.VNE.scrape;

import dagger.BindsInstance;
import dagger.Subcomponent;
import jayeson.lib.betting.api.actioncontext.ScrapeContext;
import jayeson.lib.betting.api.actionresults.ScrapeResult;
import jayeson.lib.betting.core.dagger.taskscope.ITaskComponent;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;

@Subcomponent(modules = BetfairGetEventOddModule.class)
@TaskScope
public interface BetfairGetEventOddComponent extends ITaskComponent<ScrapeResult> {
    @Override
    BetfairGetEventOddTask getTask();

    @Subcomponent.Builder
    interface Builder extends BuilderContext<ScrapeResult, ScrapeContext> {

        BetfairGetEventOddComponent build();

        @Override @BindsInstance
        BuilderContext bindsContext(ScrapeContext context);

    }
}
