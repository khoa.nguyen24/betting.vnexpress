package jayeson.lib.betting.VNE.scrape;

import jayeson.lib.betting.VNE.scrape.model.DataTransfer;

import java.util.List;

public interface IGetEventFragment {
    GetEventFragment create(Boolean isLive, List<String> eventIds, DataTransfer dataTransfer);
}
