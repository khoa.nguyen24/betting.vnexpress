package jayeson.lib.betting.VNE.scrape.model;

import jayeson.lib.betting.api.datastructure.SoccerSportbookRecord;
import jayeson.lib.betting.api.datastructure.SportbookRecord;
import jayeson.lib.feed.soccer.SoccerSegment;

public class SoccerLiveState implements ILiveState{
    private String eventId;
    private int hostScore;
    private int guestScore;
    private SoccerSegment segment;
    private int duration;
    private boolean isFinished;

    public SoccerLiveState(String eventId, int hostScore, int guestScore, SoccerSegment segment, int duration, boolean isFinished) {
        this.eventId = eventId;
        this.hostScore = hostScore;
        this.guestScore = guestScore;
        this.segment = segment;
        this.duration = duration;
        this.isFinished = isFinished;
    }

    @Override
    public SportbookRecord toRecord(){
        SoccerSportbookRecord record = new SoccerSportbookRecord();
        record.setEventId(eventId);
        record.setHostPoint(hostScore);
        record.setGuestPoint(guestScore);
        record.setEventSegment(segment);
        record.setEventDuration(duration);
        return record;
    }

    @Override
    public SportbookRecord setState(SportbookRecord r){
        SoccerSportbookRecord record = (SoccerSportbookRecord) r;
        record.setEventId(eventId);
        record.setHostPoint(hostScore);
        record.setGuestPoint(guestScore);
        record.setEventSegment(segment);
        record.setEventDuration(duration);
        return record;
    }

    public int getHostScore() {
        return hostScore;
    }

    public int getGuestScore() {
        return guestScore;
    }

    public SoccerSegment getSegment() {
        return segment;
    }

    public int getDuration() {
        return duration;
    }

    @Override
    public String getEventId() {
        return eventId;
    }

    @Override
    public boolean isFinished(){
        return isFinished;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }
}
