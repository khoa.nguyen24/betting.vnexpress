package jayeson.lib.betting.VNE.scrape.parser;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import jayeson.lib.betting.VNE.json.marketcatalogue.MarketCatalogue;
import jayeson.lib.betting.VNE.scrape.model.*;
import jayeson.lib.betting.api.datastructure.SportbookRecord;
import jayeson.lib.betting.api.datastructure.TennisSportbookRecord;
import jayeson.lib.betting.VNE.scrape.model.*;
import jayeson.lib.feed.api.LBType;
import jayeson.lib.feed.api.OddFormat;
import jayeson.lib.feed.api.OddType;
import jayeson.lib.feed.api.twoside.PivotBias;
import jayeson.lib.feed.api.twoside.PivotType;
import jayeson.lib.feed.basketball.BasketballSegment;
import jayeson.lib.feed.tennis.TennisEventType;
import jayeson.lib.feed.tennis.TennisTimeType;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BetfairTennisParser extends BetfairAbstractParser {

    private static final Logger logger = LoggerFactory.getLogger("betfair.scrape");

    @Override
    public MarketData parseMarket(MarketCatalogue catalogue) {
        try {
            String marketId = catalogue.marketId;
            String marketName = catalogue.marketName;
            String eventId = catalogue.event.id;
            if (StringUtils.isAnyBlank(marketId, eventId, marketName)) return null;

            Pair<TennisTimeType, String> typeMap = getTypeMap(marketName);
            if (typeMap == null) return null;

            return new MarketData(marketId, eventId, PivotType.ONE_TWO, typeMap.getLeft(), 0.0, typeMap.getRight());
        } catch (Exception ex) {
            logger.error("Failed to parse market: ", ex);
        }

        return null;
    }

    @Override
    public List<SportbookRecord> parseMarketBook(MarketData marketData, EventData event, ILiveState liveState,
                                                 List<RunnerInfo> infoList) {
        try {
            TennisSportbookRecord base = constructBaseRecord(marketData, event);
            base.setEventType("game".equals(marketData.getEventType()) ? TennisEventType.GAME : TennisEventType.SET);
            if (OddType.LIVE.equals(event.getOddType())) {
                updateLiveState( base, liveState);
            } else {
                base.setEventSegment(BasketballSegment.PENDING);
            }

            List<SportbookRecord> records = parseByLBType(base, marketData, infoList);
            records.removeIf(record  -> record == null || (record.getRateOver() <= 0 && record.getRateUnder() <= 0));
            return records;
        } catch (Exception ex) {
            logger.error("Failed to parse market book: ", ex);
        }

        return null;
    }

    @Override
    public ILiveState parseLiveState(JsonNode event) {
        try {
            String eventId = event.get("eventId").asText();
            JsonNode scoreNode = event.get("score");
            if (scoreNode == null || !scoreNode.has("home") || !scoreNode.has("away")) return null;

            JsonNode homeNode = scoreNode.get("home");
            JsonNode awayNode = scoreNode.get("away");
            if (!homeNode.has("score") || !awayNode.has("score") ||
                !homeNode.has("games") || !homeNode.has("sets") ||
                !awayNode.has("games") || !awayNode.has("sets")) return null;

            boolean isHostAv = homeNode.get("score").isTextual() && "Av".equals(homeNode.get("score").asText());
            boolean isGuestAv = awayNode.get("score").isTextual() && "Av".equals(awayNode.get("score").asText());
            int hostPoint = isHostAv ? awayNode.get("score").asInt() : homeNode.get("score").asInt();
            int guestPoint = isGuestAv ? homeNode.get("score").asInt() : awayNode.get("score").asInt();
            int hostGameScore = homeNode.get("games").asInt();
            int hostSetScore = homeNode.get("sets").asInt();
            int guestGameScore = awayNode.get("games").asInt();
            int guestSetScore = awayNode.get("sets").asInt();
            int[] hostGame = new int[]{-1, -1, -1, -1, -1};
            int[] guestGame = new int[]{-1, -1, -1, -1, -1};
            if (homeNode.has("gameSequence") && awayNode.has("gameSequence")
                && homeNode.get("gameSequence").isArray() && awayNode.get("gameSequence").isArray()) {
                ArrayNode homeSequences = (ArrayNode) homeNode.get("gameSequence");
                ArrayNode awaySequences = (ArrayNode) awayNode.get("gameSequence");
                for (int i = 0; i < 5; i++) {
                    Pair<Integer, Integer> gamePair = getGamePair(homeSequences, awaySequences, i, hostGameScore, guestGameScore);
                    if (gamePair == null) break;

                    hostGame[i] = gamePair.getLeft();
                    guestGame[i] = gamePair.getRight();
                }
            }

            String matchStatus = event.get("matchStatus") == null ? null : event.get("matchStatus").asText();
            return new TennisLiveState(eventId, hostPoint, guestPoint, 0, hostSetScore, guestSetScore, hostGameScore, guestGameScore,
                    hostGame[0], guestGame[0], hostGame[1], guestGame[1], hostGame[2], guestGame[2],
                    hostGame[3], guestGame[3], hostGame[4], guestGame[4], isHostAv, isGuestAv, "Finished".equals(matchStatus));
        } catch (Exception ex) {
            logger.error("Failed to parse live state: ", ex);
        }

        return null;
    }

    private Pair<Integer, Integer> getGamePair(ArrayNode homeSeq, ArrayNode awaySeq, int index, int... defaultValues) {
        if (homeSeq.has(index) && awaySeq.has(index)) {
            return Pair.of(homeSeq.get(index).asInt(), awaySeq.get(index).asInt());
        }

        if (index == 0 || (homeSeq.has(index-1) && awaySeq.has(index-1))) {
            return Pair.of(defaultValues[0], defaultValues[1]);
        }

        return null;
    }

    private List<SportbookRecord> parseByLBType(TennisSportbookRecord base, MarketData marketData, List<RunnerInfo> infoList) {
        List<SportbookRecord> result = new ArrayList<>();
        for (LBType lbType : LB_TYPE_LIST) {
            switch (marketData.getType()) {
                case HDP:
                    result.addAll(parseHDP(base, infoList, marketData, lbType));
                    break;
                case TOTAL:
                    result.add(parseTotal(base, infoList, marketData, lbType));
                    break;
                case ONE_TWO:
                    result.add(parseOneTwo(base, infoList, marketData, lbType));
                    break;
            }
        }

        return result;
    }

    protected void updateLiveState(TennisSportbookRecord base, ILiveState liveState) {
        if (liveState instanceof TennisLiveState) {
            TennisLiveState tnLiveState = (TennisLiveState) liveState;
            base.setHostPoint(tnLiveState.getHostPoint());
            base.setGuestPoint(tnLiveState.getGuestPoint());

            base.setHostSetScore(tnLiveState.getHostSetScore());
            base.setHostGameScore(tnLiveState.getHostGameScore());
            base.setHostFirstSetGame(tnLiveState.getHostFirstSetGame());
            base.setHostSecondSetGame(tnLiveState.getHostSecondSetGame());
            base.setHostThirdSetGame(tnLiveState.getHostThirdSetGame());
            base.setHostFourthSetGame(tnLiveState.getHostFourthSetGame());
            base.setHostFifthSetGame(tnLiveState.getHostFifthSetGame());

            base.setGuestSetScore(tnLiveState.getGuestSetScore());
            base.setGuestGameScore(tnLiveState.getGuestGameScore());
            base.setGuestFirstSetGame(tnLiveState.getGuestFirstSetGame());
            base.setGuestSecondSetGame(tnLiveState.getGuestSecondSetGame());
            base.setGuestThirdSetGame(tnLiveState.getGuestThirdSetGame());
            base.setGuestFourthSetGame(tnLiveState.getGuestFourthSetGame());
            base.setGuestFifthSetGame(tnLiveState.getGuestFifthSetGame());

            base.setEventDuration(tnLiveState.getDuration());
            base.setHostAv(tnLiveState.isHostAv());
            base.setGuestAv(tnLiveState.isGuestAv());
        } else {
            base.setEventSegment(BasketballSegment.LIVE);
        }
    }

    private List<TennisSportbookRecord> parseHDP(TennisSportbookRecord base, List<RunnerInfo> infoList,
                                                 MarketData marketData, LBType lbType) {
        Map<Double, List<RunnerInfo>> runnerPairMap = groupRunner(infoList, PivotType.HDP);
        List<TennisSportbookRecord> records = new ArrayList<>();
        for (Map.Entry<Double, List<RunnerInfo>> entry : runnerPairMap.entrySet()){
            TennisSportbookRecord record = generateRecordFromRunner(base, entry.getValue(), marketData, PivotType.HDP, lbType);
            if (record == null) continue;

            record.setLbType(lbType);
            Pair<PivotBias, Double> pivotInfo = getPivotInfo(entry.getKey());
            record.setPivotBias(pivotInfo.getLeft());
            record.setPivotValue(pivotInfo.getRight());
            if (PivotBias.NEUTRAL.equals(pivotInfo.getLeft())) {
                record.setRateOverUid(record.getRateOverUid() + "&HDP&HK");
                record.setRateUnderUid(record.getRateUnderUid() + "&HDP&HK");
                record.setPivotType(PivotType.ONE_TWO);
            }

            records.add(record);
        }

        return records;
    }

    private TennisSportbookRecord parseTotal(TennisSportbookRecord base, List<RunnerInfo> infoList,
                                             MarketData marketData, LBType lbType) {
        TennisSportbookRecord record = generateRecordFromRunner(base, infoList, marketData, PivotType.TOTAL, lbType);
        if (record == null) return null;

        record.setLbType(lbType);
        record.setPivotBias(PivotBias.NEUTRAL);
        return record;
    }

    private TennisSportbookRecord parseOneTwo(TennisSportbookRecord base, List<RunnerInfo> infoList,
                                              MarketData marketData, LBType lbType) {
        TennisSportbookRecord record = generateRecordFromRunner(base, infoList, marketData, PivotType.ONE_TWO, lbType);
        if (record == null) return null;

        record.setLbType(lbType);
        record.setPivotValue(0.0);
        record.setPivotBias(PivotBias.NEUTRAL);
        record.setOddFormat(OddFormat.EU);
        return record;
    }

    private TennisSportbookRecord generateRecordFromRunner(TennisSportbookRecord base, List<RunnerInfo> infoList,
                                                           MarketData marketData, PivotType pivotType, LBType lbType) {
        try {
            TennisSportbookRecord record = base.clone();
            int[] rateIndexes = PivotType.TOTAL.equals(pivotType) ? new int[]{1, 0} : new int[]{0, 1};

            //Parse rate over
            Pair<Double, String> overInfo = getRateInfo(infoList.get(rateIndexes[0]), lbType,
                marketData.getEventId(), marketData.getId(), pivotType, marketData.getPivotValue());
            if (overInfo == null) return null;

            record.setRateOver(overInfo.getLeft());
            record.setRateOverUid(overInfo.getRight());

            //Parse rate under
            Pair<Double, String> underInfo = getRateInfo(infoList.get(rateIndexes[1]), lbType,
                marketData.getEventId(), marketData.getId(), pivotType, marketData.getPivotValue());
            if (underInfo == null) return null;

            record.setRateUnder(underInfo.getLeft());
            record.setRateUnderUid(underInfo.getRight());

            //Parse rate equal
            if (PivotType.ONE_TWO.equals(pivotType) && infoList.size() == 3 && infoList.get(2) != null) {
                Pair<Double, String> equalInfo = getRateInfo(infoList.get(2), lbType, marketData.getEventId(), marketData.getId(),
                    pivotType, marketData.getPivotValue());
                record.setRateEqual(equalInfo.getLeft());
                record.setRateEqualUid(equalInfo.getRight());
            }

            return record;

        } catch (Exception ex) {
            logger.error("Failed to generate record from runner: ", ex);
        }

        return null;
    }

    private Pair<TennisTimeType, String> getTypeMap(String marketName) {
        if ("Match Odds".equals(marketName)) {
            return Pair.of(new TennisTimeType(1, 0, 0), "set");
        }

        if (marketName.matches("Set (\\d) Winner")) {
            int setNum = Integer.parseInt(marketName.replaceAll("Set (\\d) Winner", "$1"));
            return Pair.of(new TennisTimeType(0, setNum,0), "game");
        }

        logger.trace("Unknown market {}", marketName);
        return null;
    }

    protected TennisSportbookRecord constructBaseRecord(MarketData marketData, EventData event) {
        try {
            TennisSportbookRecord base = new TennisSportbookRecord();
            base.setLeague(event.getLeague());
            base.setLeagueId(event.getLeague());
            base.setHost(event.getHost());
            base.setGuest(event.getGuest());
            base.setEventId(event.getId());
            base.setEventStartTime(event.getStartTime());
            base.setPivotValue(marketData.getPivotValue());
            base.setTimeType(marketData.getTimeType());
            base.setPivotType(marketData.getType());
            return base;
        } catch (Exception ex) {
            logger.error("Failed to construct base record: ", ex);
        }

        return null;
    }
}
