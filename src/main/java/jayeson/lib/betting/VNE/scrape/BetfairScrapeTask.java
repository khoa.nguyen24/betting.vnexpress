package jayeson.lib.betting.VNE.scrape;

import jayeson.lib.betting.VNE.scrape.model.DataTransfer;
import jayeson.lib.betting.api.actioncontext.ScrapeContext;
import jayeson.lib.betting.api.actionresults.ScrapeResult;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.api.task.ScrapTaskHandler;
import jayeson.lib.betting.api.task.TaskObservable;
import jayeson.lib.betting.core.BettingAccount;
import jayeson.lib.betting.core.dagger.qualifier.TaskId;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.task.ScrapeTask;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;

import javax.inject.Inject;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

@TaskScope
public class BetfairScrapeTask extends ScrapeTask {

    private final GetEventFragmentFactory getEventFragmentFactory;

    @Inject
    public BetfairScrapeTask(ITaskExecutor executor, IResponseListener responseListener, AccountTaskInfo data,
                             BettingAccount account, @TaskId String id, ScrapTaskHandler<ScrapeResult> handler,
                             TaskObservable<AccountTaskInfo> observable, ScrapeContext context, ScrapeResult result,
                             GetEventFragmentFactory getEventFragmentFactory) {
        super(executor, responseListener, data, account, id, handler, observable, context, result);
        this.getEventFragmentFactory = getEventFragmentFactory;
    }

    @Override
    protected void execute() {
        account.getScrapeLogger().info("START_SCRAPE_TASK {}_{} ", context.getSportType(), context.getRequestTime());
        account.getScrapeLogger().info("{} LAUNCH_GET_LIVE_EVENT_FRAGMENT", context.getSportType());
        GetEventFragment fragment = getEventFragmentFactory.create(true, Collections.emptyList(), new DataTransfer());
        executor.schedule(fragment, 50L, TimeUnit.MILLISECONDS);
    }
}
