package jayeson.lib.betting.VNE.scrape.utils;

import com.google.common.collect.ImmutableList;
import jayeson.lib.betting.api.actioncontext.ScrapeContext;
import jayeson.lib.betting.api.actionresults.ActionResultCode;
import jayeson.lib.betting.api.actionresults.DeltaScrapeResult;
import jayeson.lib.betting.api.actionresults.ScrapeResult;
import jayeson.lib.betting.api.datastructure.SportbookRecord;
import jayeson.lib.feed.api.OddType;
import jayeson.lib.feed.api.SportType;
import okhttp3.HttpUrl;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.TimeZone;

public class BetFairScrapeUtility {

    public static final int NUMBER_OF_ITEMS_PER_REQUEST = 40;
    public static final int MAX_REQUESTS_PER_BATCH = 100;

    public static final List<String> MARKET_PROJECTION = ImmutableList.of("EVENT","COMPETITION", "RUNNER_DESCRIPTION");

    static final List<String> MARKET_TYPE_CODE_SOCCER = ImmutableList.of("MATCH_ODDS", "HALF_TIME", "OVER_UNDER_05", "OVER_UNDER_10", "OVER_UNDER_15",
            "OVER_UNDER_20", "OVER_UNDER_25", "OVER_UNDER_30", "OVER_UNDER_35", "OVER_UNDER_40", "OVER_UNDER_45", "OVER_UNDER_50", "OVER_UNDER_55",
            "OVER_UNDER_60", "OVER_UNDER_65", "OVER_UNDER_70", "OVER_UNDER_75", "OVER_UNDER_80", "OVER_UNDER_85",
            "FIRST_HALF_GOALS_05", "FIRST_HALF_GOALS_10", "FIRST_HALF_GOALS_15", "FIRST_HALF_GOALS_20", "FIRST_HALF_GOALS_25", "FIRST_HALF_GOALS_30",
            "ASIAN_HANDICAP", "MONEY_LINE", "ALT_TOTAL_GOALS", "MATCH_ODDS_UNMANAGED", //"TO_QUALIFY",
            "OVER_UNDER_85_CORNR", "OVER_UNDER_105_CORNR",  "OVER_UNDER_25_CARDS", "OVER_UNDER_35_CARDS",
            "TEAM_A_OVER_UNDER_05", "TEAM_A_OVER_UNDER_15", "TEAM_A_OVER_UNDER_25", "TEAM_B_OVER_UNDER_05", "TEAM_B_OVER_UNDER_15", "TEAM_B_OVER_UNDER_25"
            );

    static final List<String> MARKET_TYPE_CODE_BASKETBALL = ImmutableList.of("MATCH_ODDS", "MONEY_LINE", "HANDICAP", "COMBINED_TOTAL");

    static final List<String> MARKET_TYPE_CODE_TENNIS = ImmutableList.of("MATCH_ODDS", "SET_WINNER");

    public static List<String> getMarketTypeCodes(SportType sportType){
        switch (sportType){
            case SOCCER:
                return MARKET_TYPE_CODE_SOCCER;
            case BASKETBALL:
                return MARKET_TYPE_CODE_BASKETBALL;
            case TENNIS:
                return MARKET_TYPE_CODE_TENNIS;
        }
        return Collections.emptyList();
    }

    public static HttpUrl getInPlayServiceApiUrl(String scheme, String domainName) {
        HttpUrl httpUrl = HttpUrl.get(domainName);
        String mainDomain = httpUrl.host().replace("www", "");

        return HttpUrl.get(scheme + "://ips" + mainDomain);
    }
    public static String getSportId(SportType sportType) {
        switch (sportType) {
            case SOCCER:
                return "1";
            case BASKETBALL:
                return "7522";
            case TENNIS:
                return "2";
        }
        return null;
    }

    public static OddType getOddType(SportbookRecord r) {
        if (r.getEventStartTime() < System.currentTimeMillis()) {
            return OddType.LIVE;
        }

        Calendar startTimeCalendar = Calendar.getInstance(TimeZone.getTimeZone("UTC-4:00"));
        startTimeCalendar.setTimeInMillis(r.getEventStartTime());

        Calendar todayCalendar = Calendar.getInstance(TimeZone.getTimeZone("UTC-4:00"));
        if (todayCalendar.get(Calendar.DAY_OF_YEAR) != startTimeCalendar.get(Calendar.DAY_OF_YEAR)) {
            return OddType.EARLY;
        } else {
            return OddType.TODAY;
        }
    }

    public static OddType getOddType(long eventStartTime, boolean isLive) {
         /*
         server at GMT+8
         statement day from 12h00 GMT+8 today to 12h00 GMT+8 next day
         so that:
         if current < 12h00 today, the match is TODAY when startTime < 12h00 today
         if current >= 12h00 today, the match is TODAY when startTime < 12h00 next day
         */
        if (isLive) {
            return OddType.LIVE;
        }

        long endToday = Timestamp.valueOf(LocalDate.now().atTime(LocalTime.NOON)).getTime();
        if(endToday < System.currentTimeMillis()) endToday += 24 * 60 * 60 * 1000;
        return endToday < eventStartTime ? OddType.EARLY : OddType.TODAY;
    }

    public static DeltaScrapeResult constructResult(ScrapeContext context, String accountId) {
        ScrapeContext scrapeContext = new ScrapeContext();

        scrapeContext.setRequestTime(context.getRequestTime());
        scrapeContext.setRequestType(context.getRequestType());
        scrapeContext.setSportType(context.getSportType());
        scrapeContext.setSportbook(context.getSportbook());
        scrapeContext.setServiceId(context.getServiceId());
        scrapeContext.setStartTimeExecuteRequest(context.getStartTimeExecuteRequest());
        scrapeContext.setTotalBatch(context.getTotalBatch());
        scrapeContext.setIndexInBatch(context.getIndexInBatch());
        scrapeContext.setParamString(context.getParamString());
        scrapeContext.setRequestStatus(context.getRequestStatus());
        scrapeContext.setOddType(OddType.LIVE);

        DeltaScrapeResult result = new DeltaScrapeResult();
        result.setCode(ActionResultCode.SUCCESSFUL);
        result.setContext(scrapeContext);
        result.setAccount(accountId);
        return result;
    }
}
