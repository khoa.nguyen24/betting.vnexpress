package jayeson.lib.betting.VNE.scrape;

import com.google.common.collect.Lists;
import jayeson.lib.betting.VNE.json.BetfairGetEventOddParam;
import jayeson.lib.betting.VNE.scrape.model.DataTransfer;
import jayeson.lib.betting.VNE.scrape.parser.GetEventOddRequestParser;
import jayeson.lib.betting.VNE.scrape.parser.GetEventOddRequestParserFactory;
import jayeson.lib.betting.VNE.scrape.utils.BetFairScrapeUtility;
import jayeson.lib.betting.api.actioncontext.ScrapeContext;
import jayeson.lib.betting.api.actionresults.ScrapeResult;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.api.task.ScrapTaskHandler;
import jayeson.lib.betting.api.task.TaskObservable;
import jayeson.lib.betting.VNE.scrape.parser.GetEventOddRequestParserFactory;
import jayeson.lib.betting.core.BettingAccount;
import jayeson.lib.betting.core.dagger.qualifier.TaskId;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.task.ScrapeTask;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;
import jayeson.lib.betting.core.utility.BettingUtility;

import javax.inject.Inject;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

@TaskScope
public class BetfairGetEventOddTask extends ScrapeTask {

    private final GetCatalogueFragmentFactory getCatalogueFragmentFactory;
    private final GetEventOddRequestParserFactory parserFactory;

    @Inject
    public BetfairGetEventOddTask(ITaskExecutor executor, IResponseListener responseListener, AccountTaskInfo data,
                                  BettingAccount account, @TaskId String id, ScrapTaskHandler<ScrapeResult> handler,
                                  TaskObservable<AccountTaskInfo> observable, ScrapeContext context, ScrapeResult result,
                                  GetCatalogueFragmentFactory getCatalogueFragmentFactory, GetEventOddRequestParserFactory parserFactory) {
        super(executor, responseListener, data, account, id, handler, observable, context, result);
        this.getCatalogueFragmentFactory = getCatalogueFragmentFactory;
        this.parserFactory = parserFactory;
    }

    @Override
    protected void execute() {
        account.getScrapeLogger().info("START_GET_EVENT_ODD_TASK {}_{} ", context.getSportType(), context.getRequestTime());
        GetEventOddRequestParser parser = parserFactory.create();
        BetfairGetEventOddParam param = parser.parse(context.getParamString());
        List<List<String>> ptnMarkets = Lists.partition(param.getMarketIds(), BetFairScrapeUtility.NUMBER_OF_ITEMS_PER_REQUEST);
        AtomicInteger fragmentLeft = new AtomicInteger(ptnMarkets.size());
        DataTransfer dataTransfer = new DataTransfer();
        for (int i = 0; i < ptnMarkets.size(); i++) {
            account.getScrapeLogger().info("{} LAUNCH_GET_CATALOGUE_FRAGMENT #{} {}",
                    context.getSportType(), i, BettingUtility.writeObjectToJson(ptnMarkets.get(i)));
            GetCatalogueFragment catalogueFragment = getCatalogueFragmentFactory.create(Collections.emptyList(), ptnMarkets.get(i), dataTransfer, fragmentLeft);
            executor.schedule(catalogueFragment, i * 100L, TimeUnit.MILLISECONDS);
        }
    }
}