package jayeson.lib.betting.VNE.scrape;

import com.google.auto.factory.AutoFactory;
import com.google.auto.factory.Provided;
import dagger.Lazy;
import jayeson.lib.betting.VNE.VNE;
import jayeson.lib.betting.VNE.VNEURIDefinition;
import jayeson.lib.betting.VNE.VNEUtility;
import jayeson.lib.betting.VNE.json.marketbook.MarketBook;
import jayeson.lib.betting.VNE.json.marketbook.MarketBookResponse;
import jayeson.lib.betting.VNE.json.marketbook.TicketRequest;
import jayeson.lib.betting.VNE.scrape.model.*;
import jayeson.lib.betting.api.actioncontext.ScrapeContext;
import jayeson.lib.betting.api.actionresults.ActionResultCode;
import jayeson.lib.betting.api.actionresults.DeltaScrapeResult;
import jayeson.lib.betting.api.actionresults.ScrapeResult;
import jayeson.lib.betting.api.datastructure.*;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.VNE.json.marketbook.Runner;
import jayeson.lib.betting.VNE.scrape.model.*;
import jayeson.lib.betting.VNE.scrape.parser.IBetfairParser;
import jayeson.lib.betting.VNE.scrape.utils.BetFairScrapeUtility;
import jayeson.lib.betting.core.exception.BuildRequestFailException;
import jayeson.lib.betting.core.http.IClient;
import jayeson.lib.betting.core.http.IResponse;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.http.request.BettingRequest;
import jayeson.lib.betting.core.http.request.JsonRequest;
import jayeson.lib.betting.core.task.AsyncHttpTaskFragment;
import jayeson.lib.betting.core.task.TaskFragmentListener;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;
import jayeson.lib.betting.core.utility.BettingUtility;
import jayeson.lib.betting.exception.BettingException;
import jayeson.lib.betting.exception.InvalidAttributeException;
import jayeson.lib.feed.api.OddType;
import jayeson.lib.feed.api.SportType;
import okhttp3.Call;
import okhttp3.Response;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@AutoFactory(
        implementing = IGetBookFragment.class
)
public class GetBookFragment extends AsyncHttpTaskFragment<ScrapeResult, MarketBookResponse[]> {

    private final BADRuntime badRuntime;
    private final VNE VNEAccount;
    private final List<String> marketIds;
    private final AtomicInteger fragmentLeft;
    private final AtomicInteger batchLeft;
    private final ScrapeContext context;
    private final SportType sportType;
    private final IBetfairParser parser;
    private final String LOG_PREFIX;
    private final Logger LOGGER;
    private static final int MAX_ODD_PUBLISHED = 75000;
    private final Lazy<MarketStreamFragment> marketStreamFragmentLazy;
    private DataTransfer dataTransfer;

    public GetBookFragment(@Provided AccountTaskInfo data, @Provided ITaskExecutor executor, @Provided IResponseListener responseListener,
                           @Provided VNE account, @Provided TaskFragmentListener<ScrapeResult> listener, @Provided ScrapeContext context,
                           @Provided ScrapeResult initialResult, @Provided IClient client, @Provided Lazy<MarketStreamFragment> marketStreamFragmentLazy,
                           @Provided BADRuntime badRuntime, @Provided IBetfairParser parser,
                           AtomicInteger batchLeft, AtomicInteger fragmentLeft, List<String> marketIds,
                           DataTransfer dataTransfer) {
        super(data, executor, responseListener, account, listener, initialResult, client);
        this.badRuntime = badRuntime;
        this.VNEAccount = account;
        this.setDef(VNEURIDefinition.getJsonRpcURI(MarketBookResponse[].class));
        this.marketStreamFragmentLazy = marketStreamFragmentLazy;
        this.context = context;
        this.parser = parser;
        this.batchLeft = batchLeft;
        this.fragmentLeft = fragmentLeft;
        this.marketIds = marketIds;
        this.dataTransfer = dataTransfer;
        this.sportType = context.getSportType();
        if(context.getRequestType() == ScrapeContext.RequestType.FETCH_DELTA_EVENT_ODD) {
            needProceed = false;
        }
        this.LOG_PREFIX = "GET_BOOK " + sportType.name() + StringUtils.SPACE + context.getRequestType().name();
        this.LOGGER = account.getScrapeLogger();
    }

    @Override
    public void execute() throws BettingException {
        LOGGER.info("START_GET_GET_BOOK_FRAGMENT {} {}", context.getSportType(), BettingUtility.writeObjectToJson(marketIds));
        BettingRequest request = buildRequest(this.marketIds);
        executeRequest(request, false);
    }

    private BettingRequest buildRequest(List<String> marketIds) throws BuildRequestFailException, InvalidAttributeException {
        if (VNEUtility.isNeedToResetBettingApi(client.host().url().toString())) {
            client.resetHost(VNEUtility.getBettingApiUrl(client.host().url().toString()));
        }

        // Prepare ticketRequestData
        TicketRequest ticketRequest = new TicketRequest(marketIds);
        List<TicketRequest> requests = new ArrayList<>();
        requests.add(ticketRequest);

        // Use JSON context
        return new JsonRequest.Builder()
                .def(this.getDef())
                .host(client.host())
                .data(requests)
                .addHeader("X-Authentication", this.badRuntime.getLoggedInUid())
                .addHeader("X-Application", VNEAccount.getAppKey())
                .build();
    }

    @Override
    public void onResponse(Call call, Response response) throws IOException {
        int code = response.code();
        String message = response.message();
        if (response.code() == 200) {
            super.onResponse(call, response);
        } else {
            LOGGER.error("{} RESPONSE_INVALID Code {} Message {} ids {}", LOG_PREFIX, code, message, this.marketIds);
        }
    }

    @Override
    public void consumeResponse(IResponse<MarketBookResponse[]> response) throws BettingException {
        try {
        LOGGER.info("{} RESPONSE_READY {}", LOG_PREFIX, response.getStatusLine().getCode());
        Pair<ActionResultCode, String> errorCodeMap = validateResponse(response);
        if (errorCodeMap.getKey().equals(ActionResultCode.FAILED)) {
            DeltaScrapeResult result = (DeltaScrapeResult) result();
            result.setContext(context);
            result.setAccount(account.getId());
            result.setCode(errorCodeMap.getKey());
            result.setMessage(errorCodeMap.getValue());
            needProceed = false;
            return;
        }

        account.getHttpLogger(this.accountTaskInfo().getAccountTaskType()).info("{} RESPONSE {}", LOG_PREFIX,
                BettingUtility.writeObjectToJson(response.content()));
        MarketBookResponse[] bookResponses = response.content();
        parseBooks(bookResponses[0].result);
        if (fragmentLeft.decrementAndGet() == 0) {
            LOGGER.info("{} BATCH #{} ALL_FRAGMENTS_COMPLETED", LOG_PREFIX, batchLeft.decrementAndGet());

            if (batchLeft.get() == 0) {
                publishRecords();
                executeNextFragment();
                LOGGER.info("{} ALL BATCH ALL_FRAGMENTS_COMPLETED", LOG_PREFIX);
            }
        }
        } catch (Exception ex){
            LOGGER.error("{} UNKNOWN ERROR ", LOG_PREFIX, ex);
        }
    }

    private void executeNextFragment(){
        if (ScrapeContext.RequestType.FETCH_DELTA.equals(context.getRequestType())){
            account.getScrapeLogger().info("START_FRAGMENT_MARKET_STREAM {}_{}_{} ", context.getSportType(),
                    context.getOddType(), context.getRequestTime());
            needProceed = true;
            executor.submit(marketStreamFragmentLazy.get());
        }
    }

    private void publishRecords() {
        DeltaScrapeResult result = BetFairScrapeUtility.constructResult(context, account.getId());
        List<DeleteRecordInfo> deletes = dataTransfer.getDeleteRecords();
        if (!deletes.isEmpty()){
            result.getDeleteRecords().put(OddType.LIVE, deletes);
            LOGGER.debug("{} Publish : {} delete", LOG_PREFIX, deletes.size());
        }

        for (OddType oddType : OddType.values()) {
            List<SportbookRecord> records = dataTransfer.getRecords(oddType);
            if(records.isEmpty()) continue;
            result.getUpsertRecords().put(oddType, records);
            LOGGER.debug("{} Publish {} : {} upsert", LOG_PREFIX, oddType, records.size());
        }
        listener().onFragmentCompleted(result, this);
    }

    private Pair<ActionResultCode, String> validateResponse(IResponse<MarketBookResponse[]> response) {
        int responseCode = response.getStatusLine().getCode();
        ActionResultCode resultCode = ActionResultCode.FAILED;
        if (responseCode != 200) {
            return Pair.of(resultCode, "The request failed with http code: " + responseCode);
        }

        if (response.content()[0].error != null) {
            return Pair.of(resultCode, "Failed to get market book for: " + BettingUtility.writeObjectToJson(response.content()[0].error));
        }

        if (response.content()[0].result == null || response.content()[0].result.size() == 0) {
            return Pair.of(resultCode, "Empty market book Responses");
        }

        return Pair.of(ActionResultCode.SUCCESSFUL, StringUtils.EMPTY);
    }

    private void parseBooks(List<MarketBook> marketResults) {
        if (marketResults.isEmpty()) return;

        String logPrefix = "FRAGMENT #" + fragmentLeft.get() + StringUtils.SPACE + LOG_PREFIX;
        try {
            LOGGER.trace("{} Received {} response", logPrefix, marketResults.size());
            for (MarketBook marketBook : marketResults) {
                String marketId = marketBook.marketId;
                List<Runner> runners = marketBook.runners;
                String marketStatus = marketBook.status;
                MarketData marketData = dataTransfer.getMarketData(marketId);

                if (marketData == null) {
                    LOGGER.trace("{} Cannot find market {} in cache", logPrefix, marketId);
                    if(ScrapeContext.RequestType.FETCH_DELTA_EVENT_ODD.equals(context.getRequestType())) dataTransfer.addDeleteId(marketId);
                    continue;
                }

                if (!"OPEN".equalsIgnoreCase(marketStatus) || runners == null){
                    LOGGER.trace("{} Skip parse market {} : {}_{}", logPrefix, marketId, marketStatus, runners == null);
                    if(ScrapeContext.RequestType.FETCH_DELTA_EVENT_ODD.equals(context.getRequestType())) dataTransfer.addDeleteId(marketId);
                    continue;
                }

                EventData event = dataTransfer.getEventData(marketData.getEventId());
                if (event == null) {
                    LOGGER.trace("{} Cannot find event {} in cache", logPrefix, marketData.getEventId());
                    if(ScrapeContext.RequestType.FETCH_DELTA_EVENT_ODD.equals(context.getRequestType())) dataTransfer.addDeleteId(marketId);
                    continue;
                }
                String eventId = event.getId();
                OddType oddType = event.getOddType();

                List<SportbookRecord> parsedBook = parseBook(marketBook, event, marketData);
                LOGGER.debug("{} Parsed {} records from market {}_{}", logPrefix, parsedBook.size(), marketId, oddType);
                if (parsedBook.isEmpty()){
                    if(ScrapeContext.RequestType.FETCH_DELTA_EVENT_ODD.equals(context.getRequestType())) dataTransfer.addDeleteId(marketId);
                    continue;
                }

                parsedBook.forEach(record -> LOGGER.debug("{} {}_{}_{} {}", logPrefix, eventId, marketId, oddType, logRecord(record)));
                dataTransfer.addRecord(parsedBook, oddType);
            }

        } catch (Exception ex) {
            LOGGER.error("{} Failed to parse ", logPrefix, ex);
        }
    }

    private String logRecord(SportbookRecord r){
        String eventType = r.getEventType() != null ? r.getEventType().name() : null;
        switch (context.getSportType()){
            case SOCCER:
                SoccerSportbookRecord scRecord = (SoccerSportbookRecord) r;
                return String.format("%s_%s_%s_%s %s_%s_%s_%s_%s_%s %s %s_%s_%s_%s_%s_%s state : %s_%s %s_%s %s_%s",
                        r.getLeague(), r.getHost(), r.getGuest(), r.getEventStartTime(),
                        r.getPivotType(), r.getPivotValue(), r.getPivotBias(), r.getTimeType(), eventType, r.getLbType(), r.getOddFormat(),
                        r.getRateOver(), r.getRateUnder(), r.getRateEqual(), r.getRateOverUid(), r.getRateUnderUid(), r.getRateEqualUid(),
                        scRecord.getEventSegment(), scRecord.getEventDuration(), scRecord.getHostPoint(), scRecord.getGuestPoint(),
                        scRecord.getHostExpelled(), scRecord.getGuestExpelled());
            case BASKETBALL:
                BasketballSportbookRecord bbRecord = (BasketballSportbookRecord) r;
                return String.format("%s_%s_%s_%s %s_%s_%s_%s_%s_%s %s %s_%s_%s_%s_%s_%s state : %s_%s %s_%s %s_%s %s_%s %s_%s %s_%s %s_%s",
                        r.getLeague(), r.getHost(), r.getGuest(), r.getEventStartTime(),
                        r.getPivotType(), r.getPivotValue(), r.getPivotBias(), r.getTimeType(), eventType, r.getLbType(), r.getOddFormat(),
                        r.getRateOver(), r.getRateUnder(), r.getRateEqual(), r.getRateOverUid(), r.getRateUnderUid(), r.getRateEqualUid(),
                        bbRecord.getEventSegment(), bbRecord.getEventDuration(), bbRecord.getHostPoint(), bbRecord.getGuestPoint(),
                        bbRecord.getHostFirstQuarterPoint(), bbRecord.getGuestFirstQuarterPoint(), bbRecord.getHostSecondQuarterPoint(), bbRecord.getGuestSecondQuarterPoint(),
                        bbRecord.getHostThirdQuarterPoint(), bbRecord.getGuestThirdQuarterPoint(), bbRecord.getHostFourthQuarterPoint(), bbRecord.getGuestFourthQuarterPoint(),
                        bbRecord.getHostOvertimePoint(), bbRecord.getGuestOvertimePoint());
            case TENNIS:
                TennisSportbookRecord tnRecord = (TennisSportbookRecord) r;
                return String.format("%s_%s_%s_%s %s_%s_%s_%s_%s_%s %s %s_%s_%s_%s_%s_%s state : %s_%s %s_%s %s_%s %s_%s %s_%s %s_%s %s_%s %s_%s",
                        r.getLeague(), r.getHost(), r.getGuest(), r.getEventStartTime(),
                        r.getPivotType(), r.getPivotValue(), r.getPivotBias(), r.getTimeType(), eventType, r.getLbType(), r.getOddFormat(),
                        r.getRateOver(), r.getRateUnder(), r.getRateEqual(), r.getRateOverUid(), r.getRateUnderUid(), r.getRateEqualUid(),
                        tnRecord.getEventSegment(), tnRecord.getEventDuration(), tnRecord.getHostPoint(), tnRecord.getGuestPoint(),
                        tnRecord.getHostGameScore(), tnRecord.getGuestGameScore(), tnRecord.getHostFirstSetGame(), tnRecord.getGuestFirstSetGame(),
                        tnRecord.getHostSecondSetGame(), tnRecord.getGuestSecondSetGame(), tnRecord.getHostThirdSetGame(), tnRecord.getGuestThirdSetGame(),
                        tnRecord.getHostFourthSetGame(), tnRecord.getGuestFourthSetGame(), tnRecord.getHostFifthSetGame(), tnRecord.getGuestFifthSetGame());
        }
        return StringUtils.EMPTY;
    }

    private List<SportbookRecord> parseBook(MarketBook book, EventData event, MarketData marketData) {
        String marketId = book.marketId;
        List<Runner> runners = book.runners;
        List<SportbookRecord> result = new ArrayList<>();
        try {
            ILiveState liveState = book.inplay ? dataTransfer.getLiveState(event.getId()) : null;
            result = generateParsedResult(marketData, event, liveState, runners);
        } catch (Exception ex) {
            LOGGER.error("{} Failed to parse for {}: ", LOG_PREFIX, marketId, ex);
        }

        return result;
    }

    private List<SportbookRecord> generateParsedResult(MarketData marketData, EventData event, ILiveState liveState, List<Runner> runners) {
        List<RunnerInfo> infoList = runners.stream()
            .map(runner -> parser.parseRunnerInfo(runner, marketData))
            .filter(Objects::nonNull)
            .collect(Collectors.toList());
        return parser.parseMarketBook(marketData, event, liveState, infoList);
    }

    @Override
    public void proceed(ScrapeResult currentResult) {
    }
}
