package jayeson.lib.betting.VNE.scrape.model;

import jayeson.lib.feed.api.TimeType;
import jayeson.lib.feed.api.twoside.PivotType;

public class MarketData {
    private String id;
    private String eventId;
    private PivotType type;

    private TimeType timeType;
    private Double pivotValue;
    private String eventType;

    public MarketData(String id, String matchId, PivotType type, TimeType timeType, Double pivotValue, String eventType) {
        this.id = id;
        this.eventId = matchId;
        this.type = type;
        this.timeType = timeType;
        this.pivotValue = pivotValue;
        this.eventType = eventType;
    }

    public String getId() {
        return id;
    }

    public String getEventId() {
        return eventId;
    }

    public PivotType getType() {
        return type;
    }

    public TimeType getTimeType() {
        return timeType;
    }

    public Double getPivotValue() {
        return pivotValue;
    }

    public String getEventType() {
        return eventType;
    }
}
