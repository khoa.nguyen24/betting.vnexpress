package jayeson.lib.betting.VNE.scrape.parser;

import com.fasterxml.jackson.databind.JsonNode;
import jayeson.lib.betting.VNE.json.eventdata.EventResult;
import jayeson.lib.betting.VNE.json.marketbook.Runner;
import jayeson.lib.betting.VNE.json.marketcatalogue.Event;
import jayeson.lib.betting.VNE.json.marketcatalogue.MarketCatalogue;
import jayeson.lib.betting.VNE.scrape.model.EventData;
import jayeson.lib.betting.VNE.scrape.model.RunnerInfo;
import jayeson.lib.betting.api.datastructure.SportbookRecord;

import jayeson.lib.betting.VNE.scrape.model.ILiveState;
import jayeson.lib.betting.VNE.scrape.model.MarketData;


import java.util.List;

public interface IBetfairParser {
    EventData parseEventData(EventResult result, boolean isLive);
    EventData parseEventData(Event event, String leagueName);
    MarketData parseMarket(MarketCatalogue catalogue);
    RunnerInfo parseRunnerInfo(Runner runner, MarketData marketData);
    List<SportbookRecord> parseMarketBook(MarketData marketData, EventData event, ILiveState liveState, List<RunnerInfo> runnerInfoList);
    ILiveState parseLiveState(JsonNode event);
}
