package jayeson.lib.betting.VNE.scrape;

import com.google.auto.factory.AutoFactory;
import com.google.auto.factory.Provided;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import jayeson.lib.betting.VNE.VNE;
import jayeson.lib.betting.VNE.VNEURIDefinition;
import jayeson.lib.betting.VNE.VNEUtility;
import jayeson.lib.betting.VNE.json.eventdata.EventResult;
import jayeson.lib.betting.VNE.json.eventdata.MatchRequest;
import jayeson.lib.betting.api.actioncontext.ScrapeContext;
import jayeson.lib.betting.api.actionresults.ActionResultCode;
import jayeson.lib.betting.api.actionresults.DeltaScrapeResult;
import jayeson.lib.betting.api.actionresults.ScrapeResult;
import jayeson.lib.betting.api.datastructure.BADRuntime;
import jayeson.lib.betting.api.datastructure.DeleteRecordInfo;
import jayeson.lib.betting.api.datastructure.SportbookRecord;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.VNE.json.eventdata.EventResponse;
import jayeson.lib.betting.VNE.scrape.model.DataTransfer;
import jayeson.lib.betting.VNE.scrape.model.EventData;
import jayeson.lib.betting.VNE.scrape.parser.IBetfairParser;
import jayeson.lib.betting.VNE.scrape.utils.BetFairScrapeUtility;
import jayeson.lib.betting.core.exception.BuildRequestFailException;
import jayeson.lib.betting.core.exception.UnknownResponseException;
import jayeson.lib.betting.core.http.IClient;
import jayeson.lib.betting.core.http.IResponse;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.http.request.BettingRequest;
import jayeson.lib.betting.core.http.request.JsonRequest;
import jayeson.lib.betting.core.task.AsyncHttpTaskFragment;
import jayeson.lib.betting.core.task.TaskFragmentListener;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;
import jayeson.lib.betting.core.utility.BettingUtility;
import jayeson.lib.betting.exception.BettingException;
import jayeson.lib.betting.exception.InvalidAttributeException;
import jayeson.lib.feed.api.OddType;
import okhttp3.Call;
import okhttp3.Response;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import javax.inject.Inject;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@AutoFactory(
        implementing = IGetEventFragment.class
)
public class GetEventFragment extends AsyncHttpTaskFragment<ScrapeResult, EventResponse[]> {

    private final BADRuntime badRuntime;
    private final VNE VNEAccount;
    private final String sportId;
    private final Boolean isLive;
    private final GetCatalogueFragmentFactory getCatalogueFragmentFactory;
    private final GetLiveStateFragmentFactory getLiveStateFragmentFactory;
    private final GetEventFragmentFactory getEventFragmentFactory;
    private final GetBookFragmentFactory getBookFragmentFactory;
    private final IBetfairParser parser;
    private final ScrapeContext context;
    private final String LOG_PREFIX;
    private final Logger LOGGER;
    private final DataTransfer dataTransfer;
    private final List<String> eventIds;

    @Inject
    public GetEventFragment(@Provided AccountTaskInfo data, @Provided ITaskExecutor executor,
                            @Provided IResponseListener responseListener, @Provided VNE account,
                            @Provided TaskFragmentListener<ScrapeResult> listener, @Provided ScrapeContext context,
                            @Provided ScrapeResult initialResult, @Provided IClient client, @Provided BADRuntime badRuntime,
                            @Provided GetLiveStateFragmentFactory getLiveStateFragmentFactory,
                            @Provided GetCatalogueFragmentFactory getCatalogueFragmentFactory,
                            @Provided GetEventFragmentFactory getEventFragmentFactory, @Provided IBetfairParser parser,
                            @Provided GetBookFragmentFactory getBookFragmentFactory,
                            Boolean isLive, List<String> eventIds, DataTransfer dataTransfer) {
        super(data, executor, responseListener, account, listener, initialResult, client);
        this.badRuntime = badRuntime;
        this.VNEAccount = account;
        this.context = context;
        this.sportId = BetFairScrapeUtility.getSportId(context.getSportType());
        this.isLive = isLive;
        this.dataTransfer = dataTransfer;
        this.eventIds = eventIds;
        this.getCatalogueFragmentFactory = getCatalogueFragmentFactory;
        this.getLiveStateFragmentFactory = getLiveStateFragmentFactory;
        this.getEventFragmentFactory = getEventFragmentFactory;
        this.getBookFragmentFactory = getBookFragmentFactory;
        this.parser = parser;
        needProceed = false;
        this.LOGGER = account.getScrapeLogger();
        this.LOG_PREFIX = "GET" + (isLive ? "" : "_NON") + "_LIVE_EVENT " + context.getSportType().name() + StringUtils.SPACE + context.getRequestType().name();
    }

    @Override
    public void execute() throws BettingException {
        LOGGER.info("START_GET{}_LIVE_EVENT_FRAGMENT {} {}", (isLive ? "" : "_NON"), context.getSportType(), BettingUtility.writeObjectToJson(eventIds));
        this.setDef(VNEURIDefinition.getJsonRpcURI(EventResponse[].class));
        BettingRequest request = buildRequest();
        executeRequest(request, true);
    }


    @Override
    public void onResponse(Call call, Response response) throws IOException {
        int code = response.code();
        String message = response.message();
        if (response.code() == 200) {
            super.onResponse(call, response);
        } else {
            LOGGER.error("{} RESPONSE_INVALID Code {} Message {}", LOG_PREFIX, code, message);
        }
    }

    private BettingRequest buildRequest() throws BuildRequestFailException, InvalidAttributeException {
        if (VNEUtility.isNeedToResetBettingApi(client.host().url().toString())) {
            client.resetHost(VNEUtility.getBettingApiUrl(client.host().url().toString()));
        }

        // Prepare matchRequestData
        List<String> typeIds = this.sportId != null ? Collections.singletonList(this.sportId) : Collections.emptyList();
        MatchRequest matchRequest = new MatchRequest(typeIds, isLive, eventIds, BetFairScrapeUtility.getMarketTypeCodes(context.getSportType()));
        List<MatchRequest> requests = new ArrayList<>();
        requests.add(matchRequest);

        account.getScrapeLogger().info("Execute get {} event with isLive={} eventIds={}",
                context.getSportType(), isLive, BettingUtility.writeObjectToJson(eventIds));
        // Use JSON context
        return new JsonRequest.Builder()
                .def(this.getDef())
                .host(client.host())
                .data(requests)
                .addHeader("X-Authentication", this.badRuntime.getLoggedInUid())
                .addHeader("X-Application", VNEAccount.getAppKey())
                .build();
    }

    @Override
    public void consumeResponse(IResponse<EventResponse[]> response) {
        List<EventData> eventDataList;
        try {
            account.getHttpLogger(this.accountTaskInfo().getAccountTaskType()).info("{} RESPONSE {}", LOG_PREFIX,
                    BettingUtility.writeObjectToJson(response.content()));
            EventResponse[] eventResponse = getMarketResponses(response);
            if (eventResponse == null) return;

            List<EventResult> eventResults = eventResponse[0].result;
            eventDataList = parseEventData(eventResults);
            if(!eventDataList.isEmpty() && isLive)
                dataTransfer.addLiveEventId(eventDataList.stream().map(EventData::getId).collect(Collectors.toSet()));
        } catch (Exception ex){
            LOGGER.error("{} Failed to parse ", LOG_PREFIX, ex);
        }
        executeNextFragment();
    }

    private EventResponse[] getMarketResponses(IResponse<EventResponse[]> response) throws UnknownResponseException {
        int code = response.getStatusLine().getCode();
        LOGGER.info("{} RESPONSE_READY {} Status {}", LOG_PREFIX, code, response.getStatusLine().getCode());
        if (code != 200) {
            LOGGER.error("{} Failed to get match for BetFair {}", LOG_PREFIX, response);
            result().setCode(ActionResultCode.FAILED);
            result().setMessage("The request failed with http code " + code);
            return null;
        }

        EventResponse[] responses = response.content();
        if (responses[0].error != null) {
            String jsonError = BettingUtility.writeObjectToJson(responses[0].error);
            LOGGER.error("{} Error: {} ", LOG_PREFIX, jsonError);
            throw new UnknownResponseException("Failed to get match", getDef(), "");
        }

        if (responses[0].result == null) {
            LOGGER.error("{} Cannot find any result in {}", LOG_PREFIX, response);
            result().setCode(ActionResultCode.FAILED);
            result().setMessage("Empty Match Responses");
            return null;
        }

        return responses;
    }

    private List<EventData> parseEventData(List<EventResult> results) {
        List<EventData> matchList;
        try {
            matchList = results.stream()
                .map(event -> parser.parseEventData(event, isLive))
                .filter(Objects::nonNull)
                .distinct()
                .collect(Collectors.toList());

            if(!matchList.isEmpty()){
                if(ScrapeContext.RequestType.FETCH_DELTA_EVENT_ODD.equals(context.getRequestType())){
                    for (EventData event : matchList){
                        EventData eventData = dataTransfer.getEventData(event.getId());
                        if(eventData != null) eventData.setOddType(event.getOddType());
                    }
                } else dataTransfer.addEventData(matchList);
            }
        } catch (Exception ex) {
            LOGGER.error("{} Failed to parse match data: ", LOG_PREFIX, ex);
        }

        return dataTransfer.getEventDataList();
    }

    @Override
    public void proceed(ScrapeResult scrapeResult) {}

    private void executeNextFragment() {
        logEventData();
        switch (context.getRequestType()){
            case FETCH_DELTA:
                if(isLive) executeGetNonLiveEvent(Collections.emptyList());
                else {
                    if(!dataTransfer.getLiveEventIds().isEmpty())   executeGetLiveState();
                    if(!dataTransfer.getEventIds().isEmpty())       executeGetCatalogue();
                }
                break;
            case FETCH_DELTA_EVENT:
                List<String> missingEvents = findMissingEvents();
                if(isLive && missingEvents.isEmpty())
                    notifyEventInfo();
                else if(isLive)
                    executeGetNonLiveEvent(missingEvents);
                else {
                    if(!missingEvents.isEmpty())
                        dataTransfer.addDeleteIds(missingEvents);
                    notifyEventInfo();
                }
                break;
            case FETCH_DELTA_EVENT_ODD:
                List<String> missingIds = findMissingEvents();
                if(isLive && !dataTransfer.getLiveEventIds().isEmpty())
                    executeGetLiveState();
                if(isLive && !missingIds.isEmpty())
                    executeGetNonLiveEvent(missingIds);
                else
                    executeGetBook();
                break;
        }
    }

    private void executeGetNonLiveEvent(List<String> eventIds){
        GetEventFragment fragment = getEventFragmentFactory.create(false, eventIds, dataTransfer);
        LOGGER.info("{} LAUNCH_GET_NON_LIVE_EVENT_FRAGMENT {}", context.getSportType(), BettingUtility.writeObjectToJson(eventIds));
        needProceed = true;
        executor.schedule(fragment, 50L, TimeUnit.MILLISECONDS);
    }

    private void executeGetLiveState() {
        List<String> eventIds = Lists.newArrayList(dataTransfer.getLiveEventIds());
        LOGGER.info("{} LAUNCH_GET_LIVE_STATE_FRAGMENT {}", context.getSportType(), BettingUtility.writeObjectToJson(eventIds));
        needProceed = true;
        GetLiveStateFragment fragment = getLiveStateFragmentFactory.create(eventIds, dataTransfer);
        executor.schedule(fragment, 50L, TimeUnit.MILLISECONDS);
    }

    private void executeGetCatalogue() {
        List<String> eventIds = Lists.newArrayList(dataTransfer.getEventIds());
        List<List<String>> ptnEvents = Lists.partition(eventIds, BetFairScrapeUtility.NUMBER_OF_ITEMS_PER_REQUEST);
        AtomicInteger fragmentLeft = new AtomicInteger(ptnEvents.size());
        for (int i = 0; i < ptnEvents.size(); i++) {
            LOGGER.info("{} LAUNCH_GET_CATALOGUE_FRAGMENT #{} size {}", context.getSportType(), i, ptnEvents.get(i).size());
            LOGGER.trace("{} Catalogue ids : {}", context.getSportType(), BettingUtility.writeObjectToJson(ptnEvents.get(i)));
            needProceed = true;
            GetCatalogueFragment catalogueFragment = getCatalogueFragmentFactory.create(ptnEvents.get(i), Collections.emptyList(), dataTransfer, fragmentLeft);
            executor.schedule(catalogueFragment, i*100L, TimeUnit.MILLISECONDS);
        }
    }

    private void executeGetBook() {
        List<String> marketIds = Lists.newArrayList(dataTransfer.getMarketIds());
        List<List<String>> ptnMarkets = Lists.partition(marketIds, BetFairScrapeUtility.NUMBER_OF_ITEMS_PER_REQUEST);
        List<List<List<String>>> ptnBookFragment = Lists.partition(ptnMarkets, BetFairScrapeUtility.MAX_REQUESTS_PER_BATCH);
        AtomicInteger batchTracker = new AtomicInteger(ptnBookFragment.size());
        for (int i = 0; i < ptnBookFragment.size(); i++) {
            LOGGER.info("{} EXECUTE BOOK FRAGMENT's BATCH #{}/{}", context.getSportType(), i+1, ptnBookFragment.size());
            List<List<String>> ptnIds = ptnBookFragment.get(i);
            AtomicInteger tracker = new AtomicInteger(ptnIds.size());
            for (int j = 0; j < ptnIds.size(); j++) {
                List<String> ids = ptnIds.get(j);
                LOGGER.info("{} LAUNCH_GET_BOOK_FRAGMENT #{} #{} size {}", context.getSportType(), i+1, j, ids.size());
                LOGGER.trace("{} Book ids : {}", context.getSportType(), BettingUtility.writeObjectToJson(ids));
                needProceed = true;
                GetBookFragment bookFragment = getBookFragmentFactory.create(batchTracker, tracker, ids, dataTransfer);
                executor.schedule(bookFragment, j * 40L, TimeUnit.MILLISECONDS);
            }
        }
    }

    private void notifyEventInfo(){
        List<DeleteRecordInfo> deletes = dataTransfer.getDeleteRecords();
        DeltaScrapeResult result = BetFairScrapeUtility.constructResult(context, account.getId());

        if (!deletes.isEmpty()){
            result.getDeleteRecords().put(OddType.LIVE, deletes);
            LOGGER.debug("{} Notify {} delete {}", LOG_PREFIX, deletes.size(), context.getSportType());
        }

        for (OddType oddType : OddType.values()){
            List<SportbookRecord> records = dataTransfer.getEventDataListByOddType(oddType).stream().map(r -> r.toRecord(context.getSportType())).collect(Collectors.toList());
            if(!records.isEmpty()){
                result.getUpsertRecords().put(oddType, records);
                LOGGER.debug("{} Notify {} upsert {}_{}", LOG_PREFIX, records.size(), context.getSportType(), oddType);
            }
        }

        listener().onFragmentCompleted(result, this);
    }

    private List<String> findMissingEvents(){
        Set<String> sent = new HashSet<>(eventIds);
        Set<String> receive = dataTransfer.getEventIds();
        return Lists.newArrayList(Sets.difference(sent, receive));
    }

    private void logEventData(){
        List<EventData> eventList = dataTransfer.getEventDataList();
        String events = eventList.stream()
                .map(event -> String.format("%s|%s|%s|%s|%s|%s", event.getId(), event.getOddType(), event.getLeague(), event.getHost(), event.getGuest(), event.getStartTime()))
                .collect(Collectors.joining(","));
        LOGGER.debug("{} Parsed events {} [{}]", LOG_PREFIX, eventList.size(), events);
        if (!dataTransfer.getDeleteRecords().isEmpty())
            LOGGER.debug("{} Can not find event info of {}", LOG_PREFIX, BettingUtility.writeObjectToJson(dataTransfer.getDeleteRecords().stream().map(DeleteRecordInfo::getEventId).collect(Collectors.toList())));
    }
}
