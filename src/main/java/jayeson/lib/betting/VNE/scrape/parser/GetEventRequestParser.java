package jayeson.lib.betting.VNE.scrape.parser;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.auto.factory.AutoFactory;
import com.google.auto.factory.Provided;
import jayeson.lib.betting.VNE.json.BetfairGetEventParam;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;

@AutoFactory
public class GetEventRequestParser {
    private ObjectMapper mapper;

    @Inject
    public GetEventRequestParser(@Provided @Named("BetfairParamObjectMapper")ObjectMapper mapper) {
        this.mapper = mapper;
    }

    public  BetfairGetEventParam parse(String requestString) {
        if (requestString != null) {
            try {
                return mapper.readValue(requestString, BetfairGetEventParam.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
