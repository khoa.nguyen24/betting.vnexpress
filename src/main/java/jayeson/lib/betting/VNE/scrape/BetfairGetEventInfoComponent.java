package jayeson.lib.betting.VNE.scrape;

import dagger.BindsInstance;
import dagger.Subcomponent;
import jayeson.lib.betting.api.actioncontext.ScrapeContext;
import jayeson.lib.betting.api.actionresults.ScrapeResult;
import jayeson.lib.betting.core.dagger.taskscope.ITaskComponent;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;

@Subcomponent(modules = BetfairGetEventInfoModule.class)
@TaskScope
public interface BetfairGetEventInfoComponent extends ITaskComponent<ScrapeResult> {
    @Override
    BetfairGetEventInfoTask getTask();

    @Subcomponent.Builder
    interface Builder extends BuilderContext<ScrapeResult, ScrapeContext> {

        BetfairGetEventInfoComponent build();

        @Override @BindsInstance
        BuilderContext bindsContext(ScrapeContext context);

    }
}
