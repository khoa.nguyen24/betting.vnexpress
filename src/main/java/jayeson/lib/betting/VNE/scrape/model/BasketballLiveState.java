package jayeson.lib.betting.VNE.scrape.model;

import jayeson.lib.betting.api.datastructure.BasketballSportbookRecord;
import jayeson.lib.betting.api.datastructure.SportbookRecord;
import jayeson.lib.feed.basketball.BasketballSegment;

public class BasketballLiveState implements ILiveState {
    private String eventId;
    private int hostScore;
    private int guestScore;
    private int hostFirstQuarterScore;
    private int guestFirstQuarterScore;
    private int hostSecondQuarterScore;
    private int guestSecondQuarterScore;
    private int hostThirdQuarterScore;
    private int guestThirdQuarterScore;
    private int hostFourthQuarterScore;
    private int guestFourthQuarterScore;
    private int hostOvertimeScore;
    private int guestOvertimeScore;
    private BasketballSegment basketballSegment;
    private int duration;
    private boolean isFinished;

    public BasketballLiveState(String eventId, int hostScore, int guestScore, int hostFirstQuarterScore, int guestFirstQuarterScore,
                               int hostSecondQuarterScore, int guestSecondQuarterScore, int hostThirdQuarterScore, int guestThirdQuarterScore,
                               int hostFourthQuarterScore, int guestFourthQuarterScore, int hostOvertimeScore, int guestOvertimeScore,
                               BasketballSegment basketballSegment, int duration, boolean isFinished) {
        this.eventId = eventId;
        this.hostScore = hostScore;
        this.guestScore = guestScore;
        this.hostFirstQuarterScore = hostFirstQuarterScore;
        this.guestFirstQuarterScore = guestFirstQuarterScore;
        this.hostSecondQuarterScore = hostSecondQuarterScore;
        this.guestSecondQuarterScore = guestSecondQuarterScore;
        this.hostThirdQuarterScore = hostThirdQuarterScore;
        this.guestThirdQuarterScore = guestThirdQuarterScore;
        this.hostFourthQuarterScore = hostFourthQuarterScore;
        this.guestFourthQuarterScore = guestFourthQuarterScore;
        this.hostOvertimeScore = hostOvertimeScore;
        this.guestOvertimeScore = guestOvertimeScore;
        this.basketballSegment = basketballSegment;
        this.duration = duration;
        this.isFinished = isFinished;
    }

    @Override
    public SportbookRecord toRecord() {
        BasketballSportbookRecord record = new BasketballSportbookRecord();
        record.setEventId(eventId);
        record.setEventSegment(basketballSegment);
        record.setEventDuration(duration);
        record.setHostPoint(hostScore);
        record.setGuestPoint(guestScore);
        record.setHostFirstQuarterPoint(hostFirstQuarterScore);
        record.setGuestFirstQuarterPoint(guestFirstQuarterScore);
        record.setHostSecondQuarterPoint(hostSecondQuarterScore);
        record.setGuestSecondQuarterPoint(guestSecondQuarterScore);
        record.setHostThirdQuarterPoint(hostThirdQuarterScore);
        record.setGuestThirdQuarterPoint(guestThirdQuarterScore);
        record.setHostFourthQuarterPoint(hostFourthQuarterScore);
        record.setGuestFourthQuarterPoint(guestFourthQuarterScore);
        record.setHostOvertimePoint(hostOvertimeScore);
        record.setGuestOvertimePoint(guestOvertimeScore);
        return record;
    }

    @Override
    public SportbookRecord setState(SportbookRecord r) {
        BasketballSportbookRecord record = (BasketballSportbookRecord) r;
        record.setEventId(eventId);
        record.setEventSegment(basketballSegment);
        record.setEventDuration(duration);
        record.setHostPoint(hostScore);
        record.setGuestPoint(guestScore);
        record.setHostFirstQuarterPoint(hostFirstQuarterScore);
        record.setGuestFirstQuarterPoint(guestFirstQuarterScore);
        record.setHostSecondQuarterPoint(hostSecondQuarterScore);
        record.setGuestSecondQuarterPoint(guestSecondQuarterScore);
        record.setHostThirdQuarterPoint(hostThirdQuarterScore);
        record.setGuestThirdQuarterPoint(guestThirdQuarterScore);
        record.setHostFourthQuarterPoint(hostFourthQuarterScore);
        record.setGuestFourthQuarterPoint(guestFourthQuarterScore);
        record.setHostOvertimePoint(hostOvertimeScore);
        record.setGuestOvertimePoint(guestOvertimeScore);
        return record;
    }

    public int getHostScore() {
        return hostScore;
    }

    public int getGuestScore() {
        return guestScore;
    }

    public int getHostFirstQuarterScore() {
        return hostFirstQuarterScore;
    }

    public int getGuestFirstQuarterScore() {
        return guestFirstQuarterScore;
    }

    public int getHostSecondQuarterScore() {
        return hostSecondQuarterScore;
    }

    public int getGuestSecondQuarterScore() {
        return guestSecondQuarterScore;
    }

    public int getHostThirdQuarterScore() {
        return hostThirdQuarterScore;
    }

    public int getGuestThirdQuarterScore() {
        return guestThirdQuarterScore;
    }

    public int getHostFourthQuarterScore() {
        return hostFourthQuarterScore;
    }

    public int getGuestFourthQuarterScore() {
        return guestFourthQuarterScore;
    }

    public int getHostOvertimeScore() {
        return hostOvertimeScore;
    }

    public int getGuestOvertimeScore() {
        return guestOvertimeScore;
    }

    public BasketballSegment getBasketballSegment() {
        return basketballSegment;
    }

    public int getDuration() {
        return duration;
    }

    @Override
    public String getEventId() {
        return eventId;
    }

    @Override
    public boolean isFinished(){
        return isFinished;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }
}
