package jayeson.lib.betting.VNE.scrape;

import dagger.Module;
import dagger.Provides;
import jayeson.lib.betting.VNE.scrape.parser.BetfairBasketBallParser;
import jayeson.lib.betting.VNE.scrape.parser.BetfairSoccerParser;
import jayeson.lib.betting.VNE.scrape.parser.IBetfairParser;
import jayeson.lib.betting.api.actioncontext.ScrapeContext;
import jayeson.lib.betting.api.actionresults.DeltaScrapeResult;
import jayeson.lib.betting.api.actionresults.ScrapeResult;
import jayeson.lib.betting.api.task.AccountTaskHandler;
import jayeson.lib.betting.api.task.ScrapTaskHandler;
import jayeson.lib.betting.VNE.scrape.parser.BetfairTennisParser;
import jayeson.lib.betting.core.dagger.taskscope.TaskModule;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;
import jayeson.lib.betting.core.task.TaskFragmentListener;

@Module(includes = {TaskModule.class})
public class BetfairGetEventInfoModule {

    @TaskScope
    @Provides
    TaskFragmentListener<ScrapeResult> provideTaskFragmentListener(BetfairGetEventInfoTask task) {
        return task;
    }

    @Provides
    ScrapTaskHandler<ScrapeResult> provideTaskHandler(AccountTaskHandler<ScrapeResult> handler){
        return (ScrapTaskHandler<ScrapeResult>) handler;
    }

    @Provides
    ScrapeResult providesResult() {return new DeltaScrapeResult();
    }

    @Provides
    IBetfairParser provideParser(ScrapeContext context) {
        switch (context.getSportType()) {
            case SOCCER:
                return new BetfairSoccerParser();
            case BASKETBALL:
                return new BetfairBasketBallParser();
            case TENNIS:
                return new BetfairTennisParser();
            default:
                return null;
        }
    }
}
