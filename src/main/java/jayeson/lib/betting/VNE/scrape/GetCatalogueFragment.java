package jayeson.lib.betting.VNE.scrape;

import com.google.auto.factory.AutoFactory;
import com.google.auto.factory.Provided;
import com.google.common.collect.Lists;
import jayeson.lib.betting.VNE.VNE;
import jayeson.lib.betting.VNE.VNEURIDefinition;
import jayeson.lib.betting.VNE.VNEUtility;
import jayeson.lib.betting.VNE.json.marketcatalogue.*;
import jayeson.lib.betting.VNE.scrape.model.DataTransfer;
import jayeson.lib.betting.VNE.scrape.model.EventData;
import jayeson.lib.betting.VNE.scrape.model.MarketData;
import jayeson.lib.betting.VNE.scrape.parser.IBetfairParser;
import jayeson.lib.betting.api.actioncontext.ScrapeContext;
import jayeson.lib.betting.api.actionresults.ActionResultCode;
import jayeson.lib.betting.api.actionresults.ScrapeResult;
import jayeson.lib.betting.api.datastructure.BADRuntime;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.VNE.json.marketcatalogue.*;
import jayeson.lib.betting.VNE.scrape.utils.BetFairScrapeUtility;
import jayeson.lib.betting.core.http.IClient;
import jayeson.lib.betting.core.http.IResponse;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.http.request.JsonRequest;
import jayeson.lib.betting.core.task.AsyncHttpTaskFragment;
import jayeson.lib.betting.core.task.TaskFragmentListener;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;
import jayeson.lib.betting.core.utility.BettingUtility;
import jayeson.lib.betting.exception.BettingException;
import jayeson.lib.feed.api.SportType;
import jayeson.lib.feed.api.twoside.PivotType;
import okhttp3.Call;
import okhttp3.Response;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

@AutoFactory(
        implementing = IGetCatalogueFragment.class
)
public class GetCatalogueFragment extends AsyncHttpTaskFragment<ScrapeResult, MarketCatalogueResponse> {

    private final BADRuntime badRuntime;
    private final VNE VNEAccount;
    private final IBetfairParser parser;
    private final AtomicInteger fragmentLeft;
    private final List<String> eventIds;
    private final List<String> marketIds;
    private final DataTransfer dataTransfer;
    private final GetBookFragmentFactory getBookFragmentFactory;
    private final GetEventFragmentFactory getEventFragmentFactory;
    private final ScrapeContext context;
    private final Logger LOGGER;
    private final String LOG_PREFIX;

    public GetCatalogueFragment(@Provided AccountTaskInfo data, @Provided ITaskExecutor executor, @Provided IResponseListener responseListener,
                                @Provided VNE account, @Provided TaskFragmentListener<ScrapeResult> listener, @Provided ScrapeContext context,
                                @Provided ScrapeResult initialResult, @Provided IClient client, @Provided BADRuntime badRuntime,
                                @Provided GetBookFragmentFactory getBookFragmentFactory, @Provided GetEventFragmentFactory getEventFragmentFactory,
                                @Provided IBetfairParser parser,
                                List<String> eventIds, List<String> marketIds, DataTransfer dataTransfer, AtomicInteger fragmentLeft) {
        super(data, executor, responseListener, account, listener, initialResult, client);
        this.badRuntime = badRuntime;
        this.VNEAccount = account;
        this.setDef(VNEURIDefinition.getJsonRpcURI(MarketCatalogueResponse.class));
        this.context = context;
        this.fragmentLeft = fragmentLeft;
        this.eventIds = eventIds;
        this.marketIds = marketIds;
        this.dataTransfer = dataTransfer;
        this.getBookFragmentFactory = getBookFragmentFactory;
        this.getEventFragmentFactory = getEventFragmentFactory;
        this.parser = parser;
        if(context.getRequestType() == ScrapeContext.RequestType.FETCH_DELTA_EVENT_ODD) {
            needProceed = false;
        }
        this.LOGGER = account.getScrapeLogger();
        this.LOG_PREFIX = "GET_CATALOGUE " + context.getSportType().name() + StringUtils.SPACE + context.getRequestType().name();
    }

    @Override
    public void execute() throws BettingException {
        LOGGER.info("START_GET_CATALOGUE_FRAGMENT {} eventIds={} marketIds={}", context.getSportType(), BettingUtility.writeObjectToJson(eventIds), BettingUtility.writeObjectToJson(marketIds));
        MarketCatalogueRequest payload = new MarketCatalogueRequest();
        payload.params = new MarketCatalogueParams();
        payload.params.filter = new MarketCatalogueFilter();
        payload.params.filter.eventIds = this.eventIds;
        if(!marketIds.isEmpty()) payload.params.filter.marketIds = this.marketIds;
        payload.params.filter.marketTypeCodes = BetFairScrapeUtility.getMarketTypeCodes(context.getSportType());
        payload.params.marketProjection = BetFairScrapeUtility.MARKET_PROJECTION;
        payload.params.maxResults = 1000;

        if (VNEUtility.isNeedToResetBettingApi(client.host().url().toString())) {
            client.resetHost(VNEUtility.getBettingApiUrl(client.host().url().toString()));
        }

        JsonRequest request = new JsonRequest.Builder()
            .def(this.getDef())
            .host(client.host())
            .data(payload)
            .addHeader("X-Authentication", this.badRuntime.getLoggedInUid())
            .addHeader("X-Application", VNEAccount.getAppKey())
            .build();

        executeRequest(request, true);
    }

    @Override
    public void proceed(ScrapeResult currentResult) {
    }

    @Override
    public void onResponse(Call call, Response response) throws IOException {
        int code = response.code();
        String message = response.message();
        if (response.code() == 200) {
            super.onResponse(call, response);
        } else {
            account.getScrapeLogger().error("FRAGMENT #{} {} RESPONSE_INVALID Code {} Message {} ids {}",
                fragmentLeft.getAndDecrement(), LOG_PREFIX, code, message, this.eventIds);
        }
    }

    @Override
    public void consumeResponse(IResponse<MarketCatalogueResponse> response) {
        try {
            int responseCode = response.getStatusLine().getCode();
            LOGGER.info("FRAGMENT #{} {} RESPONSE_READY Status {}", fragmentLeft.get(), LOG_PREFIX, responseCode);
            Pair<ActionResultCode, String> errorCodeMap = validateResponse(response);
            if (errorCodeMap.getKey().equals(ActionResultCode.FAILED)) {
                result().setCode(errorCodeMap.getKey());
                result().setMessage(errorCodeMap.getValue());
                LOGGER.info("FRAGMENT #{} {} RESPONSE_ERROR {}", fragmentLeft.get(), LOG_PREFIX, errorCodeMap.getValue());
                needProceed = false;
                return;
            }

            account.getHttpLogger(accountTaskInfo().getAccountTaskType()).info("{} RESPONSE {}", LOG_PREFIX, BettingUtility.writeObjectToJson(response.content()));
            MarketCatalogueResponse catalogueResponse = response.content();
            List<MarketData> marketDataList = parseCatalogues(catalogueResponse.result);
            if (!marketDataList.isEmpty()) {
                dataTransfer.addMarketData(marketDataList);
            }

            if (fragmentLeft.decrementAndGet() == 0) {
                LOGGER.info("{} ALL_FRAGMENTS_COMPLETED {}_{}", LOG_PREFIX, dataTransfer.getEventIds().size(), dataTransfer.getMarketIds().size());
                executeNextFragment();
            }
        } catch (Exception ex) {
            LOGGER.error("FRAGMENT #{} {} Failed to parse market catalogue: ", fragmentLeft.get(), LOG_PREFIX, ex);
        }
    }

    private Pair<ActionResultCode, String> validateResponse(IResponse<MarketCatalogueResponse> response) {
        int responseCode = response.getStatusLine().getCode();
        ActionResultCode resultCode = ActionResultCode.FAILED;
        if (responseCode != 200) {
            return Pair.of(resultCode, "The request failed with http code: " + responseCode);
        }

        if (response.content().error != null) {
            String jsonError = BettingUtility.writeObjectToJson(response.content().error);
            return Pair.of(resultCode, "Failed to get market catalogue: " + jsonError);
        }

        if (response.content().result == null || response.content().result.length == 0) {
            return Pair.of(resultCode, "Empty result for market catalogue");
        }

        return Pair.of(ActionResultCode.SUCCESSFUL, StringUtils.EMPTY);
    }

    private List<MarketData> parseCatalogues(MarketCatalogue[] catalogues) {
        List<MarketData> marketDataList = new ArrayList<>();
        try {
            for (MarketCatalogue catalogue : catalogues) {
                Competition competition = catalogue.competition;
                Event event = catalogue.event;
                if (competition == null || StringUtils.isBlank(competition.name) ||
                    event == null || StringUtils.isBlank(event.id)) continue;

                String eventId = event.id;
                EventData eventData = dataTransfer.getEventData(eventId);
                if (eventData == null){
                    if(ScrapeContext.RequestType.FETCH_DELTA_EVENT_ODD.equals(context.getRequestType())){
                        eventData = parser.parseEventData(event, competition.name);
                        dataTransfer.addEventData(eventData);
                    } else continue;
                } else eventData.setLeague(competition.name);

                MarketData marketData = parser.parseMarket(catalogue);
                if (marketData != null) {
                    marketDataList.add(marketData);
                }
                if(SportType.TENNIS.equals(context.getSportType()) && marketData != null && eventData != null
                        && PivotType.ONE_TWO.equals(marketData.getType()) && catalogue.runners.size() == 2){
                    eventData.setHost(catalogue.runners.get(0).runnerName);
                    eventData.setGuest(catalogue.runners.get(1).runnerName);
                }
            }
        } catch (Exception ex) {
            LOGGER.error("{} FRAGMENT #{} Failed to parse catalogue: ", LOG_PREFIX, fragmentLeft.get(), ex);
        }

        return marketDataList;
    }

    private void executeNextFragment() {
        logCatalogue();
        if(dataTransfer.hasMarket()){
            switch (context.getRequestType()){
                case FETCH_DELTA:
                    executeGetBook();
                    break;
                case FETCH_DELTA_EVENT_ODD:
                    executeGetLiveEvent();
                    break;
            }
        }
    }

    private void executeGetBook() {
        List<String> marketIds = Lists.newArrayList(dataTransfer.getMarketIds());
        List<List<String>> ptnMarkets = Lists.partition(marketIds, BetFairScrapeUtility.NUMBER_OF_ITEMS_PER_REQUEST);
        List<List<List<String>>> ptnBookFragment = Lists.partition(ptnMarkets, BetFairScrapeUtility.MAX_REQUESTS_PER_BATCH);
        AtomicInteger batchTracker = new AtomicInteger(ptnBookFragment.size());
        for (int i = 0; i < ptnBookFragment.size(); i++) {
            LOGGER.info("{} EXECUTE BOOK FRAGMENT's BATCH #{}/{}", context.getSportType(), i+1, ptnBookFragment.size());
            List<List<String>> ptnIds = ptnBookFragment.get(i);
            AtomicInteger tracker = new AtomicInteger(ptnIds.size());
            for (int j = 0; j < ptnIds.size(); j++) {
                List<String> ids = ptnIds.get(j);
                LOGGER.info("{} LAUNCH_GET_BOOK_FRAGMENT #{} #{} size {}", context.getSportType(), i+1, j, ids.size());
                LOGGER.trace("{} Book ids : {}", context.getSportType(), BettingUtility.writeObjectToJson(ids));
                GetBookFragment bookFragment = getBookFragmentFactory.create(batchTracker, tracker, ids, dataTransfer);
                needProceed = true;
                executor.schedule(bookFragment, j * 40L, TimeUnit.MILLISECONDS);
            }
        }
    }

    private void executeGetLiveEvent(){
        List<String> eventIds = Lists.newArrayList(dataTransfer.getEventIdFromMarkets());
        GetEventFragment fragment = getEventFragmentFactory.create(true, eventIds, dataTransfer);
        LOGGER.info("{} LAUNCH_GET_LIVE_EVENT_FRAGMENT {}", context.getSportType(), BettingUtility.writeObjectToJson(eventIds));
        needProceed = true;
        executor.schedule(fragment, 50L, TimeUnit.MILLISECONDS);
    }

    private void logCatalogue() {
        List<MarketData> marketDataList = dataTransfer.getMarketDataList();
        LOGGER.debug("{} Parsed {} markets :", LOG_PREFIX, marketDataList.size());
        for (MarketData marketData : marketDataList) {
            String marketString = String.format("%s %s %s %s %s %s", marketData.getEventId(), marketData.getId(),
                    marketData.getType().name(), marketData.getPivotValue(), marketData.getTimeType().name(), marketData.getEventType());
            LOGGER.trace("{} {}", LOG_PREFIX,  marketString);
        }
    }
}
