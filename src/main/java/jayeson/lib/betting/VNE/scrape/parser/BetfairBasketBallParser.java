package jayeson.lib.betting.VNE.scrape.parser;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import jayeson.lib.betting.VNE.scrape.model.*;
import jayeson.lib.betting.api.datastructure.BasketballSportbookRecord;
import jayeson.lib.betting.api.datastructure.SportbookRecord;
import jayeson.lib.betting.VNE.json.marketcatalogue.MarketCatalogue;
import jayeson.lib.betting.VNE.scrape.model.*;
import jayeson.lib.feed.api.LBType;
import jayeson.lib.feed.api.OddFormat;
import jayeson.lib.feed.api.OddType;
import jayeson.lib.feed.api.TimeType;
import jayeson.lib.feed.api.twoside.PivotBias;
import jayeson.lib.feed.api.twoside.PivotType;
import jayeson.lib.feed.basketball.BasketballEventType;
import jayeson.lib.feed.basketball.BasketballSegment;
import jayeson.lib.feed.basketball.BasketballTimeType;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class BetfairBasketBallParser extends BetfairAbstractParser {

    private static final Logger logger = LoggerFactory.getLogger("betfair.scrape");

    @Override
    public MarketData parseMarket(MarketCatalogue catalogue) {
        try {
            String marketId = catalogue.marketId;
            String marketName = catalogue.marketName;
            String eventId = catalogue.event.id;
            if (StringUtils.isAnyBlank(marketId, eventId, marketName)) return null;

            Pair<PivotType, TimeType> typePair = getTypePair(marketName);
            if (typePair == null) return null;

            return new MarketData(marketId, eventId, typePair.getLeft(), typePair.getRight(), 0.0, "normal");
        } catch (Exception ex) {
            logger.error("Failed to parse market: ", ex);
        }

        return null;
    }

    @Override
    public List<SportbookRecord> parseMarketBook(MarketData marketData, EventData event, ILiveState liveState,
                                                 List<RunnerInfo> infoList) {
        try {
            BasketballSportbookRecord base = constructBaseRecord(marketData, event);
            base.setEventType(BasketballEventType.NONE);
            if (OddType.LIVE.equals(event.getOddType())) {
                updateLiveState(base, liveState);
            } else {
                base.setEventSegment(BasketballSegment.PENDING);
            }

            List<SportbookRecord> records = parseByLBType(base, marketData, infoList);
            records.removeIf(record  -> record == null || (record.getRateOver() <= 0 && record.getRateUnder() <= 0));
            return records;
        } catch (Exception ex) {
            logger.error("Failed to parse market book: ", ex);
        }

        return Collections.emptyList();
    }

    @Override
    public ILiveState parseLiveState(JsonNode event) {
        try {
            String eventId = event.get("eventId").asText();
            JsonNode scoreNode = event.get("score");
            if (scoreNode == null || (!scoreNode.has("home") || !scoreNode.has("away") ||
                !event.has("timeElapsed") || !event.has("matchStatus"))) return null;

            int duration = event.get("timeElapsed").asInt();
            BasketballSegment segment = getBasketBallSegment(event.get("matchStatus").asText());
            if (segment == null) return null;

            JsonNode homeNode = scoreNode.get("home");
            JsonNode awayNode = scoreNode.get("away");
            if (!homeNode.has("score") && !awayNode.has("score")) return null;

            int hostScore = homeNode.get("score").asInt();
            int guestScore = awayNode.get("score").asInt();
            int[] hostScores = new int[]{-1, -1, -1, -1, -1};
            int[] guestScores = new int[]{-1, -1, -1, -1, -1};
            if (homeNode.has("quarterByQuarter") && awayNode.has("quarterByQuarter")
                && homeNode.get("quarterByQuarter").isArray() && awayNode.get("quarterByQuarter").isArray()) {
                ArrayNode homeQuarters = (ArrayNode) homeNode.get("quarterByQuarter");
                ArrayNode awayQuarters = (ArrayNode) awayNode.get("quarterByQuarter");
                for (int i = 0; i < 5; i++) {
                    hostScores[i] = homeQuarters.has(i) ? homeQuarters.get(i).asInt() : -1;
                    guestScores[i] = awayQuarters.has(i) ? awayQuarters.get(i).asInt() : -1;
                }
            }
            String matchStatus = event.get("matchStatus").asText();
            return new BasketballLiveState(eventId, hostScore, guestScore, hostScores[0], guestScores[0], hostScores[1], guestScores[1],
                    hostScores[2], guestScores[2], hostScores[3], guestScores[3], hostScores[4], guestScores[4], segment, duration, "Finished".equals(matchStatus));
        } catch (Exception ex) {
            logger.error("Failed to parse live state: ", ex);
        }

        return null;
    }

    private BasketballSegment getBasketBallSegment(String matchStatus) {
        if (matchStatus == null) return null;
        switch (matchStatus) {
            case "StartFirstQuarter":
                return BasketballSegment.QUARTER_1;
            case "StartSecondQuarter":
                return BasketballSegment.QUARTER_2;
            case "StartThirdQuarter":
                return BasketballSegment.QUARTER_3;
            case "StartFourthQuarter":
                return BasketballSegment.QUARTER_4;
            case "FirstHalfEnd":
                return BasketballSegment.HALF_BREAK;
            case "StopQuarter":
                return BasketballSegment.QUARTER_BREAK;
            default:
                return BasketballSegment.LIVE;
        }
    }

    private List<SportbookRecord> parseByLBType(BasketballSportbookRecord base, MarketData marketData, List<RunnerInfo> infoList) {
        List<SportbookRecord> result = new ArrayList<>();
        for (LBType lbType : LB_TYPE_LIST) {
            switch (marketData.getType()) {
                case HDP:
                    result.addAll(parseHDP(base, infoList, marketData, lbType));
                    break;
                case TOTAL:
                    result.addAll(parseTotal(base, infoList, marketData, lbType));
                    break;
                case ONE_TWO:
                    result.add(parseOneTwo(base, infoList, marketData, lbType));
                    break;
            }
        }

        return result;
    }

    private void updateLiveState(BasketballSportbookRecord base, ILiveState liveState) {
        if (liveState instanceof BasketballLiveState) {
            BasketballLiveState bbLiveState = (BasketballLiveState) liveState;
            base.setHostPoint(bbLiveState.getHostScore());
            base.setGuestPoint(bbLiveState.getGuestScore());

            base.setHostFirstQuarterPoint(bbLiveState.getHostFirstQuarterScore());
            base.setHostSecondQuarterPoint(bbLiveState.getHostSecondQuarterScore());
            base.setHostThirdQuarterPoint(bbLiveState.getHostThirdQuarterScore());
            base.setHostFourthQuarterPoint(bbLiveState.getHostFourthQuarterScore());
            base.setHostOvertimePoint(bbLiveState.getHostOvertimeScore());

            base.setGuestFirstQuarterPoint(bbLiveState.getGuestFirstQuarterScore());
            base.setGuestSecondQuarterPoint(bbLiveState.getGuestSecondQuarterScore());
            base.setGuestThirdQuarterPoint(bbLiveState.getGuestThirdQuarterScore());
            base.setGuestFourthQuarterPoint(bbLiveState.getGuestFourthQuarterScore());
            base.setGuestOvertimePoint(bbLiveState.getGuestOvertimeScore());

            base.setEventDuration(bbLiveState.getDuration());
            base.setEventSegment(bbLiveState.getBasketballSegment());
        } else {
            base.setEventSegment(null);
        }
    }

    private List<BasketballSportbookRecord> parseHDP(BasketballSportbookRecord base, List<RunnerInfo> runnerInfoList,
                                                     MarketData marketData, LBType lbType) {
        List<BasketballSportbookRecord> records = new ArrayList<>();
        Map<Double, List<RunnerInfo>> runnerPairMap = groupRunner(runnerInfoList, PivotType.HDP);
        for (Map.Entry<Double, List<RunnerInfo>> entry : runnerPairMap.entrySet()) {
            BasketballSportbookRecord record = generateRecordFromRunner(base, entry.getValue(), marketData, PivotType.HDP, lbType);
            if (record == null) continue;

            Pair<PivotBias, Double> pivotInfo = getPivotInfo(entry.getKey());
            record.setLbType(lbType);
            record.setPivotBias(pivotInfo.getLeft());
            record.setPivotValue(pivotInfo.getRight());
            record.setRateEqualUid("pivotInRunner");
            records.add(record);
        }

        return records;
    }

    private List<BasketballSportbookRecord> parseTotal(BasketballSportbookRecord base, List<RunnerInfo> runnerInfoList,
                                                       MarketData marketData, LBType lbType) {
        List<BasketballSportbookRecord> records = new ArrayList<>();
        Map<Double, List<RunnerInfo>> runnerPairMap = groupRunner(runnerInfoList, PivotType.TOTAL);
        for (Map.Entry<Double, List<RunnerInfo>> entry : runnerPairMap.entrySet()) {
            BasketballSportbookRecord record = generateRecordFromRunner(base, entry.getValue(), marketData, PivotType.TOTAL, lbType);
            if (record == null) continue;

            record.setLbType(lbType);
            record.setPivotBias(PivotBias.NEUTRAL);
            record.setPivotValue(entry.getKey());
            record.setRateEqualUid("pivotInRunner");
            records.add(record);
        }

        return records;
    }

    private BasketballSportbookRecord parseOneTwo(BasketballSportbookRecord base, List<RunnerInfo> infoList,
                                                  MarketData marketData, LBType lbType) {
        BasketballSportbookRecord record = generateRecordFromRunner(base, infoList, marketData, PivotType.ONE_TWO, lbType);
        if (record == null) return null;

        record.setLbType(lbType);
        record.setPivotValue(0.0);
        record.setPivotBias(PivotBias.NEUTRAL);
        record.setOddFormat(OddFormat.EU);
        return record;
    }

    private BasketballSportbookRecord generateRecordFromRunner(BasketballSportbookRecord base, List<RunnerInfo> infoList,
                                                               MarketData marketData, PivotType pivotType, LBType lbType) {
        try {
            BasketballSportbookRecord record = base.clone();
            int[] rateIndexes = PivotType.TOTAL.equals(pivotType) ? new int[]{1, 0} : new int[]{0, 1};

            //Parse rate over
            Pair<Double, String> overInfo = getRateInfo(infoList.get(rateIndexes[0]), lbType,
                marketData.getEventId(), marketData.getId(), pivotType, marketData.getPivotValue());
            if (overInfo == null) return null;

            record.setRateOver(overInfo.getLeft());
            record.setRateOverUid(overInfo.getRight());

            //Parse rate under
            Pair<Double, String> underInfo = getRateInfo(infoList.get(rateIndexes[1]), lbType,
                marketData.getEventId(), marketData.getId(), pivotType, marketData.getPivotValue());
            if (underInfo == null) return null;

            record.setRateUnder(underInfo.getLeft());
            record.setRateUnderUid(underInfo.getRight());

            //Parse rate equal
            if (PivotType.ONE_TWO.equals(pivotType) && infoList.size() == 3 && infoList.get(2) != null) {
                Pair<Double, String> equalInfo = getRateInfo(infoList.get(2), lbType, marketData.getEventId(), marketData.getId(),
                    pivotType, marketData.getPivotValue());
                record.setRateEqual(equalInfo.getLeft());
                record.setRateEqualUid(equalInfo.getRight());
            }

            return record;
        } catch (Exception ex) {
            logger.error("Failed to generate record from runner: ", ex);
        }

        return null;
    }

    @Override
    protected String getRateId(String matchId, String marketId, RunnerInfo data, PivotType pivotType, Double pivotValue) {
        if (PivotType.ONE_TWO.equals(pivotType)) {
            return String.format("%s&%s&%s", matchId, marketId, data.getSelectionId());
        }

        return String.format("%s&%s&%s&%s", matchId, marketId, data.getSelectionId(), data.getHandicap());
    }

    private Pair<PivotType, TimeType> getTypePair(String marketName) {
        switch (marketName) {
            case "Handicap":
                return Pair.of(PivotType.HDP, BasketballTimeType.FT);
            case "Total Points":
                return Pair.of(PivotType.TOTAL, BasketballTimeType.FT);
            case "Moneyline":
            case "Match Odds":
                return Pair.of(PivotType.ONE_TWO, BasketballTimeType.FT);
            default:
                logger.trace("Unknown market {}", marketName);
                return null;
        }
    }

    protected BasketballSportbookRecord constructBaseRecord(MarketData marketData, EventData event) {
        try {
            BasketballSportbookRecord base = new BasketballSportbookRecord();
            base.setLeague(event.getLeague());
            base.setLeagueId(event.getLeague());
            base.setHost(event.getHost());
            base.setGuest(event.getGuest());
            base.setEventId(event.getId());
            base.setEventStartTime(event.getStartTime());
            base.setPivotValue(marketData.getPivotValue());
            base.setTimeType(marketData.getTimeType());
            base.setPivotType(marketData.getType());
            return base;
        } catch (Exception ex) {
            logger.error("Failed to construct base record: ", ex);
        }

        return null;
    }
}
