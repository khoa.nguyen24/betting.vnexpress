package jayeson.lib.betting.VNE.bet;

import dagger.Subcomponent;
import jayeson.lib.betting.core.dagger.taskscope.BetMasterTaskComponent;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;

@TaskScope
@Subcomponent(modules = BetFairBetMasterModule.class)
public interface BetFairBetMasterComponent extends BetMasterTaskComponent {

    @Subcomponent.Builder
    interface Builder extends BetMasterTaskComponent.Builder {
        BetFairBetMasterComponent build();
    }
}