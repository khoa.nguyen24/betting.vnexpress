package jayeson.lib.betting.VNE.bet;

import dagger.Module;
import dagger.Provides;
import jayeson.lib.betting.VNE.runningbetlist.BetFairRunningBetListTask;
import jayeson.lib.betting.api.actioncontext.BetContext;
import jayeson.lib.betting.api.actionresults.*;
import jayeson.lib.betting.core.dagger.qualifier.NewlyPlacedBet;
import jayeson.lib.betting.core.dagger.qualifier.SubmitBetRequest;
import jayeson.lib.betting.core.dagger.taskscope.MasterTaskModule;
import jayeson.lib.betting.core.dagger.taskscope.PlaceBetModule;
import jayeson.lib.betting.core.dagger.taskscope.TaskModule;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;
import jayeson.lib.betting.core.task.BasicAccountTask;
import jayeson.lib.betting.core.task.GetMiniBetListTask;
import jayeson.lib.betting.core.task.TaskFragmentListener;

@Module(includes = {TaskModule.class, MasterTaskModule.class, PlaceBetModule.class})
public class BetFairBetMasterModule {

    @TaskScope @Provides @SubmitBetRequest
    BasicAccountTask<BetResult> provideSubmitPlaceBet(BetFairBetTask task) {
        return task;
    }

    @TaskScope @Provides
    TaskFragmentListener<BetResult> provideSubmitBetListener(BetFairBetTask task) {
        return task;
    }

    @TaskScope @Provides
    BetResult provideResult(ExchangeBetResult exchangeBetResult) {
        return exchangeBetResult;
    }

    @TaskScope @Provides
    ExchangeBetResult provideExchangeBetResult(BetContext context) {
        return new ExchangeBetResult(context.getBetTicket());
    }

    @TaskScope @Provides @NewlyPlacedBet
    GetMiniBetListTask<MiniBetResult> provideMiniBetList(BetFairRunningBetListTask task) {
        return task;
    }

    @TaskScope @Provides
    TaskFragmentListener<ListActionResult<MiniBetResult>> provideMiniBetListener(BetFairRunningBetListTask task) {
        return task;
    }

    @TaskScope @Provides
    ListActionResult<MiniBetResult> provideMiniBetResult () {
        return new ListMiniBetResult();
    }
}
