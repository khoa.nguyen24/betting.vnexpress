package jayeson.lib.betting.VNE.bet;

import com.google.common.base.Strings;
import jayeson.lib.betting.VNE.VNE;
import jayeson.lib.betting.VNE.VNEURIDefinition;
import jayeson.lib.betting.VNE.VNEUtility;
import jayeson.lib.betting.VNE.json.bet.BetInstructionReport;
import jayeson.lib.betting.VNE.json.bet.BetResponseData;
import jayeson.lib.betting.VNE.json.marketcatalogue.Description;
import jayeson.lib.betting.api.actioncontext.BetContext;
import jayeson.lib.betting.api.actionresults.BetResult;
import jayeson.lib.betting.api.actionresults.BetResultCode;
import jayeson.lib.betting.api.actionresults.BetTicketResult;
import jayeson.lib.betting.api.actionresults.ExchangeBetResult;
import jayeson.lib.betting.api.datastructure.BADRuntime;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.VNE.betticket.BetFairBetTicketResult;
import jayeson.lib.betting.VNE.json.bet.BetRequestData;
import jayeson.lib.betting.core.exception.BuildRequestFailException;
import jayeson.lib.betting.core.exception.UnknownResponseException;
import jayeson.lib.betting.core.http.IClient;
import jayeson.lib.betting.core.http.IResponse;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.http.request.BettingRequest;
import jayeson.lib.betting.core.http.request.JsonRequest;
import jayeson.lib.betting.core.task.AsyncHttpTaskFragment;
import jayeson.lib.betting.core.task.TaskFragmentListener;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;
import jayeson.lib.betting.core.utility.BettingUtility;
import jayeson.lib.betting.exception.BettingException;
import jayeson.lib.betting.exception.InvalidAttributeException;
import jayeson.lib.betting.exception.NewValueException;
import jayeson.lib.feed.api.twoside.PivotType;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

public class BetFairPlaceBet extends AsyncHttpTaskFragment<BetResult, BetResponseData[]> {
	private final VNE VNEAccount;
	private BetContext context;
	private final BADRuntime badRuntime;
	private Description marketDescription;

	@Inject
	public BetFairPlaceBet(AccountTaskInfo data, ITaskExecutor executor, IResponseListener responseListener,
						   VNE account, TaskFragmentListener<BetResult> listener, BetResult initialResult,
						   BetContext context, IClient client, BADRuntime badRuntime) {

		super(data, executor, responseListener, account, listener, initialResult, client);
		this.context = context;
		this.badRuntime = badRuntime;
		this.setDef(VNEURIDefinition.getJsonRpcURI(BetResponseData[].class));
		needProceed = false;
		this.VNEAccount = account;
	}

	@Override
	public void proceed(BetResult currentResult) { }

	@Override
	public void execute() throws BettingException {
		BettingRequest request = buildRequest(context.getBetTicket());
		executeRequest(request, false);
	}

	private BettingRequest buildRequest(BetTicketResult ticket) throws BuildRequestFailException, InvalidAttributeException, NewValueException {
		// Prepare payload
		if (!(ticket instanceof BetFairBetTicketResult)) {
			throw new InvalidAttributeException(String.format("Ticket should be in BetFairBetTicketResult, %s", ticket), "");
		}

		account.getCommonLogger().info("BetContext targetOdd: {}", context.getTargetOdd());
		BetRequestData betRequestData = new BetRequestData((BetFairBetTicketResult) ticket, context, marketDescription);
		List<BetRequestData> requests = new ArrayList<>();
		requests.add(betRequestData);

		if (VNEUtility.isNeedToResetBettingApi(client.host().url().toString())) {
			client.resetHost(VNEUtility.getBettingApiUrl(client.host().url().toString()));
		}

		// Use JSON context
		return new JsonRequest.Builder()
				.def(this.getDef())
				.host(client.host())
				.data(requests)
				.addHeader("X-Authentication", this.badRuntime.getLoggedInUid())
				.addHeader("X-Application", VNEAccount.getAppKey())
				.build();
	}

	@Override
	public void consumeResponse(IResponse<BetResponseData[]> response) throws BettingException {
		account.getCommonLogger().info("RESPONSE_READY_{}", BettingUtility.writeObjectToJson(response.getStatusLine()));
		BetResponseData data = response.content()[0];

		if(data.error != null
				|| data.result == null || data.result.instructionReports == null || data.result.instructionReports.isEmpty()) {
			account.getCommonLogger().error("Failed to place bet: {} ", BettingUtility.writeObjectToJson(data.error));
			throw new UnknownResponseException("Failed to place bet", getDef(), "");
		}

		// Only one offer is submitted
		if(data.result.instructionReports.size() != 1) {
			throw new UnknownResponseException("Returned offer is not 1!", getDef(), "");
		}

		if ("INSUFFICIENT_FUNDS".equals(data.result.errorCode)) {
			result().setCode(BetResultCode.BET_FAILED_LOW_BALANCE);
			result().setMessage("Account low balance");
			return;
		}

		// Parse offer data
		BetInstructionReport instruction =  data.result.instructionReports.get(0);
		parseInstructionData(instruction);

	}

	// Parse offer response
	private void parseInstructionData(BetInstructionReport bfReport) throws UnknownResponseException {
		switch (bfReport.status.toLowerCase()) {
			case "success":
				parseInstructionReport(bfReport);
				break;
			case "timeout":
				result().setCode(BetResultCode.BET_FAILED_ODD_CLOSED);
				break;
			case "failure":
				if (!Strings.isNullOrEmpty(bfReport.betId)) {

					// In VOIDED or LAPSED case
					parseInstructionReport(bfReport);
				} else {
					result().setCode(BetResultCode.FAILED);
				}
				account.getCommonLogger().warn("Place bet process failure with error code: {}", bfReport.errorCode);
				break;
			default:
				//unexpected case
				account.getCommonLogger().error("Unexpected behaviour: {} ", BettingUtility.writeObjectToJson(bfReport));
				throw new UnknownResponseException("Don't support this behaviour ", getDef(), BettingUtility.writeObjectToJson(bfReport));
		}
	}

	private void parseInstructionReport(BetInstructionReport bfReport) {
		// Bet ID, assigned unique offer ID
		result().setBetId(bfReport.betId);
		((ExchangeBetResult)result()).setDeleteId(bfReport.betId);

		// Already matched bet stake
		double matchedStake = bfReport.sizeMatched;
		double effectiveOdd = bfReport.averagePriceMatched;

		if (effectiveOdd > 1 && PivotType.ONE_TWO != context.getBetTicket().getBetInfoContext().getPivotType()) {
			effectiveOdd = effectiveOdd - 1.0;
		}

		result().setBetAmount(matchedStake);
		result().setBetOdd(VNEUtility.oddTo3Decimal(effectiveOdd));

		// Status
		parseStatus(bfReport, matchedStake);
	}

	private void parseStatus(BetInstructionReport bfReport, double matchedStake) {
		if (!VNEUtility.greaterThanZero(matchedStake)) {

			if ("EXECUTION_COMPLETE".equals(bfReport.orderStatus)) {

				// In case the match stake is zero and execution complete, we can reject immediately
				result().setCode(BetResultCode.BET_SUCCESS_REJECTED);
			} else {
				result().setCode(BetResultCode.BET_SUCCESS_UNMATCHED);
			}

			if (!VNEUtility.greaterThanZero(result().getBetOdd())) {

				// Because of unmatched odd, it will be updated by PLM
				// Return current odd of bet ticket to support to refund process
				result().setBetOdd(context.getBetTicket().getCurrentOdd());
			}
		} else if (matchedStake < bfReport.instruction.limitOrder.size) {
			result().setCode(BetResultCode.BET_SUCCESS_PARTIAL_MATCH);
		} else if (matchedStake >= bfReport.instruction.limitOrder.size) {
			result().setCode(BetResultCode.BET_SUCCESS_FULLY_MATCHED);
		}
	}

	@Override
	public boolean logHttpRequest() {
		return true;
	}

	public Description getMarketDescription() {
		return marketDescription;
	}

	public void setMarketDescription(Description marketDescription) {
		this.marketDescription = marketDescription;
	}
}
