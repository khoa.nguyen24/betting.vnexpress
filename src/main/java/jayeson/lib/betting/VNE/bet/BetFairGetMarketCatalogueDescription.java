package jayeson.lib.betting.VNE.bet;

import dagger.Lazy;
import jayeson.lib.betting.VNE.VNE;
import jayeson.lib.betting.VNE.VNEURIDefinition;
import jayeson.lib.betting.VNE.VNEUtility;
import jayeson.lib.betting.VNE.betticket.BetFairBetTicketResult;
import jayeson.lib.betting.VNE.json.marketcatalogue.*;
import jayeson.lib.betting.api.actioncontext.BetContext;
import jayeson.lib.betting.api.actionresults.*;
import jayeson.lib.betting.api.datastructure.BADRuntime;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.VNE.json.marketcatalogue.*;
import jayeson.lib.betting.core.exception.UnknownResponseException;
import jayeson.lib.betting.core.http.IClient;
import jayeson.lib.betting.core.http.IResponse;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.http.request.BettingRequest;
import jayeson.lib.betting.core.http.request.JsonRequest;
import jayeson.lib.betting.core.task.AsyncHttpTaskFragment;
import jayeson.lib.betting.core.task.TaskFragmentListener;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;
import jayeson.lib.betting.core.utility.BettingUtility;
import jayeson.lib.betting.exception.BettingException;
import jayeson.lib.betting.exception.InvalidAttributeException;

import javax.inject.Inject;
import java.util.*;

public class BetFairGetMarketCatalogueDescription extends AsyncHttpTaskFragment<BetResult, MarketCatalogueResponse> {

    private final VNE VNEAccount;
    private final BADRuntime badRuntime;
    private final BetContext context;
    private final Lazy<BetFairPlaceBet> placeBetLazy;
    private Description marketDescription;
    private String marketId;

    @Inject
    public BetFairGetMarketCatalogueDescription(AccountTaskInfo data, ITaskExecutor executor, IResponseListener responseListener,
                                                VNE account, TaskFragmentListener<BetResult> listener, BetResult initialResult,
                                                BetContext context, IClient client, BADRuntime badRuntime, Lazy<BetFairPlaceBet> placeBetLazy) {

        super(data, executor, responseListener, account, listener, initialResult, client);
        this.context = context;
        this.badRuntime = badRuntime;
        this.setDef(VNEURIDefinition.getJsonRpcURI(MarketCatalogueResponse.class));
        needProceed = false;
        this.VNEAccount = account;
        this.placeBetLazy = placeBetLazy;
    }

    @Override
    public void proceed(BetResult currentResult) {
        BetFairPlaceBet fragment = placeBetLazy.get();
        fragment.setMarketDescription(marketDescription);

        account.getCommonLogger().info("LAUNCH_FRAGMENT_PLACE_BET");
        executor.submit(placeBetLazy.get());
    }

    @Override
    public void execute() throws BettingException {
        BettingRequest request = buildRequest(context.getBetTicket());
        executeRequest(request, true);
    }

    private JsonRequest buildRequest(BetTicketResult ticket) throws InvalidAttributeException, jayeson.lib.betting.core.exception.BuildRequestFailException {
        if (!(ticket instanceof BetFairBetTicketResult)) {
            throw new InvalidAttributeException(String.format("Ticket should be in BetFairBetTicketResult, %s", ticket), "");
        }

        marketId = VNEUtility.getMarketId(ticket.getBetInfoContext());

        MarketCatalogueRequest payload = new MarketCatalogueRequest();
        payload.params = new MarketCatalogueParams();
        payload.params.filter = new MarketCatalogueFilter();
        payload.params.filter.marketIds = Collections.singletonList(marketId);
        payload.params.maxResults = 1;
        payload.params.marketProjection = Collections.singletonList("MARKET_DESCRIPTION");

        if (VNEUtility.isNeedToResetBettingApi(client.host().url().toString())) {
            client.resetHost(VNEUtility.getBettingApiUrl(client.host().url().toString()));
        }

        JsonRequest request = new JsonRequest.Builder()
                .def(this.getDef())
                .host(client.host())
                .data(payload)
                .addHeader("X-Authentication", this.badRuntime.getLoggedInUid())
                .addHeader("X-Application", VNEAccount.getAppKey())
                .build();
        return request;
    }

    @Override
    public void consumeResponse(IResponse<MarketCatalogueResponse> response) throws BettingException {
        account.getCommonLogger().info("RESPONSE_READY_{}", response.getStatusLine());
        if (response.getStatusLine().getCode() != 200) {
            account.getCommonLogger().error("FAIL to get market catalogue {}", response);
            result().setCode(ActionResultCode.FAILED);
            result().setMessage("The request failed with http code " + response.getStatusLine().getCode());
            needProceed = false;
            return;
        }

        MarketCatalogueResponse catalogueResponse = response.content();

        if (catalogueResponse.error != null) {
            account.getCommonLogger().error("Failed to get market catalogue: {} ", BettingUtility.writeObjectToJson(catalogueResponse.error));
            throw new UnknownResponseException("Failed to get market catalogue", getDef(), "");
        }

        if (catalogueResponse.result == null || catalogueResponse.result.length == 0) {
            account.getCommonLogger().error("FAIL to parse market catalogue {}", response);
            result().setCode(ActionResultCode.FAILED);
            result().setMessage("Unable to extract market data");
            needProceed = false;
            return;
        }

        MarketCatalogue marketCatalogue = catalogueResponse.result[0];
        if (!marketId.equals(marketCatalogue.marketId) || isInvalidMarketDescription(marketCatalogue)) {
            account.getCommonLogger().error("FAIL to parse market catalogue {}", marketCatalogue);
            result().setCode(ActionResultCode.FAILED);
            result().setMessage("Unable to extract market catalogue data");
            needProceed = false;
            return;
        }

        marketDescription = marketCatalogue.description;
        needProceed = true;
    }

    private boolean isInvalidMarketDescription(MarketCatalogue marketCatalogue) {
        return marketCatalogue.description ==  null
                || marketCatalogue.description.priceLadder == null
                || marketCatalogue.description.priceLadder.type == null
                || (PriceLadderType.LINE_RANGE.equals(marketCatalogue.description.priceLadder.type)
                    && marketCatalogue.description.lineRangeInfo == null);
    }

    @Override
    public boolean logHttpRequest() {
        return true;
    }
}
