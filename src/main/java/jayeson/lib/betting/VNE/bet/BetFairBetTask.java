package jayeson.lib.betting.VNE.bet;

import dagger.Lazy;
import jayeson.lib.betting.api.actioncontext.BetContext;
import jayeson.lib.betting.api.actionresults.ActionResultCode;
import jayeson.lib.betting.api.actionresults.BetResult;
import jayeson.lib.betting.api.actionresults.BetResultCode;
import jayeson.lib.betting.api.task.AccountTaskHandler;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.api.task.AccountTaskState;
import jayeson.lib.betting.api.task.TaskObservable;
import jayeson.lib.betting.core.BettingAccount;
import jayeson.lib.betting.core.dagger.qualifier.TaskId;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.task.BetTask;
import jayeson.lib.betting.core.task.TaskFragment;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;

import javax.inject.Inject;

@TaskScope
public class BetFairBetTask extends BetTask<BetResult> {

	private static final int PLACE_BET_WL = 50;
	private final Lazy<BetFairGetMarketCatalogueDescription> getMarketDescription;

	@Inject
	public BetFairBetTask(Lazy<BetFairGetMarketCatalogueDescription> getMarketDescription,
                          ITaskExecutor executor, IResponseListener responseListener, AccountTaskInfo data,
                          BettingAccount account, @TaskId String id, AccountTaskHandler<BetResult> handler,
                          TaskObservable<AccountTaskInfo> observable, BetContext betContext, BetResult result) {
		super(executor, responseListener, data, account, id, PLACE_BET_WL, handler, observable, betContext, result);
		this.getMarketDescription = getMarketDescription;
	}

	@Override
	public void execute() {
		account.getCommonLogger().info("LAUNCH_GET_MARKET_CATALOGUE_FRAGMENT");
		executor.submit(getMarketDescription.get());
	}

	@Override
	public void onFragmentCompleted(BetResult currentResult, TaskFragment<BetResult> fragment) {
		account.getCommonLogger().info("FRAGMENT_FINISHED_{}", id);

		// Save this fragment and reset finished workload
		finishedFragments.add(fragment);

		synchronized (data) {
			// Abort if this task is cancelled
			if (data.getState() == AccountTaskState.CANCELLED) {
				currentResult.setCode(ActionResultCode.FAILED);
				currentResult.setMessage("Task is cancelled");
				account.getCommonLogger().info("CANCELLED_NOTIFY_HANDLER_{}", id);
				handler.process(currentResult, this);
				return;
			}

			if (fragment.needProceed()) {
				// Update progress and continue to next fragment
				updateProgress(50, null);

				account.getCommonLogger().info("TASK_PROCEED_{}", id);
				fragment.proceed(currentResult);
				return;
			}

			account.getCommonLogger().info("TASK_FINISHED_{}_{}", id, currentResult.getCode());

			// Update progress
			if (currentResult.getCode() == BetResultCode.BET_SUCCESS_FULLY_MATCHED
					    || currentResult.getCode() == BetResultCode.BET_SUCCESS_PARTIAL_MATCH) {
				updateProgress(100, AccountTaskState.SUCCEEDED);
			} else {
				updateProgress(100, AccountTaskState.FAILED);
			}

			// Notify handler
			account.getCommonLogger().info("NOTIFY_HANDLER_{}", id);
			handler.process(currentResult, this);
		}
	}
}
