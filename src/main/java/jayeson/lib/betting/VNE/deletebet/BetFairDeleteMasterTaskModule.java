package jayeson.lib.betting.VNE.deletebet;

import dagger.Module;
import dagger.Provides;
import jayeson.lib.betting.api.actioncontext.BetHistoryByBetIdContext;
import jayeson.lib.betting.api.actioncontext.DeleteBetContext;
import jayeson.lib.betting.api.actionresults.DeleteBetResult;
import jayeson.lib.betting.api.actionresults.ListBetHistoryResult;
import jayeson.lib.betting.api.task.AccountTaskHandler;
import jayeson.lib.betting.VNE.getbethistorybyid.BetFairGetHistoriesByIdsModule;
import jayeson.lib.betting.core.dagger.taskscope.MasterTaskModule;
import jayeson.lib.betting.core.dagger.taskscope.TaskModule;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;
import jayeson.lib.betting.core.task.TaskFragmentListener;

import java.util.Collections;

@Module(includes = {TaskModule.class
        , MasterTaskModule.class
        , BetFairGetHistoriesByIdsModule.class
})
public class BetFairDeleteMasterTaskModule {

    @TaskScope
    @Provides
    AccountTaskHandler<DeleteBetResult> provideDeleteTaskHandler(BetFairDeleteMasterTask task) {
        return task.getDeleteBetTaskHandler();
    }

    @TaskScope
    @Provides
    AccountTaskHandler<ListBetHistoryResult> provideGetHistoryByIdTaskHandler(BetFairDeleteMasterTask masterTask) {
        return masterTask.getHistoryByIdTaskHandler();
    }

    @TaskScope
    @Provides
    TaskFragmentListener<DeleteBetResult> provideDeleteBetTaskFragmentListener(CancelOrderTask task) {
        return task;
    }

    @TaskScope
    @Provides
    DeleteBetResult provideDeleteBetResult() {
        return new DeleteBetResult();
    }

    @TaskScope
    @Provides
    BetHistoryByBetIdContext provideGetHistoryContext(DeleteBetContext context) {
        return new BetHistoryByBetIdContext(Collections.singletonList(context.getBetId()));
    }
}
