package jayeson.lib.betting.VNE.deletebet;

import dagger.Lazy;
import jayeson.lib.betting.api.actioncontext.DeleteBetContext;
import jayeson.lib.betting.api.actionresults.DeleteBetResult;
import jayeson.lib.betting.api.task.AccountTaskHandler;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.api.task.TaskObservable;
import jayeson.lib.betting.core.BettingAccount;
import jayeson.lib.betting.core.dagger.qualifier.TaskId;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.task.DeleteBetTask;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;

import javax.inject.Inject;

@TaskScope
public class CancelOrderTask extends DeleteBetTask {
	
	private static final int DELETE_BET_WL = 10;
	private Lazy<GetMarketIdFragment> getMarketIdFragmentLazy;

	@Inject
	public CancelOrderTask(Lazy<GetMarketIdFragment> getMarketIdFragmentLazy,
						   ITaskExecutor executor, IResponseListener responseListener, AccountTaskInfo data,
						   BettingAccount account, @TaskId String id, AccountTaskHandler<DeleteBetResult> handler,
						   TaskObservable<AccountTaskInfo> observable, DeleteBetContext context, DeleteBetResult result) {
		super(executor, responseListener, data, account, id, DELETE_BET_WL, handler, observable, context, result);
		this.getMarketIdFragmentLazy = getMarketIdFragmentLazy;
	}

	@Override
	public void execute() {
		account.getCommonLogger().info("LAUNCH_FRAGMENT_GET_MARKET_ID");
		executor.submit(getMarketIdFragmentLazy.get());
	}

}
