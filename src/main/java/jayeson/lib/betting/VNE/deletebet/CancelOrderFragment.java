package jayeson.lib.betting.VNE.deletebet;

import dagger.Lazy;
import jayeson.lib.betting.VNE.VNE;
import jayeson.lib.betting.VNE.VNEURIDefinition;
import jayeson.lib.betting.VNE.VNEUtility;
import jayeson.lib.betting.VNE.json.deletebet.CancelExecutionReport;
import jayeson.lib.betting.api.actioncontext.DeleteBetContext;
import jayeson.lib.betting.api.actionresults.ActionResultCode;
import jayeson.lib.betting.api.actionresults.DeleteBetResult;
import jayeson.lib.betting.api.datastructure.BADRuntime;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.VNE.json.deletebet.CancelOrderRequestPayload;
import jayeson.lib.betting.VNE.json.deletebet.DeleteResponseData;
import jayeson.lib.betting.core.exception.UnknownResponseException;
import jayeson.lib.betting.core.http.IClient;
import jayeson.lib.betting.core.http.IResponse;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.http.request.JsonRequest;
import jayeson.lib.betting.core.task.AsyncHttpTaskFragment;
import jayeson.lib.betting.core.task.TaskFragmentListener;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;
import jayeson.lib.betting.core.utility.BettingUtility;
import jayeson.lib.betting.exception.BettingException;

import javax.inject.Inject;

/**
 * This fragment is to delete the order, which needs
 * marketId --> cancelOrders API
 * and
 * isOneTwo --> identify pivot type to determine odd format
 */
public class CancelOrderFragment extends AsyncHttpTaskFragment<DeleteBetResult, DeleteResponseData> {

    private final VNE VNEAccount;
    private String marketId;
    private boolean isOneTwo;
    private DeleteBetContext context;
    private final BADRuntime badRuntime;
    private Lazy<ConfirmAvgOddsFrg> avgOddsFragmentLazy;

    @Inject
    public CancelOrderFragment(AccountTaskInfo data, ITaskExecutor executor,
                               IResponseListener responseListener, VNE account,
                               TaskFragmentListener<DeleteBetResult> listener,
                               DeleteBetResult initialResult, DeleteBetContext context, IClient client,
                               BADRuntime badRuntime, Lazy<ConfirmAvgOddsFrg> avgOddsFragmentLazy) {
        super(data, executor, responseListener, account, listener, initialResult, client);
        this.context = context;
        this.badRuntime = badRuntime;
        this.avgOddsFragmentLazy = avgOddsFragmentLazy;
        this.setDef(VNEURIDefinition.getJsonRpcURI(DeleteResponseData.class));
        this.VNEAccount = account;
    }

    @Override
    public void proceed(DeleteBetResult currentResult) {
        account.getCommonLogger().info("LAUNCH_FRAGMENT_CONFIRM_AVG_ODDS");
        ConfirmAvgOddsFrg avgOddsFragment =  avgOddsFragmentLazy.get();
        avgOddsFragment.setBetId(context.getBetId());
        avgOddsFragment.setOneTwo(isOneTwo);
        executor.submit(avgOddsFragment);
    }

    @Override
    public void execute() throws BettingException {
        CancelOrderRequestPayload payload = new CancelOrderRequestPayload(marketId, context.getBetId());

        if (VNEUtility.isNeedToResetBettingApi(client.host().url().toString())) {
            client.resetHost(VNEUtility.getBettingApiUrl(client.host().url().toString()));
        }

        JsonRequest request = new JsonRequest.Builder()
                .def(this.getDef())
                .host(client.host())
                .data(payload)
                .addHeader("X-Authentication", this.badRuntime.getLoggedInUid())
                .addHeader("X-Application", VNEAccount.getAppKey())
                .build();

        executeRequest(request, true);
    }

    @Override
    public void consumeResponse(IResponse<DeleteResponseData> response) throws BettingException {
        account.getCommonLogger().info("RESPONSE_READY_{}", response.getStatusLine().getCode());
        if (response.getStatusLine().getCode() != 200) {
            account.getCommonLogger().error("FAIL to delete order {}", response);
            result().setCode(ActionResultCode.FAILED);
            result().setMessage("The request failed with http code " + response.getStatusLine().getCode());
            return;
        }

        DeleteResponseData deleteResponseData = response.content();

        if(deleteResponseData.error != null) {
            account.getCommonLogger().error("Failed to delete order: {} ", BettingUtility.writeObjectToJson(deleteResponseData.error));
            throw new UnknownResponseException("Failed to delete order", getDef(), "");
        }

        CancelExecutionReport report = deleteResponseData.result;
        needProceed = false;

        if (report.status.equalsIgnoreCase("FAILURE")){
            account.getCommonLogger().debug("The bet matched, Unable to delete bet");
            needProceed = true;
        } else if(report.status.equalsIgnoreCase("SUCCESS") && report.instructionReports.size() > 0) {
            // set cancel stake
            result().setCancelledStake(report.instructionReports.get(0).sizeCancelled);
            needProceed = true;
        } else {
            //unexpected case
            account.getCommonLogger().error("Unexpected behaviour: {} ", BettingUtility.writeObjectToJson(report));
            throw new UnknownResponseException("Don't support this behaviour ", getDef(), BettingUtility.writeObjectToJson(report));
        }

    }

    @Override
    public boolean logHttpRequest() {
        return true;
    }

    public String getMarketId() {
        return marketId;
    }

    public void setMarketId(String marketId) {
        this.marketId = marketId;
    }

    public boolean isOneTwo() {
        return isOneTwo;
    }

    public void setOneTwo(boolean oneTwo) {
        isOneTwo = oneTwo;
    }
}
