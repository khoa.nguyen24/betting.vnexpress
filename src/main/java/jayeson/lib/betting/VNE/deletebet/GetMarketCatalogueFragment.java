package jayeson.lib.betting.VNE.deletebet;

import dagger.Lazy;
import jayeson.lib.betting.VNE.VNE;
import jayeson.lib.betting.VNE.VNEURIDefinition;
import jayeson.lib.betting.VNE.VNEUtility;
import jayeson.lib.betting.VNE.json.marketcatalogue.*;
import jayeson.lib.betting.api.actionresults.ActionResultCode;
import jayeson.lib.betting.api.actionresults.DeleteBetResult;
import jayeson.lib.betting.api.datastructure.BADRuntime;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.VNE.json.marketcatalogue.*;
import jayeson.lib.betting.core.exception.UnknownResponseException;
import jayeson.lib.betting.core.http.IClient;
import jayeson.lib.betting.core.http.IResponse;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.http.request.JsonRequest;
import jayeson.lib.betting.core.task.AsyncHttpTaskFragment;
import jayeson.lib.betting.core.task.TaskFragmentListener;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;
import jayeson.lib.betting.core.utility.BettingUtility;
import jayeson.lib.betting.exception.BettingException;
import jayeson.lib.feed.api.SportType;
import jayeson.lib.feed.api.twoside.PivotType;

import javax.inject.Inject;
import java.util.Collections;

public class GetMarketCatalogueFragment extends AsyncHttpTaskFragment<DeleteBetResult, MarketCatalogueResponse> {

    private final VNE VNEAccount;
    private String marketId;
    private boolean isOneTwo;
    private final BADRuntime badRuntime;
    private final Lazy<CancelOrderFragment> cancelFragmentLazy;

    @Inject
    public GetMarketCatalogueFragment(AccountTaskInfo data, ITaskExecutor executor,
                                      IResponseListener responseListener, VNE account,
                                      TaskFragmentListener<DeleteBetResult> listener,
                                      DeleteBetResult initialResult, IClient client,
                                      BADRuntime badRuntime, Lazy<CancelOrderFragment> cancelFragmentLazy) {
        super(data, executor, responseListener, account, listener, initialResult, client);
        this.badRuntime = badRuntime;
        this.cancelFragmentLazy = cancelFragmentLazy;
        this.isOneTwo = false;
        this.setDef(VNEURIDefinition.getJsonRpcURI(MarketCatalogueResponse.class));
        this.VNEAccount = account;
    }

    @Override
    public void proceed(DeleteBetResult currentResult) {
        CancelOrderFragment cancelOrderFragment = cancelFragmentLazy.get();
        cancelOrderFragment.setMarketId(marketId);
        cancelOrderFragment.setOneTwo(isOneTwo);
        executor.submit(cancelOrderFragment);
        account.getCommonLogger().info("LAUNCH_FRAGMENT_CANCEL_ORDER");
    }

    @Override
    public void execute() throws BettingException {
        MarketCatalogueRequest payload = new MarketCatalogueRequest();
        payload.params = new MarketCatalogueParams();
        payload.params.filter = new MarketCatalogueFilter();
        payload.params.filter.marketIds = Collections.singletonList(marketId);
        payload.params.marketProjection = Collections.singletonList("EVENT_TYPE");
        payload.params.maxResults = 1;

        if (VNEUtility.isNeedToResetBettingApi(client.host().url().toString())) {
            client.resetHost(VNEUtility.getBettingApiUrl(client.host().url().toString()));
        }

        JsonRequest request = new JsonRequest.Builder()
                .def(this.getDef())
                .host(client.host())
                .data(payload)
                .addHeader("X-Authentication", this.badRuntime.getLoggedInUid())
                .addHeader("X-Application", VNEAccount.getAppKey())
                .build();

        executeRequest(request, true);
    }

    @Override
    public void consumeResponse(IResponse<MarketCatalogueResponse> response) throws BettingException {
        account.getCommonLogger().info("RESPONSE_READY_{}", response.getStatusLine());
        if (response.getStatusLine().getCode() != 200) {
            account.getCommonLogger().error("FAIL to get market catalogue {}", response);
            result().setCode(ActionResultCode.FAILED);
            result().setMessage("The request failed with http code " + response.getStatusLine().getCode());
            needProceed = false;
            return;
        }

        MarketCatalogueResponse catalogueResponse = response.content();

        if(catalogueResponse.error != null) {
            account.getCommonLogger().error("Failed to get market catalogue: {} ", BettingUtility.writeObjectToJson(catalogueResponse.error));
            throw new UnknownResponseException("Failed to get market catalogue", getDef(), "");
        }

        if (catalogueResponse.result == null || catalogueResponse.result.length == 0) {
            account.getCommonLogger().error("FAIL to parse market catalogue {}", response);
            result().setCode(ActionResultCode.FAILED);
            result().setMessage("Unable to extract market data");
            needProceed = false;
            return;
        }

        MarketCatalogue market = catalogueResponse.result[0];
        account.getCommonLogger().debug("SUCCESS to parse market catalogue {} {}", marketId, market.marketName);

        SportType sportType = VNEUtility.getSportTypeFromMarketCatalogue(market);
        if (sportType != null) {
            isOneTwo = PivotType.ONE_TWO.equals(VNEUtility.getPivotTypeFromMarketCatalogue(market, sportType));
        }
    }

    @Override
    public boolean logHttpRequest() {
        return true;
    }

    public String getMarketId() {
        return marketId;
    }

    public void setMarketId(String marketId) {
        this.marketId = marketId;
    }
}
