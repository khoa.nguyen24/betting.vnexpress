package jayeson.lib.betting.VNE.deletebet;

import jayeson.lib.betting.VNE.VNE;
import jayeson.lib.betting.VNE.VNEUtility;
import jayeson.lib.betting.VNE.json.OrderStatus;
import jayeson.lib.betting.VNE.json.listcurrentorders.CurrentOrderSummary;
import jayeson.lib.betting.VNE.json.listcurrentorders.CurrentOrderSummaryReport;
import jayeson.lib.betting.VNE.json.listcurrentorders.ListCurrentOrdersResponse;
import jayeson.lib.betting.api.actioncontext.DeleteBetContext;
import jayeson.lib.betting.api.actionresults.ActionResultCode;
import jayeson.lib.betting.api.actionresults.DeleteBetResult;
import jayeson.lib.betting.api.actionresults.DeleteBetResultCode;
import jayeson.lib.betting.api.datastructure.BADRuntime;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.core.exception.UnknownResponseException;
import jayeson.lib.betting.core.http.IClient;
import jayeson.lib.betting.core.http.IResponse;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.task.TaskFragmentListener;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;
import jayeson.lib.betting.core.utility.BettingUtility;
import jayeson.lib.betting.exception.BettingException;

import javax.inject.Inject;

public class ConfirmAvgOddsFrg extends GetCurrentOrderFrg {
    private boolean isOneTwo;
    private String betId;

    @Inject
    public ConfirmAvgOddsFrg(AccountTaskInfo data, ITaskExecutor executor, IResponseListener responseListener,
                             VNE account, TaskFragmentListener<DeleteBetResult> listener,
                             DeleteBetResult initialResult, DeleteBetContext context,
                             IClient client, BADRuntime badRuntime) {
        super(data, executor, responseListener, account, listener, initialResult, context, client, badRuntime);
        needProceed = false;
    }

    @Override
    public void consumeResponse(IResponse<ListCurrentOrdersResponse> response) throws BettingException {
        account.getCommonLogger().info("RESPONSE_READY_{}", response.getStatusLine().getCode());
        if (response.getStatusLine().getCode() != 200) {
            account.getCommonLogger().error("FAIL to get order {}", response);
            result().setCode(ActionResultCode.FAILED);
            result().setMessage("The request failed with http code " + response.getStatusLine().getCode());
            return;
        }

        ListCurrentOrdersResponse orderResponse = response.content();

        if(orderResponse.error != null) {
            account.getCommonLogger().error("Failed to get confirm avg odd: {} ", BettingUtility.writeObjectToJson(orderResponse.error));
            throw new UnknownResponseException("Failed to get confirm avg odd", getDef(), "");
        }

        CurrentOrderSummaryReport result = orderResponse.result;
        if (result.currentOrders.size() > 0) {
            CurrentOrderSummary order = result.currentOrders.get(0);
            if (OrderStatus.EXECUTABLE.equals(order.status)) {
                // Either EXECUTABLE (an unmatched amount remains) or EXECUTION_COMPLETE (no unmatched amount remains).
                // We should return FAILED to BI/OE retry another delete bet request.
                account.getCommonLogger().error("Delete failed, process is EXECUTABLE: {}", betId);
                result().setCode(DeleteBetResultCode.FAILED);
                return;
            }

            account.getCommonLogger().debug("Successfully delete bet: {}", betId);

            // Get the average odd matched
            double matchedStake = order.sizeMatched;
            result().setMatchedStake(matchedStake);
            double averagePriceMatched = order.averagePriceMatched;
            if (!isOneTwo) {
                averagePriceMatched -= 1;
            }
    
            result().setEffectiveOdd(VNEUtility.oddTo3Decimal(averagePriceMatched));

            if (Math.abs(matchedStake) < 0.01) {
                result().setCode(DeleteBetResultCode.SUCCESSFUL);
            } else {
                result().setCode(DeleteBetResultCode.DELETE_FAILED_BET_CONFIRMED);
            }
        } else {
            account.getCommonLogger().debug("Bet fully deleted: {}", betId);
            result().setEffectiveOdd(0.0);
            result().setMatchedStake(0.0);
            // bet fully unmatched
            result().setCode(DeleteBetResultCode.SUCCESSFUL);
        }

    }

    public String getBetId() {
        return betId;
    }

    public void setBetId(String betId) {
        this.betId = betId;
    }

    public boolean isOneTwo() {
        return isOneTwo;
    }

    public void setOneTwo(boolean oneTwo) {
        isOneTwo = oneTwo;
    }
}
