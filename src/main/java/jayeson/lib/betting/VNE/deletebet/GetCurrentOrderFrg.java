package jayeson.lib.betting.VNE.deletebet;

import jayeson.lib.betting.VNE.VNE;
import jayeson.lib.betting.VNE.VNEURIDefinition;
import jayeson.lib.betting.VNE.VNEUtility;
import jayeson.lib.betting.VNE.json.listcurrentorders.ListCurrentOrdersResponse;
import jayeson.lib.betting.api.actioncontext.DeleteBetContext;
import jayeson.lib.betting.api.actionresults.DeleteBetResult;
import jayeson.lib.betting.api.datastructure.BADRuntime;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.VNE.json.deletebet.CurrentOrderRequestPayload;
import jayeson.lib.betting.core.http.IClient;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.http.request.JsonRequest;
import jayeson.lib.betting.core.task.AsyncHttpTaskFragment;
import jayeson.lib.betting.core.task.TaskFragmentListener;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;
import jayeson.lib.betting.exception.BettingException;

public abstract class GetCurrentOrderFrg extends AsyncHttpTaskFragment<DeleteBetResult, ListCurrentOrdersResponse> {

    private final VNE VNEAccount;
    protected DeleteBetContext context;
    protected final BADRuntime badRuntime;

    public GetCurrentOrderFrg(AccountTaskInfo data, ITaskExecutor executor,
                              IResponseListener responseListener, VNE account,
                              TaskFragmentListener<DeleteBetResult> listener,
                              DeleteBetResult initialResult, DeleteBetContext context, IClient client,
                              BADRuntime badRuntime) {
        super(data, executor, responseListener, account, listener, initialResult, client);
        this.context = context;
        this.badRuntime = badRuntime;
        this.setDef(VNEURIDefinition.getJsonRpcURI(ListCurrentOrdersResponse.class));
        this.VNEAccount = account;
    }

    @Override
    public void proceed(DeleteBetResult currentResult) { }

    @Override
    public void execute() throws BettingException {
        CurrentOrderRequestPayload payload = new CurrentOrderRequestPayload(context.getBetId());

        if (VNEUtility.isNeedToResetBettingApi(client.host().url().toString())) {
            client.resetHost(VNEUtility.getBettingApiUrl(client.host().url().toString()));
        }

        JsonRequest request = new JsonRequest.Builder()
                .def(this.getDef())
                .host(client.host())
                .data(payload)
                .addHeader("Accept", "application/json")
                .addHeader("X-Authentication", this.badRuntime.getLoggedInUid())
                .addHeader("X-Application", VNEAccount.getAppKey())
                .build();

        executeRequest(request, true);
    }

    @Override
    public boolean logHttpRequest() {
        return true;
    }
}
