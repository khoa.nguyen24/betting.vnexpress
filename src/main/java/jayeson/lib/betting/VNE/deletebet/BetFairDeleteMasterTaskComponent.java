package jayeson.lib.betting.VNE.deletebet;

import dagger.BindsInstance;
import dagger.Subcomponent;
import jayeson.lib.betting.api.actioncontext.DeleteBetContext;
import jayeson.lib.betting.api.actionresults.DeleteBetResult;
import jayeson.lib.betting.api.task.AccountTaskHandler;
import jayeson.lib.betting.core.dagger.qualifier.MasterTaskHandler;
import jayeson.lib.betting.core.dagger.taskscope.ITaskComponent;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;

@TaskScope
@Subcomponent(modules = {BetFairDeleteMasterTaskModule.class})
public interface BetFairDeleteMasterTaskComponent extends ITaskComponent<DeleteBetResult> {
    @Override
    BetFairDeleteMasterTask getTask();

    @Subcomponent.Builder
    interface Builder extends BuilderContext<DeleteBetResult, DeleteBetContext> {
        @Override
        @BindsInstance
        Builder bindsHandler(@MasterTaskHandler AccountTaskHandler<DeleteBetResult> handler);

        BetFairDeleteMasterTaskComponent build();
    }
}
