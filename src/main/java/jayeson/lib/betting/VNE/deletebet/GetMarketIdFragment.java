package jayeson.lib.betting.VNE.deletebet;

import dagger.Lazy;
import jayeson.lib.betting.VNE.VNE;
import jayeson.lib.betting.VNE.json.listcurrentorders.CurrentOrderSummary;
import jayeson.lib.betting.VNE.json.listcurrentorders.CurrentOrderSummaryReport;
import jayeson.lib.betting.VNE.json.listcurrentorders.ListCurrentOrdersResponse;
import jayeson.lib.betting.api.actioncontext.DeleteBetContext;
import jayeson.lib.betting.api.actionresults.DeleteBetResult;
import jayeson.lib.betting.api.actionresults.DeleteBetResultCode;
import jayeson.lib.betting.api.datastructure.BADRuntime;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.core.exception.UnknownResponseException;
import jayeson.lib.betting.core.http.IClient;
import jayeson.lib.betting.core.http.IResponse;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.task.TaskFragmentListener;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;
import jayeson.lib.betting.core.utility.BettingUtility;
import jayeson.lib.betting.exception.BettingException;

import javax.inject.Inject;

/**
 * There is no market id on delete context, so need to call current order fragment to get market id, so that it can be used
 * to identify pivot type to format the confirm odds
 */
public class GetMarketIdFragment extends GetCurrentOrderFrg {
    private String marketId;
    private final Lazy<GetMarketCatalogueFragment> getMarketCatalogueFragmentLazy;

    @Inject
    public GetMarketIdFragment(AccountTaskInfo data, ITaskExecutor executor, IResponseListener responseListener,
                               VNE account, TaskFragmentListener<DeleteBetResult> listener,
                               DeleteBetResult initialResult, DeleteBetContext context, IClient client,
                               BADRuntime badRuntime, Lazy<GetMarketCatalogueFragment> getMarketCatalogueFragmentLazy) {
        super(data, executor, responseListener, account, listener, initialResult, context, client, badRuntime);
        this.getMarketCatalogueFragmentLazy = getMarketCatalogueFragmentLazy;
        needProceed = false;
    }

    @Override
    public void proceed(DeleteBetResult currentResult) {
        GetMarketCatalogueFragment cancelOrderFragment = getMarketCatalogueFragmentLazy.get();
        cancelOrderFragment.setMarketId(marketId);
        executor.submit(cancelOrderFragment);
        account.getCommonLogger().info("LAUNCH_FRAGMENT_GET_MARKET_ID_FRAGMENT");
    }

    @Override
    public void consumeResponse(IResponse<ListCurrentOrdersResponse> response) throws BettingException {
        account.getCommonLogger().info("RESPONSE_READY_{}", response.getStatusLine());
        if (response.getStatusLine().getCode() != 200) {
            account.getCommonLogger().error("FAIL to get market id {}, delete id {}", response, context.getBetId());
            result().setCode(DeleteBetResultCode.FAILED);
            result().setMessage("The request failed with http code " + response.getStatusLine().getCode());
            return;
        }

        ListCurrentOrdersResponse orderResponse = response.content();

        if(orderResponse.error != null) {
            account.getCommonLogger().error("Failed to get market id: {} ", BettingUtility.writeObjectToJson(orderResponse.error));
            throw new UnknownResponseException("Failed to get market id", getDef(), "");
        }

        CurrentOrderSummaryReport result = orderResponse.result;

        if(result.currentOrders.size() > 0) {
            CurrentOrderSummary currentOrderSummary = result.currentOrders.get(0);
            marketId = currentOrderSummary.marketId;
            needProceed = true;
        } else {
            result().setCode(DeleteBetResultCode.NONE);
            account.getCommonLogger().warn("Can not get current order, the bet may be settled, delete id {}", context.getBetId());
            needProceed = false;
            return;
        }
    }

    @Override
    public boolean logHttpRequest() {
        return true;
    }
}
