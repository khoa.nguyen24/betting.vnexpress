package jayeson.lib.betting.VNE.deletebet;

import dagger.Lazy;
import jayeson.lib.betting.VNE.VNEUtility;
import jayeson.lib.betting.VNE.getbethistorybyid.BetFairHistoryResult;
import jayeson.lib.betting.api.actioncontext.DeleteBetContext;
import jayeson.lib.betting.api.actionresults.*;
import jayeson.lib.betting.api.task.*;
import jayeson.lib.betting.VNE.getbethistorybyid.BetFairGetHistoriesByIdsTask;
import jayeson.lib.betting.core.BettingAccount;
import jayeson.lib.betting.core.dagger.qualifier.MasterTaskHandler;
import jayeson.lib.betting.core.dagger.qualifier.MasterTaskId;
import jayeson.lib.betting.core.dagger.qualifier.MasterTaskInfo;
import jayeson.lib.betting.core.dagger.qualifier.MasterTaskObservable;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;
import jayeson.lib.betting.core.task.MasterAccountTask;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;

import javax.inject.Inject;
import java.util.Optional;

/**
 * @author Nam Nguyen
 */
@TaskScope
public class BetFairDeleteMasterTask extends MasterAccountTask<DeleteBetResult> {
    private final Lazy<CancelOrderTask> deleteBetTaskLazy;
    private final Lazy<BetFairGetHistoriesByIdsTask> getHistoryByIdTaskLazy;
    private final AccountTaskHandler<DeleteBetResult> deleteBetTaskHandler =
            (DeleteBetResult result, AccountTask<DeleteBetResult> task) -> {

                synchronized (data) {
                    if (data.getState() == AccountTaskState.CANCELLED) {
                        reportResult(ActionResultCode.FAILED, "Task is cancelled");
                        return;
                    }
                    // Update progress
                    if (result.getCode() == ActionResultCode.NONE) {
                        updateProgress(50, null);
                        checkBetHistories();
                    } else {
                        updateProgress(100, AccountTaskState.SUCCEEDED);
                        reportResult(result().getCode(), result().getMessage());
                    }
                }
            };
    private DeleteBetContext context;
    private final AccountTaskHandler<ListBetHistoryResult> getHistoryByIdTaskHandler =
            (ListBetHistoryResult result, AccountTask<ListBetHistoryResult> task) -> {
                synchronized (data) {
                    if (data.getState() == AccountTaskState.CANCELLED) {
                        reportResult(ActionResultCode.FAILED, "Task is cancelled");
                        return;
                    }
                    // Update progress
                    if (result.getCode() == ActionResultCode.FAILED) {
                        updateProgress(100, AccountTaskState.FAILED);
                        reportResult(result.getCode(), "Fail to get histories in delete bet master task"
                                + task.id() + ": " + result.getMessage());
                        return;
                    }

                    // Require provide BetFairHistoryResult for History module
                    BetFairHistoryResult betSettled = (BetFairHistoryResult) getHistoryResult(result);
                    if (betSettled != null) {
                        result().setEffectiveOdd(betSettled.getBetOdd());

                        if (BetFairHistoryResult.DeleteStatus.CANCELLED.equals(betSettled.getDeleteStatus())
                                || !VNEUtility.greaterThanZero(betSettled.getBetAmount())) {
                            account.getCommonLogger().info("Bet fully deleted: {}", context.getBetId());
                            result().setMessage("Bet fully deleted");
                            result().setCode(DeleteBetResultCode.SUCCESSFUL);

                            result().setMatchedStake(0.0);
                        } else {
                            account.getCommonLogger().info("Delete failed bet is confirmed and settled: {}",
                                    context.getBetId());
                            result().setMessage("Delete failed bet is confirmed and settled");
                            result().setCode(DeleteBetResultCode.DELETE_FAILED_BET_CONFIRMED);

                            result().setMatchedStake(betSettled.getBetAmount());
                        }
                    } else {
                        account.getCommonLogger().error("Fail to delete bet, delete id {}", context.getBetId());
                        result().setMessage("Fail to delete bet");
                        result().setCode(DeleteBetResultCode.FAILED);
                    }

                    updateProgress(100, AccountTaskState.SUCCEEDED);
                    reportResult(result().getCode(), result().getMessage());
                }
            };

    @Inject
    public BetFairDeleteMasterTask(ITaskExecutor executor, BettingAccount account,
                                   @MasterTaskInfo AccountTaskInfo data,
                                   @MasterTaskId String id,
                                   @MasterTaskHandler AccountTaskHandler<DeleteBetResult> handler,
                                   @MasterTaskObservable TaskObservable<AccountTaskInfo> observable,
                                   Lazy<CancelOrderTask> deleteBetTaskLazy,
                                   Lazy<BetFairGetHistoriesByIdsTask> getHistoryByIdTaskLazy,
                                   DeleteBetContext context,
                                   DeleteBetResult result) {
        super(executor, data, account, id, handler, observable, result);
        this.deleteBetTaskLazy = deleteBetTaskLazy;
        this.getHistoryByIdTaskLazy = getHistoryByIdTaskLazy;
        this.context = context;
    }

    private BetHistoryResult getHistoryResult(ListBetHistoryResult result) {
        Optional<BetHistoryResult> optional = Optional.empty();
        if (result.getResultList() != null && !result.getResultList().isEmpty()) {
            optional = result.getResultList().stream()
                    .filter(h -> context.getBetId().equals(h.getBetId()))
                    .findFirst();
        }
        return optional.orElse(null);
    }

    @Override
    public void execute() {
        currentSubTask = proceedDeleteBet();
    }

    private TaskObservable<AccountTaskInfo> proceedDeleteBet() {
        CancelOrderTask deleteBetTask = deleteBetTaskLazy.get();
        executor.submit(deleteBetTask);
        return deleteBetTask.getObservable();
    }

    private void checkBetHistories() {
        currentSubTask = proceedBetHistories();
    }

    private TaskObservable<AccountTaskInfo> proceedBetHistories() {
        BetFairGetHistoriesByIdsTask getBetHistoryTask = getHistoryByIdTaskLazy.get();
        executor.submit(getBetHistoryTask);
        return getBetHistoryTask.getObservable();
    }

    public AccountTaskHandler<DeleteBetResult> getDeleteBetTaskHandler() {
        return deleteBetTaskHandler;
    }

    public AccountTaskHandler<ListBetHistoryResult> getHistoryByIdTaskHandler() {
        return getHistoryByIdTaskHandler;
    }
}
