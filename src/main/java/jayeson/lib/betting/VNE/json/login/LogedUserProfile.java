package jayeson.lib.betting.VNE.json.login;

public class LogedUserProfile {
    long user_id;
    String user_session;

    String country;
    String address;
    String   city;
    long create_time;
    String user_avatar;
    String  user_email;
    String user_fullname;


    public String getCountry() {
        return country;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public long getCreate_time() {
        return create_time;
    }

    public String getUser_avatar() {
        return user_avatar;
    }

    public String getUser_email() {
        return user_email;
    }

    public String getUser_fullname() {
        return user_fullname;
    }

    public long getUser_id() {
        return user_id;
    }

    public String getUser_session() {
        return user_session;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public void setUser_session(String user_session) {
        this.user_session = user_session;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setCreate_time(long create_time) {
        this.create_time = create_time;
    }

    public void setUser_avatar(String user_avatar) {
        this.user_avatar = user_avatar;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public void setUser_fullname(String user_fullname) {
        this.user_fullname = user_fullname;
    }

    @Override
    public String toString() {
        return "LogedUserProfile{" +
                "user_id=" + user_id +
                ", user_session='" + user_session + '\'' +
                ", country='" + country + '\'' +
                ", address='" + address + '\'' +
                ", city='" + city + '\'' +
                ", create_time=" + create_time +
                ", user_avatar='" + user_avatar + '\'' +
                ", user_email='" + user_email + '\'' +
                ", user_fullname='" + user_fullname + '\'' +
                '}';
    }
}
