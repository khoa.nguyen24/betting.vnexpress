package jayeson.lib.betting.VNE.json.deletebet;

import java.util.ArrayList;
import java.util.List;

public class CancelOrderParam {

    public String marketId;
    public List<CancelInstruction> instructions;

    public CancelOrderParam() {
        this.instructions = new ArrayList<>();
    }

    public CancelOrderParam(String marketId, String betId) {
        this();
        this.marketId = marketId;
        this.instructions.add(new CancelInstruction(betId));
    }

    @Override
    public String toString() {
        return "CancelOrderParam{" +
                "marketId='" + marketId + '\'' +
                ", instructions=" + instructions +
                '}';
    }
}
