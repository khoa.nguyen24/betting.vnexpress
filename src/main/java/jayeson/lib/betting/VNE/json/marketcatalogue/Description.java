package jayeson.lib.betting.VNE.json.marketcatalogue;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Description {
    public boolean persistenceEnabled;
    public String bettingType;
    public String marketType;

    public LineRangeInfo lineRangeInfo;

    @JsonProperty("priceLadderDescription")
    public PriceLadderDescription priceLadder;
}
