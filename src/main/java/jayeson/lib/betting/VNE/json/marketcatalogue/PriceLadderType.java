package jayeson.lib.betting.VNE.json.marketcatalogue;

public enum PriceLadderType {
    CLASSIC,
    FINEST,
    LINE_RANGE
}
