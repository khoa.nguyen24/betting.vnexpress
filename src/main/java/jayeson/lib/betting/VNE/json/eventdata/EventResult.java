package jayeson.lib.betting.VNE.json.eventdata;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EventResult {
    public MarketEvent event;
    public Integer marketCount;
}
