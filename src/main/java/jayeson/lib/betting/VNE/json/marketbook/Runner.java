package jayeson.lib.betting.VNE.json.marketbook;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Runner {
    public Integer selectionId;
    public Double handicap;
    public String status;
    public Double lastPriceTraded;
    public Double totalMatched;
    public Ex ex;
}