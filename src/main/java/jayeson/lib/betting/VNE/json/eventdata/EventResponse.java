package jayeson.lib.betting.VNE.json.eventdata;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jayeson.lib.betting.VNE.json.RpcResponse;

import java.util.List;

/**
 * [
 *     {
 *         "jsonrpc": "2.0",
 *         "result": [
 *             {
 *                 "event": {
 *                     "id": "30073516",
 *                     "name": "Wuhan Zall v Henan",
 *                     "countryCode": "CN",
 *                     "timezone": "GMT",
 *                     "openDate": "2020-10-23T07:30:00.000Z"
 *                 },
 *                 "marketCount": 11
 *             },
 *             {
 *                 "event": {
 *                     "id": "30081168",
 *                     "name": "Qingdao Eagles v Tianjin Pioneers",
 *                     "countryCode": "GB",
 *                     "timezone": "GMT",
 *                     "openDate": "2020-10-23T07:30:00.000Z"
 *                 },
 *                 "marketCount": 1
 *             },
 *             {
 *                 "event": {
 *                     "id": "30004695",
 *                     "name": "DA v EWL",
 *                     "countryCode": "GB",
 *                     "timezone": "Europe/London",
 *                     "openDate": "2070-09-08T14:30:00.000Z"
 *                 },
 *                 "marketCount": 7
 *             }
 *         ],
 *         "id": 1
 *     }
 * ]
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class EventResponse extends RpcResponse<List<EventResult>> {
}
