package jayeson.lib.betting.VNE.json.clearedorders;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jayeson.lib.betting.VNE.json.OrderType;
import jayeson.lib.betting.VNE.json.PersistenceType;
import jayeson.lib.feed.api.LBType;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ClearedOrderSummary {
    public String eventTypeId;
    public String eventId;
    public String marketId;
    public Long selectionId;
    public Double handicap;
    public String betId;
    public String placedDate;
    public PersistenceType persistenceType;
    public OrderType orderType;
    public LBType side;
    public ItemDescription itemDescription;
    public String betOutcome; //WIN/LOSE/PLACE
    public Double priceRequested;
    public Date settledDate;
    public Date lastMatchedDate;
    public Integer betCount;
    public Double commission;
    public Double priceMatched;
    public Boolean priceReduced;
    public Double sizeSettled;
    public Double profit;
    public Double sizeCancelled;
    public String customerOrderRef;
    public String customerStrategyRef;

    @Override
    public String toString() {
        return "ClearedOrderSummary{" +
                "eventTypeId='" + eventTypeId + '\'' +
                ", eventId='" + eventId + '\'' +
                ", marketId='" + marketId + '\'' +
                ", selectionId=" + selectionId +
                ", handicap=" + handicap +
                ", betId='" + betId + '\'' +
                ", placedDate='" + placedDate + '\'' +
                ", persistenceType=" + persistenceType +
                ", orderType=" + orderType +
                ", side=" + side +
                ", itemDescription=" + itemDescription +
                ", betOutcome='" + betOutcome + '\'' +
                ", priceRequested=" + priceRequested +
                ", settledDate=" + settledDate +
                ", lastMatchedDate=" + lastMatchedDate +
                ", betCount=" + betCount +
                ", commission=" + commission +
                ", priceMatched=" + priceMatched +
                ", priceReduced=" + priceReduced +
                ", sizeSettled=" + sizeSettled +
                ", profit=" + profit +
                ", sizeCancelled=" + sizeCancelled +
                ", customerOrderRef='" + customerOrderRef + '\'' +
                ", customerStrategyRef='" + customerStrategyRef + '\'' +
                '}';
    }
}
