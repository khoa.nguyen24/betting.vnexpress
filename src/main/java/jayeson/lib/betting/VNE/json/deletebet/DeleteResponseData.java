package jayeson.lib.betting.VNE.json.deletebet;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jayeson.lib.betting.VNE.json.RpcResponse;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DeleteResponseData extends RpcResponse<CancelExecutionReport> {
}
