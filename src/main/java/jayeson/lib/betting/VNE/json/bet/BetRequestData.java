package jayeson.lib.betting.VNE.json.bet;

import jayeson.lib.betting.VNE.betticket.BetFairBetTicketResult;
import jayeson.lib.betting.VNE.json.RpcRequest;
import jayeson.lib.betting.VNE.json.marketcatalogue.Description;
import jayeson.lib.betting.api.actioncontext.BetContext;
import jayeson.lib.betting.core.exception.BuildRequestFailException;
import jayeson.lib.betting.exception.InvalidAttributeException;
import jayeson.lib.betting.exception.NewValueException;

public class BetRequestData extends RpcRequest<BetParam> {

    public BetRequestData() {
        this.method = "SportsAPING/v1.0/placeOrders";
    }

    public BetRequestData(BetFairBetTicketResult ticket, BetContext betContext, Description marketDescription)
            throws BuildRequestFailException, InvalidAttributeException, NewValueException {
        this();
        this.params = new BetParam(ticket, betContext, marketDescription);
    }
}
