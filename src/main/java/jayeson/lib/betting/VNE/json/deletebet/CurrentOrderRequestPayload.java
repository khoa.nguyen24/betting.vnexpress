package jayeson.lib.betting.VNE.json.deletebet;

import jayeson.lib.betting.VNE.json.RpcRequest;

import java.util.ArrayList;

public class CurrentOrderRequestPayload extends RpcRequest<CurrentOrderParam> {

    public CurrentOrderRequestPayload() {
        super();
        this.method = "SportsAPING/v1.0/listCurrentOrders";
        this.params = new CurrentOrderParam();
        this.params.betIds = new ArrayList<>();
    }

    public CurrentOrderRequestPayload(String betId) {
        this();
        params.betIds.add(betId);
    }
}