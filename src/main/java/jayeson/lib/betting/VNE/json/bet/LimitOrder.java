package jayeson.lib.betting.VNE.json.bet;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jayeson.lib.betting.VNE.VNEUtility;
import jayeson.lib.betting.VNE.json.marketcatalogue.Description;
import jayeson.lib.betting.api.actioncontext.BetContext;
import jayeson.lib.betting.api.actionresults.BetTicketResult;
import jayeson.lib.betting.VNE.json.PersistenceType;
import jayeson.lib.betting.exception.InvalidAttributeException;
import jayeson.lib.betting.exception.NewValueException;
import jayeson.lib.feed.api.twoside.PivotType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LimitOrder {
    public double size;
    public double price;
    public PersistenceType persistenceType;
    public LimitOrder() { }

    public LimitOrder(BetTicketResult ticket, BetContext betContext, Description marketDescription) throws InvalidAttributeException, NewValueException {
        this.size = betContext.getActualBetStake();

        double submitOdd = betContext.getTargetOdd();
        if (ticket.getBetInfoContext().getPivotType() != PivotType.ONE_TWO) {
            submitOdd += 1;
        }

        // Related to #33339, we use the user target odd to submit odd to Betfair.
        // To fix #33406, we round the submit odd by increment rules of betfair
        this.price = VNEUtility.roundOdd(submitOdd, ticket.getBetInfoContext().getLbType(), marketDescription);

        this.persistenceType = PersistenceType.LAPSE;
    }

    @Override
    public String toString() {
        return "LimitOrder{" +
                "size=" + size +
                ", price=" + price +
                ", persistenceType=" + persistenceType +
                '}';
    }
}
