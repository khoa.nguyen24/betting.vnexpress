package jayeson.lib.betting.VNE.json.marketcatalogue;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Runner {
    public Long selectionId;
    public String runnerName;
    public Double handicap;
    public Integer sortPriority;
}
