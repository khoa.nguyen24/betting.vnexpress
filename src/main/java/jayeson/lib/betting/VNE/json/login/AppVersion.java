package jayeson.lib.betting.VNE.json.login;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AppVersion {
    public String owner;
    public String versionId;
    public String version;
    public String applicationKey;
    public boolean delayData;
    public boolean subscriptionRequired;
    public boolean ownerManaged;
    public boolean active;
}
