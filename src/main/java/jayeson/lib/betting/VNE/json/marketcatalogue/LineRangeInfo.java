package jayeson.lib.betting.VNE.json.marketcatalogue;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LineRangeInfo {

    // minPrice - Minimum value for the outcome, in market units for this market (eg 0 runs)
    public double minUnitValue;

    // maxPrice - Maximum value for the outcome, in market units for this market (eg 100 runs)
    public double maxUnitValue;

    // interval - The odds ladder on this market will be between the range of minUnitValue and maxUnitValue,
    // in increments of the interval value.e.g. If minUnitValue=10 runs, maxUnitValue=20 runs, interval=0.5 runs,
    // then valid odds include 10, 10.5, 11, 11.5 up to 20 runs.
    public double interval;

    // unit - The type of unit the lines are incremented in by the interval (e.g: runs, goals or seconds)
    public String marketUnit;

    @Override
    public String toString() {
        return "LineRangeInfo{" +
                "minUnitValue=" + minUnitValue +
                ", maxUnitValue=" + maxUnitValue +
                ", interval=" + interval +
                ", marketUnit='" + marketUnit + '\'' +
                '}';
    }
}
