package jayeson.lib.betting.VNE.json.login;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AppKey {
    public String appName;
    public Long appId;
    @JsonProperty("appVersions")
    public List<AppVersion> appVersions;
}
