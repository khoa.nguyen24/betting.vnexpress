package jayeson.lib.betting.VNE.json.marketcatalogue;

import java.util.List;

public class MarketCatalogueParams {
    public MarketCatalogueFilter filter;
    public Integer maxResults;
    public List<String> marketProjection;
}
