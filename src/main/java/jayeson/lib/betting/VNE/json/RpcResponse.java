package jayeson.lib.betting.VNE.json;

public abstract class RpcResponse<T> extends RpcMessage {
    public T result;
    public Object error;

    @Override
    public String toString() {
        return "RpcResponse{" +
                "result=" + result +
                ", error=" + error +
                ", jsonrpc='" + jsonrpc + '\'' +
                ", id=" + id +
                '}';
    }
}