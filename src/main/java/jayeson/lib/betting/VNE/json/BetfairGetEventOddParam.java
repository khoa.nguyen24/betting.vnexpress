package jayeson.lib.betting.VNE.json;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class BetfairGetEventOddParam {
    @JsonProperty("marketIds")
    List<String> marketIds;

    public List<String> getMarketIds() {
        return marketIds;
    }
}
