package jayeson.lib.betting.VNE.json.marketcatalogue;

import java.util.List;

public class MarketCatalogueFilter {
    public List<String> marketIds;
    public List<String> eventIds;
    public List<String> marketTypeCodes;
}
