package jayeson.lib.betting.VNE.json.marketcatalogue;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MarketCatalogue {
    public String marketId;
    public String marketName;
    public Double totalMatched;

    @JsonProperty("runners")
    public List<Runner> runners;

    @JsonProperty("description")
    public Description description;

    @JsonProperty("competition")
    public Competition competition;

    @JsonProperty("event")
    public Event event;

    public String eventType;

    @JsonProperty("eventType")
    public void unpackNameFromNestedEventType(Map<String, String> eventType) {

        // Name of event type is sport type
        this.eventType = eventType.get("name");
    }
}
