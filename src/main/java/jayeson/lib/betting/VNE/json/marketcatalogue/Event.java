package jayeson.lib.betting.VNE.json.marketcatalogue;

public class Event {
    public String id;
    public String name;
    public String countryCode;
    public String timezone;
    public String openDate;
}
