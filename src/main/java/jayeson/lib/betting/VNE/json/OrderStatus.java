package jayeson.lib.betting.VNE.json;

public enum OrderStatus {
    PENDING,
    EXECUTION_COMPLETE,
    EXECUTABLE,
    EXPIRED
}
