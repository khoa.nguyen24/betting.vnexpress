package jayeson.lib.betting.VNE.json.eventdata;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MarketEvent {
    public String id;
    public String name;
    public String countryCode;
    public String timezone;
    public String openDate;
}
