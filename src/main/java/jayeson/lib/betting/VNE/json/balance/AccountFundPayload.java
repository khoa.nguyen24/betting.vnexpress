package jayeson.lib.betting.VNE.json.balance;

import jayeson.lib.betting.VNE.json.RpcRequest;

public class AccountFundPayload extends RpcRequest<AccountFundParam> {

    public AccountFundPayload() {
        super();
        this.method = "AccountAPING/v1.0/getAccountFunds";

        // The Global Exchange wallet
        params = new AccountFundParam("UK");
    }
}
