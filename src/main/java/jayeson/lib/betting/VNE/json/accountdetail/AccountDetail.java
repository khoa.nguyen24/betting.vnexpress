package jayeson.lib.betting.VNE.json.accountdetail;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountDetail {
    public String currencyCode;
    public String firstName;
    public String lastName;
    public String localeCode;
    public String region;
    public String timezone;
    public Double discountRate;
    public Integer pointsBalance;
    public String countryCode;
}
