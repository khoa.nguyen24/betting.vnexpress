package jayeson.lib.betting.VNE.json.listcurrentorders;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jayeson.lib.betting.VNE.json.OrderType;
import jayeson.lib.betting.VNE.json.PriceSize;
import jayeson.lib.betting.VNE.json.OrderStatus;
import jayeson.lib.betting.VNE.json.PersistenceType;
import jayeson.lib.feed.api.LBType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CurrentOrderSummary {
    public String betId;
    public String marketId;
    public Long selectionId;
    public Double handicap;
    public PriceSize priceSize;
    public Double bspLiability;
    public LBType side;
    public OrderStatus status;
    public PersistenceType persistenceType;
    public OrderType orderType;
    public String placedDate;
    public String matchedDate;
    public Double averagePriceMatched;
    public Double sizeMatched;
    public Double sizeRemaining;
    public Double sizeLapsed;
    public Double sizeCancelled;
    public Double sizeVoided;
    public String regulatorAuthCode;
    public String regulatorCode;
    public String customerOrderRef;
    public String customerStrategyRef;
}
