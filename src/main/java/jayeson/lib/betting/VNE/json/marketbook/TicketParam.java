package jayeson.lib.betting.VNE.json.marketbook;

import jayeson.lib.betting.VNE.VNEUtility;
import jayeson.lib.betting.api.actioncontext.BetInfoContext;
import jayeson.lib.betting.core.exception.BuildRequestFailException;

import java.util.ArrayList;
import java.util.List;

public class TicketParam {
    public List<String> marketIds;
    public PriceProjection priceProjection;

    public TicketParam(){
        this.marketIds = new ArrayList<>();
    }

    public TicketParam(BetInfoContext context) throws BuildRequestFailException {
        this();
        this.marketIds.add(VNEUtility.getMarketId(context));
        this.priceProjection = new PriceProjection();

    }

    public TicketParam(List<String> lstMarketId) throws BuildRequestFailException {
        this();
        this.marketIds.addAll(lstMarketId);
        this.priceProjection = new PriceProjection();

    }

    @Override
    public String toString() {
        return "BetParam{" +
                "marketIds='" + marketIds + '\'' +
                ", priceProjection=" + priceProjection + '}';
    }
}
