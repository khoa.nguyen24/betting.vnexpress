package jayeson.lib.betting.VNE.json.marketbook;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Arrays;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PriceProjection {
    public String virtualise;
    public List<String> priceData;

    public PriceProjection() {
        this.priceData = Arrays.asList("EX_BEST_OFFERS");
        this.virtualise = "true";
    }
}
