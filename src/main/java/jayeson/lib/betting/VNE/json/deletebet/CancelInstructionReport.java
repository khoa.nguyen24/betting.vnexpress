package jayeson.lib.betting.VNE.json.deletebet;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CancelInstructionReport {
    public String status;
    public CancelInstruction instruction;
    public double sizeCancelled;
    public Date cancelledDate;

    @Override
    public String toString() {
        return "CancelInstructionReport{" +
                "status='" + status + '\'' +
                ", instruction=" + instruction +
                ", sizeCancelled=" + sizeCancelled +
                ", cancelledDate=" + cancelledDate +
                '}';
    }
}