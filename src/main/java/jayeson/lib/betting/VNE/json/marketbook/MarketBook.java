package jayeson.lib.betting.VNE.json.marketbook;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MarketBook {
    public String marketId;
    public boolean isMarketDataDelayed;
    public String status;
    public Integer betDelay;
    public boolean bspReconciled;
    public boolean complete;
    public boolean inplay;
    public Integer numberOfWinners;
    public Integer numberOfRunners;
    public Integer numberOfActiveRunners;
    public Date lastMatchTime;
    public Double totalMatched;
    public Double totalAvailable;
    public boolean crossMatching;
    public boolean runnersVoidable;
    public String version;
    @JsonProperty("runners")
    public List<Runner> runners;


}