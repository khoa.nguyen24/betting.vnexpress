package jayeson.lib.betting.VNE.json.marketbook;

import jayeson.lib.betting.api.actioncontext.BetInfoContext;
import jayeson.lib.betting.VNE.json.RpcRequest;
import jayeson.lib.betting.core.exception.BuildRequestFailException;

import jayeson.lib.betting.exception.InvalidAttributeException;

import java.util.List;

public class TicketRequest extends RpcRequest<TicketParam> {
    /**
     * [{
     * "jsonrpc": "2.0",
     * "method": "SportsAPING/v1.0/listMarketBook",
     * "params": {
     *      "marketIds":["1.127771425"],
     *      "priceProjection":{
     *          "priceData":["EX_BEST_OFFERS"],
     *          "virtualise":"true"
     *      }},
     * "id": 1
     * }]
     *
     */

    public TicketRequest() {
        super();
        this.method = "SportsAPING/v1.0/listMarketBook";
    }

    public TicketRequest(BetInfoContext context) throws BuildRequestFailException, InvalidAttributeException {
        this();
        this.params = new TicketParam(context);
    }

    public TicketRequest(List<String> marketIds) throws BuildRequestFailException, InvalidAttributeException {
        this();
        this.params = new TicketParam(marketIds);
    }
}