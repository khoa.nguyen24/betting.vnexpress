package jayeson.lib.betting.VNE.json.login;

public class LoginUserResponse {
    private int error;
    private String url_redirect;

    public static VNEUserProfile profile;

    public static class VNEUserProfile {
        long user_id;
        String user_session;

        public long getUser_id() {
            return user_id;
        }

        public String getUser_session() {
            return user_session;
        }

        public void setUser_id(long user_id) {
            this.user_id = user_id;
        }

        public void setUser_session(String user_session) {
            this.user_session = user_session;
        }

        @Override
        public String toString() {
            return "VNEUserProfile{" +
                    "user_id=" + user_id +
                    ", user_session='" + user_session + '\'' +
                    '}';
        }
    }

    public int getError() {
        return error;
    }

    public String getUrl_redirect() {
        return url_redirect;
    }

    public VNEUserProfile getProfile() {
        return profile;
    }

    public void setError(int error) {
        this.error = error;
    }

    public void setUrl_redirect(String url_redirect) {
        this.url_redirect = url_redirect;
    }

    public void setProfile(VNEUserProfile profile) {
        this.profile = profile;
    }

    @Override
    public String toString() {
        return "LogedUser{" +
                "error=" + error +
                ", url_redirect='" + url_redirect + '\'' +
                ", profile=" + profile +
                '}';
    }
}
