package jayeson.lib.betting.VNE.json.listcurrentorders;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CurrentOrderParams {
    public Integer fromRecord;
    public Integer recordCount;
}
