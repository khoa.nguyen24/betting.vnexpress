package jayeson.lib.betting.VNE.json.marketcatalogue;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jayeson.lib.betting.VNE.json.RpcResponse;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MarketCatalogueResponse extends RpcResponse<MarketCatalogue[]> {
}
