package jayeson.lib.betting.VNE.json.listcurrentorders;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jayeson.lib.betting.VNE.json.RpcResponse;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ListCurrentOrdersResponse extends RpcResponse<CurrentOrderSummaryReport> {
}
