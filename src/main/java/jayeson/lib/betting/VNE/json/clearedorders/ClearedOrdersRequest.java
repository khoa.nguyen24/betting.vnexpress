package jayeson.lib.betting.VNE.json.clearedorders;

import jayeson.lib.betting.VNE.json.RpcRequest;

public class ClearedOrdersRequest extends RpcRequest<ClearedOrdersParams> {
    public ClearedOrdersRequest() {
        super();
        method = "SportsAPING/v1.0/listClearedOrders";
    }
}
