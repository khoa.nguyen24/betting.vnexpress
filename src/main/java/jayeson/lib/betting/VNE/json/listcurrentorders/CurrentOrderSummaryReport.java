package jayeson.lib.betting.VNE.json.listcurrentorders;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CurrentOrderSummaryReport {
    public List<CurrentOrderSummary> currentOrders;
    public boolean moreAvailable;
}
