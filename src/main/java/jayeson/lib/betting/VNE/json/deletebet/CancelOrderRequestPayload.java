package jayeson.lib.betting.VNE.json.deletebet;

import jayeson.lib.betting.VNE.json.RpcRequest;

public class CancelOrderRequestPayload extends RpcRequest<CancelOrderParam> {

    public CancelOrderRequestPayload() {
        super();
        this.method = "SportsAPING/v1.0/cancelOrders";
        params = new CancelOrderParam();
    }

    public CancelOrderRequestPayload(String marketId, String betId) {
        this();
        params = new CancelOrderParam(marketId, betId);
    }
}