package jayeson.lib.betting.VNE.json.eventdata;

import jayeson.lib.betting.VNE.json.RpcRequest;
import jayeson.lib.betting.core.exception.BuildRequestFailException;
import jayeson.lib.betting.exception.InvalidAttributeException;

import java.util.List;

public class MatchRequest extends RpcRequest<MarketParam> {
    /**
     * [{
     * 	"jsonrpc":"2.0",
     * 	"method":"SportsAPING/v1.0/listEvents",
     * 	"params":{
     *         "filter":{
     *             "inPlayOnly": true,  //remove
     *             "eventTypeIds": [1,7522,2],
     *             "marketTypeCodes": ["MATCH_ODDS", "HALF_TIME", "OVER_UNDER_05", "OVER_UNDER_10", "OVER_UNDER_15", "OVER_UNDER_20", "OVER_UNDER_25", "OVER_UNDER_30", "OVER_UNDER_35", "OVER_UNDER_40", "OVER_UNDER_45", "OVER_UNDER_50", "FIRST_HALF_GOALS_05", "FIRST_HALF_GOALS_10", "FIRST_HALF_GOALS_15", "FIRST_HALF_GOALS_20", "FIRST_HALF_GOALS_25", "FIRST_HALF_GOALS_30", "ASIAN_HANDICAP", "SET_WINNER", "MONEY_LINE"]
     *         }
     *        },
     * 	"id": 1
     * }]
     *
     */

    public MatchRequest() {
        super();
        this.method = "SportsAPING/v1.0/listEvents";
    }

    public MatchRequest(List<String> eventTypeIds, Boolean inPlayOnly, List<String> eventIds, List<String> marketTypeCodes) throws BuildRequestFailException, InvalidAttributeException {
        this();
        this.params = new MarketParam(eventTypeIds, inPlayOnly, eventIds, marketTypeCodes);
    }
}
