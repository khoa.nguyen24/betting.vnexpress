package jayeson.lib.betting.VNE.json.login;

public class GetProfileResponse {
    int error;
    LogedUserProfile profile;

    public void setError(int error) {
        this.error = error;
    }

    public void setProfile(LogedUserProfile profile) {
        this.profile = profile;
    }

    public int getError() {
        return error;
    }

    public LogedUserProfile getProfile() {
        return profile;
    }

    @Override
    public String toString() {
        return "GetProfileResponse{" +
                "error=" + error +
                ", profile=" + profile +
                '}';
    }
}
