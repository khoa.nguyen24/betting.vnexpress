package jayeson.lib.betting.VNE.json.accountfunds;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jayeson.lib.betting.VNE.json.RpcResponse;

/**
 * {
 *         "jsonrpc": "2.0",
 *         "result": {
 *             "availableToBetBalance": 2650.61,
 *             "exposure": -12.33,
 *             "retainedCommission": 0,
 *             "exposureLimit": -100000,
 *             "discountRate": 60,
 *             "pointsBalance": 2512,
 *             "wallet": "UK"
 *         },
 *         "id": 1
 * }
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountFundsResponse extends RpcResponse<AccountFund> {
}
