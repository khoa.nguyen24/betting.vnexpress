package jayeson.lib.betting.VNE.json;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class BetfairGetEventParam {
    @JsonProperty("eventIds")
    private List<String> eventIds;

    public List<String> getEventIds() {
        return eventIds;
    }
}
