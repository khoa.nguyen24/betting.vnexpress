package jayeson.lib.betting.VNE.json.accountfunds;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountFund {
    public Float availableToBetBalance;
    public Float exposure;
    public Integer retainedCommission;
    public Integer exposureLimit;
    public Integer discountRate;
    public Integer pointsBalance;
    public String wallet;
}
