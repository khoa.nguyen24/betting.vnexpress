package jayeson.lib.betting.VNE.json.deletebet;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CancelExecutionReport {
    public String status;
    public String marketId;
    public List<CancelInstructionReport> instructionReports;

    @Override
    public String toString() {
        return "CancelExecutionReport{" +
                "status='" + status + '\'' +
                ", marketId='" + marketId + '\'' +
                ", instructionReports=" + instructionReports +
                '}';
    }
}
