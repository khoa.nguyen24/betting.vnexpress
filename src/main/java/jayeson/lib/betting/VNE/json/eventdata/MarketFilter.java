package jayeson.lib.betting.VNE.json.eventdata;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MarketFilter {
    public List<String> eventIds;
    public Boolean inPlayOnly;
    public List<String> marketTypeCodes;
    public List<String> eventTypeIds;

    public MarketFilter(List<String> eventTypeIds, Boolean inPlayOnly, List<String> eventIds, List<String> marketTypeCodes){
        this.marketTypeCodes = marketTypeCodes;
        this.eventTypeIds = eventTypeIds;
        if(inPlayOnly != null) this.inPlayOnly = inPlayOnly;
        if(eventIds != null && !eventIds.isEmpty()) this.eventIds = eventIds;
    }
}
