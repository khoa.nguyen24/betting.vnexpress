package jayeson.lib.betting.VNE.json.marketbook;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import jayeson.lib.betting.VNE.json.PriceSize;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Ex {
    @JsonProperty("availableToBack")
    public List<PriceSize> availableToBack;
    @JsonProperty("availableToLay")
    public List<PriceSize> availableToLay;
    @JsonProperty("tradedVolume")
    public List<PriceSize> tradedVolume;
}