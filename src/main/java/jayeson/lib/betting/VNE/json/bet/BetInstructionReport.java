package jayeson.lib.betting.VNE.json.bet;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BetInstructionReport {
	@JsonProperty("instruction")
	public BetInstruction instruction;
	@JsonProperty("betId")
	public String betId;
	@JsonProperty("placedDate")
	public Date placedDate;
	@JsonProperty("averagePriceMatched")
	public double averagePriceMatched;
	@JsonProperty("sizeMatched")
	public double sizeMatched;
	@JsonProperty("status")
	public String status;
	@JsonProperty("errorCode")
	public String errorCode;
	@JsonProperty("orderStatus")
	public String orderStatus;

	public BetInstructionReport() { }

	@Override
	public String toString() {
		return "BetInstructionReport{" +
				"instruction=" + instruction +
				", betId='" + betId + '\'' +
				", placedDate=" + placedDate +
				", averagePriceMatched=" + averagePriceMatched +
				", sizeMatched=" + sizeMatched +
				", status='" + status + '\'' +
				", errorCode='" + errorCode + '\'' +
				", orderStatus='" + orderStatus + '\'' +
				'}';
	}
}
