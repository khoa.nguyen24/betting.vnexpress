package jayeson.lib.betting.VNE.json.marketcatalogue;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PriceLadderDescription {
    public PriceLadderType type;

    public PriceLadderDescription() {
    }

    public PriceLadderDescription(PriceLadderType type) {
        this.type = type;
    }
}
