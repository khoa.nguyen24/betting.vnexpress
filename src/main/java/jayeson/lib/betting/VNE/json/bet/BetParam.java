package jayeson.lib.betting.VNE.json.bet;

import jayeson.lib.betting.VNE.VNEUtility;
import jayeson.lib.betting.VNE.json.marketbook.MarketVersion;
import jayeson.lib.betting.VNE.json.marketcatalogue.Description;
import jayeson.lib.betting.api.actioncontext.BetContext;
import jayeson.lib.betting.VNE.betticket.BetFairBetTicketResult;
import jayeson.lib.betting.core.exception.BuildRequestFailException;
import jayeson.lib.betting.exception.InvalidAttributeException;
import jayeson.lib.betting.exception.NewValueException;

import java.util.ArrayList;
import java.util.List;

public class BetParam {
    public String marketId;
    public List<BetInstruction> instructions;
    public MarketVersion marketVersion;

    public BetParam() {
        this.instructions = new ArrayList<>();
    }

    public BetParam(BetFairBetTicketResult ticket, BetContext betContext, Description marketDescription) throws BuildRequestFailException, InvalidAttributeException, NewValueException {
        this();

        this.marketId = VNEUtility.getMarketId(ticket.getBetInfoContext());
        BetInstruction instruction = new BetInstruction(ticket, betContext, marketDescription);
        instructions.add(instruction);
        this.marketVersion = new MarketVersion(ticket.getMarketVersion());
    }
    
    @Override
    public String toString() {
        return "BetParam{" +
                "marketId='" + marketId + '\'' +
                ", instructions=" + instructions +
                '}';
    }
}
