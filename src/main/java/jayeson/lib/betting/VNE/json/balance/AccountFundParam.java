package jayeson.lib.betting.VNE.json.balance;

public class AccountFundParam {
    private String wallet;

    public AccountFundParam(String wallet) {
        this.wallet = wallet;
    }

    public String getWallet() {
        return wallet;
    }

    public void setWallet(String wallet) {
        this.wallet = wallet;
    }

    @Override
    public String toString() {
        return "AccountFundParam{" +
                "wallet='" + wallet + '\'' +
                '}';
    }
}
