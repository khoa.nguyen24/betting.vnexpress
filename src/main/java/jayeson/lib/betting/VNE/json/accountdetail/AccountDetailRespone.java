package jayeson.lib.betting.VNE.json.accountdetail;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jayeson.lib.betting.VNE.json.RpcResponse;

/**
 * [
 * {
 * "jsonrpc":"2.0",
 * "result":{
 * "currencyCode":"GBP",
 * "firstName":"Ole Group",
 * "lastName":"International BV",
 * "localeCode":"en_GB",
 * "region":"GBR",
 * "timezone":"Europe/London",
 * "discountRate":50.0,
 * "pointsBalance":0,
 * "countryCode":"AN"
 * },
 * "id":1
 * }
 * ]
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountDetailRespone extends RpcResponse<AccountDetail> {
}
