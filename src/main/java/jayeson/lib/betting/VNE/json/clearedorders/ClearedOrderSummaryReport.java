package jayeson.lib.betting.VNE.json.clearedorders;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ClearedOrderSummaryReport {
    public List<ClearedOrderSummary> clearedOrders;
    public boolean moreAvailable;
}