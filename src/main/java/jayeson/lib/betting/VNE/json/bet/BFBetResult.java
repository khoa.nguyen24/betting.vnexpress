package jayeson.lib.betting.VNE.json.bet;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BFBetResult {
    public String marketId;
    public List<BetInstructionReport> instructionReports;
    public String status;
    public String errorCode;

    @Override
    public String toString() {
        return "BetResult{" +
                "marketId='" + marketId + '\'' +
                ", instructionReports=" + instructionReports +
                ", status='" + status + '\'' +
                ", errorCode='" + errorCode + '\'' +
                '}';
    }
}
