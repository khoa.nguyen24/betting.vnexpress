package jayeson.lib.betting.VNE.json.listcurrentorders;

import jayeson.lib.feed.api.SportType;
import jayeson.lib.feed.api.twoside.PivotType;
import jayeson.lib.feed.api.twoside.TargetType;

import java.util.ArrayList;
import java.util.List;

public class CurrentOrderType {
    private SportType sportType;
    private PivotType pivotType;
    private List<TargetType> targetTypes;

    public CurrentOrderSummary currentOrderSummary;

    public CurrentOrderType(CurrentOrderSummary currentOrderSummary) {
        this.currentOrderSummary = currentOrderSummary;
        this.targetTypes = new ArrayList<>();
    }

    public SportType getSportType() {
        return sportType;
    }

    public void setSportType(SportType sportType) {
        this.sportType = sportType;
    }

    public PivotType getPivotType() {
        return pivotType;
    }

    public void setPivotType(PivotType pivotType) {
        this.pivotType = pivotType;
    }

    public List<TargetType> getTargetTypes() {
        return targetTypes;
    }

    public void setTargetTypes(List<TargetType> targetTypes) {
        this.targetTypes = targetTypes;
    }

    public CurrentOrderSummary getCurrentOrderSummary() {
        return currentOrderSummary;
    }

    public void setCurrentOrderSummary(CurrentOrderSummary currentOrderSummary) {
        this.currentOrderSummary = currentOrderSummary;
    }
}
