package jayeson.lib.betting.VNE.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PriceSize {
    public Double price;
    public Double size;
}
