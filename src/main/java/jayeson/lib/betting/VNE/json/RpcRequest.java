package jayeson.lib.betting.VNE.json;

public abstract class RpcRequest<T> extends RpcMessage {
    public String method;
    public T params;

    public RpcRequest() {
        this.jsonrpc = "2.0";
        this.id = 1;
    }
}
