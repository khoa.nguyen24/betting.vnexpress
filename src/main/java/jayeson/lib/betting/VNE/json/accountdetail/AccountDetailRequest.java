package jayeson.lib.betting.VNE.json.accountdetail;

import jayeson.lib.betting.VNE.json.RpcRequest;

public class AccountDetailRequest extends RpcRequest<String> {
    public AccountDetailRequest() {
        method = "AccountAPING/v1.0/getAccountDetails";
        params = "{}";
    }
}
