package jayeson.lib.betting.VNE.json.login;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jayeson.lib.betting.VNE.json.RpcResponse;

import java.util.List;

/**
 * [
 *     {
 *         "jsonrpc": "2.0",
 *         "result": [
 *             {
 *                 "appName": "OleSystemBettingOleeu1",
 *                 "appId": 88401,
 *                 "appVersions": [
 *                     {
 *                         "owner": "oleeu1",
 *                         "versionId": 81184,
 *                         "version": "1.0-DELAY",
 *                         "applicationKey": "KIfL9sRwkWM0qkcL",
 *                         "delayData": true,
 *                         "subscriptionRequired": true,
 *                         "ownerManaged": false,
 *                         "active": true
 *                     },
 *                     {
 *                         "owner": "oleeu1",
 *                         "versionId": 81183,
 *                         "version": "1.0",
 *                         "applicationKey": "BxziRKzmQlCw52gJ",
 *                         "delayData": false,
 *                         "subscriptionRequired": true,
 *                         "ownerManaged": false,
 *                         "active": false
 *                     }
 *                 ]
 *             }
 *         ],
 *         "id": 1
 *     }
 * ]
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class AppKeyRespone extends RpcResponse<List<AppKey>> {
}
