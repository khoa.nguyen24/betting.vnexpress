package jayeson.lib.betting.VNE.json.clearedorders;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ItemDescription {
    public String eventTypeDesc;
    public String eventDesc;
    public String marketDesc;
    public String marketType;
    public String marketStart;
    public String runnerDesc;
    public Integer numberOfWinners;
    public Double eachWayDivisor;

    @Override
    public String toString() {
        return "ItemDescription{" +
                "eventTypeDesc='" + eventTypeDesc + '\'' +
                ", eventDesc='" + eventDesc + '\'' +
                ", marketDesc='" + marketDesc + '\'' +
                ", marketType='" + marketType + '\'' +
                ", marketStart='" + marketStart + '\'' +
                ", runnerDesc='" + runnerDesc + '\'' +
                ", numberOfWinners=" + numberOfWinners +
                ", eachWayDivisor=" + eachWayDivisor +
                '}';
    }
}
