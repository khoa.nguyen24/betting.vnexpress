package jayeson.lib.betting.VNE.json.clearedorders;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jayeson.lib.betting.VNE.json.RpcResponse;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ClearedOrderSummaryResponse extends RpcResponse<ClearedOrderSummaryReport> {
}