package jayeson.lib.betting.VNE.json.bet;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jayeson.lib.betting.VNE.json.RpcResponse;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BetResponseData extends RpcResponse<BFBetResult> {
}
