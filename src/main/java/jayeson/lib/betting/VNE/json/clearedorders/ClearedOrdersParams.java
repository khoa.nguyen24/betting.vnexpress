package jayeson.lib.betting.VNE.json.clearedorders;

import jayeson.lib.betting.VNE.json.BFBetStatus;

import java.util.List;

public class ClearedOrdersParams {
    public BFBetStatus betStatus;
    public BFTimeRange settledDateRange;
    public int fromRecord;
    public int recordCount;
    public boolean includeItemDescription = true;
    public List<String> betIds;
}
