package jayeson.lib.betting.VNE.json.eventdata;

import jayeson.lib.betting.core.exception.BuildRequestFailException;

import java.util.List;

public class MarketParam {
    public MarketFilter filter;

    public MarketParam(List<String> eventTypeIds, boolean inPlayOnly, List<String> eventIds, List<String> marketTypeCodes) throws BuildRequestFailException {
        filter = new MarketFilter(eventTypeIds, inPlayOnly, eventIds, marketTypeCodes);
    }

    @Override
    public String toString() {
        return "MatchParam {" +
                "filter='" + filter + '}';
    }
}
