package jayeson.lib.betting.VNE.json.deletebet;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CancelInstruction {
    public String betId;

    public CancelInstruction() {
    }

    public CancelInstruction(String betId) {
        this.betId = betId;
    }

    @Override
    public String toString() {
        return "CancelInstruction{" +
                "betId='" + betId + '\'' +
                '}';
    }
}
