package jayeson.lib.betting.VNE.json.marketcatalogue;

import jayeson.lib.betting.VNE.json.RpcRequest;

public class MarketCatalogueRequest extends RpcRequest<MarketCatalogueParams> {

    public MarketCatalogueRequest() {
        super();
        this.method = "SportsAPING/v1.0/listMarketCatalogue";
    }
}
