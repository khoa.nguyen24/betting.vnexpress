package jayeson.lib.betting.VNE.json.bet;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jayeson.lib.betting.VNE.VNEUtility;
import jayeson.lib.betting.VNE.json.OrderType;
import jayeson.lib.betting.VNE.json.marketcatalogue.Description;
import jayeson.lib.betting.api.actioncontext.BetContext;
import jayeson.lib.betting.api.actioncontext.BetInfoContext;
import jayeson.lib.betting.api.actionresults.BetTicketResult;
import jayeson.lib.betting.core.exception.BuildRequestFailException;
import jayeson.lib.betting.exception.InvalidAttributeException;
import jayeson.lib.betting.exception.NewValueException;
import jayeson.lib.feed.api.LBType;
import jayeson.lib.feed.api.twoside.PivotType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BetInstruction {
    public long selectionId;
    public double handicap;
    public LimitOrder limitOrder;
    public OrderType orderType;
    public String side;

    public BetInstruction() { }

    public BetInstruction(BetTicketResult ticket, BetContext betContext, Description marketDescription)
            throws BuildRequestFailException, InvalidAttributeException, NewValueException {
        BetInfoContext betInfoContext = ticket.getBetInfoContext();
        selectionId = Long.parseLong(VNEUtility.findSelectionId(betInfoContext));
        if(PivotType.HDP.equals(ticket.getBetInfoContext().getPivotType())) {
            handicap = VNEUtility.toHdpPivot(ticket.getBetInfoContext());
        } else if(PivotType.TOTAL.equals(ticket.getBetInfoContext().getPivotType())) {
            double pivotValue = Double.parseDouble(ticket.getBetInfoContext().getPivotValue());
            if (VNEUtility.isGoalLinesSelection(ticket.getBetInfoContext().getSportType(), pivotValue)) {
                handicap = pivotValue;
            } else {
                // Soccer & Tennis
                handicap = 0;
            }
        } else {
            // One_Two
            handicap = 0;
        }

        orderType = OrderType.LIMIT;

        side = "BACK";
        if(betInfoContext.getLbType().equals(LBType.LAY)) {
            side = "LAY";
        }

        limitOrder = new LimitOrder(ticket, betContext, marketDescription);
    }

    @Override
    public String toString() {
        return "BetInstruction{" +
                "selectionId='" + selectionId + '\'' +
                ", handicap=" + handicap +
                ", limitOrder=" + limitOrder +
                ", orderType=" + orderType +
                ", side='" + side + '\'' +
                '}';
    }
}
