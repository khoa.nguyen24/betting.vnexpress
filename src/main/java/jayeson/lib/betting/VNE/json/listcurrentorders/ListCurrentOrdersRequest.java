package jayeson.lib.betting.VNE.json.listcurrentorders;

import jayeson.lib.betting.VNE.json.RpcRequest;

public class ListCurrentOrdersRequest extends RpcRequest<CurrentOrderParams> {

    public ListCurrentOrdersRequest() {
        super();
        this.method = "SportsAPING/v1.0/listCurrentOrders";
    }
}
