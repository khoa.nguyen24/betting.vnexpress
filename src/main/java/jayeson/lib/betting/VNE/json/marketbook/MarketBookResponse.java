package jayeson.lib.betting.VNE.json.marketbook;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jayeson.lib.betting.VNE.json.RpcResponse;

import java.util.List;

/**
 * [
 *     {
 *         "jsonrpc": "2.0",
 *         "result": [
 *             {
 *                 "marketId": "1.170443743",
 *                 "isMarketDataDelayed": false,
 *                 "status": "CLOSED",
 *                 "betDelay": 5,
 *                 "bspReconciled": false,
 *                 "complete": true,
 *                 "inplay": false,
 *                 "numberOfWinners": 1,
 *                 "numberOfRunners": 3,
 *                 "numberOfActiveRunners": 0,
 *                 "totalMatched": 0.0,
 *                 "totalAvailable": 0.0,
 *                 "crossMatching": false,
 *                 "runnersVoidable": false,
 *                 "version": 3243065462,
 *                 "runners": [
 *                     {
 *                         "selectionId": 44785,
 *                         "handicap": 0.0,
 *                         "status": "LOSER",
 *                         "ex": {
 *                             "availableToBack": [],
 *                             "availableToLay": [],
 *                             "tradedVolume": []
 *                         }
 *                     },
 *                     {
 *                         "selectionId": 5774350,
 *                         "handicap": 0.0,
 *                         "status": "WINNER",
 *                         "ex": {
 *                             "availableToBack": [],
 *                             "availableToLay": [],
 *                             "tradedVolume": []
 *                         }
 *                     },
 *                     {
 *                         "selectionId": 58805,
 *                         "handicap": 0.0,
 *                         "status": "LOSER",
 *                         "ex": {
 *                             "availableToBack": [],
 *                             "availableToLay": [],
 *                             "tradedVolume": []
 *                         }
 *                     }
 *                 ]
 *             }
 *         ],
 *         "id": 1
 *     }
 * ]
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MarketBookResponse extends RpcResponse<List<MarketBook>> {
}
