package jayeson.lib.betting.VNE.datatransfer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import jayeson.lib.betting.core.http.IResponse;
import jayeson.lib.betting.core.http.response.StringHttpResponse;

import java.io.IOException;

public class ResponseDeserializer extends StdDeserializer<IResponse>{

	private static final long serialVersionUID = 4902885588539718460L;

	public ResponseDeserializer() {
		this(null);
	}
	
	protected ResponseDeserializer(Class<IResponse> vc) {
		super(vc);
	}

	@Override
	public IResponse deserialize(JsonParser jp, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		
		JsonNode node = jp.getCodec().readTree(jp);
        String content = node.get("content").asText();
        
		return new StringHttpResponse(content, null);
	}

}
