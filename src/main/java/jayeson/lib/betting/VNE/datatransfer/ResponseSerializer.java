package jayeson.lib.betting.VNE.datatransfer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import jayeson.lib.betting.core.http.IResponse;

import java.io.IOException;

public class ResponseSerializer extends StdSerializer<IResponse> {

	private static final long serialVersionUID = 5643685044608815320L;

	public ResponseSerializer() {
		this(null);
	}
	
	protected ResponseSerializer(Class<IResponse> t) {
		super(t);
	}

	@Override
	public void serialize(IResponse value, JsonGenerator jgen, SerializerProvider provider) throws IOException {
		jgen.writeStartObject();
        jgen.writeStringField("content", value.stringContent());
        jgen.writeEndObject();
	}

}
