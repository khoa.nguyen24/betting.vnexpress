package jayeson.lib.betting.VNE.datatransfer;

import com.fasterxml.jackson.databind.ObjectMapper;
import jayeson.lib.betting.VNE.VNE;
import jayeson.lib.betting.core.dagger.accountscope.AccountScope;
import jayeson.lib.betting.core.datatransfer.BettingAccountDataDeserializer;

import javax.inject.Inject;
import javax.inject.Named;

@AccountScope
public class BetFairAccountDataDeserializer extends BettingAccountDataDeserializer<VNE.BetFairSerializableAccountData>{
	
	@Inject
	public BetFairAccountDataDeserializer(@Named("BetFairObjectMapper") ObjectMapper mapper) {
		super(mapper);
	}

	@Override
	public VNE.BetFairSerializableAccountData deserialize(byte[] serializedData) throws Exception {
		return mapper.readValue(serializedData, VNE.BetFairSerializableAccountData.class);
	}

}
