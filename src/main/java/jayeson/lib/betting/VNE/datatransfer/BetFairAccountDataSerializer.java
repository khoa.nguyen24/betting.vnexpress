package jayeson.lib.betting.VNE.datatransfer;

import com.fasterxml.jackson.databind.ObjectMapper;
import jayeson.lib.betting.VNE.VNE;
import jayeson.lib.betting.core.dagger.accountscope.AccountScope;
import jayeson.lib.betting.core.datatransfer.BettingAccountDataSerializer;

import javax.inject.Inject;
import javax.inject.Named;

@AccountScope
public class BetFairAccountDataSerializer extends BettingAccountDataSerializer<VNE.BetFairSerializableAccountData>{

	@Inject
	public BetFairAccountDataSerializer(@Named("BetFairObjectMapper") ObjectMapper mapper) {
		super(mapper);
	}

	@Override
	public byte[] serialize(VNE.BetFairSerializableAccountData accountData) throws Exception {
		return mapper.writeValueAsBytes(accountData);
	}

}
