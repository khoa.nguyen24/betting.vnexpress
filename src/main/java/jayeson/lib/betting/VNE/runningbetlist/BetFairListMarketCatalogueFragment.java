package jayeson.lib.betting.VNE.runningbetlist;

import jayeson.lib.betting.VNE.VNE;
import jayeson.lib.betting.VNE.VNEURIDefinition;
import jayeson.lib.betting.VNE.VNEUtility;
import jayeson.lib.betting.VNE.json.listcurrentorders.CurrentOrderType;
import jayeson.lib.betting.VNE.json.marketcatalogue.*;
import jayeson.lib.betting.api.actionresults.ActionResultCode;
import jayeson.lib.betting.api.actionresults.ListActionResult;
import jayeson.lib.betting.api.actionresults.MiniBetResult;
import jayeson.lib.betting.api.datastructure.BADRuntime;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.VNE.json.marketcatalogue.*;
import jayeson.lib.betting.core.exception.UnknownResponseException;
import jayeson.lib.betting.core.http.IClient;
import jayeson.lib.betting.core.http.IResponse;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.http.request.JsonRequest;
import jayeson.lib.betting.core.task.AsyncHttpTaskFragment;
import jayeson.lib.betting.core.task.TaskFragmentListener;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;
import jayeson.lib.betting.core.utility.BettingUtility;
import jayeson.lib.betting.exception.BettingException;
import jayeson.lib.feed.api.SportType;
import jayeson.lib.feed.api.twoside.PivotType;

import javax.inject.Inject;
import java.util.*;

public class BetFairListMarketCatalogueFragment extends AsyncHttpTaskFragment<ListActionResult<MiniBetResult>, MarketCatalogueResponse> {

    private final VNE VNEAccount;
    private final BADRuntime badRuntime;
    private String marketId;
    private Map<String, List<CurrentOrderType>> missingBet = new HashMap<>();

    @Inject
    public BetFairListMarketCatalogueFragment(AccountTaskInfo data, ITaskExecutor executor,
                                              IResponseListener responseListener, VNE account,
                                              TaskFragmentListener<ListActionResult<MiniBetResult>> listener,
                                              ListActionResult<MiniBetResult> initialResult, IClient client,
                                              BADRuntime badRuntime) {
        super(data, executor, responseListener, account, listener, initialResult, client);
        this.badRuntime = badRuntime;
        this.setDef(VNEURIDefinition.getJsonRpcURI(MarketCatalogueResponse.class));
        this.VNEAccount = account;
        needProceed = false;
    }

    @Override
    public void proceed(ListActionResult<MiniBetResult> currentResult) {
    }

    @Override
    public void execute() throws BettingException {
        MarketCatalogueRequest payload = new MarketCatalogueRequest();
        payload.params = new MarketCatalogueParams();
        payload.params.filter = new MarketCatalogueFilter();
        payload.params.filter.marketIds = new ArrayList<>(missingBet.keySet());
        payload.params.maxResults = missingBet.size();
        payload.params.marketProjection = Arrays.asList("RUNNER_DESCRIPTION", "EVENT_TYPE");

        if (VNEUtility.isNeedToResetBettingApi(client.host().url().toString())) {
            client.resetHost(VNEUtility.getBettingApiUrl(client.host().url().toString()));
        }

        JsonRequest request = new JsonRequest.Builder()
                .def(this.getDef())
                .host(client.host())
                .data(payload)
                .addHeader("X-Authentication", this.badRuntime.getLoggedInUid())
                .addHeader("X-Application", VNEAccount.getAppKey())
                .build();

        executeRequest(request, true);
    }

    @Override
    public void consumeResponse(IResponse<MarketCatalogueResponse> response) throws BettingException {
        account.getCommonLogger().info("RESPONSE_READY_{}", response.getStatusLine());
        if (response.getStatusLine().getCode() != 200) {
            account.getCommonLogger().error("FAIL to get market catalogue {}", response);
            result().setCode(ActionResultCode.FAILED);
            result().setMessage("The request failed with http code " + response.getStatusLine().getCode());
            needProceed = false;
            return;
        }

        MarketCatalogueResponse catalogueResponse = response.content();

        if (catalogueResponse.error != null) {
            account.getCommonLogger().error("Failed to get market catalogue: {} ", BettingUtility.writeObjectToJson(catalogueResponse.error));
            throw new UnknownResponseException("Failed to get market catalogue", getDef(), "");
        }

        if (catalogueResponse.result == null || catalogueResponse.result.length == 0) {
            account.getCommonLogger().error("FAIL to parse market catalogue {}", response);
            result().setCode(ActionResultCode.FAILED);
            result().setMessage("Unable to extract market data");
            needProceed = false;
            return;
        }

        Map<String, CurrentOrderType> currentOrderTypeMap = new HashMap<>();
        for (MarketCatalogue m : catalogueResponse.result) {
            List<CurrentOrderType> currentOrderTypes = missingBet.get(m.marketId);

            SportType sportType = VNEUtility.getSportTypeFromMarketCatalogue(m);
            if (sportType != null) {
                PivotType pivotType = VNEUtility.getPivotTypeFromMarketCatalogue(m, sportType);

                if (pivotType != null) {
                    currentOrderTypes.forEach(b -> b.setPivotType(pivotType));
                    VNEUtility.extractTargetType(pivotType, m.runners, currentOrderTypes, currentOrderTypeMap);
                }
            }
        }

        completeResult(currentOrderTypeMap);
    }

    private void completeResult(Map<String, CurrentOrderType> currentOrderTypeMap) {
        result().getResultList().forEach(
                r -> {
                    if (currentOrderTypeMap.containsKey(r.getBetId())) {
                        CurrentOrderType currentOrderType = currentOrderTypeMap.get(r.getBetId());
                        if ((r.getTargetTypes() == null || r.getTargetTypes().isEmpty())
                                && currentOrderType.getTargetTypes() != null) {
                            r.setTargetTypes(currentOrderType.getTargetTypes());

                            if (!VNEUtility.is1X2Game(r.getTargetType())) { //not EU odd format
                                r.setBetOdd(VNEUtility.oddTo3Decimal(r.getBetOdd() - 1));
                            }
                        } else {
                            account.getCommonLogger().error("Not able to identify target type {}", r);
                            r.setCode(ActionResultCode.FAILED);
                        }

                        account.getRecentBetCache().put(account.getBAD().getSportbook().getId(), account.getUsername(), r.getBetId(), r);
                    }
                }
        );
    }


    @Override
    public boolean logHttpRequest() {
        return true;
    }

    public String getMarketId() {
        return marketId;
    }

    public void setMarketId(String marketId) {
        this.marketId = marketId;
    }

    public Map<String, List<CurrentOrderType>> getMissingBet() {
        return missingBet;
    }

    public void setMissingBet(Map<String, List<CurrentOrderType>> missingBet) {
        this.missingBet = missingBet;
    }
}
