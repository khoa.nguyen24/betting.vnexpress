package jayeson.lib.betting.VNE.runningbetlist;

import dagger.Lazy;
import jayeson.lib.betting.api.actionresults.ListActionResult;
import jayeson.lib.betting.api.actionresults.MiniBetResult;
import jayeson.lib.betting.api.task.AccountTaskHandler;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.api.task.TaskObservable;
import jayeson.lib.betting.core.BettingAccount;
import jayeson.lib.betting.core.dagger.qualifier.TaskId;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.task.GetMiniBetListTask;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;

import javax.inject.Inject;

public class BetFairRunningBetListTask extends GetMiniBetListTask<MiniBetResult> {
    private static final int WORK_LOAD = 10;
    private final Lazy<BetFairListCurrentOrdersFragment> listCurrentOrdersFragment;

    @Inject
    public BetFairRunningBetListTask(ITaskExecutor executor, IResponseListener responseListener, AccountTaskInfo data,
                                     BettingAccount account, @TaskId String id,
                                     AccountTaskHandler<ListActionResult<MiniBetResult>> handler,
                                     TaskObservable<AccountTaskInfo> observable, ListActionResult<MiniBetResult> result,
                                     Lazy<BetFairListCurrentOrdersFragment> listCurrentOrdersFragment) {
        super(executor, responseListener, data, account, id, WORK_LOAD, handler, observable, result);
        this.listCurrentOrdersFragment = listCurrentOrdersFragment;
    }

    @Override
    protected void execute() {
        account.getCommonLogger().info("LAUNCH_LIST_CURRENT_ORDER_FRAGMENT");
        executor.submit(listCurrentOrdersFragment.get());
    }
}
