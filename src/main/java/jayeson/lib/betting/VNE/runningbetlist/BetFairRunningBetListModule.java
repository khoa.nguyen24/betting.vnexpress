package jayeson.lib.betting.VNE.runningbetlist;

import dagger.Module;
import dagger.Provides;
import jayeson.lib.betting.api.actionresults.ListActionResult;
import jayeson.lib.betting.api.actionresults.ListMiniBetResult;
import jayeson.lib.betting.api.actionresults.MiniBetResult;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;
import jayeson.lib.betting.core.task.TaskFragmentListener;

@Module
public class BetFairRunningBetListModule {

    @TaskScope
    @Provides
    TaskFragmentListener<ListActionResult<MiniBetResult>> provideTaskFragmentListener(BetFairRunningBetListTask task) {
        return task;
    }

    @TaskScope
    @Provides
    ListActionResult<MiniBetResult> provideResult() {
        return new ListMiniBetResult();
    }
}
