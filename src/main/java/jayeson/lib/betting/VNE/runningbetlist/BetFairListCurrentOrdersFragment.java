package jayeson.lib.betting.VNE.runningbetlist;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import dagger.Lazy;
import jayeson.lib.betting.VNE.VNE;
import jayeson.lib.betting.VNE.VNEURIDefinition;
import jayeson.lib.betting.VNE.VNEUtility;
import jayeson.lib.betting.VNE.json.listcurrentorders.*;
import jayeson.lib.betting.api.actionresults.ActionResultCode;
import jayeson.lib.betting.api.actionresults.ListActionResult;
import jayeson.lib.betting.api.actionresults.MiniBetResult;
import jayeson.lib.betting.api.datastructure.BADRuntime;
import jayeson.lib.betting.api.datastructure.BetStatus;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.VNE.json.OrderStatus;
import jayeson.lib.betting.VNE.json.listcurrentorders.*;
import jayeson.lib.betting.core.exception.UnknownResponseException;
import jayeson.lib.betting.core.http.IClient;
import jayeson.lib.betting.core.http.IResponse;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.http.request.JsonRequest;
import jayeson.lib.betting.core.task.AsyncHttpTaskFragment;
import jayeson.lib.betting.core.task.TaskFragmentListener;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;
import jayeson.lib.betting.core.utility.BettingUtility;
import jayeson.lib.betting.exception.BettingException;
import jayeson.lib.betting.exception.ParsingFailException;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class BetFairListCurrentOrdersFragment extends AsyncHttpTaskFragment<ListActionResult<MiniBetResult>, ListCurrentOrdersResponse[]> {

    private final BADRuntime badRuntime;
    private final VNE VNEAccount;
    private int recordIndex;
    private static final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    private final Map<String, List<CurrentOrderType>> missingCachedBet = new HashMap<>();
    private boolean haveMissingTargetType = false;
    private final Lazy<BetFairListMarketCatalogueFragment> betFairListMarketCatalogueFragmentLazy;

    @Inject
    public BetFairListCurrentOrdersFragment(AccountTaskInfo data, ITaskExecutor executor,
                                            IResponseListener responseListener, VNE account,
                                            TaskFragmentListener<ListActionResult<MiniBetResult>> listener,
                                            ListActionResult<MiniBetResult> initialResult, IClient client,
                                            BADRuntime badRuntime, Lazy<BetFairListMarketCatalogueFragment> betFairListMarketCatalogueFragmentLazy) {
        super(data, executor, responseListener, account, listener, initialResult, client);
        this.badRuntime = badRuntime;
        this.setDef(VNEURIDefinition.getJsonRpcURI(ListCurrentOrdersResponse[].class));
        recordIndex = 0;
        this.VNEAccount = account;
        this.betFairListMarketCatalogueFragmentLazy = betFairListMarketCatalogueFragmentLazy;
    }

    @Override
    public void execute() throws BettingException {
        ArrayNode arrayNode = JsonNodeFactory.instance.arrayNode();
        ListCurrentOrdersRequest listRequest = new ListCurrentOrdersRequest();
        listRequest.params = new CurrentOrderParams();
        listRequest.params.fromRecord = recordIndex;

        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.convertValue(listRequest, JsonNode.class);
        arrayNode.add(node);

        if (VNEUtility.isNeedToResetBettingApi(client.host().url().toString())) {
            client.resetHost(VNEUtility.getBettingApiUrl(client.host().url().toString()));
        }

        // Build request
        JsonRequest request = new JsonRequest.Builder()
                .def(this.getDef())
                .host(client.host())
                .header("X-Authentication", this.badRuntime.getLoggedInUid())
                .header("X-Application", VNEAccount.getAppKey())
                .data(arrayNode)
                .build();

        executeRequest(request, true);
    }

    @Override
    public void consumeResponse(IResponse<ListCurrentOrdersResponse[]> response) throws BettingException {
        if (response == null || response.content() == null) {
            account.getCommonLogger().error("Fail to get list current orders");
            needProceed = false;
            result().setCode(ActionResultCode.FAILED);
            return;
        }
        ListCurrentOrdersResponse listOrdersResponse = response.content()[0];

        if(listOrdersResponse.error != null) {
            account.getCommonLogger().error("Failed to get account detail: {} ", BettingUtility.writeObjectToJson(listOrdersResponse.error));
            throw new UnknownResponseException("Failed to get account detail", getDef(), "");
        }

        CurrentOrderSummaryReport report = listOrdersResponse.result;

        if (report.currentOrders != null) {
            result().getResultList().addAll(parseMiniBetList(report.currentOrders));

            // Update index and check if more records is available
            recordIndex += report.currentOrders.size();
        }

        if (!report.moreAvailable) {
            setResultCode();
            haveMissingTargetType = !missingCachedBet.isEmpty();
            needProceed = haveMissingTargetType;
        }
    }

    private void setResultCode() {
        long size = result().getResultList().size();
        long successfulCount = result().getResultList().stream().filter(item -> item.getCode() == ActionResultCode.SUCCESSFUL).count();
        if (successfulCount == size) {
            result().setCode(ActionResultCode.SUCCESSFUL);
        } else if (successfulCount == 0) {
            result().setCode(ActionResultCode.FAILED);
        } else {
            result().setCode(ActionResultCode.PARTIAL);
        }
    }

    private List<MiniBetResult> parseMiniBetList(List<CurrentOrderSummary> currentOrders) {
        return currentOrders.stream()
                .filter(this::isOrderNeedToProcess)
                .map(this::mapOrderToMiniBetResult)
                .collect(Collectors.toList());
    }

    private MiniBetResult mapOrderToMiniBetResult(CurrentOrderSummary summary) {
        MiniBetResult result = new MiniBetResult();
        try {
            result.setCode(ActionResultCode.SUCCESSFUL);
            result.setBetId(summary.betId);
            result.setBettingAccountId(account.getId());
            result.setBetAmount(summary.sizeMatched);
            result.setBetPivot(summary.handicap.toString());
            if (summary.status == OrderStatus.EXECUTION_COMPLETE) {
                result.setBetStatus(BetStatus.ACCEPTED);
            } else if (summary.status == OrderStatus.EXECUTABLE) {
                result.setBetStatus(BetStatus.EXCHANGE_MATCH_AWAIT);
            }
            result.setBetTime(convertTimestamp(summary.placedDate));

            result.setBetOdd(VNEUtility.oddTo3Decimal(summary.averagePriceMatched));
            parseTargetTypes(summary, result);

            if (result.getTargetType() != null) {
                if (!VNEUtility.is1X2Game(result.getTargetType())) { //keep EU odd format
                    result.setBetOdd(VNEUtility.oddTo3Decimal(summary.averagePriceMatched - 1));
                }
            } else {
                CurrentOrderType currentOrderType = new CurrentOrderType(summary);
                if (missingCachedBet.containsKey(summary.marketId)) {
                    missingCachedBet.get(summary.marketId).add(currentOrderType);
                } else {
                    List<CurrentOrderType> currentOrderTypes = new ArrayList<>();
                    currentOrderTypes.add(currentOrderType);
                    missingCachedBet.put(summary.marketId, currentOrderTypes);
                }
            }
        } catch (ParsingFailException e) {
            account.getCommonLogger().error("Fail to parse current order: ", e);
            result.setCode(ActionResultCode.FAILED);
        }
        return result;
    }

    private void parseTargetTypes(CurrentOrderSummary summary, MiniBetResult result) {
        Optional<MiniBetResult> betCache = account.getRecentBetCache().get(account.getBAD().getSportbook().getId(), account.getUsername(), summary.betId);
        if (betCache.isPresent()) {
            MiniBetResult bet = betCache.get();
            result.setTargetTypes(bet.getTargetTypes());
            result.setSportType(bet.getSportType());
            result.setTimeType(bet.getTimeType());
            account.getCommonLogger().trace("Use Recent Bet Cache to parse target type {}, sportType {}, timeType {}",
                    bet.getTargetTypes(), bet.getSportType(), bet.getTimeType());
        }
    }

    private Timestamp convertTimestamp(String placedDate) throws ParsingFailException {
        try {
            Date betTime = formatter.parse(placedDate);
            return new Timestamp(betTime.getTime());
        } catch (ParseException e) {
            throw new ParsingFailException("current order record has malformed bet time", this.getDef().uri(), "", "");
        }
    }

    private boolean isOrderNeedToProcess(CurrentOrderSummary summary) {
        return OrderStatus.EXECUTION_COMPLETE == summary.status || OrderStatus.EXECUTABLE == summary.status;
    }

    @Override
    public void proceed(ListActionResult<MiniBetResult> currentResult) {
        if (!haveMissingTargetType) {
            executor.submit(this);
            account.getCommonLogger().info("LAUNCH_LIST_CURRENT_ORDER_FRAGMENT");
        } else {
            BetFairListMarketCatalogueFragment fragment = betFairListMarketCatalogueFragmentLazy.get();
            fragment.setMissingBet(missingCachedBet);
            executor.submit(betFairListMarketCatalogueFragmentLazy.get());
            account.getCommonLogger().info("LAUNCH_LIST_MARKET_CATALOGUE_FRAGMENT");
        }
    }

    @Override
    public boolean logHttpRequest() {
        return true;
    }
}
