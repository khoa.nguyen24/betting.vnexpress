package jayeson.lib.betting.VNE.runningbetlist;

import dagger.Subcomponent;
import jayeson.lib.betting.api.actionresults.ListActionResult;
import jayeson.lib.betting.api.actionresults.MiniBetResult;
import jayeson.lib.betting.core.dagger.taskscope.ITaskComponent;
import jayeson.lib.betting.core.dagger.taskscope.TaskModule;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;

@Subcomponent(modules = {BetFairRunningBetListModule.class, TaskModule.class})
@TaskScope
public interface BetFairRunningBetListComponent extends ITaskComponent<ListActionResult<MiniBetResult>> {
    @Override
    BetFairRunningBetListTask getTask();

    @Subcomponent.Builder
    interface Builder extends ITaskComponent.Builder<ListActionResult<MiniBetResult>> {
        @Override
        BetFairRunningBetListComponent build();
    }
}
