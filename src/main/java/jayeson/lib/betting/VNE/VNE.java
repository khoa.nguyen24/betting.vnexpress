package jayeson.lib.betting.VNE;

import com.google.common.eventbus.EventBus;
import jayeson.lib.betting.VNE.scrape.BetfairGetEventOddComponent;
import jayeson.lib.betting.api.IExchangeBettingAccount;
import jayeson.lib.betting.api.actioncontext.*;
import jayeson.lib.betting.api.actionresults.*;
import jayeson.lib.betting.api.datastructure.BAD;
import jayeson.lib.betting.api.datastructure.BAS;
import jayeson.lib.betting.api.datatransfer.IAccountDataDeserializer;
import jayeson.lib.betting.api.datatransfer.IAccountDataSerializer;
import jayeson.lib.betting.api.task.AccountTaskHandler;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.api.task.ScrapTaskHandler;
import jayeson.lib.betting.api.task.TaskObservable;
import jayeson.lib.betting.VNE.betticket.BetFairGetTicketComponent;
import jayeson.lib.betting.VNE.betticket.BetFairGetTicketTask;
import jayeson.lib.betting.VNE.deletebet.BetFairDeleteMasterTask;
import jayeson.lib.betting.VNE.deletebet.BetFairDeleteMasterTaskComponent;
import jayeson.lib.betting.VNE.getbalance.BetFairGetBalanceComponent;
import jayeson.lib.betting.VNE.getbalance.BetFairGetBalanceTask;
import jayeson.lib.betting.VNE.getbethistory.BetFairHistoryMasterTask;
import jayeson.lib.betting.VNE.getbethistory.BetFairHistoryMasterTaskComponent;
import jayeson.lib.betting.VNE.login.VNELoginTask;
import jayeson.lib.betting.VNE.login.LoginComponent;
import jayeson.lib.betting.VNE.logout.VNELogoutTask;
import jayeson.lib.betting.VNE.logout.LogoutComponent;
import jayeson.lib.betting.VNE.maintenance.BetFairMaintenanceComponent;
import jayeson.lib.betting.VNE.maintenance.BetFairMaintenanceTask;
import jayeson.lib.betting.VNE.recentbetstatus.RecentBetStatusMasterTask;
import jayeson.lib.betting.VNE.recentbetstatus.RecentBetStatusMasterTaskComponent;
import jayeson.lib.betting.VNE.runningbetlist.BetFairRunningBetListComponent;
import jayeson.lib.betting.VNE.runningbetlist.BetFairRunningBetListTask;
import jayeson.lib.betting.VNE.scrape.BetfairGetEventInfoComponent;
import jayeson.lib.betting.VNE.scrape.BetfairGetEventStateComponent;
import jayeson.lib.betting.VNE.scrape.BetfairScrapeComponent;
import jayeson.lib.betting.core.BettingAccount;
import jayeson.lib.betting.core.cache.CancelledBetCache;
import jayeson.lib.betting.core.cache.RecentBetCache;
import jayeson.lib.betting.core.dagger.accountscope.AccountScope;
import jayeson.lib.betting.core.dagger.qualifier.AccountEventBus;
import jayeson.lib.betting.core.dagger.qualifier.BettingHost;
import jayeson.lib.betting.core.dagger.qualifier.SessionExpiredTime;
import jayeson.lib.betting.core.http.IClient;
import jayeson.lib.betting.core.task.AccountTaskType;
import jayeson.lib.betting.core.task.ITaskCreator;
import jayeson.lib.betting.core.task.ScrapeTask;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;
import jayeson.lib.betting.datastructure.OddFormatSettingConstraint;
import jayeson.lib.betting.utility.BaseBettingUtility;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

@AccountScope
public class VNE extends BettingAccount implements IExchangeBettingAccount {

	private String accountCurrency;
	private String appKey;
	private long lastRefreshTime;

	private Set<IAccountChangeListener> accountChangeListeners;

	public String getAppKey() {
		return appKey;
	}

	public String getAccountCurrency() {
		return accountCurrency;
	}

	public void setAccountCurrency(String currencyCode) {
		this.accountCurrency = currencyCode;
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	public static class BetFairSerializableAccountData extends BettingSerializableAccountData<VNE> {

		private static final long serialVersionUID = -2724794848846034560L;

		public String accountCurrency;
		public String appKey;

		public BetFairSerializableAccountData() {
		}

		public BetFairSerializableAccountData(VNE account) {
			super(account);

			this.appKey = account.appKey;
			this.accountCurrency = account.accountCurrency;
		}
	}

	@Inject
	public VNE(@AccountEventBus EventBus eventBus, ITaskExecutor executor, IClient httpClient,
			   BAD bad, RecentBetCache recentBetCache, CancelledBetCache cancelledBetCache, ITaskCreator taskCreator,
			   @BettingHost String host, @SessionExpiredTime int sessionExpiredTime,
			   @Named("BetFairSerializer") IAccountDataSerializer accountDataSerializer,
			   @Named("BetFairDeserializer") IAccountDataDeserializer accountDataDeserializer) {
		super(taskCreator, eventBus, executor, httpClient, bad, recentBetCache, cancelledBetCache, host, sessionExpiredTime,
				accountDataSerializer, accountDataDeserializer);
		accountChangeListeners = new HashSet<>();
	}


	@Override
	public TaskObservable<AccountTaskInfo> login(AccountTaskHandler<LoginResult> loginHandler,
												 Consumer<AccountTaskInfo> observer) {
		// Clean up
		cleanUp();
		VNELoginTask loginTask = (VNELoginTask) taskCreator.create(LoginComponent.class,
				wrapHandler(loginHandler), AccountTaskType.LOGIN);
		// Launch
		addTask(loginTask.id(), loginTask);
		executor.submit(loginTask);

		// Change status to starting
		setAccountStatus(BAS.STARTING);
		
		return loginTask.getObservable().addObserver(observer);
	}

	@Override
	public TaskObservable<AccountTaskInfo> logout(AccountTaskHandler<LogoutResult> logoutHandler, 
			Consumer<AccountTaskInfo> observer) {

		VNELogoutTask logoutTask = (VNELogoutTask) taskCreator.create(LogoutComponent.class,
				wrapHandler(logoutHandler), AccountTaskType.LOGOUT);

		// Launch
		addTask(logoutTask.id(), logoutTask);
		executor.submit(logoutTask);

		// Change status to stopping
		setAccountStatus(BAS.STOPPING);
		getCommonLogger().debug("Account logout");
		notifyDisconnect();
		
		return logoutTask.getObservable().addObserver(observer);
	}

	@Override
	public TaskObservable<AccountTaskInfo> getBetTicket(BetInfoContext context,
			AccountTaskHandler<BetTicketResult> betTicketHandler, Consumer<AccountTaskInfo> observer) {

		// Create get ticket task
		BetFairGetTicketTask ticketTask = (BetFairGetTicketTask) taskCreator.create(BetFairGetTicketComponent.class,
				wrapHandler(betTicketHandler), context, AccountTaskType.GET_BET_TICKET);

		if (!validateContext(betTicketHandler, ticketTask.id(), ticketTask.getObservable().addObserver(observer), context))
			return ticketTask.getObservable();

		// Launch
		addTask(ticketTask.id(), ticketTask);
		executor.submit(ticketTask);

		return ticketTask.getObservable();
	}

	@Override
	public TaskObservable<AccountTaskInfo> getBetHistory(BetHistoryByStatementContext context,
														 AccountTaskHandler<ListBetHistoryResult> betHistoryHandler, Consumer<AccountTaskInfo> observer) {
		BetFairHistoryMasterTask betHistoryTask = (BetFairHistoryMasterTask) taskCreator.create(BetFairHistoryMasterTaskComponent.class,
				wrapHandler(betHistoryHandler), context, AccountTaskType.GET_BET_HISTORY);

		addTask(betHistoryTask.id(), betHistoryTask);
		executor.submit(betHistoryTask);

		return betHistoryTask.getObservable().addObserver(observer);
	}

	@Override
	public TaskObservable<AccountTaskInfo> doMaintenance(AccountTaskHandler<MaintenanceResult> maintenanceHandler,
														 Consumer<AccountTaskInfo> observer) {

		BetFairMaintenanceTask maintenanceTask = (BetFairMaintenanceTask) taskCreator.create(BetFairMaintenanceComponent.class,
				wrapHandler(maintenanceHandler), AccountTaskType.DO_MAINTENANCE);

		addTask(maintenanceTask.id(), maintenanceTask);
		executor.submit(maintenanceTask);

		return maintenanceTask.getObservable().addObserver(observer);
	}

	@Override
	public TaskObservable<AccountTaskInfo> getBalance(AccountTaskHandler<BalanceResult> balanceHandler,
													  Consumer<AccountTaskInfo> observer) {
		// Create get balance task
		BetFairGetBalanceTask getBalanceTask = (BetFairGetBalanceTask) taskCreator.create(BetFairGetBalanceComponent.class,
				wrapHandler(balanceHandler), AccountTaskType.GET_BALANCE);

		// Launch
		addTask(getBalanceTask.id(), getBalanceTask);
		executor.submit(getBalanceTask);

		return getBalanceTask.getObservable().addObserver(observer);
	}

	@Override
	public TaskObservable<AccountTaskInfo> getRunningBetList(AccountTaskHandler<ListMiniBetResult> miniBetListHandler,
															  Consumer<AccountTaskInfo> observer) {
		BetFairRunningBetListTask betListTask = (BetFairRunningBetListTask) taskCreator.create(BetFairRunningBetListComponent.class,
				wrapHandler(miniBetListHandler), AccountTaskType.GET_RUNNING_BET_LIST);

		// Launch
		addTask(betListTask.id(), betListTask);
		executor.submit(betListTask);

		return betListTask.getObservable().addObserver(observer);
	}

	@Override
	public TaskObservable<AccountTaskInfo> getRecentBetStatus(RecentBetStatusContext context,
															  AccountTaskHandler<ListRecentBetStatusResult> handler, Consumer<AccountTaskInfo> observer) {
		RecentBetStatusMasterTask task = (RecentBetStatusMasterTask) taskCreator.create(RecentBetStatusMasterTaskComponent.class,
				wrapHandler(handler), context, AccountTaskType.GET_RECENT_BET_STATUS);

		// Launch
		addTask(task.id(), task);
		executor.submit(task);

		return task.getObservable().addObserver(observer);
	}

	@Override
	public TaskObservable<AccountTaskInfo> isBookMaintenance(AccountTaskHandler<CheckBookMaintenanceResult> handler,
															 Consumer<AccountTaskInfo> observer) {
		return BaseBettingUtility.generateUnsupportedOperation(getBAD(), handler, observer, new CheckBookMaintenanceResult());
	}

	@Override
	public TaskObservable<AccountTaskInfo> deleteBet(DeleteBetContext context,
			AccountTaskHandler<DeleteBetResult> deleteBetHandler, Consumer<AccountTaskInfo> observer) {
		BetFairDeleteMasterTask deleteBet = (BetFairDeleteMasterTask) taskCreator.create(BetFairDeleteMasterTaskComponent.class,
				wrapHandler(deleteBetHandler), context, AccountTaskType.DELETE_BET);

		addTask(deleteBet.id(), deleteBet);
		executor.submit(deleteBet);

		return deleteBet.getObservable().addObserver(observer);
	}

	@Override
	public OddFormatSettingConstraint oddFormatSettingConstraint() {
		return OddFormatSettingConstraint.PER_PARAM;
	}

	@Override
	public byte[] serializeData() throws Exception {
		return accountDataSerializer.serialize(new BetFairSerializableAccountData(this));
	}

	@Override
	public void restoreAccountData(byte[] serializedData) throws Exception {
		BetFairSerializableAccountData accountData = (BetFairSerializableAccountData) accountDataDeserializer.deserialize(serializedData);
		this.restoreAccountData(accountData);
	}

	@Override
	protected void restoreAccountData(BettingSerializableAccountData<?> accountData) {
		super.restoreAccountData(accountData);

		this.accountCurrency = ((BetFairSerializableAccountData)accountData).accountCurrency;
		this.appKey = ((BetFairSerializableAccountData)accountData).appKey;
	}

	@Override
	public TaskObservable<AccountTaskInfo> scrape(ScrapeContext scrapeContext, ScrapTaskHandler<ScrapeResult> scrapTaskHandler, Consumer<AccountTaskInfo> observer) {
		ScrapeTask task = null;
		switch (scrapeContext.getRequestType()) {
			case FETCH_DELTA:
				task = (ScrapeTask) taskCreator.createScrape(BetfairScrapeComponent.class, wrapScrapeHandler(scrapTaskHandler),
						scrapeContext, AccountTaskType.SCRAPE_FEED);
				break;
			case FETCH_DELTA_STATE:
				task = (ScrapeTask) taskCreator.createScrape(BetfairGetEventStateComponent.class, wrapScrapeHandler(scrapTaskHandler),
						scrapeContext, AccountTaskType.SCRAPE_FEED);
				break;
			case FETCH_DELTA_EVENT:
				task = (ScrapeTask) taskCreator.createScrape(BetfairGetEventInfoComponent.class, wrapScrapeHandler(scrapTaskHandler),
						scrapeContext, AccountTaskType.SCRAPE_FEED);
				break;
			case FETCH_DELTA_EVENT_ODD:
				task = (ScrapeTask) taskCreator.createScrape(BetfairGetEventOddComponent.class, wrapScrapeHandler(scrapTaskHandler),
						scrapeContext, AccountTaskType.SCRAPE_FEED);
				break;
		}
		if (task != null){
			// Launch
			addTask(task.id(), task);
			executor.submit(task);
			return task.getObservable().addObserver(observer);
		} else {
			return null;
		}
	}


	public long getLastRefreshTime() {
		return lastRefreshTime;
	}

	public void setLastRefreshTime(long lastRefreshTime) {
		this.lastRefreshTime = lastRefreshTime;
	}

	@Override
	public void disconnect() {
		super.disconnect();
		notifyDisconnect();
	}

	private void notifyDisconnect() {
		accountChangeListeners.forEach(IAccountChangeListener::accountDisconnect);
		accountChangeListeners.removeIf(IAccountChangeListener::removable);
	}

	public void registerAccountChangeListener(IAccountChangeListener listener) {
		accountChangeListeners.add(listener);
	}

	public void unregisterAccountChangeListener(IAccountChangeListener listener) {
		accountChangeListeners.remove(listener);
	}
}