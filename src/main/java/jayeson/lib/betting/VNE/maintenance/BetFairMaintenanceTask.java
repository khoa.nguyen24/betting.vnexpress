package jayeson.lib.betting.VNE.maintenance;

import dagger.Lazy;
import jayeson.lib.betting.VNE.VNE;
import jayeson.lib.betting.api.actionresults.ActionResultCode;
import jayeson.lib.betting.api.actionresults.BalanceResult;
import jayeson.lib.betting.api.actionresults.MaintenanceResult;
import jayeson.lib.betting.api.task.*;
import jayeson.lib.betting.VNE.getbalance.BetFairGetBalanceTask;
import jayeson.lib.betting.core.BettingAccount;
import jayeson.lib.betting.core.dagger.qualifier.MasterTaskHandler;
import jayeson.lib.betting.core.dagger.qualifier.MasterTaskId;
import jayeson.lib.betting.core.dagger.qualifier.MasterTaskInfo;
import jayeson.lib.betting.core.dagger.qualifier.MasterTaskObservable;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;
import jayeson.lib.betting.core.task.MaintenanceMasterTask;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;

import javax.inject.Inject;

/**
 * @author Nam Nguyen
 */
@TaskScope
public class BetFairMaintenanceTask extends MaintenanceMasterTask {

	private static final long REFRESH_TIME_INTERVAL = 300000L;

	private Lazy<BetFairGetBalanceTask> betFairGetBalanceTaskLazy;
	private long currTime;
	private final AccountTaskHandler<BalanceResult> getBalanceHandler = (BalanceResult result, AccountTask<BalanceResult> task) -> {
		if (result.getCode() == ActionResultCode.SUCCESSFUL) {

			account.getCommonLogger().info("Check balance success: {}", result.getBalance());
			((VNE) account).setLastRefreshTime(currTime);
			updateProgress(100, AccountTaskState.SUCCEEDED);
		} else {
			updateProgress(100, AccountTaskState.FAILED);
		}

		// Notify handler
		reportResult(result.getCode(), result.getMessage());
	};

	@Inject
	public BetFairMaintenanceTask(ITaskExecutor executor, BettingAccount account,
								  @MasterTaskInfo AccountTaskInfo data,
								  @MasterTaskId String id,
								  @MasterTaskHandler AccountTaskHandler<MaintenanceResult> handler,
								  @MasterTaskObservable TaskObservable<AccountTaskInfo> observable,
								  Lazy<BetFairGetBalanceTask> betFairGetBalanceTaskLazy, MaintenanceResult result) {
		super(executor, data, account, id, handler, observable, result);
		this.betFairGetBalanceTaskLazy = betFairGetBalanceTaskLazy;
	}

	@Override
	public void execute() {
		currTime = System.currentTimeMillis();
		if (currTime - ((VNE) account).getLastRefreshTime() < getRefreshTimeInterval()) {
			updateProgress(100, AccountTaskState.SUCCEEDED);
			reportResult(ActionResultCode.SUCCESSFUL, "");
			return;
		}

		currentSubTask = proceedSubTask();
	}

	private TaskObservable<AccountTaskInfo> proceedSubTask() {
		BetFairGetBalanceTask task = betFairGetBalanceTaskLazy.get();
		executor.submit(task);
		return task.getObservable();
	}

	public AccountTaskHandler<BalanceResult> getGetBalanceHandler() {
		return getBalanceHandler;
	}

	protected long getRefreshTimeInterval() {
		return BetFairMaintenanceTask.REFRESH_TIME_INTERVAL;
	}
}
