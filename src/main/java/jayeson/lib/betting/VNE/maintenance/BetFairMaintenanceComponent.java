package jayeson.lib.betting.VNE.maintenance;

import dagger.BindsInstance;
import dagger.Subcomponent;
import jayeson.lib.betting.api.actionresults.MaintenanceResult;
import jayeson.lib.betting.api.task.AccountTaskHandler;
import jayeson.lib.betting.core.dagger.qualifier.MasterTaskHandler;
import jayeson.lib.betting.core.dagger.taskscope.ITaskComponent;
import jayeson.lib.betting.core.dagger.taskscope.MasterTaskModule;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;

@Subcomponent(modules = {
        MasterTaskModule.class,
        BetFairMaintenanceModule.class})
@TaskScope
public interface BetFairMaintenanceComponent extends ITaskComponent<MaintenanceResult> {

    @Override
    BetFairMaintenanceTask getTask();

    @Subcomponent.Builder
    interface Builder extends ITaskComponent.Builder<MaintenanceResult> {

        @Override
        @BindsInstance
        ITaskComponent.Builder bindsHandler(@MasterTaskHandler AccountTaskHandler<MaintenanceResult> handler);

        BetFairMaintenanceComponent build();
    }
}
