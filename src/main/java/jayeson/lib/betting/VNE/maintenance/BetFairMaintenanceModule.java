package jayeson.lib.betting.VNE.maintenance;

import dagger.Module;
import dagger.Provides;
import jayeson.lib.betting.api.actionresults.BalanceResult;
import jayeson.lib.betting.api.actionresults.MaintenanceResult;
import jayeson.lib.betting.api.task.AccountTaskHandler;
import jayeson.lib.betting.VNE.getbalance.BetFairGetBalanceModule;
import jayeson.lib.betting.core.dagger.taskscope.MasterTaskModule;
import jayeson.lib.betting.core.dagger.taskscope.TaskModule;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;

@Module(includes = {MasterTaskModule.class, TaskModule.class, BetFairGetBalanceModule.class})
public class BetFairMaintenanceModule {

    @TaskScope
    @Provides
    AccountTaskHandler<BalanceResult> provideBalanceTaskHandler(BetFairMaintenanceTask task) {
        return task.getGetBalanceHandler();
    }

    @TaskScope
    @Provides
    MaintenanceResult provideMaintenanceResult() {
        return new MaintenanceResult();
    }
}
