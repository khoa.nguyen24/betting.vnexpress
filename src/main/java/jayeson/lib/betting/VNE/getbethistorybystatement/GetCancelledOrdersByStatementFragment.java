package jayeson.lib.betting.VNE.getbethistorybystatement;

import jayeson.lib.betting.VNE.VNE;
import jayeson.lib.betting.api.actioncontext.BetHistoryByStatementContext;
import jayeson.lib.betting.api.datastructure.BADRuntime;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.VNE.json.BFBetStatus;
import jayeson.lib.betting.VNE.json.clearedorders.ClearedOrderSummaryResponse;
import jayeson.lib.betting.core.http.IClient;
import jayeson.lib.betting.core.http.IResponse;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.task.TaskFragmentListener;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;
import jayeson.lib.betting.exception.BettingException;

import javax.inject.Inject;

public class GetCancelledOrdersByStatementFragment extends ListClearedOrdersByStatementFragment {

	@Inject
	public GetCancelledOrdersByStatementFragment(AccountTaskInfo data, ITaskExecutor executor, IResponseListener responseListener,
                                                 VNE account, TaskFragmentListener<BetFairListBetHistoriesByStatementResult> listener,
                                                 BADRuntime badRuntime, BetFairListBetHistoriesByStatementResult initialResult,
                                                 BetHistoryByStatementContext context, IClient client) {
		super(data, executor, responseListener, account, listener, badRuntime, initialResult, context, client, BFBetStatus.CANCELLED);
		needProceed = true;
	}

	@Override
	public void consumeResponse(IResponse<ClearedOrderSummaryResponse[]> response) throws BettingException {
		super.consumeResponse(response);
		if (isRequestDone) {
			needProceed = false;
		}
	}

	@Override
	public boolean logHttpRequest() {
		return false;
	}
}
