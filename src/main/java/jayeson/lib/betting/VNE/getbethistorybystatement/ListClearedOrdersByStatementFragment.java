package jayeson.lib.betting.VNE.getbethistorybystatement;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import jayeson.lib.betting.VNE.VNE;
import jayeson.lib.betting.VNE.VNEURIDefinition;
import jayeson.lib.betting.VNE.VNEUtility;
import jayeson.lib.betting.VNE.getbethistorybyid.BetFairHistoryResult;
import jayeson.lib.betting.VNE.json.clearedorders.*;
import jayeson.lib.betting.api.actioncontext.BetHistoryByStatementContext;
import jayeson.lib.betting.api.actionresults.ActionResultCode;
import jayeson.lib.betting.api.actionresults.BetHistoryResult;
import jayeson.lib.betting.api.datastructure.BADRuntime;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.VNE.json.BFBetStatus;
import jayeson.lib.betting.VNE.json.clearedorders.*;
import jayeson.lib.betting.core.exception.UnknownResponseException;
import jayeson.lib.betting.core.http.IClient;
import jayeson.lib.betting.core.http.IResponse;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.http.request.JsonRequest;
import jayeson.lib.betting.core.task.AsyncHttpTaskFragment;
import jayeson.lib.betting.core.task.TaskFragmentListener;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;
import jayeson.lib.betting.core.utility.BettingUtility;
import jayeson.lib.betting.exception.BettingException;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;
import java.util.stream.Collectors;

public class ListClearedOrdersByStatementFragment extends AsyncHttpTaskFragment<BetFairListBetHistoriesByStatementResult, ClearedOrderSummaryResponse[]> {

    private final BADRuntime badRuntime;
    private final VNE VNEAccount;
    protected boolean isRequestDone = true;
    private final BetHistoryByStatementContext context;
    private final BFBetStatus requestBetStatus;
    private int fromRecord;

    public ListClearedOrdersByStatementFragment(AccountTaskInfo data, ITaskExecutor executor, IResponseListener responseListener,
                                                VNE account, TaskFragmentListener<BetFairListBetHistoriesByStatementResult> listener,
                                                BADRuntime badRuntime, BetFairListBetHistoriesByStatementResult initialResult,
                                                BetHistoryByStatementContext context, IClient client, BFBetStatus requestBetStatus) {
        super(data, executor, responseListener, account, listener, initialResult, client);
        this.context = context;
        this.badRuntime = badRuntime;
        this.requestBetStatus = requestBetStatus;
        this.setDef(VNEURIDefinition.getJsonRpcURI(ClearedOrderSummaryResponse[].class));
        this.VNEAccount = account;
        needProceed = true;
    }

    @Override
    public void proceed(BetFairListBetHistoriesByStatementResult currentResult) {
        executor.submit(this);
        account.getCommonLogger().info("LAUNCH_BET_HISTORIES_BY_TIME_FROM_{}_TO_{}",
                context.getEndStatementDay(), context.getEndStatementDay());
    }

    /**
     * [{"jsonrpc": "2.0", "method": "SportsAPING/v1.0/listClearedOrders", "params": {"betStatus":"SETTLED","settledDateRange":{}}, "id": 1}]
     *
     * @throws BettingException
     */
    @Override
    public void execute() throws BettingException {
        ArrayNode arrayNode = JsonNodeFactory.instance.arrayNode();
        ClearedOrdersRequest data = new ClearedOrdersRequest();

        ClearedOrdersParams params = new ClearedOrdersParams();
        params.betStatus = requestBetStatus;
        data.params = completeParams(params);

        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.convertValue(data, JsonNode.class);
        arrayNode.add(node);

        if (VNEUtility.isNeedToResetBettingApi(client.host().url().toString())) {
            client.resetHost(VNEUtility.getBettingApiUrl(client.host().url().toString()));
        }

        // Build request
        JsonRequest request = new JsonRequest.Builder()
                .def(this.getDef())
                .host(client.host())
                .header("X-Authentication", this.badRuntime.getLoggedInUid())
                .header("X-Application", VNEAccount.getAppKey())
                .data(arrayNode)
                .build();

        executeRequest(request, true);
    }

    private ClearedOrdersParams completeParams(ClearedOrdersParams params) {

        // Current timezone of system is GMT+8
        // Prepare convert start day time and end day time to GMT+0 for query to BetFair server
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));

        params.settledDateRange = new BFTimeRange();
        params.settledDateRange.from = formatter.format(context.getStartStatementDay());
        params.settledDateRange.to = formatter.format(context.getEndStatementDay());
        params.fromRecord = fromRecord;

        return params;
    }

    @Override
    public void consumeResponse(IResponse<ClearedOrderSummaryResponse[]> response) throws BettingException {
        if (response.getStatusLine().getCode() != 200) {
            account.getCommonLogger().error("FAIL to request Histories by statement time of BetFair {}", response);
            result().setCode(ActionResultCode.FAILED);
            result().setMessage("The request failed with http code " + response.getStatusLine().getCode());
            return;
        }

        ClearedOrderSummaryResponse report = response.content()[0];

        if (report.error != null) {
            account.getCommonLogger().error("Failed to get clear orders by statement time: {} ", BettingUtility.writeObjectToJson(report.error));
            throw new UnknownResponseException("Failed to get clear orders", getDef(), "");
        }

        ClearedOrderSummaryReport result = report.result;

        if (!result.clearedOrders.isEmpty()) {
            result().getResultList().addAll(parseBetInfo(result.clearedOrders));
        }

        if (result.moreAvailable) {
            fromRecord += result.clearedOrders.size();
            isRequestDone = false;
        } else {
            result().setCode(ActionResultCode.SUCCESSFUL);
            isRequestDone = true;
        }
    }

    private List<BetHistoryResult> parseBetInfo(List<ClearedOrderSummary> clearedOrders) {
        return clearedOrders.stream()
                .map(each -> {
                    BetFairHistoryResult result = new BetFairHistoryResult();
                    result.setCode(ActionResultCode.SUCCESSFUL);
                    result.setBettingAccountId(account.getId());
                    result.setBetId(each.betId);

                    return result;
                }).collect(Collectors.toList());
    }

    @Override
    public boolean logHttpRequest() {
        return false;
    }
}
