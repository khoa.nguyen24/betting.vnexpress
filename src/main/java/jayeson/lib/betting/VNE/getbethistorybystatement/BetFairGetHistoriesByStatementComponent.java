package jayeson.lib.betting.VNE.getbethistorybystatement;

import dagger.Subcomponent;
import jayeson.lib.betting.api.actioncontext.BetHistoryByStatementContext;
import jayeson.lib.betting.core.dagger.taskscope.ITaskComponent;
import jayeson.lib.betting.core.dagger.taskscope.TaskModule;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;

@Subcomponent(modules = {BetFairGetHistoriesByStatementModule.class, TaskModule.class})
@TaskScope
public interface BetFairGetHistoriesByStatementComponent extends ITaskComponent<BetFairListBetHistoriesByStatementResult> {

    @Override
    BetFairGetHistoriesByStatementTask getTask();

    @Subcomponent.Builder
    interface Builder extends BuilderContext<BetFairListBetHistoriesByStatementResult, BetHistoryByStatementContext> {
        @Override
        BetFairGetHistoriesByStatementComponent build();
    }
}
