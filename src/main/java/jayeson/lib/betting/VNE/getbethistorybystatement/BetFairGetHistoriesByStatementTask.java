package jayeson.lib.betting.VNE.getbethistorybystatement;

import dagger.Lazy;
import jayeson.lib.betting.api.actioncontext.BetHistoryByStatementContext;
import jayeson.lib.betting.api.actioncontext.BetHistoryContext;
import jayeson.lib.betting.api.task.AccountTaskHandler;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.api.task.TaskObservable;
import jayeson.lib.betting.core.BettingAccount;
import jayeson.lib.betting.core.dagger.qualifier.TaskId;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.task.BasicAccountTask;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;

import javax.inject.Inject;

@TaskScope
public class BetFairGetHistoriesByStatementTask extends BasicAccountTask<BetFairListBetHistoriesByStatementResult> {
	private static final int WORK_LOAD = 10;
	private final Lazy<GetSettledOrdersByStatementFragment> settledOrdersFragmentLazy;

	protected BetHistoryByStatementContext context;

	@Inject
	public BetFairGetHistoriesByStatementTask(Lazy<GetSettledOrdersByStatementFragment> settledOrdersFragmentLazy,
											  ITaskExecutor executor, IResponseListener responseListener, AccountTaskInfo data,
											  BettingAccount account, @TaskId String id, AccountTaskHandler<BetFairListBetHistoriesByStatementResult> handler,
											  TaskObservable<AccountTaskInfo> observable, BetHistoryByStatementContext context,
											  BetFairListBetHistoriesByStatementResult result) {
		super(executor, responseListener, data, account, id, WORK_LOAD, handler, observable, result);
		this.context = context;
		this.settledOrdersFragmentLazy = settledOrdersFragmentLazy;
	}

	@Override
	public void execute() {
		account.getCommonLogger().info("LAUNCH_GET_HISTORIES_BY_STATEMENT_FRAGMENT");
		executor.submit(settledOrdersFragmentLazy.get());
	}

	public BetHistoryContext getContext() {
		return this.context;
	}
}
