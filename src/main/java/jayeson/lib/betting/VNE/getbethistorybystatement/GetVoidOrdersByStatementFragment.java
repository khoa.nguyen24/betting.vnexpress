package jayeson.lib.betting.VNE.getbethistorybystatement;

import dagger.Lazy;
import jayeson.lib.betting.VNE.VNE;
import jayeson.lib.betting.api.actioncontext.BetHistoryByStatementContext;
import jayeson.lib.betting.api.datastructure.BADRuntime;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.VNE.json.BFBetStatus;
import jayeson.lib.betting.core.http.IClient;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.task.TaskFragmentListener;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;

import javax.inject.Inject;

public class GetVoidOrdersByStatementFragment extends ListClearedOrdersByStatementFragment {

	private final Lazy<GetLapsedOrdersByStatementFragment> lapsedOrdersFragmentLazy;

	@Inject
	public GetVoidOrdersByStatementFragment(AccountTaskInfo data, ITaskExecutor executor, IResponseListener responseListener,
                                            VNE account, TaskFragmentListener<BetFairListBetHistoriesByStatementResult> listener,
                                            BADRuntime badRuntime, BetFairListBetHistoriesByStatementResult initialResult,
                                            BetHistoryByStatementContext context, IClient client,
                                            Lazy<GetLapsedOrdersByStatementFragment> lapsedOrdersFragmentLazy) {
		super(data, executor, responseListener, account, listener, badRuntime, initialResult, context, client, BFBetStatus.VOIDED);
		needProceed = true;
		this.lapsedOrdersFragmentLazy = lapsedOrdersFragmentLazy;
	}

	@Override
	public void proceed(BetFairListBetHistoriesByStatementResult currentResult) {
		if (isRequestDone) {
			GetLapsedOrdersByStatementFragment fragment = lapsedOrdersFragmentLazy.get();
			executor.submit(fragment);
			account.getCommonLogger().info("LAUNCH_BET_HISTORIES_BY_TIME_LAPSED");
		} else {
			super.proceed(currentResult);
		}
	}

	@Override
	public boolean logHttpRequest() {
		return false;
	}
}
