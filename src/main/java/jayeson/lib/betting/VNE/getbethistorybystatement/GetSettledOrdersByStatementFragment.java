package jayeson.lib.betting.VNE.getbethistorybystatement;

import dagger.Lazy;
import jayeson.lib.betting.VNE.VNE;
import jayeson.lib.betting.api.actioncontext.BetHistoryByStatementContext;
import jayeson.lib.betting.api.datastructure.BADRuntime;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.VNE.json.BFBetStatus;
import jayeson.lib.betting.core.http.IClient;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.task.TaskFragmentListener;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;

import javax.inject.Inject;

public class GetSettledOrdersByStatementFragment extends ListClearedOrdersByStatementFragment {

	private final Lazy<GetVoidOrdersByStatementFragment> voidOrdersFragmentLazy;

	@Inject
	public GetSettledOrdersByStatementFragment(AccountTaskInfo data, ITaskExecutor executor, IResponseListener responseListener,
                                               VNE account, TaskFragmentListener<BetFairListBetHistoriesByStatementResult> listener,
                                               BADRuntime badRuntime, BetFairListBetHistoriesByStatementResult initialResult,
                                               BetHistoryByStatementContext context, IClient client,
                                               Lazy<GetVoidOrdersByStatementFragment> voidOrdersFragmentLazy) {
		super(data, executor, responseListener, account, listener, badRuntime, initialResult, context, client, BFBetStatus.SETTLED);
		needProceed = true;
		this.voidOrdersFragmentLazy = voidOrdersFragmentLazy;
	}

	@Override
	public void proceed(BetFairListBetHistoriesByStatementResult currentResult) {
		if (isRequestDone) {
			GetVoidOrdersByStatementFragment fragment = voidOrdersFragmentLazy.get();
			executor.submit(fragment);
			account.getCommonLogger().info("LAUNCH_BET_HISTORIES_BY_TIME_VOID");
		} else{
			super.proceed(currentResult);
		}
	}

	@Override
	public boolean logHttpRequest() {
		return false;
	}
}
