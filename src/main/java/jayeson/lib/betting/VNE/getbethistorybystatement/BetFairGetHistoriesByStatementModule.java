package jayeson.lib.betting.VNE.getbethistorybystatement;

import dagger.Module;
import dagger.Provides;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;
import jayeson.lib.betting.core.task.TaskFragmentListener;

@Module
public class BetFairGetHistoriesByStatementModule {
	
	@TaskScope @Provides
	BetFairListBetHistoriesByStatementResult provideResult() {
		return new BetFairListBetHistoriesByStatementResult();
	}
	
	@TaskScope @Provides
	TaskFragmentListener<BetFairListBetHistoriesByStatementResult> provideTaskFragmentListener(BetFairGetHistoriesByStatementTask task) {
        return task;
    }
}
