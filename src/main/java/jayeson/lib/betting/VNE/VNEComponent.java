package jayeson.lib.betting.VNE;

import dagger.BindsInstance;
import dagger.Component;
import jayeson.lib.betting.core.dagger.corescope.CoreBaseComponent;
import jayeson.lib.betting.core.dagger.BettingComponent;
import jayeson.lib.betting.core.dagger.accountscope.AccountScope;
import jayeson.lib.betting.core.dagger.accountscope.BettingAccountModule;
import jayeson.lib.betting.dagger.AccountComponent;

/**
 * @author DongCatchay
 **/
@Component(
        dependencies = CoreBaseComponent.class,
        modules = {BettingAccountModule.class, VNEModule.class}
)
@AccountScope
public interface VNEComponent extends AccountComponent {

    @Component.Builder
    interface Builder extends AccountComponent.Builder {

        Builder bindsComponentCoreBaseComponent(CoreBaseComponent component);
        @BindsInstance
        Builder bindsComponentBettingComponent(BettingComponent component);

        @Override
        VNEComponent build();
    }
}
