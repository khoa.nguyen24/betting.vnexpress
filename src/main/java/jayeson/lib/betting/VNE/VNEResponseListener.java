package jayeson.lib.betting.VNE;

import jayeson.lib.betting.core.BettingAccount;
import jayeson.lib.betting.core.connectionchecker.ConnectionChecker;
import jayeson.lib.betting.core.dagger.qualifier.DomainsToBeTested;
import jayeson.lib.betting.core.exception.BookMaintenanceException;
import jayeson.lib.betting.core.exception.SessionExpiredException;
import jayeson.lib.betting.core.http.IResponse;
import jayeson.lib.betting.core.http.ResponseListener;

import javax.inject.Inject;
import java.util.List;

/**
 * 
 * @author WEIMING
 *
 */
public class VNEResponseListener extends ResponseListener {
	@Inject
	public VNEResponseListener(BettingAccount account, @DomainsToBeTested List<String> domainsToBeTested, ConnectionChecker connectionChecker) {
		super(account, domainsToBeTested, connectionChecker);
	}

	@Override
	public void detectLogout(IResponse<?> response) {
		// check 401 not logged in
		if(response.getStatusLine().getCode() == 401) {
			throw new SessionExpiredException("Account is not logged in!");
		}

		// Only check StringResponse for session out
		if (response.stringContent().contains("NO_APP_KEY")) {
			throw new SessionExpiredException("Account has been kicked out!");
		} else if(response.stringContent().contains("INVALID_SESSION_INFORMATION")) {
			throw new SessionExpiredException("Account has been kicked out!");
		}
	}

	@Override
	public void detectUnallowedLocationError(IResponse<?> response) {}

	@Override
	public void detectBookMaintenance(IResponse<?> response) throws BookMaintenanceException { }
}
