package jayeson.lib.betting.VNE;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import jayeson.lib.betting.VNE.json.SessionData;
import jayeson.lib.betting.VNE.json.login.AppKeyRespone;
import jayeson.lib.betting.VNE.json.login.GetProfileResponse;
import jayeson.lib.betting.VNE.json.login.LoginUserResponse;
import jayeson.lib.betting.core.http.HttpMethod;
import jayeson.lib.betting.core.http.IEnumURIDefinition;
import jayeson.lib.betting.core.http.URIDataHolder;
import jayeson.lib.betting.core.http.contentbuilder.JsonContentBuilder;

public enum VNEURIDefinition implements IEnumURIDefinition {
	
	/******************************* LOGIN IN *******************************/
	// Do the login
	//Login page
	LOGIN_PAGE(
			new URIDataHolder().
			uri						("/authen/users/login").
			regexes					(ImmutableMap.<String, String>builder()
										.put("csrf", "hidden.*csrf.*value=\"(.*?)\"")
										.build())
	),

	// Do the login
	LOGIN_HOME(
			new URIDataHolder().
			uri						("/authen/users/login").
			method					(HttpMethod.POST).
			requiredParams			(ImmutableList.of(
										"csrf", "myvne_email", "myvne_password"))
					.contentBuilder(new JsonContentBuilder<>(LoginUserResponse.class))
	),

	GET_APP_KEY(
			new URIDataHolder().
			uri						("/visualisers/account").
			method					(HttpMethod.POST).
			regexes					(ImmutableMap.<String, String>builder()
										.put("appKey", "applicationKey\":\"(.*)\",.*delayData\":true")
										.put("commercialKey", "applicationKey.*\"(.*)\",")
										.build()).
			contentBuilder(new JsonContentBuilder<>(AppKeyRespone[].class))
	),

	// Do the login
	CREATE_SESSION(
			new URIDataHolder().
			uri						("/api/login").
			method					(HttpMethod.POST).
			requiredParams			(ImmutableList.of("username", "password")).
			contentBuilder			(new JsonContentBuilder<>(SessionData.class))
	),
	
	/******************************* LOGOUT **********************************/
	
	LOGOUT(
			new URIDataHolder().
			uri						("/authen/users/logout").
			method					(HttpMethod.GET)
	),
	
	/*************************** MAINTENANCE  ********************************/
	
	GET_SESSION(
			new URIDataHolder().
			uri						("/api/keepAlive").
			contentBuilder			(new JsonContentBuilder<>(SessionData.class))
	),
	
	/***************************** Balance ***********************************/
	

	/************************** GETBET TICKET ********************************/

	/************************** GET ACCOUNT DETAIL ********************************/

	/******************************** PLACE BET ******************************/

	/********************************** DELETE BET *****************************/

	/********************************** GET BET *****************************/
	
	/******************************** MINI BET LIST **************************/
	
	/****************************** STATEMENT & BET HISTORY ******************/

	/****************************** LIVE STATE ******************/
	GET_PROFILE(
			new URIDataHolder().
					uri						("/authen/users/profile").
					method					(HttpMethod.GET).
					contentBuilder(new JsonContentBuilder<>(GetProfileResponse.class)
					)
	),;

	
	// Company name
	public static final String COMPANY_NAME = "VN EXPRESS";
    // Data holder
	private URIDataHolder data;
	
	VNEURIDefinition(URIDataHolder data) {
		this.data = data;
	}

	public static<T> URIDataHolder getJsonRpcURI(Class<T> responseClass) {
		return new URIDataHolder().
				uri						("/exchange/betting/json-rpc/v1").
				method					(HttpMethod.POST).
				contentBuilder			(new JsonContentBuilder<T>(responseClass));
	}

	public static<T> URIDataHolder getAccountJsonRpcURI(Class<T> responseClass) {
		return new URIDataHolder().
				uri						("/exchange/account/json-rpc/v1").
				method					(HttpMethod.POST).
				contentBuilder			(new JsonContentBuilder<T>(responseClass));
	}

	@Override
	public String sportbook() {
		return COMPANY_NAME;
	}

	@Override
	public URIDataHolder data() {
		return data;
	}

	public static final String STREAM_API_HOST = "stream-api.betfair.com";
	public static final int STREAM_API_PORT = 443;
}
