package jayeson.lib.betting.VNE.recentbetstatus;

import dagger.Lazy;
import jayeson.lib.betting.VNE.runningbetlist.BetFairRunningBetListTask;
import jayeson.lib.betting.api.actioncontext.RecentBetStatusContext;
import jayeson.lib.betting.api.actionresults.*;
import jayeson.lib.betting.api.datastructure.BetStatus;
import jayeson.lib.betting.api.task.*;
import jayeson.lib.betting.VNE.getbethistorybyid.BetFairGetHistoriesByIdsTask;
import jayeson.lib.betting.core.BettingAccount;
import jayeson.lib.betting.core.dagger.qualifier.MasterTaskHandler;
import jayeson.lib.betting.core.dagger.qualifier.MasterTaskId;
import jayeson.lib.betting.core.dagger.qualifier.MasterTaskInfo;
import jayeson.lib.betting.core.dagger.qualifier.MasterTaskObservable;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;
import jayeson.lib.betting.core.task.GetRecentBetStatusMasterTask;
import jayeson.lib.betting.core.task.IsBaseChecker;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;

import javax.inject.Inject;
import java.util.stream.Collectors;

/**
 * After get mini bets by master task, if still have MISSING bets
 */
@TaskScope
public class RecentBetStatusMasterTask extends GetRecentBetStatusMasterTask {
    private Lazy<IsBaseChecker> checker;
    private final Lazy<BetFairRunningBetListTask> runningBetListTaskLazy;
    private final Lazy<BetFairGetHistoriesByIdsTask> getBetByIdTaskLazy;

    @Inject
    public RecentBetStatusMasterTask(ITaskExecutor executor, BettingAccount account,
                                     @MasterTaskInfo AccountTaskInfo data,
                                     @MasterTaskId String id,
                                     @MasterTaskHandler AccountTaskHandler<ListRecentBetStatusResult> handler,
                                     @MasterTaskObservable TaskObservable<AccountTaskInfo> observable,
                                     Lazy<BetFairRunningBetListTask> getMiniBetListTaskLazy,
                                     Lazy<BetFairGetHistoriesByIdsTask> getBetByIdTaskLazy,
                                     Lazy<IsBaseChecker> checker,
                                     RecentBetStatusContext recentBetStatusContext,
                                     ListRecentBetStatusResult result) {
        super(executor, data, account, id, handler, observable, recentBetStatusContext, result);
        this.runningBetListTaskLazy = getMiniBetListTaskLazy;
        this.getBetByIdTaskLazy = getBetByIdTaskLazy;
        this.checker = checker;
    }

    @Override
    public void execute() {
        result().setResultList(context.getBetIds().stream().map((id) -> {
            RecentBetStatusResult res = new RecentBetStatusResult(id);
            res.setStatus(BetStatus.MISSING);
            return res;
        }).collect(Collectors.toList()));

        currentSubTask = proceedGetMiniBetList();
    }

    private TaskObservable<AccountTaskInfo> proceedGetMiniBetList() {
        BetFairRunningBetListTask miniBetListTask = runningBetListTaskLazy.get();
        executor.submit(miniBetListTask);
        return miniBetListTask.getObservable();
    }

    private final AccountTaskHandler<ListActionResult<MiniBetResult>> miniBetTaskHandler =
            (ListActionResult<MiniBetResult> result, AccountTask<ListActionResult<MiniBetResult>> task) -> {

        synchronized (data) {
            if (data.getState() == AccountTaskState.CANCELLED) {
                reportResult(ActionResultCode.FAILED, "Task is cancelled");
                return;
            }
            // Update progress
            if(result.getCode() != ActionResultCode.SUCCESSFUL) {
                updateProgress(100, AccountTaskState.FAILED);
                reportResult(result.getCode(), "Fail to get mini bets " + task.id() + ": " + result.getMessage());
                return;
            }
            account.getCommonLogger().info("map mini bet to recent bet!");
            mapMiniBetToRecentBet(result, result());
            boolean isEnough = checker.get().apply((ListMiniBetResult) result);
            if(!isEnough) {
                updateProgress(50, null);
                getBetHistoryById();
            } else {
                updateProgress(100, AccountTaskState.SUCCEEDED);
                reportResult(result());
            }
        }
    };

    private void getBetHistoryById() {
        currentSubTask = proceedBetHistoryById();
    }

    private TaskObservable<AccountTaskInfo> proceedBetHistoryById() {
        BetFairGetHistoriesByIdsTask getBetByIdTask = getBetByIdTaskLazy.get();
        executor.submit(getBetByIdTask);
        return getBetByIdTask.getObservable();
    }

    private final AccountTaskHandler<ListBetHistoryResult> betHistoryByIdTaskHandler =
            (ListBetHistoryResult result, AccountTask<ListBetHistoryResult> task) -> {
                synchronized (data) {
                    if (data.getState() == AccountTaskState.CANCELLED) {
                        reportResult(ActionResultCode.FAILED, "Task is cancelled");
                        return;
                    }
                    // Update progress
                    if(result.getCode() == ActionResultCode.FAILED) {
                        updateProgress(100, AccountTaskState.FAILED);
                        reportResult(result.getCode(), "Fail to get bet by id" + task.id() + ": " + result.getMessage());
                        return;
                    }

                    mapMiniBetToRecentBet(result, result());

                    updateProgress(100, AccountTaskState.SUCCEEDED);
                    reportResult(result.getCode(), result.getMessage());
                }
            };

    public AccountTaskHandler<ListActionResult<MiniBetResult>> getMiniBetTaskHandler() {
        return miniBetTaskHandler;
    }

    public AccountTaskHandler<ListBetHistoryResult> getBetByIdTaskHandler() {
        return betHistoryByIdTaskHandler;
    }
}
