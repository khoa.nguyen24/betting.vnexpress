package jayeson.lib.betting.VNE.recentbetstatus;

import dagger.BindsInstance;
import dagger.Subcomponent;
import jayeson.lib.betting.api.actioncontext.RecentBetStatusContext;
import jayeson.lib.betting.api.actionresults.ListRecentBetStatusResult;
import jayeson.lib.betting.api.task.AccountTaskHandler;
import jayeson.lib.betting.core.dagger.qualifier.MasterTaskHandler;
import jayeson.lib.betting.core.dagger.taskscope.ITaskComponent;
import jayeson.lib.betting.core.dagger.taskscope.TaskModule;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;

@TaskScope
@Subcomponent(modules = {TaskModule.class, RecentBetStatusMasterTaskModule.class})
public interface RecentBetStatusMasterTaskComponent extends ITaskComponent<ListRecentBetStatusResult> {
    @Override
    RecentBetStatusMasterTask getTask();

    @Subcomponent.Builder
    interface Builder extends BuilderContext<ListRecentBetStatusResult, RecentBetStatusContext> {
        @Override @BindsInstance
        Builder bindsHandler(@MasterTaskHandler AccountTaskHandler<ListRecentBetStatusResult> handler);

        RecentBetStatusMasterTaskComponent build();
    }
}
