package jayeson.lib.betting.VNE.recentbetstatus;

import dagger.Module;
import dagger.Provides;
import jayeson.lib.betting.VNE.runningbetlist.BetFairRunningBetListTask;
import jayeson.lib.betting.api.actioncontext.BetHistoryByBetIdContext;
import jayeson.lib.betting.api.actioncontext.RecentBetStatusContext;
import jayeson.lib.betting.api.actionresults.*;
import jayeson.lib.betting.api.datastructure.BetStatus;
import jayeson.lib.betting.api.task.AccountTaskHandler;
import jayeson.lib.betting.VNE.getbethistorybyid.BetFairGetHistoriesByIdsModule;
import jayeson.lib.betting.core.dagger.taskscope.MasterTaskModule;
import jayeson.lib.betting.core.dagger.taskscope.TaskModule;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;
import jayeson.lib.betting.core.task.IsBaseChecker;
import jayeson.lib.betting.core.task.IsRecentBetChecker;
import jayeson.lib.betting.core.task.TaskFragmentListener;

import java.util.List;
import java.util.stream.Collectors;

@Module(includes = {TaskModule.class, MasterTaskModule.class, BetFairGetHistoriesByIdsModule.class})
public class RecentBetStatusMasterTaskModule {

    @TaskScope @Provides
    ListRecentBetStatusResult provideResult(RecentBetStatusContext context) {
        ListRecentBetStatusResult result = new ListRecentBetStatusResult();
        result.setResultList(context.getBetIds().stream().map(id -> {
                    RecentBetStatusResult res = new RecentBetStatusResult(id);
                    res.setStatus(BetStatus.MISSING);
                    return res;}
                ).collect(Collectors.toList())
        );
        return result;
    }

    @TaskScope @Provides
    AccountTaskHandler<ListBetHistoryResult> provideBetByIDTaskHandler(RecentBetStatusMasterTask masterTask) {
        return masterTask.getBetByIdTaskHandler();
    }


    @TaskScope @Provides
    AccountTaskHandler<ListActionResult<MiniBetResult>> provideMiniTaskHandler(RecentBetStatusMasterTask task) {
        return task.getMiniBetTaskHandler();
    }

    @TaskScope @Provides
    TaskFragmentListener<ListActionResult<MiniBetResult>> provideMiniTaskFragmentListener(BetFairRunningBetListTask task) {
        return task;
    }

    @TaskScope @Provides
    ListActionResult<MiniBetResult> provideMiniResult() {
        return new ListMiniBetResult();
    }

    @TaskScope @Provides
    BetHistoryByBetIdContext provideBetHistoryContext(ListRecentBetStatusResult result) {
        List<String> ids = result.getResultList().stream()
                .filter(bet -> bet.getStatus().equals(BetStatus.MISSING))
                .map(RecentBetStatusResult::getId)
                .collect(Collectors.toList());
        return new BetHistoryByBetIdContext(ids);
    }

    @TaskScope @Provides
    IsBaseChecker provideRecentBetChecker(IsRecentBetChecker checker) {
        return checker;
    }

}
