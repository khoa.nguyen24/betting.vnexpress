package jayeson.lib.betting.VNE;

public interface IAccountChangeListener {
    void accountDisconnect();
    default boolean removable() {
        return false;
    }
}
