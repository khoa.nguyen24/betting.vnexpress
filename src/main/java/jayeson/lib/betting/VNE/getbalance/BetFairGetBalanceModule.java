package jayeson.lib.betting.VNE.getbalance;

import dagger.Module;
import dagger.Provides;
import jayeson.lib.betting.api.actionresults.BalanceResult;
import jayeson.lib.betting.core.dagger.taskscope.TaskModule;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;
import jayeson.lib.betting.core.task.TaskFragmentListener;

/**
 * @author Nam Nguyen
 */
@Module(includes = {TaskModule.class})
public class BetFairGetBalanceModule {

	@TaskScope
	@Provides
	BalanceResult providesResult() {
		return new BalanceResult();
	}

	@TaskScope
	@Provides
	TaskFragmentListener<BalanceResult> provideTaskFragmentListener(BetFairGetBalanceTask task) {
		return task;
	}
}
