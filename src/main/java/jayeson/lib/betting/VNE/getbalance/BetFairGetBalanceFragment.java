package jayeson.lib.betting.VNE.getbalance;


import jayeson.lib.betting.VNE.VNE;
import jayeson.lib.betting.VNE.VNEURIDefinition;
import jayeson.lib.betting.VNE.VNEUtility;
import jayeson.lib.betting.VNE.json.balance.AccountFundPayload;
import jayeson.lib.betting.api.actionresults.ActionResultCode;
import jayeson.lib.betting.api.actionresults.BalanceResult;
import jayeson.lib.betting.api.datastructure.BADRuntime;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.VNE.json.accountfunds.AccountFundsResponse;
import jayeson.lib.betting.core.http.IClient;
import jayeson.lib.betting.core.http.IResponse;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.http.request.JsonRequest;
import jayeson.lib.betting.core.task.AsyncHttpTaskFragment;
import jayeson.lib.betting.core.task.TaskFragmentListener;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;
import jayeson.lib.betting.exception.BettingException;

import javax.inject.Inject;

/**
 * @author Nam Nguyen
 */

public class BetFairGetBalanceFragment extends AsyncHttpTaskFragment<BalanceResult, AccountFundsResponse> {

	private final BADRuntime badRuntime;
	private final VNE VNEAccount;

	@Inject
	public BetFairGetBalanceFragment(AccountTaskInfo data, ITaskExecutor executor,
									 IResponseListener responseListener, VNE account,
									 TaskFragmentListener<BalanceResult> listener, BalanceResult initialResult,
									 IClient client, BADRuntime badRuntime) {
		super(data, executor, responseListener, account, listener, initialResult, client);
		this.setDef(VNEURIDefinition.getAccountJsonRpcURI(AccountFundsResponse.class));
		this.VNEAccount = account;
		this.badRuntime = badRuntime;
		needProceed = false;
	}

	@Override
	public void execute() throws BettingException {
		AccountFundPayload payload = new AccountFundPayload();

		if (VNEUtility.isNeedToResetBettingApi(client.host().url().toString())) {
			client.resetHost(VNEUtility.getBettingApiUrl(client.host().url().toString()));
		}

		JsonRequest request = new JsonRequest.Builder()
				.def(this.getDef())
				.host(client.host())
				.data(payload)
				.addHeader("X-Authentication", this.badRuntime.getLoggedInUid())
				.addHeader("X-Application", VNEAccount.getAppKey())
				.build();

		executeRequest(request, true);
	}

	@Override
	public void consumeResponse(IResponse<AccountFundsResponse> response) throws BettingException {
		account.getCommonLogger().info("RESPONSE_READY_{}", response.getStatusLine());
		if (response.getStatusLine().getCode() != 200) {
			account.getCommonLogger().error("Failed to get balance for BetFair {}", response);
			result().setCode(ActionResultCode.FAILED);
			result().setMessage("The request failed with http code " + response.getStatusLine().getCode());
			return;
		}

		if (response.content() != null) {
			if (response.content().result != null) {
				account.getCommonLogger().info("AVAILABLE_TO_BET_BALANCE_{}", response.content().result.availableToBetBalance);
				result().setCode(ActionResultCode.SUCCESSFUL);
				result().setBalance(response.content().result.availableToBetBalance);
				return;
			} else {
				account.getCommonLogger().error("GET_BALANCE_ERROR_{}", response.content().error);
			}
		} else {
			account.getCommonLogger().error("GET_BALANCE_FAILED");
		}

		result().setCode(ActionResultCode.FAILED);
	}

	@Override
	public void proceed(BalanceResult currentResult) {
	}

	@Override
	public boolean logHttpRequest() {
		return true;
	}
}
