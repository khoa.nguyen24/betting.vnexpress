package jayeson.lib.betting.VNE.getbalance;


import dagger.Subcomponent;
import jayeson.lib.betting.api.actionresults.BalanceResult;
import jayeson.lib.betting.core.dagger.taskscope.ITaskComponent;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;

@TaskScope
@Subcomponent(modules = BetFairGetBalanceModule.class)
public interface BetFairGetBalanceComponent extends ITaskComponent<BalanceResult> {

    @Override
    BetFairGetBalanceTask getTask();

    @Subcomponent.Builder
    interface Builder extends ITaskComponent.Builder<BalanceResult> {
        BetFairGetBalanceComponent build();
    }
}
