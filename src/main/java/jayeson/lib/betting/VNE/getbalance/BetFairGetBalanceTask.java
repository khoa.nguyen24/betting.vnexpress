package jayeson.lib.betting.VNE.getbalance;

import dagger.Lazy;
import jayeson.lib.betting.api.actionresults.BalanceResult;
import jayeson.lib.betting.api.task.AccountTaskHandler;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.api.task.TaskObservable;
import jayeson.lib.betting.core.BettingAccount;
import jayeson.lib.betting.core.dagger.qualifier.TaskId;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.task.BalanceTask;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;

import javax.inject.Inject;

/**
 * @author Nam Nguyen
 */
public class BetFairGetBalanceTask extends BalanceTask {

	public static int WORK_LOAD = 10;
	private Lazy<BetFairGetBalanceFragment> betFairGetBalanceFragment;

	@Inject
	public BetFairGetBalanceTask(ITaskExecutor executor, IResponseListener responseListener,
								 AccountTaskInfo data, BettingAccount account, @TaskId String id,
								 AccountTaskHandler<BalanceResult> handler, TaskObservable<AccountTaskInfo> observable,
								 Lazy<BetFairGetBalanceFragment> betFairGetBalanceFragment, BalanceResult result) {
		super(executor, responseListener, data, account, id, WORK_LOAD, handler, observable, result);
		this.betFairGetBalanceFragment = betFairGetBalanceFragment;
	}

	@Override
	public void execute() {
		account.getCommonLogger().info("START_GET_BALANCE_TASK");
		executor.submit(betFairGetBalanceFragment.get());
		account.getCommonLogger().info("LAUNCH_GET_BALANCE_FRAGMENT");
	}
}
