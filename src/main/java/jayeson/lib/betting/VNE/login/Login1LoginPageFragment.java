package jayeson.lib.betting.VNE.login;

import com.google.common.base.Strings;
import dagger.Lazy;
import jayeson.lib.betting.VNE.VNEURIDefinition;
import jayeson.lib.betting.VNE.VNEUtility;
import jayeson.lib.betting.api.actionresults.LoginResult;
import jayeson.lib.betting.api.actionresults.LoginResultCode;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.core.BettingAccount;
import jayeson.lib.betting.core.http.IClient;
import jayeson.lib.betting.core.http.IResponse;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.http.request.BasicRequest;
import jayeson.lib.betting.core.http.request.BettingRequest;
import jayeson.lib.betting.core.task.AsyncHttpTaskFragment;
import jayeson.lib.betting.core.task.TaskFragmentListener;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;
import jayeson.lib.betting.exception.BettingException;
import okhttp3.HttpUrl;

import javax.inject.Inject;

public class Login1LoginPageFragment extends AsyncHttpTaskFragment<LoginResult, String> {
    private Lazy<Login2GetSessionFragment> loginHomeFragment;
    private String csrf;

    @Inject
    public Login1LoginPageFragment(AccountTaskInfo data, ITaskExecutor executor, IResponseListener responseListener,
                                   BettingAccount account, TaskFragmentListener<LoginResult> listener, LoginResult initialResult,
                                   Lazy<Login2GetSessionFragment> loginHomeFragment, IClient client) {
        super(data, executor, responseListener, account, listener, initialResult, client);
        this.loginHomeFragment = loginHomeFragment;
        needProceed = false;
    }

    @Override
    public void proceed(LoginResult currentResult) {
        Login2GetSessionFragment fragment = loginHomeFragment.get();
        fragment.setCsrf(this.csrf);
        account.getCommonLogger().info("LAUNCH_FRAGMENT_LOGIN_HOME");
        executor.submit(fragment);
    }



    // Where do client config? How to set client host?
    // Need to set vnexpress.net here
    @Override
//            client.resetHost(VNEUtility.getLoginApiUrl(client.host().scheme(), client.originalUrl()));

    public void execute() throws BettingException {
        client.resetHost(VNEUtility.getLoginApiUrl(client.host().scheme(),client.originalUrl()));
        BettingRequest request = new BasicRequest.Builder()
                .def(VNEURIDefinition.LOGIN_PAGE)
                .host(client.host())
                .build();
        System.out.println("Login 1 befor exe - code: " + result().getCode());
        executeRequest(request, true);
    }

    /**
     * Get tmxId from Html, <input type="hidden" name="tmxId" value="d8e4bdc4-4bb6-48c1-b1ab-38cb67d95cb7"/>
     * @param response
     *            Wrapped HttpResponse returned by HttpClient
     * @throws BettingException
     */
    @Override
    public void consumeResponse(IResponse<String> response) throws BettingException {
        account.getCommonLogger().info("RESPONSE_READY_{}", response.getStatusLine());

        this.csrf = VNEUtility.extractValue(VNEURIDefinition.LOGIN_PAGE.regexPattern("csrf"),
                response.content(), 1);

        if (Strings.isNullOrEmpty(this.csrf)) {
            needProceed = false;
            result().setCode(LoginResultCode.FAILED);
            result().setMessage("Fail to get csrf");
        } else {
            needProceed = true;
            System.out.println("Login 1 after consume- code: " + result().getCode());
        }
    }

    @Override
    public boolean logHttpRequest() {
        return true;
    }
}
