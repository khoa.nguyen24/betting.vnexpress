package jayeson.lib.betting.VNE.login;

import dagger.Module;
import dagger.Provides;
import jayeson.lib.betting.api.actionresults.LoginResult;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;
import jayeson.lib.betting.core.task.TaskFragmentListener;

@Module
public class LoginModule {

	@TaskScope @Provides
	LoginResult providesResult() {
		return new LoginResult();
	}
	
	@TaskScope @Provides
	TaskFragmentListener<LoginResult> provideTaskFragmentListener(VNELoginTask task) {
        return task;
    }
}
