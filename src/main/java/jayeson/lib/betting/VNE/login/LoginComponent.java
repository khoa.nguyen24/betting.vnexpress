package jayeson.lib.betting.VNE.login;


import dagger.Subcomponent;
import jayeson.lib.betting.api.actionresults.LoginResult;
import jayeson.lib.betting.core.dagger.taskscope.ITaskComponent;
import jayeson.lib.betting.core.dagger.taskscope.TaskModule;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;

@Subcomponent(modules = {LoginModule.class, TaskModule.class})
@TaskScope
public interface LoginComponent extends ITaskComponent<LoginResult> {

    @Override
    VNELoginTask getTask();

    @Subcomponent.Builder
    interface Builder extends ITaskComponent.Builder<LoginResult> {
        @Override
        LoginComponent build();
    }
}
