package jayeson.lib.betting.VNE.login;

import jayeson.lib.betting.VNE.VNE;
import jayeson.lib.betting.VNE.VNEURIDefinition;
import jayeson.lib.betting.VNE.VNEUtility;
import jayeson.lib.betting.VNE.json.accountdetail.AccountDetailRespone;
import jayeson.lib.betting.VNE.json.login.GetProfileResponse;
import jayeson.lib.betting.VNE.json.login.LoginUserResponse;
import jayeson.lib.betting.api.actionresults.ActionResultCode;
import jayeson.lib.betting.api.actionresults.LoginResult;
import jayeson.lib.betting.api.datastructure.BADRuntime;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.core.exception.UnknownResponseException;
import jayeson.lib.betting.core.http.IClient;
import jayeson.lib.betting.core.http.IResponse;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.http.request.BasicRequest;
import jayeson.lib.betting.core.http.request.BettingRequest;
import jayeson.lib.betting.core.task.AsyncHttpTaskFragment;
import jayeson.lib.betting.core.task.TaskFragmentListener;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;
import jayeson.lib.betting.core.utility.BettingUtility;
import jayeson.lib.betting.exception.BettingException;

import javax.inject.Inject;

public class LoginAdditionalGetAccountDetailFragment extends AsyncHttpTaskFragment<LoginResult, GetProfileResponse> {
    private final BADRuntime badRuntime;
    private final VNE account;

    @Inject
    public LoginAdditionalGetAccountDetailFragment(AccountTaskInfo data, ITaskExecutor executor, IResponseListener responseListener,
                                                   VNE account, TaskFragmentListener<LoginResult> listener,
                                                   LoginResult initialResult, IClient client, BADRuntime badRuntime) {
        super(data, executor, responseListener, account, listener, initialResult, client);
        this.badRuntime = badRuntime;
        this.account = account;
        this.setDef(VNEURIDefinition.getAccountJsonRpcURI(AccountDetailRespone[].class));
        needProceed = false;
    }


    @Override
    public void execute() throws BettingException {
        client.resetHost(VNEUtility.getLoginApiUrl(client.host().scheme(),client.originalUrl()));
        System.out.println("HOST: " + client.host());
        // Build request
        BettingRequest request = new BasicRequest.Builder()
                .def(VNEURIDefinition.GET_PROFILE)
                .host(client.host())
                .build();

        executeRequest(request, true);


    }

    @Override
    public void consumeResponse(IResponse<GetProfileResponse> iResponse) throws BettingException {
        account.getCommonLogger().info("RESPONSE_READY_{}", response.getStatusLine());
        if (response.getStatusLine().getCode() != 200) {
            account.getCommonLogger().error("FAIL to get account detail for BetFair {}", response);
            result().setCode(ActionResultCode.FAILED);
            result().setMessage("The request failed with http code " + response.getStatusLine().getCode());
            return;
        }
        GetProfileResponse data= iResponse.content();

        if(data.getError() != 0) {
            account.getCommonLogger().error("Failed to get account detail: {} ", BettingUtility.writeObjectToJson(data.getError()));
            throw new UnknownResponseException("Failed to get account detail", getDef(), "");
        }
        System.out.println(data.toString());

    }

    @Override
    public void proceed(LoginResult loginResult) {
    }

    @Override
    public boolean logHttpRequest() {
        return true;
    }
}
