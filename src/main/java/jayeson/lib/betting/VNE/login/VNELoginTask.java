package jayeson.lib.betting.VNE.login;

import com.google.common.base.Strings;
import dagger.Lazy;
import jayeson.lib.betting.VNE.VNE;
import jayeson.lib.betting.api.actionresults.LoginResult;
import jayeson.lib.betting.api.task.AccountTaskHandler;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.api.task.TaskObservable;
import jayeson.lib.betting.core.dagger.qualifier.TaskId;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.task.LoginTask;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;
import okhttp3.HttpUrl;

import javax.inject.Inject;

@TaskScope
public class VNELoginTask extends LoginTask {

	public static final int LOGIN_WL = 10;
	private Lazy<Login1LoginPageFragment> homeFragmentLazy;
	private final VNE account;

	@Inject
	public VNELoginTask(ITaskExecutor executor, IResponseListener responseListener, AccountTaskInfo data,
						VNE account, @TaskId String id, AccountTaskHandler<LoginResult> handler,
						TaskObservable<AccountTaskInfo> observable, LoginResult result,
						Lazy<Login1LoginPageFragment> homeFragmentLazy) {
		super(executor, responseListener, data, account, id, LOGIN_WL, handler, observable, result);
		this.homeFragmentLazy = homeFragmentLazy;
		this.account = account;
	}

	@Override
	public void execute() {
		account.getCommonLogger().info("START_LOGGING_IN");
		account.getCommonLogger().info("LAUNCH_FRAGMENT_HOME");
		executor.submit(homeFragmentLazy.get());
	}
}
