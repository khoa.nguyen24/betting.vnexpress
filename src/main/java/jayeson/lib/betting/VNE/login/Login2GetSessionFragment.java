package jayeson.lib.betting.VNE.login;

import com.google.common.base.Strings;
import dagger.Lazy;
import jayeson.lib.betting.VNE.VNEURIDefinition;
import jayeson.lib.betting.VNE.VNEUtility;
import jayeson.lib.betting.VNE.json.login.LoginUserResponse;
import jayeson.lib.betting.api.actionresults.ActionResultCode;
import jayeson.lib.betting.api.actionresults.LoginResult;
import jayeson.lib.betting.api.actionresults.LoginResultCode;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.core.BettingAccount;
import jayeson.lib.betting.core.http.IClient;
import jayeson.lib.betting.core.http.IResponse;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.http.request.FormRequest;
import jayeson.lib.betting.core.task.AsyncHttpTaskFragment;
import jayeson.lib.betting.core.task.TaskFragmentListener;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;
import jayeson.lib.betting.exception.BettingException;

import javax.inject.Inject;
import java.net.HttpCookie;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Login2GetSessionFragment extends AsyncHttpTaskFragment<LoginResult, LoginUserResponse> {
//    private Lazy<Login3GetAppKeyFragment> getAppKeyFragmentLazy;
    private String csrf;
    private String ssoid;

    private String userId;

    private Lazy<LoginAdditionalGetAccountDetailFragment> loginAdditionalGetAccountDetailFragmentLazy;
    @Inject
    public Login2GetSessionFragment(AccountTaskInfo data, ITaskExecutor executor, IResponseListener responseListener,
                                    BettingAccount account, TaskFragmentListener<LoginResult> listener, LoginResult initialResult,
                                    Lazy<LoginAdditionalGetAccountDetailFragment> loginAdditionalGetAccountDetailFragmentLazy, IClient client) {
        super(data, executor, responseListener, account, listener, initialResult, client);
        this.loginAdditionalGetAccountDetailFragmentLazy = loginAdditionalGetAccountDetailFragmentLazy;
        needProceed = false;
    }

    @Override
    public void proceed(LoginResult currentResult) {
        LoginAdditionalGetAccountDetailFragment fragment = loginAdditionalGetAccountDetailFragmentLazy.get();
//        fragment.setSsoid(ssoid);
//        account.getCommonLogger().info("LAUNCH_FRAGMENT_GET_APP_KEY");
        System.out.println("Continue");
          executor.submit(fragment);
    }

    @Override
    public void execute() throws BettingException {
        client.resetHost(VNEUtility.getLoginApiUrl(client.host().scheme(),client.originalUrl()));
        Map<String, String> params = new HashMap<>();
        params.put("myvne_email", account.getUsername());
        params.put("myvne_password", account.getPassword());
        params.put("csrf", this.csrf);

        FormRequest request = new FormRequest.Builder()
                .def(VNEURIDefinition.LOGIN_HOME)
                .host(client.host())
                .data(params)
                .build();

        executeRequest(request, true);
    }

    @Override
    public void consumeResponse(IResponse<LoginUserResponse> response) throws BettingException {
        account.getCommonLogger().info("RESPONSE_READY_{}", response.getStatusLine());
        System.out.println("RESPONSE " + response.content().toString());
        LoginUserResponse data = response.content();
//        String error = VNEUtility.extractValue(
//                VNEURIDefinition.LOGIN_HOME.regexPattern("error"), response.content(), 1);
        int error = data.getError();
        System.out.println("error: " + error);
        if (response.getStatusLine().getCode() != 200 && (error != 0) ){
            account.getCommonLogger().error("Failed login HomePage_{}", response.getStatusLine());
            result().setCode(LoginResultCode.FAILED);
            result().setMessage("Fail to login HomePage");
            needProceed = false;
            return;
        }

        findSsoid();
        findUserId();
        System.out.println("Done sso and user id: " + ssoid + "and " + userId);

        if (Strings.isNullOrEmpty(ssoid)) {
            account.getCommonLogger().error("Failed to find Ssoid on {}", account.getOriginalUrl());
            result().setMessage("Fail to get Ssoid");
            result().setCode(ActionResultCode.FAILED);
            needProceed = false;
            return;
        }
        if (error == 0){
            result().setCode(ActionResultCode.SUCCESSFUL);
            System.out.println("LOGIN SUSCCESS, CONTINUE GET PROFILE");
            needProceed = true;
        } else {
            result().setCode(ActionResultCode.FAILED);
            System.out.println("login 2 failed");
        }
    }

    private void findSsoid() {
        List<HttpCookie> cookies =  client.cookieStore().getCookies();

        for (HttpCookie cookie: cookies) {
            if (cookie.getName().equalsIgnoreCase("myvne_sso"))
                ssoid = cookie.getValue();
        }
    }

    private void findUserId() {
        List<HttpCookie> cookies =  client.cookieStore().getCookies();

        for (HttpCookie cookie: cookies) {
            if (cookie.getName().equalsIgnoreCase("myvne_user_id"))
                userId = cookie.getValue();
        }
    }

    public void setCsrf(String csrf) {
        this.csrf = csrf;
    }

    @Override
    public boolean logHttpRequest() {
        return true;
    }
}
