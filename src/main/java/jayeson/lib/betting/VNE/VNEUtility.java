package jayeson.lib.betting.VNE;

import com.google.common.collect.ImmutableMap;
import jayeson.lib.betting.api.actioncontext.BetInfoContext;
import jayeson.lib.betting.VNE.json.listcurrentorders.CurrentOrderType;
import jayeson.lib.betting.VNE.json.marketcatalogue.Description;
import jayeson.lib.betting.VNE.json.marketcatalogue.MarketCatalogue;
import jayeson.lib.betting.VNE.json.marketcatalogue.Runner;
import jayeson.lib.betting.core.exception.BuildRequestFailException;
import jayeson.lib.betting.core.http.extractor.Pair;
import jayeson.lib.betting.exception.InvalidAttributeException;
import jayeson.lib.betting.exception.NewValueException;
import jayeson.lib.betting.utility.OddsConverter;
import jayeson.lib.feed.api.LBType;
import jayeson.lib.feed.api.SportType;
import jayeson.lib.feed.api.twoside.PivotType;
import jayeson.lib.feed.api.twoside.TargetType;
import jayeson.lib.feed.basketball.BasketballTimeType;
import jayeson.lib.feed.soccer.SoccerTimeType;
import okhttp3.HttpUrl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VNEUtility {

  /**
   * Need synchronize with Feed to correct input data.
   *
   * @see: scraper/src/sample/sportbook/parser/betfair_parser.rb #parse_market -> Market
   */
  public static final Map<String, Pair<PivotType, SoccerTimeType>> soccerKey =
      ImmutableMap.of(
          "match odds", new Pair<>(PivotType.ONE_TWO, SoccerTimeType.FT),
          "half time", new Pair<>(PivotType.ONE_TWO, SoccerTimeType.HT),
          "asian handicap", new Pair<>(PivotType.HDP, SoccerTimeType.FT));

  public static final List<Pair<String, Pair<PivotType, SoccerTimeType>>> soccerRegex =
      Arrays.asList(
          new Pair<>(
              "^(O|o)ver\\/(U|u)nder (\\d\\.\\d) (G|g)oals",
              new Pair<>(PivotType.TOTAL, SoccerTimeType.FT)),
          new Pair<>(
              "^(F|f)irst (H|h)alf (G|g)oals (\\d\\.\\d)",
              new Pair<>(PivotType.TOTAL, SoccerTimeType.HT)),
          new Pair<>(
              "^(C|c)orners (O|o)ver\\/(U|u)nder (\\d+\\.\\d)",
              new Pair<>(PivotType.TOTAL, SoccerTimeType.FT)),
          new Pair<>(
              "^(C|c)ards (O|o)ver\\/(U|u)nder (\\d+\\.\\d)",
              new Pair<>(PivotType.TOTAL, SoccerTimeType.FT)),
          new Pair<>("^(G|g)oal (L|l)ines", new Pair<>(PivotType.TOTAL, SoccerTimeType.FT)));

  /**
   * Need synchronize with Feed to correct input data.
   *
   * @see: scraper/src/sample/sportbook/parser/betfair_parser.rb #parse_market -> BasketballMarket
   */
  public static final Map<String, Pair<PivotType, BasketballTimeType>> basketballKey =
      ImmutableMap.of(
          "handicap", new Pair<>(PivotType.HDP, BasketballTimeType.FT),
          "total points", new Pair<>(PivotType.TOTAL, BasketballTimeType.FT),
          "moneyline", new Pair<>(PivotType.ONE_TWO, BasketballTimeType.FT),
          "match odds", new Pair<>(PivotType.ONE_TWO, BasketballTimeType.FT));

  /**
   * Need synchronize with Feed to correct input data.
   *
   * @see: scraper/src/sample/sportbook/parser/betfair_parser.rb #parse_market -> TennisMarket
   *
   * <p>Currently, we do not care about the time type of tennis.
   */
  public static final Map<String, PivotType> tennisKey =
      ImmutableMap.of("match odds", PivotType.ONE_TWO);

  public static final List<Pair<String, PivotType>> tennisRegex =
      Arrays.asList(new Pair<>("^(S|s)et (\\d) (W|w)inner", PivotType.ONE_TWO));

  public static final List<String> casePivotOneTwo =
      Arrays.asList("match odds", "half time", "moneyline");

  /**
   * We support get selections of Gold lines:
   * - SOCCER:
   *    * normal over/under:     x.5
   *    * goal lines over/under: x.25, x.75, x.0
   * - BASKETBALL: only goal lines over/under
   * - TENNIS:     does not support over/under
   *
   * <p>Need synchronize with Feed to correct input data.
   * @see: scraper/src/sample/sportbook/parser/betfair_parser.rb
   *
   * @param sportType the sport type of selection
   * @param pivot     the pivot value of selection
   * @return is goal lines selection or not
   */
  public static boolean isGoalLinesSelection(SportType sportType, double pivot) {
    boolean soccer =
        SportType.SOCCER.equals(sportType)
            && BigDecimal.valueOf(pivot - (int) pivot).compareTo(BigDecimal.valueOf(0.5)) != 0;

    boolean basketball = SportType.BASKETBALL.equals(sportType);

    // TODO: update this method if Feed support gold lines over/under selection for tennis

    return soccer || basketball;
  }

    public static String extractValue(String patternStr, String content, int idxGroup) {
        Matcher matcher = Pattern.compile(patternStr).matcher(content);
        if (matcher.find()) {
            return matcher.group(idxGroup);
        }
        return "";
    }

    public static boolean extractValue(String patternStr, String content) {
        Matcher matcher = Pattern.compile(patternStr).matcher(content);
        return matcher.find();
    }

    public static HttpUrl getBettingApiUrl(String domainName) {
        final String regex = "(http.*):\\/\\/(.*?)\\.(.*)";
        final String subst = "$1://api.$3";

        final Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
        final Matcher matcher = pattern.matcher(domainName);

        final String result = matcher.replaceAll(subst);
        return HttpUrl.get(result);
    }

    public static HttpUrl getLoginApiUrl(String scheme, String domainName) {
        HttpUrl httpUrl = HttpUrl.get(domainName); // https://www.vnexpress.net/
        String mainDomain = httpUrl.host(); // vnexpress.net

        return HttpUrl.get(scheme + "://my." + mainDomain);
    }

    public static boolean isNeedToResetBettingApi(String domainName) {
        final String regex = "\\/\\/api\\.";

        final Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
        final Matcher matcher = pattern.matcher(domainName);

        return !matcher.find();
    }

    /**
     * rate format: event_id&market_id&selection_id&original_pivot
     */
    public static String getMarketId(BetInfoContext betInfoContext) throws BuildRequestFailException {
        String[] oddId = betInfoContext.getOddId().split(OddsConverter.ODD_DELIMETER);
        if (oddId.length < 2) {
            throw new BuildRequestFailException("Fail to get market id", null);
        }
        
        return oddId[1];
    }
    
    public static String findSelectionId(BetInfoContext context) throws BuildRequestFailException {
        String rawUid = context.getOddId();
        String[] idPair = rawUid.split(OddsConverter.ODD_DELIMETER);
        if (idPair.length < 3) {
            throw new BuildRequestFailException("Cannot find selection id", null);
        }
        return idPair[2];
    }
    
    public static double toHdpPivot(BetInfoContext context) throws BuildRequestFailException {
        String rawUid = context.getOddId();
        String[] idPair = rawUid.split(OddsConverter.ODD_DELIMETER);
        if (idPair.length < 4) {
            throw new BuildRequestFailException("Cannot find selection id", null);
        }
        return Double.parseDouble(idPair[3]);
    }
    
    /**
     * #26646 - Lee Selamat said:
     * Betfair settlement never use 3 decimal places, they only use 2 decimal places for settlement.
     * If betfair give you 0.019, you can not use this figure.. you need to use 0.01 only.
     *
     * #26803 - "averagePriceMatched":1.5594999999999999 -> 1.56
     */
    public static double oddTo3Decimal(double odd) {
        if (countNoDecimal(odd) <= 3) return odd;
        String newValue = String.format("%.4f", odd);
        return new BigDecimal(newValue).setScale(3, BigDecimal.ROUND_FLOOR).doubleValue();
    }

    /**
     * Rounds an odd by rules of Betfair Price Increments.
     * {@see https://docs.developer.betfair.com/display/1smk3cen4v3lu3yomq5qye0ni/Additional+Information}
     * Placing a bet outside of these increments will result in an INVALID_ODDS error.
     *
     * @param odd    the odd to round
     * @param lbType the LAY/BACK type
     * @param marketDescription the market description
     * @return the rounded-down odd if LAY or the rounded-up odd if BACK
     * @throws InvalidAttributeException
     */
    public static double roundOdd(double odd, LBType lbType, Description marketDescription) throws InvalidAttributeException, NewValueException {
        double increment = getIncrement(odd, marketDescription);
        return LBType.LAY.equals(lbType)
                ? roundByIncrement(odd, increment, RoundingMode.FLOOR) // LAY
                : roundByIncrement(odd, increment, RoundingMode.CEILING); // BACK
    }

    /**
     * Gets the price increment per price 'group'.
     * The Betfair Price increment used for a market is defined by the PriceLadderType returned within
     * the Market Description (listMarketCatalogue / Market Definition (Exchange Stream API).
     *
     * @param odd   the input odd
     * @param marketDescription the market description
     * @return the increment by the price ladder type range
     * @throws InvalidAttributeException
     */
    public static double getIncrement(double odd, Description marketDescription) throws InvalidAttributeException, NewValueException {
        if (marketDescription != null
                && marketDescription.priceLadder != null
                && marketDescription.priceLadder.type != null) {
            switch (marketDescription.priceLadder.type) {
                case CLASSIC:
                    return getClassicIncrement(odd);
                case FINEST:
                    return getFinestIncrement(odd);
                case LINE_RANGE:
                    return getLineRangeIncrement(odd, marketDescription);
                default:
                    throw new NewValueException(
                            "Not handle price ladder type description",
                            "PriceLadderType",
                            String.valueOf(marketDescription.priceLadder.type));
            }
        }
        throw new InvalidAttributeException("Not handle price ladder description", String.valueOf(marketDescription));
    }

    /**
     * Betfair Price Increments: CLASSIC
     *
     * <p>
     * Price        |   Increment
     * 1.01 -> 2    |    0.01
     * 2 -> 3       |   0.02
     * 3 -> 4       |   0.05
     * 4 -> 6       |   0.1
     * 6 -> 10      |   0.2
     * 10 -> 20     |   0.5
     * 20 -> 30     |   1
     * 30 -> 50     |   2
     * 50 -> 100    |   5
     * 100 -> 1000  |   10
     *
     * @param odd the classic input odd
     * @return the increment by the classic range
     * @throws InvalidAttributeException
     */
    public static double getClassicIncrement(double odd) throws InvalidAttributeException {
        if (odd >= 1d && odd < 2d) {
            return 0.01d;
        } else if (odd >= 2d && odd < 3d) {
            return 0.02d;
        } else if (odd >= 3d && odd < 4d) {
            return 0.05d;
        } else if (odd >= 4d && odd < 6d) {
            return 0.1d;
        } else if (odd >= 6d && odd < 10d) {
            return 0.2d;
        } else if (odd >= 10d && odd < 20d) {
            return 0.5d;
        } else if (odd >= 20d && odd < 30d) {
            return 1d;
        } else if (odd >= 30d && odd < 50d) {
            return 2d;
        } else if (odd >= 50d && odd < 100d) {
            return 5d;
        } else if (odd >= 100d && odd <= 1000d) {
            return 10d;
        }
        throw new InvalidAttributeException("Not handle classic type odd out of range 1 to 1000", String.valueOf(odd));
    }

    /**
     * Betfair Price Increments: FINEST
     *
     * <p>
     * Price           |   Increment
     * 1.01 -> 1000    |    0.01
     *
     * @param odd the finest input odd
     * @return the increment by the finest range
     * @throws InvalidAttributeException
     */
    public static double getFinestIncrement(double odd) throws InvalidAttributeException {
        if (odd >= 1d && odd <= 1000d) {
            return 0.01d;
        }
        throw new InvalidAttributeException("Not handle finest type odd out of range 1 to 1000", String.valueOf(odd));
    }

    /**
     * Betfair Price Increments: LINE_RANGE
     *
     * <p>
     * Price          |   Increment
     * 0.1 -> 1000    |   Specified by Market Line Range Info
     *
     * @param odd the line range input odd
     * @param marketDescription the market description
     * @return the increment by the market line range info
     * @throws InvalidAttributeException
     */
    public static double getLineRangeIncrement(double odd, Description marketDescription) throws InvalidAttributeException {
        if (marketDescription != null && marketDescription.lineRangeInfo != null
                && odd >= marketDescription.lineRangeInfo.minUnitValue
                && odd <= marketDescription.lineRangeInfo.maxUnitValue) {
            return marketDescription.lineRangeInfo.interval;
        }
        throw new InvalidAttributeException(
                String.format("Not handle line range type odd of LineRangeInfo: %s", marketDescription),
                String.valueOf(odd));
    }

    /**
     * Rounds by an increment via rounding mode.
     *
     * @param value the number to round
     * @param increment the increment
     * @param roundingMode the rounding mode
     * @return the rounded result
     */
    public static double roundByIncrement(double value, double increment, RoundingMode roundingMode) {

        // Our system uses 3 decimal places
        BigDecimal standardizedValue = BigDecimal.valueOf(roundByScale(value, 3));
        BigDecimal standardizedIncrement = BigDecimal.valueOf(roundByScale(increment, 3));

        return standardizedValue
                // Divide value by increment and round the divided to an integer value
                .divide(standardizedIncrement, 0, roundingMode)
                // Multiply the divided integer to increment to obtain the rounded value
                .multiply(standardizedIncrement)
                .doubleValue();
    }

    public static double roundByIncrement(double value, double increment) {
        int scale = countNoDecimal(increment);
        return roundByScale(value, scale);
    }

    public static double roundByScale(double value, int scale) {
        if (countNoDecimal(value) <= scale) return value;
        String newValue = String.format("%.7f", value);
        return new BigDecimal(newValue).setScale(scale, BigDecimal.ROUND_HALF_EVEN).doubleValue();
    }

    public static int countNoDecimal(double value) {
        String text = Double.toString(Math.abs(value));
        int integerPlaces = text.indexOf('.');
        return text.length() - integerPlaces - 1;
    }

    public static boolean is1X2Game(TargetType targetType) {
        List<TargetType> target1X2 = Arrays.asList(TargetType.ONE, TargetType.TWO, TargetType.DRAW);
        return target1X2.contains(targetType);
    }

    public static PivotType getPivotTypeTennis(MarketCatalogue market) {
        String lowerMarketName = market.marketName.toLowerCase().trim();
        if (tennisKey.containsKey(lowerMarketName)) {
            return tennisKey.get(lowerMarketName);
        }
        for (Pair<String, PivotType> r: tennisRegex) {
            if (lowerMarketName.matches(r.getKey())) {
                return r.getValue();
            }
        }
        return null;
    }

    public static PivotType getPivotTypeSoccer(MarketCatalogue market) {
        String lowerMarketName = market.marketName.toLowerCase().trim();
        if (soccerKey.containsKey(lowerMarketName)) {
            return soccerKey.get(lowerMarketName).getKey();
        }
        for (Pair<String, Pair<PivotType, SoccerTimeType>> r: soccerRegex) {
            if (lowerMarketName.matches(r.getKey())) {
                return r.getValue().getKey();
            }
        }
        return null;
    }

    public static PivotType getPivotTypeBasketball(MarketCatalogue market) {
        String lowerMarketName = market.marketName.toLowerCase().trim();
        return basketballKey.containsKey(lowerMarketName) ?
                basketballKey.get(lowerMarketName).getKey()
                : null;
    }

    public static PivotType getPivotTypeFromMarketCatalogue(MarketCatalogue market, SportType sportType) {
        if (sportType.equals(SportType.BASKETBALL)) {
            return getPivotTypeBasketball(market);
        } else if (sportType.equals(SportType.SOCCER)) {
            return getPivotTypeSoccer(market);
        } else if (sportType.equals(SportType.TENNIS)) {
            return getPivotTypeTennis(market);
        }
        return null;
    }

    public static SportType getSportTypeFromMarketCatalogue(MarketCatalogue market) {
        if (market.eventType != null) {
            return SportType.valueOf(market.eventType.toUpperCase());
        }
        return null;
    }

    public static Map<Long, TargetType> findSelectionOneTwo(List<Runner> runners) {
        Map<Long, TargetType> result = new HashMap<>();
        if (runners.size() == 2 || runners.size() == 3) {
            result.put(runners.get(0).selectionId, TargetType.ONE);
            result.put(runners.get(1).selectionId, TargetType.TWO);
        }
        if (runners.size() == 3 && runners.get(2).runnerName.toLowerCase().contains("the draw")) {
            result.put(runners.get(2).selectionId, TargetType.DRAW);
        }
        return result;
    }

    public static Map<Long, TargetType> findSelectionUnderOver(List<Runner> runners) {
        Map<Long, TargetType> result = new HashMap<>();
        for (Runner r : runners) {
            if (r.runnerName.toLowerCase().contains("under")) {
                result.put(r.selectionId, TargetType.UNDER);
            } else if (r.runnerName.toLowerCase().contains("over")) {
                result.put(r.selectionId, TargetType.OVER);
            }

            if (result.size() >= 2) {
                break;
            }
        }
        return result;
    }

    public static Map<Long, TargetType> findSelectionHomeAway(List<Runner> runners) {
        Map<Long, TargetType> result = new HashMap<>();
        if (runners.size() >= 2) {
            result.put(runners.get(0).selectionId, TargetType.HOME);
            result.put(runners.get(1).selectionId, TargetType.AWAY);
        }
        return result;
    }

    public static void extractTargetTypeOfOneTwo(List<Runner> runners, List<CurrentOrderType> currentOrderTypes, Map<String, CurrentOrderType> currentOrderTypeMap) {
        Map<Long, TargetType> selectionOneTwo = findSelectionOneTwo(runners);
        for (CurrentOrderType bet : currentOrderTypes) {
            TargetType targetType = selectionOneTwo.get(bet.getCurrentOrderSummary().selectionId);
            if (targetType != null) {
                bet.getTargetTypes().add(targetType);
            }

            currentOrderTypeMap.put(bet.getCurrentOrderSummary().betId, bet);
        }
    }

    public static void extractTargetTypeOfTotal(List<Runner> runners, List<CurrentOrderType> currentOrderTypes, Map<String, CurrentOrderType> currentOrderTypeMap) {
        Map<Long, TargetType> selectionUnderOver = findSelectionUnderOver(runners);
        for (CurrentOrderType bet : currentOrderTypes) {
            TargetType targetType = selectionUnderOver.get(bet.getCurrentOrderSummary().selectionId);
            if (targetType != null) {
                bet.getTargetTypes().add(targetType);
            }

            currentOrderTypeMap.put(bet.getCurrentOrderSummary().betId, bet);
        }
    }

    public static void extractTargetTypeOfHDP(List<Runner> runners, List<CurrentOrderType> currentOrderTypes, Map<String, CurrentOrderType> currentOrderTypeMap) {
        Map<Long, TargetType> selectionHomeAway = findSelectionHomeAway(runners);
        for (CurrentOrderType bet : currentOrderTypes) {
            TargetType targetType = selectionHomeAway.get(bet.getCurrentOrderSummary().selectionId);
            double handicap = bet.getCurrentOrderSummary().handicap;

            if (targetType != null) {
                bet.getTargetTypes().add(targetType);
            }
            bet.getTargetTypes().add(handicap >= 0d ? TargetType.TAKE : TargetType.GIVE);

            currentOrderTypeMap.put(bet.getCurrentOrderSummary().betId, bet);
        }
    }

    public static void extractTargetType(PivotType pivotType, List<Runner> runners, List<CurrentOrderType> currentOrderTypes, Map<String, CurrentOrderType> currentOrderTypeMap) {
        switch (pivotType) {
            case HDP:
                extractTargetTypeOfHDP(runners, currentOrderTypes, currentOrderTypeMap);
                break;
            case TOTAL:
                extractTargetTypeOfTotal(runners, currentOrderTypes, currentOrderTypeMap);
                break;
            case ONE_TWO:
                extractTargetTypeOfOneTwo(runners, currentOrderTypes, currentOrderTypeMap);
                break;
            default:
                break;
        }
    }

    public static boolean greaterThanZero(double value) {
        return new BigDecimal(value).compareTo(BigDecimal.ZERO) == 1;
    }
}
