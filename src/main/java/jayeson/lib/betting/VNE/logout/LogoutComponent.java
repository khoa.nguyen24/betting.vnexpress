package jayeson.lib.betting.VNE.logout;


import dagger.Subcomponent;
import jayeson.lib.betting.api.actionresults.LogoutResult;
import jayeson.lib.betting.core.dagger.taskscope.ITaskComponent;
import jayeson.lib.betting.core.dagger.taskscope.TaskModule;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;

@Subcomponent(modules = {LogoutModule.class, TaskModule.class})
@TaskScope
public interface LogoutComponent extends ITaskComponent<LogoutResult> {

    @Override
    VNELogoutTask getTask();

    @Subcomponent.Builder
    interface Builder extends ITaskComponent.Builder<LogoutResult> {
        @Override
        LogoutComponent build();
    }
}
