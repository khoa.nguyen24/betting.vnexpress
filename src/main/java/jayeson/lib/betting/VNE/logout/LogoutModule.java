package jayeson.lib.betting.VNE.logout;

import dagger.Module;
import dagger.Provides;
import jayeson.lib.betting.api.actionresults.LogoutResult;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;
import jayeson.lib.betting.core.task.TaskFragmentListener;

@Module
public class LogoutModule {

	@TaskScope @Provides
	LogoutResult providesResult() {
		return new LogoutResult();
	}
	
	@TaskScope @Provides
	TaskFragmentListener<LogoutResult> provideTaskFragmentListener(VNELogoutTask task) {
        return task;
    }
}
