package jayeson.lib.betting.VNE.logout;

import dagger.Lazy;
import jayeson.lib.betting.api.actionresults.LogoutResult;
import jayeson.lib.betting.api.task.AccountTaskHandler;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.api.task.TaskObservable;
import jayeson.lib.betting.core.BettingAccount;
import jayeson.lib.betting.core.dagger.qualifier.TaskId;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.task.LogoutTask;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;

import javax.inject.Inject;

/**
 * 
 * @author WEIMING
 *
 */
@TaskScope
public class VNELogoutTask extends LogoutTask {
	public static final int LOGOUT_WL = 10;
	private Lazy<Logout> logoutLazy;

	@Inject
	public VNELogoutTask(Lazy<Logout> logoutLazy,
						 ITaskExecutor executor, IResponseListener responseListener, AccountTaskInfo data,
						 BettingAccount account, @TaskId String id, AccountTaskHandler<LogoutResult> handler,
						 TaskObservable<AccountTaskInfo> observable, LogoutResult result) {
		super(executor, responseListener, data, account, id, LOGOUT_WL, handler, observable, result);
		this.logoutLazy = logoutLazy;
	}

	@Override
	public void execute() {
		account.getCommonLogger().info("LAUNCH_FRAGMENT_LOGOUT");
		executor.submit(logoutLazy.get());
	}
	
}
