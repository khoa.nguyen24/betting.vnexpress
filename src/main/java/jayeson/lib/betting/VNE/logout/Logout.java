package jayeson.lib.betting.VNE.logout;

import dagger.Lazy;
import jayeson.lib.betting.VNE.VNE;
import jayeson.lib.betting.VNE.VNEURIDefinition;
import jayeson.lib.betting.VNE.VNEUtility;
import jayeson.lib.betting.VNE.login.LoginAdditionalGetAccountDetailFragment;
import jayeson.lib.betting.api.actionresults.ActionResultCode;
import jayeson.lib.betting.api.actionresults.LogoutResult;
import jayeson.lib.betting.api.datastructure.BADRuntime;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.core.http.IClient;
import jayeson.lib.betting.core.http.IResponse;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.http.request.BasicRequest;
import jayeson.lib.betting.core.http.request.BettingRequest;
import jayeson.lib.betting.core.http.request.FormRequest;
import jayeson.lib.betting.core.task.AsyncHttpTaskFragment;
import jayeson.lib.betting.core.task.TaskFragmentListener;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;
import jayeson.lib.betting.exception.BettingException;

import javax.inject.Inject;

public class Logout extends AsyncHttpTaskFragment<LogoutResult, String> {

	private final BADRuntime badRuntime;
	private final VNE VNEAccount;
	private Lazy<GetProfileToCheck> getProfileToCheck;

	@Inject
	public Logout(AccountTaskInfo data, ITaskExecutor executor, IResponseListener responseListener,
				  VNE account, TaskFragmentListener<LogoutResult> listener,
				  BADRuntime badRuntime, LogoutResult initialResult,
				  Lazy<GetProfileToCheck> getProfileToCheck, IClient client) {
		super(data, executor, responseListener, account, listener, initialResult, client);
		needProceed = false;
		this.badRuntime = badRuntime;
		this.VNEAccount = account;
		this.getProfileToCheck = getProfileToCheck;
	}

	@Override
	public void proceed(LogoutResult currentResult) {
		GetProfileToCheck fragment = getProfileToCheck.get();
		executor.submit(fragment);
	}

	@Override
	public void execute() throws BettingException {
		client.resetHost(VNEUtility.getLoginApiUrl(client.host().scheme(),client.originalUrl()));
		BettingRequest request = new BasicRequest.Builder()
				.def(VNEURIDefinition.LOGOUT)
				.host(client.host())
				.build();
		executeRequest(request, true);
	}

	@Override
	public void consumeResponse(IResponse<String> response) throws BettingException {
		account.getCommonLogger().info("RESPONSE_READY_{}", response.getStatusLine());
		// Logout success
//		String error = VNEUtility.extractValue(VNEURIDefinition.LOGIN_PAGE.regexPattern("error"),
//				response.content(), 1);
			result().setCode(ActionResultCode.SUCCESSFUL);
		System.out.println("LOGOUT SUCCESSFUL, CONTINUE GET PROFILE");
		needProceed = true;
		}


	@Override
	public boolean logHttpRequest() {
		return true;
	}
}
