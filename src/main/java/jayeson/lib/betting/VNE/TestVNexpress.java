package jayeson.lib.betting.VNE;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import jayeson.lib.betting.api.actioncontext.*;
import jayeson.lib.betting.api.actionresults.*;
import jayeson.lib.betting.api.datastructure.*;
import jayeson.lib.betting.api.task.AccountTask;
import jayeson.lib.betting.api.task.AccountTaskHandler;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.api.task.ScrapTaskHandler;
import jayeson.lib.betting.core.ActionLogConstructor;
import jayeson.lib.betting.core.dagger.BettingComponent;
import jayeson.lib.betting.core.dagger.DaggerBettingComponent;
import jayeson.lib.betting.core.dagger.corescope.CoreBaseComponent;
import jayeson.lib.betting.core.dagger.corescope.DaggerCoreBaseComponent;
import jayeson.lib.betting.dagger.AccountComponent;
import jayeson.lib.feed.api.*;
import jayeson.lib.feed.api.twoside.PivotBias;
import jayeson.lib.feed.api.twoside.PivotType;
import jayeson.lib.feed.api.twoside.TargetType;
import jayeson.lib.feed.basketball.BasketballEventType;
import jayeson.lib.feed.basketball.BasketballTimeType;
import jayeson.utility.JacksonConfig;
import jayeson.utility.JacksonConfigFormat;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class TestVNexpress {
	public static VNE ba;
	public static DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	
	public static void main(String[] args) throws Exception {
		ba = createAccount();

//		testScrape();
		testLogin();
		testLogout();
//		Thread.sleep(2000);
//		testMaintenance();
//		BetTicketResult ticket = testBetTicket();
//		Thread.sleep(2000);
//
//		testBetHistory();
//		Thread.sleep(2000);
//		testMiniBetList();
//		BetTicketResult result = testBetTicket();
//		testBet(result);
//		testDeleteBet();
//		testBetTicket();

	}
	
	public static VNE createAccount() {

//		Proxy proxy = new Proxy();
//		proxy.setUrl("vnexpress.net");
//		proxy.setPort(80);
//		proxy.setUsername("");
//		proxy.setPassword("");
		
		BAD data = new BAD();
		data.setUsername("ndkhoalk97@gmail.com");
		data.setId(data.getUsername());
		data.setPassword("Sdt0337246997");
		data.setOriginalUrl("https://vnexpress.net/");
//		"?appKey=L8htwVGd9KeIZdgl");
		data.setOddFormat(OddFormat.HK);
//		data.setProxy(proxy);
		
		Sportbook sb = new Sportbook();
		sb.setId("BETFAIR");
		data.setSportbook(sb);

		BettingComponent bettingComponent = DaggerBettingComponent.create();
		CoreBaseComponent coreBaseComponent = DaggerCoreBaseComponent.builder()
				.bindsComponentBettingComponent(bettingComponent)
				.build();

		AccountComponent accountComponent = DaggerVNEComponent.builder()
				.bindsComponentBettingComponent(bettingComponent)
				.bindsComponentCoreBaseComponent(coreBaseComponent)
				.bindsInstanceBAD(data)
				.build();
		
		return (VNE)accountComponent.account();
	}
	
	// Test Export&Import login state
	public static void testHotReload() throws Exception {
		JacksonConfig config = new JacksonConfig(JacksonConfigFormat.JSON);
		AccountSessionState sessionState;
		
		// Login and export login state
		testLogin();
		if(ba.status() != BAS.ACTIVE) {
			System.out.println("Login Fail!");
			return;
		}
		
		sessionState = ba.exportSessionState();
		config.saveConfig("testaccount.json", sessionState, true);
		
		// Load the session data back
		sessionState = (AccountSessionState)config.readConfig("testaccount.json", 
				AccountSessionState.class);
		
		// Wait for 1 minutes
		Thread.sleep(60000);
		System.out.println("====================== 1 Minute ======================");
		
		ba = createAccount();
		ba.importSessionState(sessionState);
		testBalance();
		
		// Wait for another 2 minutes
		Thread.sleep(2*60000);
		System.out.println("====================== 2 Minute ======================");
		
		ba = createAccount();
		ba.importSessionState(sessionState);
		testBalance();
		
		// Wait for another 4 minutes
		Thread.sleep(4*60000);
		System.out.println("====================== 4 Minute ======================");
		
		ba = createAccount();
		ba.importSessionState(sessionState);
		testBalance();
		
		// Wait for another 8 minutes
		Thread.sleep(8*60000);
		System.out.println("====================== 8 Minute ======================");
		
		ba = createAccount();
		ba.importSessionState(sessionState);
		testBalance();
	}

	public static void testLogin() throws Exception {
//		ObjectMapper mapper = new ObjectMapper();
//		AccountSessionState session = null;
//		String appKey = "";
//		String currency = "";
//		File sessionFolder = Paths.get(String.format("session/%s", ba.getSportbook().getId().toLowerCase())).toFile();
//		File sessionFile = Paths.get(String.format("session/%s/%s.json", ba.getSportbook().getId().toLowerCase(), ba.getUsername())).toFile();
//		File sessionAppKeyFile = Paths.get(String.format("session/%s/%s_appKey.json", ba.getSportbook().getId().toLowerCase(), ba.getUsername())).toFile();
//		File sessionCurrencyFile = Paths.get(String.format("session/%s/%s_currency.json", ba.getSportbook().getId().toLowerCase(), ba.getUsername())).toFile();
//		// Create images folder if not exist
//		sessionFolder.mkdirs();
//		try {
//			// convert JSON file to map
//			session = mapper.readValue(sessionFile, AccountSessionState.class);
//			appKey = readKey(sessionAppKeyFile);
//			currency = readKey(sessionCurrencyFile);
//		} catch (Throwable e) {
//			e.printStackTrace();
//		}
//		if (session != null && session.getId().equals(ba.getId())) {
//			System.out.println("Reuse Session: " + session);
//			ba.importSessionState(session);
//			ba.setAppKey(appKey);
//			ba.setAccountCurrency(currency);
//			return;
//		}
		// Login
		CountDownLatch readySignal = new CountDownLatch(1);
		TaskHandler<LoginResult> handler = new TaskHandler<>(readySignal);
		ba.login(handler, new TaskObserver());
		readySignal.await();
		LoginResult result = handler.getResult();
		System.out.println("Login Result: " + result.getCode().name());
//		try {
//			session = ba.exportSessionState();
//			mapper.writeValue(sessionFile, session);
//			writeKey(sessionAppKeyFile, ba.getAppKey());
//			writeKey(sessionCurrencyFile, ba.getAccountCurrency());
//		} catch (Throwable e) {
//			e.printStackTrace();
//		}
	}
	private static void writeKey(File sessionAppKeyFile, String appKey) throws IOException {
		FileWriter myWriter = null;
		try {
			myWriter = new FileWriter(sessionAppKeyFile.getAbsoluteFile());
			myWriter.write(appKey);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (myWriter != null)
				myWriter.close();
		}
	}
	private static String readKey(File sessionAppKeyFile) {
		Scanner myReader = null;
		try {
			myReader = new Scanner(sessionAppKeyFile);
			while (myReader.hasNextLine()) {
				return myReader.nextLine();
			}
			return "";
		} catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
			return "";
		} finally {
			if (myReader != null)
				myReader.close();
		}
	}

	private static void testSyncRecentBetList() throws Exception {
		CountDownLatch readySignal = new CountDownLatch(1);
		TaskHandler<SyncBetListResult> handler = new TaskHandler<>(readySignal);
		ba.syncRecentBetList(handler, new TaskObserver());
		readySignal.await();
	}
	
	public static void testMaintenance() throws Exception {
		CountDownLatch readySignal = new CountDownLatch(1);
		TaskHandler<MaintenanceResult> handler = new TaskHandler<>(readySignal);
		ba.doMaintenance(handler, new TaskObserver());
		readySignal.await(30, TimeUnit.SECONDS);
		
		MaintenanceResult result = handler.getResult();
		System.out.println("Maintenance Result: " + result.getCode().name());
	}
	
	public static void testLogout() throws Exception {
		CountDownLatch readySignal = new CountDownLatch(1);
		TaskHandler<LogoutResult> handler = new TaskHandler<>(readySignal);
		ba.logout(handler, new TaskObserver());
		readySignal.await();
		
		LogoutResult result = handler.getResult();
		System.out.println("Logout Result: " + result.getCode().name());
	}
	
	public static BetTicketResult testBetTicket() throws Exception {
		// Fake context
		BetInfoContext context = new BetInfoContext();
		/**
		 ===INPUT_TICKET_AP===
		 SPORT_TYPE: BASKETBALL
		 TARGET_TYPE: GIVE
		 TARGET_ODD: 0.62
		 ODD_FORMAT: HK
		 ===INPUT_TICKET_CONTEXT===
		 EVENT: BACK NONE, 1cc8ac5784202031
		 EARLY, {"sportType":"BASKETBALL","name":"FT"}, BACK
		 Nba, Los Angeles Clippers vs Los Angeles Lakers; TEAM_SWAP: false
		 RATE_UID: ['1.171044696&6023845&1.5', '1.171044696&5368712&-1.5', '']; ODD_ID: 1.171044696&5368712&-1.5
		 HDP, 1.5, GUEST
		 Score: -1 - -1
		 */


		context.setSportType(SportType.BASKETBALL);
		context.setTargetType(TargetType.GIVE);
		context.setOddFormatOfTargetOdd(OddFormat.HK);
		context.setTargetOddFormat(OddFormat.HK);
		context.setEventType(BasketballEventType.NONE);
		context.setOddType(OddType.EARLY);
		context.setTimeType(BasketballTimeType.FT);
		context.setPivotType(PivotType.HDP);
		context.setTargetOdd(0.05000000074505806f);
		context.setPivotValue("2.75");
		context.setBias(PivotBias.HOST);
		context.setLbType(LBType.BACK);
		context.setOddId("1.171044696&5368712&-1.5"); // market;runner;event
		context.setRateOverUid("1.171044696&6023845&1.5");
		context.setRateUnderUid("1.171044696&5368712&-1.5");
		context.setRateEqualUid("");

		System.out.println(ActionLogConstructor.buildGetTicketStartMsg(context));
		CountDownLatch readySignal = new CountDownLatch(1);
		TaskHandler<BetTicketResult> handler = new TaskHandler<>(readySignal);
		ba.getBetTicket(context, handler, new TaskObserver());
		readySignal.await();
		
		BetTicketResult result = handler.getResult();
		System.out.println(ActionLogConstructor.buildGetTicketContextResultMsg(result));
		return result;
	}
	
	public static BetResult testBet(BetTicketResult result) throws Exception {
		// Bet context
		BetContext context = new BetContext();
		context.setBetTicket(result);
		context.setAcceptBetterOdd(true);
		context.setActionStake(result.getMinStake());
		context.setActualBetStake(result.getMinStake());
//		context.setTargetOddFormat(OddFormat.HK);

		CountDownLatch readySignal = new CountDownLatch(1);
		TaskHandler<BetResult> handler = new TaskHandler<>(readySignal);
		ba.bet(context, handler, new TaskObserver());
		readySignal.await();

		BetResult betResult = handler.getResult();
		System.out.println("Result: " + betResult.getCode().name());
		System.out.println(ActionLogConstructor.buildBetResultMsg(betResult));

		return betResult;
	}
	
	public static void testDeleteBet() throws Exception {
		// Delete bet context
		DeleteBetContext context = new DeleteBetContext();
		context.setBetId("201239840320");
		context.setOriginalStake(2.0);
		
		CountDownLatch readySignal = new CountDownLatch(1);
		TaskHandler<DeleteBetResult> handler = new TaskHandler<>(readySignal);
		ba.deleteBet(context, handler, new TaskObserver());
		readySignal.await();
		
		DeleteBetResult result = handler.getResult();
		System.out.println("Delete Result: " + result.getCode());
		System.out.println("Matched Stake: " + result.getMatchedStake());
		System.out.println("Cancelled Stake: " + result.getCancelledStake());
		System.out.println("Effective Odd: " + result.getEffectiveOdd());
	}
	
	public static void testBalance() throws Exception {
		CountDownLatch readySignal = new CountDownLatch(1);
		TaskHandler<BalanceResult> handler = new TaskHandler<>(readySignal);
		ba.getBalance(handler, new TaskObserver());
		readySignal.await(30, TimeUnit.SECONDS);
		
		BalanceResult result = handler.getResult();
		System.out.println("Balance Result: " + result.getCode().name());
		System.out.println("Account Balance: " + result.getBalance());
	}
	
	public static void testMiniBetList() throws Exception {
		CountDownLatch readySignal = new CountDownLatch(1);
		TaskHandler<ListMiniBetResult> handler = new TaskHandler<>(readySignal);
		ba.getMiniBetList(handler, new TaskObserver());
		readySignal.await();

		ListMiniBetResult result = handler.getResult();
		System.out.println("Running Bets: " + result);

		for(MiniBetResult bet : result.getResultList()) {
			System.out.println("====================");
			System.out.println("Code: " + bet.getCode().name());
			System.out.println("Bet Id: " + bet.getBetId());
			System.out.println("Bet Amount: " + bet.getBetAmount());
			System.out.println("Bet Odd: " + bet.getBetOdd());
			System.out.println("Bet Pivot: " + bet.getBetPivot());
			System.out.println("Sport Type: " + bet.getSportType());
			System.out.println("TimeType: " + TimeType.serialize(bet.getTimeType()));
			System.out.println("Target Type: " + bet.getTargetType());
			System.out.println("Bet Status: " + bet.getBetStatus());
		}
	}
	
	public static void testRecentBetsStatus() {
		RecentBetStatusContext context = new RecentBetStatusContext();
		context.setBetIds(ImmutableList.of("836125852990048"));
		ba.getRecentBetStatus(context, (result, task) -> {
			System.out.println(String.format("RecentBetStatus Result: %s", result));
		}, new TaskObserver());
	}
	
	public static void testStatements() throws Exception {
		BetStatementContext context = new BetStatementContext();
		context.setStartTime(1514653200000l);
		context.setEndTime(1514739600000l);
		
		CountDownLatch readySignal = new CountDownLatch(1);
		TaskHandler<ListBetStatementResult> handler = new TaskHandler<>(readySignal);
		ba.getBetStatement(context, handler, new TaskObserver());
		readySignal.await(30, TimeUnit.SECONDS);
		
		ListBetStatementResult result = handler.getResult();
		System.out.println("Get Statements: " + result);
	}
	
	public static void testBetHistory() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		BetHistoryByStatementContext context = new BetHistoryByStatementContext(0, 0);
		context.setStartStatementDay(System.currentTimeMillis() - 3*24*60*60*1000L);
		context.setEndStatementDay(System.currentTimeMillis() + 1*24*60*60*1000L);

		CountDownLatch readySignal = new CountDownLatch(1);
		TaskHandler<ListBetHistoryResult> handler = new TaskHandler<>(readySignal);
		ba.getBetHistory(context, handler, new TaskObserver());
		readySignal.await();

		ListBetHistoryResult result = handler.getResult();
		System.out.println("Get Bet History: " + result.getCode().name());
		System.out.println("History Bets: " + result.getResultList().size());
		System.out.println(ActionLogConstructor.buildGetBetHistoryResultMsg(result));

		for(BetHistoryResult bet : result.getResultList()) {
			System.out.println("====================");
			System.out.println("Code: " + bet.getCode().name());
			System.out.println("Sport Type: " + bet.getSportType());
			System.out.println("League: " + bet.getLeague());
			System.out.println("Bet Id: " + bet.getBetId());
			System.out.println("Bet Amount: " + bet.getBetAmount());
			System.out.println("Bet Odd: " + bet.getBetOdd());
			System.out.println("Bet Pivot: " + bet.getBetPivot());
			System.out.println("Target Type: " + bet.getTargetType());
			System.out.println("Return Amount: " + bet.getReturnAmount());
			System.out.println("Bet Status: " + bet.getBetStatus());
			System.out.println("Origin Bet Status:" + bet.getOriginBetStatus());
			System.out.println("Time Type: " + TimeType.serialize(bet.getTimeType()));
			System.out.println("Scores: " + objectMapper.writeValueAsString(bet.getScores()));
			System.out.println("Statement: " + bet.getStatementDay());
		}
	}
	
	public static class TaskObserver implements Consumer<AccountTaskInfo> {
		@Override
		public void accept(AccountTaskInfo info) {
			System.out.println("Task Update: " + info.getId() + ", " + info.getProgress() + ", " 
					+ info.isCancellable() + ", " + info.getState());
		}
	}
	
	public static class TaskHandler <T extends ActionResult> implements AccountTaskHandler<T> {
		// Wait for the task to callback 
		private CountDownLatch readySignal = new CountDownLatch(1);
		// Result
		private T result;
		
		public TaskHandler(CountDownLatch readySignal) {
			this.readySignal = readySignal;
		}

		@Override
		public void process(T result, AccountTask<T> task) {
			this.result = result;
			readySignal.countDown();
		}

		public T getResult() {
			return result;
		}

	}

	private static void testScrape() throws Exception {
		CountDownLatch completedSignal = new CountDownLatch(1);

		ScrapeHandler<ScrapeResult> handler = new ScrapeHandler<>(completedSignal);

		ScrapeContext context = new ScrapeContext();
		context.setSportType(SportType.SOCCER);
		context.setRequestType(ScrapeContext.RequestType.FETCH_DELTA_EVENT);
		context.setParamString("{\"eventIds\":[\"1\", \"2\", \"3\", \"4\", \"5\"]}");

		System.out.println("####### START SCRAPE DELTA DATA #######");;
		System.out.println("####### Scrape Context" );
		System.out.println("Request Type: " + context.getRequestType());
		System.out.println("Sport Type: " + context.getSportType());

		ba.scrape(context, handler, new TaskObserver());

		completedSignal.await();
	}

	public static class ScrapeHandler<T extends ScrapeResult> implements ScrapTaskHandler<T> {
		private final CountDownLatch completedSignal;
		private final MainScrapeResult summarizedResult;
		private final ObjectMapper mapper;
		private long lastime;

		public ScrapeHandler(CountDownLatch completedSignal) {
			this.mapper = new ObjectMapper();
			this.completedSignal = completedSignal;
			this.summarizedResult = new MainScrapeResult();
			this.lastime = System.currentTimeMillis();
		}

		@Override
		public void completed(ScrapeResult result, AccountTask<T> task) {
			System.out.println("[ScrapeHandler] task execution completed, code " + result.getCode() + " message " + result.getMessage());
			try {
				System.out.println("####### Scrape Completed");
				System.out.println("Summarized Result");
				for (OddType oddType : summarizedResult.getRecords().keySet()) {
					System.out.println("OddType: " + oddType);
					if (summarizedResult.getSpecialIds().containsKey(oddType)) {
						System.out.println("Special Ids: " + mapper.writeValueAsString(summarizedResult.getSpecialIds().get(oddType)));
					}
					System.out.println("Records size: " + summarizedResult.getRecords().get(oddType).size());
					System.out.println("Records:");
					for (SportbookRecord r : summarizedResult.getRecords().get(oddType)) {
						System.out.println(mapper.writeValueAsString(r));
					}
				}
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
			completedSignal.countDown();
		}

		@Override
		public void error(ScrapeResult result, AccountTask<T> task) {
			System.out.println("[ScrapeHandler] task execution error, code " + result.getCode() + " message " + result.getMessage());
			completedSignal.countDown();
		}

		@Override
		public void process(T result, AccountTask<T> task)  {
			if (result instanceof  DeltaScrapeResult) {
				DeltaScrapeResult deltaResult = (DeltaScrapeResult) result;
				System.out.println("[ScrapeHandler] process execution result, code " + result.getCode() + " message " + result.getMessage());
				for (Map.Entry<OddType, List<DeleteRecordInfo>> entry : deltaResult.getDeleteRecords().entrySet()) {
					System.out.printf("Odd Type %s deleted size %s\n", entry.getKey(), entry.getValue().size());
				}
				for (Map.Entry<OddType, List<SportbookRecord>> entry : deltaResult.getUpsertRecords().entrySet()) {
					System.out.printf("Odd Type %s upsert size %s\n", entry.getKey(), entry.getValue().size());
				}
			} else {
				System.out.println("Invalid format result");
			}

			long now = System.currentTimeMillis();
//			if (now - lastime > 150000) {
//				completedSignal.countDown();
//			}
		}
	}

}
