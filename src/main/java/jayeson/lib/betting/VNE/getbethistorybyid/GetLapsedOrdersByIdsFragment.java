package jayeson.lib.betting.VNE.getbethistorybyid;

import dagger.Lazy;
import jayeson.lib.betting.VNE.VNE;
import jayeson.lib.betting.api.actioncontext.BetHistoryByBetIdContext;
import jayeson.lib.betting.api.actionresults.ListBetHistoryResult;
import jayeson.lib.betting.api.datastructure.BADRuntime;
import jayeson.lib.betting.api.datastructure.BetStatus;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.VNE.json.BFBetStatus;
import jayeson.lib.betting.VNE.json.clearedorders.ClearedOrderSummary;
import jayeson.lib.betting.core.http.IClient;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.task.TaskFragmentListener;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;
import jayeson.lib.betting.exception.ParsingFailException;

import javax.inject.Inject;

public class GetLapsedOrdersByIdsFragment extends ListClearedOrdersByIdsFragment {

	private final Lazy<GetCancelledOrdersByIdsFragment> cancelledOrdersFragmentLazy;

	@Inject
	public GetLapsedOrdersByIdsFragment(AccountTaskInfo data, ITaskExecutor executor, IResponseListener responseListener,
                                        VNE account, TaskFragmentListener<ListBetHistoryResult> listener,
                                        BADRuntime badRuntime, ListBetHistoryResult initialResult,
                                        BetHistoryByBetIdContext context, IClient client, Lazy<GetCancelledOrdersByIdsFragment> cancelledOrdersFragmentLazy) {
		super(data, executor, responseListener, account, listener, badRuntime, initialResult, context, client, BFBetStatus.LAPSED);
		needProceed = true;
		this.cancelledOrdersFragmentLazy = cancelledOrdersFragmentLazy;
	}

	@Override
	public void proceed(ListBetHistoryResult currentResult) {
		if (isRequestDone) {
			GetCancelledOrdersByIdsFragment fragment = cancelledOrdersFragmentLazy.get();
			executor.submit(fragment);
			account.getCommonLogger().info("LAUNCH_BET_HISTORIES_BY_IDS_CANCELLED");
		} else {
			super.proceed(currentResult);
		}
	}

	@Override
	protected void setBetStatus(BetFairHistoryResult result, ClearedOrderSummary each) throws ParsingFailException {
		result.setBetStatus(BetStatus.REJECTED);
		result.setDeleteStatus(BetFairHistoryResult.DeleteStatus.LAPSED);
	}

	@Override
	public boolean logHttpRequest() {
		return false;
	}
}
