package jayeson.lib.betting.VNE.getbethistorybyid;

import jayeson.lib.betting.VNE.VNE;
import jayeson.lib.betting.api.actioncontext.BetHistoryByBetIdContext;
import jayeson.lib.betting.api.actionresults.ListBetHistoryResult;
import jayeson.lib.betting.api.datastructure.BADRuntime;
import jayeson.lib.betting.api.datastructure.BetStatus;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.VNE.json.BFBetStatus;
import jayeson.lib.betting.VNE.json.clearedorders.ClearedOrderSummary;
import jayeson.lib.betting.VNE.json.clearedorders.ClearedOrderSummaryResponse;
import jayeson.lib.betting.core.http.IClient;
import jayeson.lib.betting.core.http.IResponse;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.task.TaskFragmentListener;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;
import jayeson.lib.betting.exception.BettingException;
import jayeson.lib.betting.exception.ParsingFailException;

import javax.inject.Inject;

public class GetCancelledOrdersByIdsFragment extends ListClearedOrdersByIdsFragment {

	@Inject
	public GetCancelledOrdersByIdsFragment(AccountTaskInfo data, ITaskExecutor executor, IResponseListener responseListener,
                                           VNE account, TaskFragmentListener<ListBetHistoryResult> listener,
                                           BADRuntime badRuntime, ListBetHistoryResult initialResult,
                                           BetHistoryByBetIdContext context, IClient client) {
		super(data, executor, responseListener, account, listener, badRuntime, initialResult, context, client, BFBetStatus.CANCELLED);
		needProceed = true;
	}

	@Override
	public void consumeResponse(IResponse<ClearedOrderSummaryResponse[]> response) throws BettingException {
		super.consumeResponse(response);
		if (isRequestDone) {
			needProceed = false;
		}
	}

	@Override
	protected void setBetStatus(BetFairHistoryResult result, ClearedOrderSummary each) throws ParsingFailException {
		result.setBetStatus(BetStatus.CANCELLED);
		result.setDeleteStatus(BetFairHistoryResult.DeleteStatus.CANCELLED);
	}

	@Override
	public boolean logHttpRequest() {
		return false;
	}
}
