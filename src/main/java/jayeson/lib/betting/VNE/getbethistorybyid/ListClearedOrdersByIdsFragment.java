package jayeson.lib.betting.VNE.getbethistorybyid;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.google.common.base.Strings;
import jayeson.lib.betting.VNE.VNE;
import jayeson.lib.betting.VNE.VNEURIDefinition;
import jayeson.lib.betting.VNE.VNEUtility;
import jayeson.lib.betting.VNE.json.clearedorders.*;
import jayeson.lib.betting.api.actioncontext.BetHistoryByBetIdContext;
import jayeson.lib.betting.api.actionresults.ActionResultCode;
import jayeson.lib.betting.api.actionresults.BetHistoryResult;
import jayeson.lib.betting.api.actionresults.ListBetHistoryResult;
import jayeson.lib.betting.api.datastructure.BADRuntime;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.VNE.json.BFBetStatus;
import jayeson.lib.betting.VNE.json.clearedorders.*;
import jayeson.lib.betting.core.exception.UnknownResponseException;
import jayeson.lib.betting.core.http.IClient;
import jayeson.lib.betting.core.http.IResponse;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.http.request.JsonRequest;
import jayeson.lib.betting.core.task.AsyncHttpTaskFragment;
import jayeson.lib.betting.core.task.TaskFragmentListener;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;
import jayeson.lib.betting.core.utility.BettingUtility;
import jayeson.lib.betting.exception.BettingException;
import jayeson.lib.betting.exception.ParsingFailException;
import jayeson.lib.feed.api.SportType;
import jayeson.lib.feed.api.twoside.TargetType;
import jayeson.lib.feed.basketball.BasketballTimeType;
import jayeson.lib.feed.soccer.SoccerTimeType;
import jayeson.lib.feed.tennis.TennisTimeType;
import org.apache.commons.text.similarity.LevenshteinDistance;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static jayeson.lib.betting.VNE.VNEUtility.extractValue;

public class ListClearedOrdersByIdsFragment extends AsyncHttpTaskFragment<ListBetHistoryResult, ClearedOrderSummaryResponse[]> {

	private final BADRuntime badRuntime;
	private final VNE VNEAccount;
	private BetHistoryByBetIdContext context;
	private BFBetStatus requestBetStatus;
	protected boolean isRequestDone = true;

	/**
	 * A maximum of 1000 betId's are allowed in a single request
	 * @see <a href="https://docs.developer.betfair.com/display/1smk3cen4v3lu3yomq5qye0ni/listClearedOrders">listClearedOrders</a>
	 */
	private static final int MAXIMUM_EACH_ROUND_BET_IDS = 1000;
	private int currentIndexBetIds = 0;
	private List<String> allBetIds;
	private List<String> subBetIds;

	private static final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

	public ListClearedOrdersByIdsFragment(AccountTaskInfo data, ITaskExecutor executor, IResponseListener responseListener,
										  VNE account, TaskFragmentListener<ListBetHistoryResult> listener,
										  BADRuntime badRuntime, ListBetHistoryResult initialResult,
										  BetHistoryByBetIdContext context, IClient client, BFBetStatus requestBetStatus) {
		super(data, executor, responseListener, account, listener, initialResult, client);
		this.context = context;
		this.badRuntime = badRuntime;
		this.requestBetStatus = requestBetStatus;
		this.setDef(VNEURIDefinition.getJsonRpcURI(ClearedOrderSummaryResponse[].class));
		needProceed = true;
		this.VNEAccount = account;
	}

	@Override
	public void proceed(ListBetHistoryResult currentResult) {
		executor.submit(this);
		account.getCommonLogger().info("LAUNCH_BET_HISTORIES_BY_IDS {}", subBetIds);
	}

	/**
	 * [{"jsonrpc": "2.0", "method": "SportsAPING/v1.0/listClearedOrders", "params": {"betStatus":"SETTLED","settledDateRange":{}}, "id": 1}]
	 * @throws BettingException
	 */
	@Override
	public void execute() throws BettingException {
		ArrayNode arrayNode = JsonNodeFactory.instance.arrayNode();
		ClearedOrdersRequest data = new ClearedOrdersRequest();

		ClearedOrdersParams params = new ClearedOrdersParams();
		params.betStatus = requestBetStatus;
		data.params = completeParams(params);

		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.convertValue(data, JsonNode.class);
		arrayNode.add(node);

		if (VNEUtility.isNeedToResetBettingApi(client.host().url().toString())) {
			client.resetHost(VNEUtility.getBettingApiUrl(client.host().url().toString()));
		}

		// Build request
		JsonRequest request = new JsonRequest.Builder()
				.def(this.getDef())
				.host(client.host())
				.header("X-Authentication", this.badRuntime.getLoggedInUid())
				.header("X-Application", VNEAccount.getAppKey())
				.data(arrayNode)
				.build();

		executeRequest(request, true);
	}

	private ClearedOrdersParams completeParams(ClearedOrdersParams params) {
		if (allBetIds == null) {
			allBetIds = new ArrayList<>();
			context.getBetIds().forEach(allBetIds::add);
		}

		int from = currentIndexBetIds;
		int to = Math.min(currentIndexBetIds + MAXIMUM_EACH_ROUND_BET_IDS, allBetIds.size());
		account.getCommonLogger().info("Query bet ids from {} to {} of allBetIds size is {}", from, to, allBetIds.size());

		// Update subBetIds for logging
		subBetIds = allBetIds.subList(from, to);
		params.betIds = subBetIds;

		// Update current index bet ids
		currentIndexBetIds = to;
		account.getCommonLogger().info("currentIndexBetIds: {}", currentIndexBetIds);

		return params;
	}

	@Override
	public void consumeResponse(IResponse<ClearedOrderSummaryResponse[]> response) throws BettingException {
		if (response.getStatusLine().getCode() != 200) {
			account.getCommonLogger().error("FAIL to request History BetFair {}", response);
			result().setCode(ActionResultCode.FAILED);
			result().setMessage("The request failed with http code " + response.getStatusLine().getCode());
			return;
		}

		ClearedOrderSummaryResponse report = response.content()[0];

		if(report.error != null) {
			account.getCommonLogger().error("Failed to get clear orders: {} ", BettingUtility.writeObjectToJson(report.error));
			throw new UnknownResponseException("Failed to get clear orders", getDef(), "");
		}

		ClearedOrderSummaryReport result = report.result;

		if (!result.clearedOrders.isEmpty())
			result().getResultList().addAll(parseBetInfo(result.clearedOrders));

		if (currentIndexBetIds < allBetIds.size()) {
			isRequestDone = false;
		} else {
			isRequestDone = true;

			if (result().getResultList().stream().allMatch(x -> x.getCode() == ActionResultCode.SUCCESSFUL)) {
				result().setCode(ActionResultCode.SUCCESSFUL);
			} else if (result().getResultList().stream().anyMatch(x -> x.getCode() == ActionResultCode.SUCCESSFUL)) {
				result().setCode(ActionResultCode.PARTIAL);
			} else {
				result().setCode(ActionResultCode.FAILED);
			}
		}
	}

	private List<BetFairHistoryResult> parseBetInfo(List<ClearedOrderSummary> clearedOrders) {
		List<BetFairHistoryResult> results = clearedOrders.stream()
				.filter(newElement -> !result().getResultList().stream()
						.anyMatch(previousElement -> previousElement.getBetId().equals(newElement.betId)))
				.map(each -> {
					BetFairHistoryResult result = new BetFairHistoryResult();
					try {
						result.setCode(ActionResultCode.SUCCESSFUL);
						result.setBettingAccountId(account.getId());
						result.setBetId(each.betId);
						if (each.profit != null)
							result.setReturnAmount(each.profit); //this amount is gross profit --> PLM will calculate profit

						if (each.sizeSettled == null)
							result.setBetAmount(0.0);
						else
							result.setBetAmount(each.sizeSettled);

						if (each.sizeCancelled != null)
					result.setBetAmount(each.sizeCancelled);

				result.setBetPivot(extractPivot(each));
				result.setStatementDay(convertToBetFairTime(each.settledDate));

				result.setSportType(parseSportType(each.eventTypeId));
				result.setBetTime(convertTimestamp(each.placedDate));
				setBetStatus(result, each);

				//Home Team vs Away Team
				parseTeams(result, each.itemDescription);

				//Target Type
				parseTargetType(result, each.itemDescription, result.getBetPivot());
				//Time Type
				parseTimeType(result, each.itemDescription, result.getSportType());
				//Score

				double odd = each.priceMatched != null? each.priceMatched : each.priceRequested;

				/**
				 * BetFair only support Decimal Odd Format, so to other markets (besides 1X2) should be back to HK
				 */
				if (!VNEUtility.is1X2Game(result.getTargetType())) {
					odd = VNEUtility.oddTo3Decimal(odd - 1);
				} else {
					odd = VNEUtility.oddTo3Decimal(odd);
				}
				result.setBetOdd(odd);

			} catch (Exception e) {
				account.getCommonLogger().error("Fail to parse Bet History: ", e);
				result.setCode(ActionResultCode.FAILED);
				result.setMessage("Fail to parse Bet History: " + e.getMessage());
			}
			return result;
		}).collect(Collectors.toList());

		return results;
	}

	private void parseTeams(BetHistoryResult result, ItemDescription itemDescription) {
		if (itemDescription == null || Strings.isNullOrEmpty(itemDescription.eventDesc))
			return;
		try {
			result.setHomeTeam(VNEUtility.extractValue("(.*)\\sv\\s(.*)", itemDescription.eventDesc, 1));
			result.setGuestTeam(VNEUtility.extractValue("(.*)\\sv\\s(.*)", itemDescription.eventDesc, 2));
		} catch (Exception ex) {
			account.getCommonLogger().error("Not able to parse teams, {}", itemDescription, ex);
		}
	}

	/**
	 * Current timezone of Betfair website is GMT+1.
	 *
	 * @param date
	 * @return
	 */
	private static LocalDate convertToBetFairTime(Date date) {
		return Instant.ofEpochMilli(date.getTime())
				.atZone(TimeZone.getTimeZone("GMT+1").toZoneId())
				.toLocalDate();
	}

	/**
	 * Extract pivot for special case when market type is Over/Under.
	 *
	 * @param clearedOrder information of bet
	 * @return the pivot text
	 */
	private String extractPivot(ClearedOrderSummary clearedOrder) {
		if (clearedOrder.itemDescription != null
				&& clearedOrder.itemDescription.runnerDesc != null) {

			// Special case occur with Over/Under (custom of Betfair)
			String marketType = clearedOrder.itemDescription.runnerDesc.toLowerCase();
			if (marketType.contains("over") || marketType.contains("under")) {

				// Extract pivot float from market description
				Pattern p = Pattern.compile("\\d+\\.\\d+");
				Matcher m = p.matcher(clearedOrder.itemDescription.runnerDesc);
				if (m.find()) {
					return "" + Float.parseFloat(m.group());
				}
			}
		}

		return clearedOrder.handicap.toString();
	}

	protected void setBetStatus(BetFairHistoryResult result, ClearedOrderSummary each) throws ParsingFailException {
		throw new ParsingFailException("Implement own settled result", this.getDef().uri(), "", "");
	}

	private void parseTargetType(BetHistoryResult result, ItemDescription itemDescription, String betPivot) throws ParsingFailException {
		String teamBet;
		int idx;

		switch (itemDescription.marketType) {
			case "MATCH_ODDS":
			case "MATCH_ODDS_UNMANAGED": //todo: remove when verify is not correct
			case "HALF_TIME":
			case "SET_WINNER":
				if(itemDescription.runnerDesc.toUpperCase().contains(TargetType.DRAW.name())){
					idx = -1;
				}
				else {
					teamBet = findTeamBet(itemDescription.eventDesc, itemDescription.runnerDesc);
					idx = itemDescription.eventDesc.indexOf(teamBet);
				}
				parse1X2TargetType(result, idx);
				break;
			case "ASIAN_HANDICAP":
			case "HANDICAP":
				teamBet = findTeamBet(itemDescription.eventDesc, itemDescription.runnerDesc);
				idx = itemDescription.eventDesc.indexOf(teamBet);
				parseHdpTargetType(result, betPivot, idx);
				break;
			default:
				String marketTypePattern = "over_under|first_half_goals|combined_total|alt_total_goals";
				if (VNEUtility.extractValue(marketTypePattern, itemDescription.marketType.toLowerCase())) {
					teamBet = findTotalTeamBet(itemDescription.runnerDesc);
					parseOverUnderTargetType(result, itemDescription, teamBet);
				} else {
					account.getCommonLogger().error("Fail to parse Target Type, {}", result);
					throw new ParsingFailException("Fail to parse Target Type", this.getDef().uri(), "", "");
				}
				break;
		}
	}

	private String findTeamBet(String eventDesc, String runnerDesc) {
		String homeGuestPattern = "(.*)\\s(v|vs)\\s(.*)";
		String homeTeam = VNEUtility.extractValue(homeGuestPattern, eventDesc, 1);
		String guestTeam = VNEUtility.extractValue(homeGuestPattern, eventDesc, 3);
		String teamBet = "";

		if (LevenshteinDistance.getDefaultInstance().apply(runnerDesc, homeTeam) <
				LevenshteinDistance.getDefaultInstance().apply(runnerDesc, guestTeam)) {
			teamBet = homeTeam;
		} else if (LevenshteinDistance.getDefaultInstance().apply(runnerDesc, homeTeam) >
				LevenshteinDistance.getDefaultInstance().apply(runnerDesc, guestTeam)) {
			teamBet = guestTeam;
		}
		return teamBet;
	}

	private String findTotalTeamBet(String runnerDesc) {
		if (LevenshteinDistance.getDefaultInstance().apply(runnerDesc.toLowerCase(), "over") <
				LevenshteinDistance.getDefaultInstance().apply(runnerDesc.toLowerCase(), "under")) {
			return "Over";
		}
		return "Under";
	}

	private void parseOverUnderTargetType(BetHistoryResult result, ItemDescription itemDescription, String teamBet) throws ParsingFailException {
		if (teamBet.equalsIgnoreCase("Under"))
			result.getTargetTypes().add(TargetType.UNDER);
		else if (teamBet.equalsIgnoreCase("Over"))
			result.getTargetTypes().add(TargetType.OVER);
		else {
			account.getCommonLogger().error("Fail to parse OVER_UNDER Target Type, {}", itemDescription);
			throw new ParsingFailException("Fail to parse OVER_UNDER Target Type", this.getDef().uri(), "", "");
		}
	}

	private void parseHdpTargetType(BetHistoryResult result, String betPivot, int idx) throws ParsingFailException {
		if (idx == 0) {
			result.getTargetTypes().add(TargetType.HOME);
			identifyGiveTake(result, betPivot);
		} else if (idx > 0) {
			result.getTargetTypes().add(TargetType.AWAY);
			identifyGiveTake(result, betPivot);
		} else {
			account.getCommonLogger().error("Fail to parse HDP Target Type, {}", result);
			throw new ParsingFailException("Fail to parse HDP Target Type", this.getDef().uri(), "", "");
		}
	}

	private void identifyGiveTake(BetHistoryResult result, String betPivot) throws ParsingFailException {
		if (betPivot.matches("0\\.0|0\\.00"))
			return;

		if (Double.parseDouble(betPivot) < 0)
			result.getTargetTypes().add(TargetType.GIVE);
		else if (Double.parseDouble(betPivot) > 0)
			result.getTargetTypes().add(TargetType.TAKE);
		else {
			account.getCommonLogger().error("Fail to parse HDP Target Type(GIVE or TAKE), {}", result);
			throw new ParsingFailException("Fail to parse HDP Target Type(GIVE or TAKE)", this.getDef().uri(), "", "");
		}
	}

	private void parse1X2TargetType(BetHistoryResult result, int idx) {
		if (idx == 0)
			result.getTargetTypes().add(TargetType.ONE);
		else if (idx > 0)
			result.getTargetTypes().add(TargetType.TWO);
		else
			result.getTargetTypes().add(TargetType.DRAW);
	}

	private void parseTimeType(BetHistoryResult result, ItemDescription itemDescription, SportType sportType) throws ParsingFailException {
		switch (sportType) {
			case SOCCER:
				parseSoccerTimeType(result, itemDescription);
				break;
			case BASKETBALL:
				parseBasketBallTimeType(result, itemDescription);
				break;
			case TENNIS:
				parseTennisTimeType(result, itemDescription);
				break;
			default:
				account.getCommonLogger().error("Fail to parse TimeType - no support other sport {}", sportType);
				throw new ParsingFailException("No support other sport, " + sportType, this.getDef().uri(), "", "");
		}
	}

	private void parseTennisTimeType(BetHistoryResult result, ItemDescription itemDescription) throws ParsingFailException {
		if (VNEUtility.extractValue("match odds", itemDescription.marketDesc.toLowerCase()))
			result.setTimeType(TennisTimeType.valueOf("FT"));
		else if (VNEUtility.extractValue("winner",itemDescription.marketDesc.toLowerCase())){
			String set = VNEUtility.extractValue("set (\\d) winner", itemDescription.marketDesc.toLowerCase(), 1);
			if (!Strings.isNullOrEmpty(set)) {
				result.setTimeType(new TennisTimeType(0, Integer.parseInt(set), 0));
			} else {
				account.getCommonLogger().error("Fail to get Set game {},", itemDescription.marketDesc);
				throw new ParsingFailException("Fail to parse TimeType", this.getDef().uri(), "", "");
			}
		} else {
			account.getCommonLogger().error("Fail to parse TimeType, {}", itemDescription.marketDesc);
			throw new ParsingFailException("Fail to parse TimeType", this.getDef().uri(), "", "");
		}
	}

	private void parseBasketBallTimeType(BetHistoryResult result, ItemDescription itemDescription) throws ParsingFailException {
		if (VNEUtility.extractValue("match odds|moneyline|handicap|total points",
				itemDescription.marketDesc.toLowerCase())){
			result.setTimeType(BasketballTimeType.FT);
		} else if (VNEUtility.extractValue("half", itemDescription.marketDesc.toLowerCase())) {
			result.setTimeType(BasketballTimeType.H1);
		} else {
			account.getCommonLogger().error("Fail to parse BasketBall TimeType {}", itemDescription);
			throw new ParsingFailException("Fail to parse BasketBall TimeType", this.getDef().uri(), "", "");
		}
	}

	private void parseSoccerTimeType(BetHistoryResult result, ItemDescription itemDescription) throws ParsingFailException {
		if (VNEUtility.extractValue(
				"match odds|moneyline|asian handicap|over\\/under (\\d+\\.\\d+) goals" +
						"|corners over\\/under (\\d+\\.\\d+)|cards over\\/under (\\d+\\.\\d+)|goal lines",
				itemDescription.marketDesc.toLowerCase())){
			result.setTimeType(SoccerTimeType.FT);
		} else if (VNEUtility.extractValue("half", itemDescription.marketDesc.toLowerCase())) {
			result.setTimeType(SoccerTimeType.HT);
		} else {
			account.getCommonLogger().error("Fail to parse Soccer TimeType {}", itemDescription);
			throw new ParsingFailException("Fail to parse Soccer TimeType", this.getDef().uri(), "", "");
		}
	}

	private SportType parseSportType(String eventTypeId) throws ParsingFailException {
		switch (eventTypeId) {
			case "1":
				return SportType.SOCCER;
			case "7522":
				return SportType.BASKETBALL;
			case "2":
				return SportType.TENNIS;
			default:
				throw new ParsingFailException("Bet record has malformed bet date", this.getDef().uri(), "", "");
		}
	}

	private Timestamp convertTimestamp(String placedDate) throws ParsingFailException {
		try {
			Date betTime = formatter.parse(placedDate);
			return new Timestamp(betTime.getTime());
		} catch (ParseException e) {
			throw new ParsingFailException("Bet record has malformed bet date", this.getDef().uri(), "", "");
		}
	}

	@Override
	public boolean logHttpRequest() {
		return false;
	}
}
