package jayeson.lib.betting.VNE.getbethistorybyid;

import dagger.Lazy;
import jayeson.lib.betting.VNE.VNE;
import jayeson.lib.betting.api.actioncontext.BetHistoryByBetIdContext;
import jayeson.lib.betting.api.actionresults.ListBetHistoryResult;
import jayeson.lib.betting.api.datastructure.BADRuntime;
import jayeson.lib.betting.api.datastructure.BetStatus;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.VNE.json.BFBetStatus;
import jayeson.lib.betting.VNE.json.clearedorders.ClearedOrderSummary;
import jayeson.lib.betting.core.http.IClient;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.task.TaskFragmentListener;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;

import javax.inject.Inject;

public class GetVoidOrdersByIdsFragment extends ListClearedOrdersByIdsFragment {

	private final Lazy<GetLapsedOrdersByIdsFragment> lapsedOrdersFragmentLazy;

	@Inject
	public GetVoidOrdersByIdsFragment(AccountTaskInfo data, ITaskExecutor executor, IResponseListener responseListener,
                                      VNE account, TaskFragmentListener<ListBetHistoryResult> listener,
                                      BADRuntime badRuntime, ListBetHistoryResult initialResult,
                                      BetHistoryByBetIdContext context, IClient client,
                                      Lazy<GetLapsedOrdersByIdsFragment> lapsedOrdersFragmentLazy) {
		super(data, executor, responseListener, account, listener, badRuntime, initialResult, context, client, BFBetStatus.VOIDED);
		needProceed = true;
		this.lapsedOrdersFragmentLazy = lapsedOrdersFragmentLazy;
	}

	@Override
	public void proceed(ListBetHistoryResult currentResult) {
		if (isRequestDone) {
			GetLapsedOrdersByIdsFragment fragment = lapsedOrdersFragmentLazy.get();
			executor.submit(fragment);
			account.getCommonLogger().info("LAUNCH_BET_HISTORIES_BY_IDS_LAPSED");
		} else {
			super.proceed(currentResult);
		}
	}

	@Override
	protected void setBetStatus(BetFairHistoryResult result, ClearedOrderSummary each) {
		result.setBetStatus(BetStatus.CANCELLED);
		result.setDeleteStatus(BetFairHistoryResult.DeleteStatus.VOIDED);
	}

	@Override
	public boolean logHttpRequest() {
		return false;
	}
}
