package jayeson.lib.betting.VNE.getbethistorybyid;

import dagger.Module;
import dagger.Provides;
import jayeson.lib.betting.api.actionresults.ListBetHistoryResult;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;
import jayeson.lib.betting.core.task.TaskFragmentListener;

@Module
public class BetFairGetHistoriesByIdsModule {
	
	@TaskScope @Provides
	ListBetHistoryResult provideResult() {
		return new ListBetHistoryResult();
	}
	
	@TaskScope @Provides
	TaskFragmentListener<ListBetHistoryResult> provideTaskFragmentListener(BetFairGetHistoriesByIdsTask task) {
        return task;
    }
}
