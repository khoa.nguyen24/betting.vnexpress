package jayeson.lib.betting.VNE.getbethistorybyid;

import dagger.Lazy;
import jayeson.lib.betting.api.actioncontext.BetHistoryByBetIdContext;
import jayeson.lib.betting.api.actionresults.ListBetHistoryResult;
import jayeson.lib.betting.api.task.AccountTaskHandler;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.api.task.TaskObservable;
import jayeson.lib.betting.core.BettingAccount;
import jayeson.lib.betting.core.dagger.qualifier.TaskId;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.task.BasicAccountTask;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;

import javax.inject.Inject;

@TaskScope
public class BetFairGetHistoriesByIdsTask extends BasicAccountTask<ListBetHistoryResult> {
	private static final int WORK_LOAD = 10;
	private final Lazy<GetSettledOrdersByIdsFragment> settledOrdersFragmentLazy;

	protected BetHistoryByBetIdContext context;

	@Inject
	public BetFairGetHistoriesByIdsTask(Lazy<GetSettledOrdersByIdsFragment> settledOrdersFragmentLazy,
										ITaskExecutor executor, IResponseListener responseListener, AccountTaskInfo data,
										BettingAccount account, @TaskId String id, AccountTaskHandler<ListBetHistoryResult> handler,
										TaskObservable<AccountTaskInfo> observable, BetHistoryByBetIdContext context,
										ListBetHistoryResult result) {
		super(executor, responseListener, data, account, id, WORK_LOAD, handler, observable, result);
		this.context = context;
		this.settledOrdersFragmentLazy = settledOrdersFragmentLazy;
	}

	@Override
	public void execute() {
		account.getCommonLogger().info("LAUNCH_GET_HISTORIES_BY_IDS_FRAGMENT");
		executor.submit(settledOrdersFragmentLazy.get());
	}

	public BetHistoryByBetIdContext getContext() {
		return this.context;
	}
}
