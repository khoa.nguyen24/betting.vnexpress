package jayeson.lib.betting.VNE.getbethistorybyid;

import dagger.Subcomponent;
import jayeson.lib.betting.api.actioncontext.BetHistoryByBetIdContext;
import jayeson.lib.betting.api.actionresults.ListBetHistoryResult;
import jayeson.lib.betting.core.dagger.taskscope.ITaskComponent;
import jayeson.lib.betting.core.dagger.taskscope.TaskModule;
import jayeson.lib.betting.core.dagger.taskscope.TaskScope;

@Subcomponent(modules = {BetFairGetHistoriesByIdsModule.class, TaskModule.class})
@TaskScope
public interface BetFairGetHistoriesByIdsComponent extends ITaskComponent<ListBetHistoryResult> {

    @Override
    BetFairGetHistoriesByIdsTask getTask();

    @Subcomponent.Builder
    interface Builder extends BuilderContext<ListBetHistoryResult, BetHistoryByBetIdContext> {
        @Override
        BetFairGetHistoriesByIdsComponent build();
    }
}
