package jayeson.lib.betting.VNE.getbethistorybyid;

import jayeson.lib.betting.api.actionresults.BetHistoryResult;

public class BetFairHistoryResult extends BetHistoryResult {

    private DeleteStatus deleteStatus;

    public DeleteStatus getDeleteStatus() {
        return deleteStatus;
    }

    public void setDeleteStatus(DeleteStatus deleteStatus) {
        this.deleteStatus = deleteStatus;
    }

    public enum DeleteStatus {

        SETTLED(0),
        VOIDED(1),
        LAPSED(2),
        CANCELLED(3);

        private int num;

        DeleteStatus(int num) {
            this.num = num;
        }

        public int getNumber() {
            return num;
        }
    }
}
