//package jayeson.lib.betting.betfair;
//
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import jayeson.lib.betting.api.actioncontext.ScrapeContext;
//import jayeson.lib.betting.api.actionresults.ActionResult;
//import jayeson.lib.betting.api.actionresults.DeltaScrapeResult;
//import jayeson.lib.betting.api.actionresults.MainScrapeResult;
//import jayeson.lib.betting.api.actionresults.ScrapeResult;
//import jayeson.lib.betting.api.datastructure.*;
//import jayeson.lib.betting.api.task.AccountTask;
//import jayeson.lib.betting.api.task.AccountTaskHandler;
//import jayeson.lib.betting.api.task.AccountTaskInfo;
//import jayeson.lib.betting.api.task.ScrapTaskHandler;
//import jayeson.lib.betting.core.dagger.BettingComponent;
//import jayeson.lib.betting.core.dagger.DaggerBettingComponent;
//import jayeson.lib.betting.core.dagger.corescope.CoreBaseComponent;
//import jayeson.lib.betting.core.dagger.corescope.DaggerCoreBaseComponent;
//import jayeson.lib.betting.dagger.AccountComponent;
//import jayeson.lib.feed.api.OddFormat;
//import jayeson.lib.feed.api.OddType;
//import jayeson.lib.feed.api.SportType;
//import org.junit.Before;
//import org.junit.Test;
//
//import java.util.List;
//import java.util.Map;
//import java.util.concurrent.CountDownLatch;
//import java.util.function.Consumer;
//
//public class BetfairTest {
//    BetFair ba;
//
//    @Test
//    public void testScrapeEvent() throws Exception {
//        CountDownLatch completedSignal = new CountDownLatch(1);
//
//        ScrapeHandler<ScrapeResult> handler = new BetfairTest.ScrapeHandler<>(completedSignal);
//
//        ScrapeContext context = new ScrapeContext();
//        context.setSportType(SportType.SOCCER);
//        context.setRequestType(ScrapeContext.RequestType.FETCH_DELTA_EVENT);
//        context.setParamString("{\"eventIds\":[\"1\", \"2\", \"3\", \"4\", \"5\"]}");
//
//        System.out.println("####### START SCRAPE DELTA DATA #######");;
//        System.out.println("####### Scrape Context" );
//        System.out.println("Request Type: " + context.getRequestType());
//        System.out.println("Sport Type: " + context.getSportType());
//
//        ba.scrape(context, handler, new BetfairTest.TaskObserver());
//
//        completedSignal.await();
//    }
//
//    @Test
//    public void testScrapeEventOdd() throws Exception {
//        CountDownLatch completedSignal = new CountDownLatch(1);
//
//        ScrapeHandler<ScrapeResult> handler = new BetfairTest.ScrapeHandler<>(completedSignal);
//
//        ScrapeContext context = new ScrapeContext();
//        context.setSportType(SportType.SOCCER);
//        context.setRequestType(ScrapeContext.RequestType.FETCH_DELTA_EVENT_ODD);
//        context.setParamString("{\"marketIds\":[\"1\", \"2\", \"3\", \"4\", \"5\"]}");
//
//        System.out.println("####### START SCRAPE DELTA DATA #######");;
//        System.out.println("####### Scrape Context" );
//        System.out.println("Request Type: " + context.getRequestType());
//        System.out.println("Sport Type: " + context.getSportType());
//
//        ba.scrape(context, handler, new BetfairTest.TaskObserver());
//
//        completedSignal.await();
//    }
//
//    @Test
//    public void testScrapeEventState() throws Exception {
//        CountDownLatch completedSignal = new CountDownLatch(1);
//
//        ScrapeHandler<ScrapeResult> handler = new BetfairTest.ScrapeHandler<>(completedSignal);
//
//        ScrapeContext context = new ScrapeContext();
//        context.setSportType(SportType.SOCCER);
//        context.setRequestType(ScrapeContext.RequestType.FETCH_DELTA_STATE);
//        context.setParamString("{\"eventIds\":[\"1\", \"2\", \"3\", \"4\", \"5\"]}");
//
//        System.out.println("####### START SCRAPE DELTA DATA #######");;
//        System.out.println("####### Scrape Context" );
//        System.out.println("Request Type: " + context.getRequestType());
//        System.out.println("Sport Type: " + context.getSportType());
//
//        ba.scrape(context, handler, new BetfairTest.TaskObserver());
//
//        completedSignal.await();
//    }
//
//    @Before
//    public void createAccount() {
//
//        Proxy proxy = new Proxy();
//        proxy.setUrl("vodds-prod-linode-japan-squid-001.olesportsresearch.com");
//        proxy.setPort(4216);
//        proxy.setUsername("trojan_horse");
//        proxy.setPassword("890mduB54QUT4986cq");
//
//        BAD data = new BAD();
//        data.setUsername("olegpp1");
//        data.setId(data.getUsername());
//        data.setPassword("Ngz@63*&SF");
//        data.setOriginalUrl("https://www.betfair.com/");
////		"?appKey=L8htwVGd9KeIZdgl");
//        data.setOddFormat(OddFormat.HK);
//        data.setProxy(proxy);
//
//        Sportbook sb = new Sportbook();
//        sb.setId("BETFAIR");
//        data.setSportbook(sb);
//
//        BettingComponent bettingComponent = DaggerBettingComponent.create();
//        CoreBaseComponent coreBaseComponent = DaggerCoreBaseComponent.builder()
//                .bindsComponentBettingComponent(bettingComponent)
//                .build();
//
//        AccountComponent accountComponent = DaggerBetFairComponent.builder()
//                .bindsComponentBettingComponent(bettingComponent)
//                .bindsComponentCoreBaseComponent(coreBaseComponent)
//                .bindsInstanceBAD(data)
//                .build();
//
//        ba = (BetFair) accountComponent.account();
//    }
//
//    public static class TaskObserver implements Consumer<AccountTaskInfo> {
//        @Override
//        public void accept(AccountTaskInfo info) {
//            System.out.println("Task Update: " + info.getId() + ", " + info.getProgress() + ", "
//                    + info.isCancellable() + ", " + info.getState());
//        }
//    }
//
//    public static class TaskHandler <T extends ActionResult> implements AccountTaskHandler<T> {
//        // Wait for the task to callback
//        private CountDownLatch readySignal = new CountDownLatch(1);
//        // Result
//        private T result;
//
//        public TaskHandler(CountDownLatch readySignal) {
//            this.readySignal = readySignal;
//        }
//
//        @Override
//        public void process(T result, AccountTask<T> task) {
//            this.result = result;
//            readySignal.countDown();
//        }
//
//        public T getResult() {
//            return result;
//        }
//
//    }
//
//    public static class ScrapeHandler<T extends ScrapeResult> implements ScrapTaskHandler<T> {
//        private final CountDownLatch completedSignal;
//        private final MainScrapeResult summarizedResult;
//        private final ObjectMapper mapper;
//        private long lastime;
//
//        public ScrapeHandler(CountDownLatch completedSignal) {
//            this.mapper = new ObjectMapper();
//            this.completedSignal = completedSignal;
//            this.summarizedResult = new MainScrapeResult();
//            this.lastime = System.currentTimeMillis();
//        }
//
//        @Override
//        public void completed(ScrapeResult result, AccountTask<T> task) {
//            System.out.println("[ScrapeHandler] task execution completed, code " + result.getCode() + " message " + result.getMessage());
//            try {
//                System.out.println("####### Scrape Completed");
//                System.out.println("Summarized Result");
//                for (OddType oddType : summarizedResult.getRecords().keySet()) {
//                    System.out.println("OddType: " + oddType);
//                    if (summarizedResult.getSpecialIds().containsKey(oddType)) {
//                        System.out.println("Special Ids: " + mapper.writeValueAsString(summarizedResult.getSpecialIds().get(oddType)));
//                    }
//                    System.out.println("Records size: " + summarizedResult.getRecords().get(oddType).size());
//                    System.out.println("Records:");
//                    for (SportbookRecord r : summarizedResult.getRecords().get(oddType)) {
//                        System.out.println(mapper.writeValueAsString(r));
//                    }
//                }
//            } catch (JsonProcessingException e) {
//                e.printStackTrace();
//            }
//            completedSignal.countDown();
//        }
//
//        @Override
//        public void error(ScrapeResult result, AccountTask<T> task) {
//            System.out.println("[ScrapeHandler] task execution error, code " + result.getCode() + " message " + result.getMessage());
//            completedSignal.countDown();
//        }
//
//        @Override
//        public void process(T result, AccountTask<T> task)  {
//            if (result instanceof DeltaScrapeResult) {
//                DeltaScrapeResult deltaResult = (DeltaScrapeResult) result;
//                System.out.println("[ScrapeHandler] process execution result, code " + result.getCode() + " message " + result.getMessage());
//                for (Map.Entry<OddType, List<DeleteRecordInfo>> entry : deltaResult.getDeleteRecords().entrySet()) {
//                    System.out.printf("Odd Type %s deleted size %s\n", entry.getKey(), entry.getValue().size());
//                }
//                for (Map.Entry<OddType, List<SportbookRecord>> entry : deltaResult.getUpsertRecords().entrySet()) {
//                    System.out.printf("Odd Type %s upsert size %s\n", entry.getKey(), entry.getValue().size());
//                }
//            } else {
//                System.out.println("Invalid format result");
//            }
//
//            long now = System.currentTimeMillis();
////			if (now - lastime > 150000) {
////				completedSignal.countDown();
////			}
//        }
//    }
//}
