package jayeson.lib.betting.betfair.betticket;

import jayeson.lib.betting.VNE.betticket.BetFairBetTicketResult;
import jayeson.lib.betting.VNE.betticket.BetFairGetTicketFragment;
import jayeson.lib.betting.api.actioncontext.BetInfoContext;
import jayeson.lib.betting.api.datastructure.BADRuntime;
import jayeson.lib.betting.api.task.AccountTaskInfo;
import jayeson.lib.betting.VNE.VNE;
import jayeson.lib.betting.core.configuration.BookConfig;
import jayeson.lib.betting.core.http.IClient;
import jayeson.lib.betting.core.http.IResponseListener;
import jayeson.lib.betting.core.task.TaskFragmentListener;
import jayeson.lib.betting.core.taskexecutor.ITaskExecutor;
import jayeson.lib.feed.api.OddType;
import jayeson.lib.feed.api.SportType;
import jayeson.lib.feed.api.twoside.PivotBias;
import jayeson.lib.feed.api.twoside.PivotType;
import jayeson.lib.feed.api.twoside.TargetType;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;

import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class VNEGetTicketFragmentTest {
    @Test
    public void validateTicket() {
        AccountTaskInfo data = Mockito.mock(AccountTaskInfo.class);
        ITaskExecutor executor = Mockito.mock(ITaskExecutor.class);
        IResponseListener responseListener = Mockito.mock(IResponseListener.class);
        VNE account = Mockito.mock(VNE.class);
        TaskFragmentListener<BetFairBetTicketResult> listener = Mockito.mock(TaskFragmentListener.class);
        BetFairBetTicketResult initialResult = Mockito.mock(BetFairBetTicketResult.class);
        BetInfoContext context = Mockito.mock(BetInfoContext.class);
        BADRuntime badRuntime = Mockito.mock(BADRuntime.class);
        IClient client = Mockito.mock(IClient.class);
        List< BookConfig.BetLimit> betLimits = Mockito.mock(List.class);

        Logger logger = Mockito.mock(Logger.class);

        BetFairGetTicketFragment a = new BetFairGetTicketFragment(data, executor, responseListener,
                account, listener, initialResult,context,badRuntime,client,betLimits);

        Mockito.when(context.getSportType()).thenReturn(SportType.SOCCER);
        Mockito.when(context.getOddType()).thenReturn(OddType.LIVE);
        Mockito.when(context.getPivotType()).thenReturn(PivotType.HDP);
        Mockito.when(account.getCommonLogger()).thenReturn(logger);

        // issue #32640
        // Score change - Bias Change
        Assert.assertFalse(a.validateTicket("***&***&***&-1.5", "0.5",1, 2, TargetType.TAKE, PivotBias.HOST));
        // 0-2 AWAY -0.5
        Assert.assertTrue(a.validateTicket("***&***&***&-1.5", "0.5",0, 2, TargetType.TAKE, PivotBias.HOST));

        // HOME / GUEST-TAKE / HOST-GIVE
        // HOME +0.5
        Assert.assertTrue(a.validateTicket("***&***&***&0.5", "0.5",0, 0, TargetType.TAKE, PivotBias.GUEST));
        // HOME +1.5
        Assert.assertTrue(a.validateTicket("***&***&***&0.5", "1.5",1, 0, TargetType.TAKE, PivotBias.GUEST));
        // HOME -0.5
        Assert.assertTrue(a.validateTicket("***&***&***&0.5", "0.5",0, 1, TargetType.GIVE, PivotBias.HOST));
        // Score change on HOME -05
        Assert.assertFalse(a.validateTicket("***&***&***&0.5", "0.5",0, 2, TargetType.GIVE, PivotBias.HOST));
        // Score change - BIAS change
        Assert.assertFalse(a.validateTicket("***&***&***&0.5", "0.5",1, 1, TargetType.GIVE, PivotBias.HOST)); // should GUEST/TAKE

        // HOME -0.5
        Assert.assertTrue(a.validateTicket("***&***&***&-0.5", "0.5",0, 0, TargetType.GIVE, PivotBias.HOST));
        // HOME +0.5
        Assert.assertTrue(a.validateTicket("***&***&***&-0.5", "0.5",1, 0, TargetType.TAKE, PivotBias.GUEST));
        // HOME -1.5
        Assert.assertTrue(a.validateTicket("***&***&***&-0.5", "1.5",0, 1, TargetType.GIVE, PivotBias.HOST));

        // AWAY / GUEST-GIVE / HOST-TAKE
        // AWAY -0.5
        Assert.assertTrue(a.validateTicket("***&***&***&-0.5", "0.5",0, 0, TargetType.GIVE, PivotBias.GUEST));
        // AWAY -1.5
        Assert.assertTrue(a.validateTicket("***&***&***&-0.5", "1.5",1, 0, TargetType.GIVE, PivotBias.GUEST));
        // AWAY +0.5
        Assert.assertTrue(a.validateTicket("***&***&***&-0.5", "0.5",0, 1, TargetType.TAKE, PivotBias.HOST));

        // AWAY +0.5
        Assert.assertTrue(a.validateTicket("***&***&***&0.5", "0.5",0, 0, TargetType.TAKE, PivotBias.HOST));
        // AWAY -0.5
        Assert.assertTrue(a.validateTicket("***&***&***&0.5 ", "0.5",1, 0, TargetType.GIVE, PivotBias.GUEST));
        // AWAY +1.5
        Assert.assertTrue(a.validateTicket("***&***&***&0.5", "1.5",0, 1, TargetType.TAKE, PivotBias.HOST));
    }
}

